﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace BlackListScrapper.Database
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext() : base("WProd")
        {
        }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Host> Hosts { get; set; }
        public DbSet<GuestReview> GuestReviews { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<HostCompany> HostCompanies { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<HostReview> HostReviews { get; set; }
        public DbSet<Inbox> Inboxes { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<HostSessionCookie> HostSessionCookies { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<AirlockSession> AirlockSessions { get; set; }
        public DbSet<Outbox> Outboxes { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<ContractTemplate> ContractTemplates { get; set; }
        public DbSet<GuestEmail> GuestEmails { get; set; }

        public DbSet<SearchAccountSession> SearchAccountSessions { get; set; }
        public DbSet<ScrapperChangesLog> ScrapperChangesLogs { get; set; }
    }
}