﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("HostSessionCookie")]
    public class HostSessionCookie
    {
        [Key]
        public int Id { get; set; }
        public int HostId { get; set; }
        public string Token { get; set; }
        public string Cookies { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public int SiteType { get; set; }
        public string SessionKey { get; set; }
    }
}