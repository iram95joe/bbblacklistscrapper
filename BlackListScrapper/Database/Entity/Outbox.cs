﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("Outboxes")]
    public class Outbox
    {
        public int Id { get; set; }
        public string ThreadId { get; set; }
        public string ResevationId { get; set; }
        public int HostId { get; set; }
        public int UserId { get; set; }
        [NotMapped]
        public string BookingStatus { get; set; }
        public string Message { get; set; }
        public int SiteType { get; set; }
        public int Type { get; set; }
        public bool IsSent { get; set; }
        public bool IsCompleted { get; set; }
    }
}