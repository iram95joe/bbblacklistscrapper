﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("Inbox")]
    public class Inbox
    {
        [Key]
        public long Id { get; set; }
        public int HostId { get; set; }
        public string ThreadId { get; set; }
        public bool IsArchived { get; set; }
        public string Status { get; set; }
        public bool HasUnread { get; set; }
        public long ListingId { get; set; }
        public string SiteGuestId { get; set; }
        public string LastMessage { get; set; }
        public DateTime LastMessageAt { get; set; }
        public string InquiryId { get; set; }
        public DateTime? InquiryDate { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public int GuestCount { get; set; }
        public int Adult { get; set; }
        public int Children { get; set; }
        public bool IsInquiryOnly { get; set; }
        public bool IsSpecialOfferSent { get; set; }
        public decimal RentalCost { get; set; }
        public decimal CleaningCost { get; set; }
        public decimal GuestFee { get; set; }
        public decimal ServiceFee { get; set; }
        public decimal Subtotal { get; set; }
        public decimal GuestPay { get; set; }
        public decimal HostEarn { get; set; }
        public int StatusType { get; set; }
        public bool CanPreApproveInquiry { get; set; }
        public bool CanWithdrawPreApprovalInquiry { get; set; }
        public bool CanDeclineInquiry { get; set; }
        //public int SiteType { get; set; }
        //public int CompanyId { get; set; }
        public int? Source { get; set; }
        public string ReplyToEmail { get; set; }
        public string EmailSubject { get; set; }
        public bool IsEnd { get; set; }

    }
}