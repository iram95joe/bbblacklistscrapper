﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    public class Log
    {
       public int Id { get; set; }
       public int UserId { get; set; }
       public string Message { get; set; }
       public int Type { get; set; }
       public bool IsSent { get; set; }
       public DateTime Datetime { get; set; }
    }
}