﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("Users")]
    public class User
    {
        public int Id { get; set; }
        public string FbUserId {get;set; }
        public string FirstName  {get;set; }
        public string LastName {get;set; }
        public string ProfilePictureUrl { get; set; }
        public int CompanyId { get; set; }
        public string AccessToken { get; set; }
        public DateTime? LastAccessDate { get; set; }
    }
}