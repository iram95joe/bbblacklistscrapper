﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("HostReviews")]
    public class HostReview
    {
        [Key]
        public int Id { get; set; }

        public string SiteHostId { get; set; }

        public string SiteGuestId { get; set; }

        public string Name { get; set; }

        public string ProfilePictureUrl { get; set; }

        public string Comments { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}