﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("HostCompanies")]
    public class HostCompany
    {   [Key]
         public int Id { get; set; }
         public int CompanyId { get; set; }
         public int HostId { get; set; }
    }
}