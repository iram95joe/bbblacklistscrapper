﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("Guest")]
    public class Guest
    {
        [Key]
        public int Id { get; set; }
        public string SiteGuestId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string SiteProfileUrl { get; set; }
        public string Location { get; set; }
    }
}