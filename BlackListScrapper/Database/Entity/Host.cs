﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("Host")]
    public class Host
    {
        [Key]
        public int Id { get; set; }
        public string SiteHostId { get; set; }
        public int SiteType { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string PhoneNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public string Birthdate { get;set;}
        public string Language { get; set; }
        public string Currency { get;set;}
        public string About {get;set;}
        public string School {get;set;}
        public string Work { get; set; }
        public string TimeZone { get; set; }
        public DateTime? DateCreated { get; set; }

    }
}