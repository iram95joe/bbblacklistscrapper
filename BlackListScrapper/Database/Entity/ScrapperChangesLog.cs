﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("ScrapperChangesLogs")]
    public class ScrapperChangesLog
    {
        public int Id { get; set; }
        public int Count { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
        public bool IsSentSMS { get; set; }
    }
}