﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("Reservations")]
    public class Reservation
    {
        [Key]
        public int Id { get; set; }
        public int HostId { get; set; }
        public string ReservationId { get; set; }
        public long ListingId { get; set; }
        public string SiteGuestId { get; set; }
        public string ConfirmationCode { get; set; }
        public string BookingStatusCode { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime? CheckOutDate { get; set; }
        public int Nights { get; set; }
        public decimal ReservationCost { get; set; }
        public decimal SumPerNightAmount { get; set; }
        public decimal CleaningFee { get; set; }
        public string Fees { get; set; }
        public string RateDetails { get; set; }
        public decimal ServiceFee { get; set; }
        public decimal TotalPayout { get; set; }
        public DateTime? PayoutDate { get; set; }
        public decimal NetRevenueAmount { get; set; }
        public DateTime? InquiryDate { get; set; }
        public DateTime? BookingDate { get; set; }
        public DateTime? BookingCancelDate { get; set; }
        public DateTime? BookingConfirmDate { get; set; }
        public DateTime? BookingConfirmCancelDate { get; set; }
        public int SiteType { get; set; }
        public int GuestCount { get; set; }
        public int Adult { get; set; }
        public int Children { get; set; }
        public bool IsActive { get; set; }
        public bool IsImported { get; set; }
        public bool IsAltered { get; set; }
        public int CompanyId { get; set; }
        public string ThreadId { get; set; }
        public string Type { get; set; }
        public int? Source { get; set; }
        public bool IsApplyAutoAssignWorker { get; set; }
        public string SignRequestUiid { get; set; }
        public string SignedDocumentDownloadLink { get; set; }
        public bool IsDocumentSigned { get; set; }
        public string DateTimeSigned { get; set; }
    }
}