﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    public class AirlockSession
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Cookies { get; set; }
        public int SiteType { get; set; }
    }
}