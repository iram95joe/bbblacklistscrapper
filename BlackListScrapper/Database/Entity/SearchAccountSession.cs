﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("SearchAccountSessions")]
    public class SearchAccountSession
    {
        public int Id { get; set; }
        public string Cookies { get; set; }
        public string Username { get; set; }
        public int Type { get; set; }
    }
}