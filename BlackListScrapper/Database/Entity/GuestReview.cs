﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("GuestReviews")]
    public class GuestReview
    {   [Key]
        public int Id { get; set; }
        //It use to connect reservation to Homeaway/Vrbo Reviews
        public string ReservationId { get; set; }
        //It use to connect reservation to Airbnb Reviews
        public string ConfirmationCode { get; set; }
        //Guest is GuestId from site
        public string SiteGuestId { get; set; }

        public string Review { get; set; }

        public int Rating { get; set; }
    }
}