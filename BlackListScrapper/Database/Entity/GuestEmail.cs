﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Entity
{
    [Table("GuestEmails")]
    public class GuestEmail
    {
        public int Id { get; set; }
        public string SiteGuestId { get; set; }
        public string Email { get; set; }
    }
}