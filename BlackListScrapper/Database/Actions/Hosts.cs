﻿using BlackListScrapper.Database.Entity;
using BlackListScrapper.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Hosts
    {
        public static Host AddOrUpdate(Host host)
        {
            using (var db = new ApplicationDbContext())
            {
                db.ChangeTracker.DetectChanges();
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp =db.Hosts.Where(x => x.SiteHostId == host.SiteHostId).FirstOrDefault();
                if(temp == null)
                {
                    host.DateCreated = DateTime.UtcNow;
                    db.Hosts.Add(host);
                    db.SaveChanges();
                    return host;
                }
                else
                {
                    temp.Password = host.Password;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return temp;
                }
            }

        }

        public static string GetSiteHostId(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Configuration.AutoDetectChangesEnabled = false;
                var temp =db.Hosts.Where(x => x.Id == hostId).FirstOrDefault();
                if (temp != null) {
                    return temp.SiteHostId;
                }
                else { return ""; }
            }
        }

        public static int GetHostId(string username,string password,int siteType)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {   db.Configuration.AutoDetectChangesEnabled = false;
                    string pass = Security.Encrypt(password);
                    return db.Hosts.Where(x => x.Username == username && x.Password == pass && x.SiteType == siteType).FirstOrDefault().Id;
                }
                catch { return 0; }
            }
                
        }
        #region GetHostById

        public static Host GetHostById(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Configuration.AutoDetectChangesEnabled = false;
                var user = (from h in db.Hosts
                            where
                                 h.Id == hostId
                            select h).FirstOrDefault();

                user.Password = Security.Decrypt(user.Password);
                return user;
            }
        }

        #endregion

    }
}