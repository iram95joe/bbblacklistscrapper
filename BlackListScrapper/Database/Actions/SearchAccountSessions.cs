﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class SearchAccountSessions
    {
        public static void AddOrUpdate(SearchAccountSession searchAccountSession)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.SearchAccountSessions.Where(x => x.Username == searchAccountSession.Username).FirstOrDefault();
                if(temp == null)
                {
                    db.SearchAccountSessions.Add(searchAccountSession);
                    db.SaveChanges();
                }
                else
                {
                    temp.Cookies = searchAccountSession.Cookies;
                    db.SaveChanges();
                }
            }
        }
        public static string GetCookies(string username)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp =db.SearchAccountSessions.Where(x => x.Username == username).FirstOrDefault();
                if (temp != null)
                    return temp.Cookies;
                else
                    return null;
                

            }
        }
    }
}