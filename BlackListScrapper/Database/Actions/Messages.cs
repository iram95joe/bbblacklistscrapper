﻿using BlackListScrapper.Database.Entity;
using BlackListScrapper.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Messages
    {
        public static bool Add(Message message)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.Messages.Where(x => x.MessageId == message.MessageId).FirstOrDefault();
                if (temp == null)
                {
                    db.Messages.Add(message);
                    db.SaveChanges();
                    MessagesHub.UpdateThreadMessages(Database.Actions.Inboxes.GetHostIdByThreadId(message.ThreadId), message);
                    return true;
                }
            }
            return false;
        }
    }
}