﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class HostCompany
    {
        public static bool AddOrUpdate(Database.Entity.HostCompany hostCompany)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.HostCompanies.Where(x => x.HostId == hostCompany.HostId && x.CompanyId ==hostCompany.CompanyId).FirstOrDefault();
                if (temp == null)
                {
                    db.HostCompanies.Add(hostCompany);
                    db.SaveChanges();
                    return true;
                }
            }
            return false;
        }
    }
}