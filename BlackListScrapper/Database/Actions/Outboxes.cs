﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Outboxes
    {
        public static bool Add(Outbox outbox)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    db.Outboxes.Add(outbox);
                    db.SaveChanges();
                    return true;
                }
            }
            catch { }
            return true;
        }
        public static bool Update(int outboxId,bool isSent)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    var temp = db.Outboxes.Where(x => x.Id == outboxId).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.IsSent = isSent;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch { }
            return true;
        }
        public static List<Outbox> GetOutboxes(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                return db.Outboxes.Where(x => x.IsSent == false && x.HostId == hostId).ToList();
              
            }
        }
    }
}