﻿using BlackListScrapper.Database.Entity;
using BlackListScrapper.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class ScrapperChangesLogs
    {
        public static void AddOrUpdate(ScrapperChangesLog log)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.ScrapperChangesLogs.Where(x => x.Message == log.Message && x.Date == DateTime.Now).FirstOrDefault();
                if (temp == null)
                {
                    db.ScrapperChangesLogs.Add(log);
                    db.SaveChanges();
                    TwilioSMS.Send(log.Message);

                }
                else
                {
                    temp.Count = temp.Count + 1;

                    if (temp.Count > 5)
                    {
                        if (temp.IsSentSMS == false)
                        {
                            TwilioSMS.Send(temp.Count + " times " + log.Message);
                            temp.IsSentSMS = true;
                        }
                    }
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
    }
}