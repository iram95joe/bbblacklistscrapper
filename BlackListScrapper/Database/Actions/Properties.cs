﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Properties
    {
        public static bool AddOrUpdate(Property property)
        {
            using (var db = new ApplicationDbContext())
            {
                if (property.Name != "" && property.Name != null)
                {
                    db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    var temp = db.Properties.Where(x => x.ListingId == property.ListingId).FirstOrDefault();
                    if (temp == null)
                    {
                        db.Properties.Add(property);
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        temp.ListingUrl = property.ListingUrl;
                        temp.Name = property.Name;
                        //temp.Longitude = property.Longitude;
                        //temp.Latitude = property.Latitude;
                        //temp.Address = property.Address;
                        temp.UnitCount = property.UnitCount;
                        temp.MinStay = property.MinStay;
                        temp.Internet = property.Internet;
                        temp.Pets = property.Pets;
                        temp.WheelChair = property.WheelChair;
                        temp.Description = property.Description;
                        temp.Reviews = property.Reviews;
                        temp.Sleeps = property.Sleeps;
                        temp.Bedrooms = property.Bedrooms;
                        temp.Bathrooms = property.Bathrooms;
                        temp.PropertyType = property.PropertyType;
                        //temp.Type = property.Type;
                        //temp.AccomodationType = property.AccomodationType;
                        temp.Smoking = property.Smoking;
                        temp.AirCondition = property.AirCondition;
                        temp.SwimmingPool = property.SwimmingPool;
                        //temp.PriceNightlyMin = property.PriceNightlyMin;
                        //temp.PriceNightlyMax = property.PriceNightlyMax;
                        //temp.PriceWeeklyMin = property.PriceWeeklyMin;
                        //temp.PriceWeeklyMax = property.PriceWeeklyMax;
                        temp.Images = property.Images;
                        temp.Status = property.Status;
                        //temp.MonthlyExpense = property.MonthlyExpense;
                        temp.ExpenseNotes = property.ExpenseNotes;
                        temp.ActiveDateStart = property.ActiveDateStart;
                        temp.ActiveDateEnd = property.ActiveDateEnd;
                        temp.CommissionId = property.CommissionId;
                        temp.CommissionAmount = property.CommissionAmount;
                        temp.AutoCreateIncomeExpense = property.AutoCreateIncomeExpense;
                        //temp.CheckInTime = property.CheckInTime;
                        //temp.CheckOutTime = property.CheckOutTime;
                        temp.IsLocallyCreated = property.IsLocallyCreated;
                        temp.IsParentProperty = property.IsParentProperty;
                        temp.ParentPropertyId = property.ParentPropertyId;
                        temp.IsSyncParent = property.IsSyncParent;
                        temp.CalendarRemark = property.CalendarRemark;
                        temp.Currency = property.Currency;
                        if (temp.TimeZoneId != "" && temp.TimeZoneName != "" && temp.TimeZoneId != null && temp.TimeZoneName != null)
                        {
                            temp.TimeZoneName = property.TimeZoneName;
                            temp.TimeZoneId = property.TimeZoneId;
                        }
                        temp.IsActive = property.IsActive;
                        temp.AllowedSelfCheckinCheckout = property.AllowedSelfCheckinCheckout;
                        temp.PriceRuleSync = property.PriceRuleSync;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
            }
            return false;
        }
        public static bool PropertyTimeZoneExist(long listiId)
        {
            using (var db = new ApplicationDbContext())
            {
                var property = db.Properties.Where(x => x.ListingId == listiId).FirstOrDefault();
                if (property != null && property.TimeZoneName != null && property.TimeZoneId != null)
                {
                    return true;
                }
                return false;
            }
        }
        public static string GetTimein(long listingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                if (temp != null)
                {
                    return temp.CheckInTime.ToString();
                }
                return null;
            }
        }
        public static string GetTimeout(long listingId)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Properties.Where(x => x.ListingId == listingId).FirstOrDefault();
                if (temp != null)
                {
                    return temp.CheckOutTime.ToString();
                }
                return null;
            }
        }
    }
}