﻿using BlackListScrapper.Database.Entity;
using BlackListScrapper.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class GuestReviews
    {
        public static bool AddOrUpdate(GuestReview reviews)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.GuestReviews.Where(x => x.ConfirmationCode == reviews.ConfirmationCode || x.ReservationId == reviews.ReservationId).FirstOrDefault();
                if (temp == null)
                {
                    db.GuestReviews.Add(reviews);
                    db.SaveChanges();
                    //if(reviews.CheckinDate.Date < DateTime.Now.Date)
                    //ShowNewReservationReview.ShowNewData(UserId, reviews);
                }
                else
                {
                    //if (temp.CheckinDate.Date < DateTime.Now.Date)
                    //{
                    //    ShowNewReservationReview.ShowNewData(UserId, temp);
                    //}
                }
            }
            return true;
        }
    }
}