﻿using BlackListScrapper.Database.Entity;
using MlkPwgen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class HostSessionCookies
    {
        public static bool Add(int hostId,string serializeCookie)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                db.HostSessionCookies.Add(new HostSessionCookie()
                    { HostId=hostId,
                        Cookies = serializeCookie,
                        CreatedAt =DateTime.Now,
                        IsActive =true,
                        SiteType =1,
                        Token = PasswordGenerator.Generate(10) });
                    db.SaveChanges();
                    return true;   
            }
        }

        public static HostSessionCookie GetSessionByToken(string token) {
            using (var db = new ApplicationDbContext())
            {
                return db.HostSessionCookies.Where(x => x.Token == token).ToList().LastOrDefault();
               
            }
        }
        public static string GetTokenByHostId(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    return db.HostSessionCookies.Where(x => x.HostId == hostId).ToList().LastOrDefault().Token;

                }
                catch { return ""; }
            }
        }
        public static HostSessionCookie GetSessionByHostId(int hostId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.HostSessionCookies.Where(x => x.HostId == hostId).ToList().LastOrDefault();

            }
        }
    }
}