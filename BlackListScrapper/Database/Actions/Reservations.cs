﻿using BlackListScrapper.Database.Entity;
using BlackListScrapper.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Reservations
    {
        public static bool AddOrUpdate(string userId,Reservation reservation)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.Reservations.Where(x => x.ConfirmationCode == reservation.ConfirmationCode).FirstOrDefault();
                if (temp == null)
                {
                    db.Reservations.Add(reservation);
                    db.SaveChanges();
                    if (reservation.CheckOutDate.Value.Date < DateTime.Now.Date)
                        ShowNewReservationReview.ShowNewData(userId, reservation);
                    return true;
                }
                else
                {
                    temp.ListingId = reservation.ListingId;
                    temp.BookingStatusCode = reservation.BookingStatusCode;
                    temp.CheckInDate = reservation.CheckInDate;
                    temp.CheckOutDate = reservation.CheckOutDate;
                    temp.Nights = reservation.Nights;
                    temp.ReservationCost = reservation.ReservationCost;
                    temp.SumPerNightAmount = reservation.SumPerNightAmount;
                    temp.CleaningFee = reservation.CleaningFee;
                    temp.Fees = reservation.Fees;
                    temp.RateDetails = reservation.RateDetails;
                    temp.ServiceFee = reservation.ServiceFee;
                    temp.TotalPayout = reservation.TotalPayout;
                    temp.PayoutDate = reservation.PayoutDate;
                    temp.NetRevenueAmount = reservation.NetRevenueAmount;
                    temp.InquiryDate = reservation.InquiryDate;
                    temp.BookingDate = reservation.BookingDate;
                    temp.BookingCancelDate = reservation.BookingCancelDate;
                    temp.BookingConfirmDate = reservation.BookingConfirmDate;
                    temp.BookingConfirmCancelDate = reservation.BookingConfirmCancelDate;
                    temp.GuestCount = reservation.GuestCount;
                    temp.Adult = reservation.Adult;
                    temp.Children = reservation.Children;
                    temp.IsActive = reservation.IsActive;
                    temp.IsImported = reservation.IsImported;
                    temp.IsAltered = reservation.IsAltered;
                    temp.Type = reservation.Type;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    if (reservation.CheckOutDate.Value.Date < DateTime.Now.Date)
                        ShowNewReservationReview.ShowNewData(userId, temp);

                }
            }
            return true;
        }
    }
}