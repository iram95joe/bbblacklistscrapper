﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class HostReviews
    {
        public static bool AddOrUpdate(BlackListScrapper.Database.Entity.HostReview hostReviews)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.HostReviews.Where(x => x.Comments == hostReviews.Comments && x.SiteHostId == hostReviews.SiteHostId ).FirstOrDefault();
                if (temp == null)
                {
                    db.HostReviews.Add(hostReviews);
                    db.SaveChanges();
                    return true;
                }
            }
            return false;
        }
    }
}