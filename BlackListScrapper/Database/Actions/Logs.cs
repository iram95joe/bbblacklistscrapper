﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Logs
    {
        public static bool Add(int userId, int type, string message,bool isSent = false)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    db.ChangeTracker.DetectChanges();
                    db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    db.Logs.Add(new Log() { UserId = userId, Type = type, Message = message, IsSent = isSent,Datetime = DateTime.Now });
                    db.SaveChanges();
                    return true;
                }
            }
            catch { }
            return true;
        }
    }
}