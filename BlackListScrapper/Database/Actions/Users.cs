﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Users
    {
        public static User AddOrUpdate(User user)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.Users.Where(x => x.FbUserId == user.FbUserId).FirstOrDefault();
                if (temp == null) 
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    if (user.CompanyId == 0)
                    {
                        var results = (from d in db.Users
                                       where d.Id == user.Id
                                       select d).FirstOrDefault();
                        if (results != null)
                        {
                            results.CompanyId = user.Id;
                        }
                        db.SaveChanges();

                    }
                    return user;
                }
                else
                {
                    return temp;
                }
            }
        }

        public static int GetLastLoginTotalDayDifference(int userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var user = db.Users.Where(x => x.Id == userId).FirstOrDefault();
                DateTime dt = DateTime.UtcNow;
                var diff = dt.Subtract(user.LastAccessDate.Value);
               var total = diff.TotalDays.ToInt();
                if (total == 0)
                {
                    return 1;
                }
                else
                    return total;
            }
        }
    }
}