﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Guests
    {
        public static bool AddOrUpdate(Guest guest)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.Guests.Where(x => x.SiteGuestId == guest.SiteGuestId).FirstOrDefault();
                if (temp == null)
                {
                    db.Guests.Add(guest);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    temp.LastName = guest.LastName;
                    temp.ProfilePictureUrl = guest.ProfilePictureUrl;
                    temp.Location = guest.Location;
                    temp.FirstName = guest.FirstName;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            return false;
        }

        public static void UpdateLastName(long siteGuestId, string Fullname,string bookingStatus)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var guest = db.Guests
                        .Where(x => x.SiteGuestId == siteGuestId.ToString())
                        .FirstOrDefault();
                    if (guest != null && bookingStatus =="A")
                    {
                        var lastName = Fullname.Replace(guest.FirstName, "").Trim();
                        if (lastName != "" && lastName !=null)
                        {
                            guest.LastName = lastName;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), "Guests", "UpdateName", string.Format("Exception in updating FullName, check the database"), "Error");

            }
        }
    }
}