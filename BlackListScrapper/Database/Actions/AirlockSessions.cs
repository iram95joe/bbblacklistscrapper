﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class AirlockSessions
    {
        public static void AddOrUpdate(AirlockSession airlockSession)
        {
            using (var db = new ApplicationDbContext())
            {
                var temp = db.AirlockSessions.Where(x => x.Username == airlockSession.Username && x.SiteType == airlockSession.SiteType).FirstOrDefault();
                if(temp == null)
                {
                    db.AirlockSessions.Add(airlockSession);
                    db.SaveChanges();
                }
                else
                {
                    temp.Cookies = airlockSession.Cookies;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public static AirlockSession GetAirlockSession(string username,int sitetype)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.AirlockSessions.Where(x => x.Username == username && x.SiteType == sitetype).FirstOrDefault();
            }
        }
    }
}