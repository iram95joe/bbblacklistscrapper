﻿using BlackListScrapper.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Database.Actions
{
    public class Inboxes
    {
        public static bool AddOrUpdate(BlackListScrapper.Database.Entity.Inbox inbox, string UserId)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.Inboxes.Where(x => x.ThreadId == inbox.ThreadId).FirstOrDefault();
                if (temp == null)
                {
                    db.Inboxes.Add(inbox);
                    db.SaveChanges();
                    ShowNewReservationReview.ShowNewInquiry(UserId, inbox.Id.ToInt());
                    return true;
                }
                else
                {
                    temp.IsArchived = inbox.IsArchived;
                    temp.Status = inbox.Status;
                    temp.HasUnread = inbox.HasUnread;
                    temp.ListingId = inbox.ListingId;
                    temp.LastMessage = inbox.LastMessage;
                    temp.LastMessageAt = inbox.LastMessageAt;
                    temp.InquiryId = inbox.InquiryId;
                    temp.InquiryDate = inbox.InquiryDate;
                    temp.CheckInDate = inbox.CheckInDate;
                    temp.CheckOutDate = inbox.CheckOutDate;
                    temp.GuestCount = inbox.GuestCount;
                    temp.Adult = inbox.Adult;
                    temp.Children = inbox.Children;
                    temp.IsInquiryOnly = inbox.IsInquiryOnly;
                    temp.IsSpecialOfferSent = inbox.IsSpecialOfferSent;
                    temp.RentalCost = inbox.RentalCost;
                    temp.CleaningCost = inbox.CleaningCost;
                    temp.GuestFee = inbox.GuestFee;
                    temp.ServiceFee = inbox.ServiceFee;
                    temp.Subtotal = inbox.Subtotal;
                    temp.GuestPay = inbox.GuestPay;
                    temp.HostEarn = inbox.HostEarn;
                    temp.StatusType = inbox.StatusType;
                    temp.CanPreApproveInquiry = inbox.CanPreApproveInquiry;
                    temp.CanWithdrawPreApprovalInquiry = inbox.CanPreApproveInquiry;
                    temp.CanDeclineInquiry = inbox.CanDeclineInquiry;
                    temp.Source = inbox.Source;
                    temp.ReplyToEmail = inbox.ReplyToEmail;
                    temp.EmailSubject = inbox.EmailSubject;
                    temp.IsEnd = inbox.IsEnd;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    ShowNewReservationReview.ShowNewInquiry(UserId, temp.Id.ToInt());
                    return true;
                }
            }
        }
        public static void UpdateInquiryDate(string threadId, DateTime inquiryDate) {

            using (var db = new ApplicationDbContext())
            {
                db.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                var temp = db.Inboxes.Where(x => x.ThreadId == threadId).FirstOrDefault();
                if (temp != null)
                {
                    temp.InquiryDate = inquiryDate == DateTime.MinValue ? (DateTime?)null : inquiryDate;
                    db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public static int GetHostIdByThreadId(string threadId){

            using (var db = new ApplicationDbContext())
            {
                var inbox = db.Inboxes.Where(x => x.ThreadId == threadId).FirstOrDefault();
                if (inbox != null)
                    return inbox.HostId;
                else
                    return 0;
            }
        }
    }
}