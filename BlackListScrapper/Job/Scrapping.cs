﻿using BlackListScrapper.Database;
using BlackListScrapper.Database.Entity;
using BlackListScrapper.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Job
{
    public class Scrapping
    {
        public static void HostStartScrape(Host host,string userId)
        {
            try
            {
                GlobalVariables.HostWithPendingLoginRequest.Remove(host.Id);
                //Stop(host.Id.ToString());
                JobScheduler.PropertyJobScheduler.Start("property" + host.Id, host.Id, host.SiteType, userId);
                JobScheduler.InboxJobScheduler.Start("inbox" + host.Id, host.Id, host.SiteType, userId);
                JobScheduler.ReservationJobScheduler.Start("reservation" + host.Id, host.Id, host.SiteType, userId);
                JobScheduler.GuestReviewJobScheduler.Start("guestreview" + host.Id, host.Id, host.SiteType, userId);
                //JobScheduler.OutboxQueueScheduler.Start("outbox"+host.Id,host.Id);
            }
            catch
            {

            }
        }

        public static void StartIncremental(int companyId,string userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var hosts= (from h in db.Hosts join hc in db.HostCompanies on h.Id equals hc.HostId where hc.CompanyId == companyId select h).ToList();
                foreach(var host in hosts)
                {
                    GlobalVariables.HostWithPendingLoginRequest.Remove(host.Id);
                    Stop(host.Id.ToString());
                    JobScheduler.PropertyJobScheduler.Start("property" + host.Id, host.Id, host.SiteType, userId);
                    JobScheduler.InboxJobScheduler.Start("inbox" + host.Id, host.Id, host.SiteType, userId);
                    JobScheduler.ReservationJobScheduler.Start("reservation" + host.Id, host.Id, host.SiteType, userId);
                    JobScheduler.GuestReviewJobScheduler.Start("guestreview" + host.Id, host.Id, host.SiteType, userId);
                    //JobScheduler.OutboxQueueScheduler.Start("outbox"+host.Id,host.Id);
                }
            }
        }

        public static void Stop(string hostId)
        {
            try
            {
                JobScheduler.PropertyJobScheduler.Stop("property" + hostId);
                JobScheduler.InboxJobScheduler.Stop("inbox" + hostId);
                JobScheduler.ReservationJobScheduler.Stop("reservation" + hostId);
                JobScheduler.GuestReviewJobScheduler.Stop("guestreview" + hostId);
                //JobScheduler.OutboxQueueScheduler.Stop("outbox" + hostId);
            }
            catch
            {

            }
        }
    }
}