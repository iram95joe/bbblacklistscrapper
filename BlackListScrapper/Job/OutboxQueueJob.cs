﻿using BlackListScrapper.Enumerations;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BlackListScrapper.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class OutboxQueueJob : IJob
    {
        IJobExecutionContext jobContext;
        public async Task Execute(IJobExecutionContext context)
        {
            int hostId = context.JobDetail.JobDataMap["hostId"].ToInt();
            //int siteType = context.JobDetail.JobDataMap["siteType"].ToInt();
            //string userId = context.JobDetail.JobDataMap["userId"].ToString();
            await Task.Factory.StartNew(() =>
            {
                Controllers.BookingController bookingController = new Controllers.BookingController();
                Controllers.MessagesController messageController = new Controllers.MessagesController();
                var data = Database.Actions.Outboxes.GetOutboxes(hostId);
                foreach (var outbox in data)
                {
                    var token = Database.Actions.HostSessionCookies.GetTokenByHostId(outbox.HostId.ToInt());
                    var type = (OutboxType)outbox.Type;
                    switch (type)
                    {
                        case OutboxType.Message:
                            messageController.SendMessageToInbox(outbox.SiteType, outbox.HostId.ToString(), outbox.ThreadId, outbox.Message, outbox.UserId.ToString(), outbox.Id);
                            break;
                        case OutboxType.Preapprove:
                            bookingController.PreApproveInquiry(outbox.UserId.ToString(), outbox.SiteType, outbox.ThreadId, outbox.HostId.ToString(), outbox.Id);
                            break;
                        case OutboxType.WithdrawPreapprove:
                            bookingController.WithdrawPreApproveInquiry(outbox.UserId.ToString(), outbox.SiteType, outbox.ThreadId, outbox.HostId.ToString(), outbox.Message, outbox.Id);
                            break;
                        case OutboxType.DeclineInquiry:
                            bookingController.DeclineInquiry(outbox.UserId.ToString(), outbox.ThreadId, outbox.HostId.ToString(), outbox.Message, outbox.Id);
                            break;
                        case OutboxType.Accept:
                            bookingController.AcceptBooking(outbox.SiteType, outbox.HostId.ToString(), outbox.ResevationId, outbox.Message, outbox.UserId.ToString(), outbox.ThreadId, outbox.Id);
                            break;
                        case OutboxType.DeclineBooking:
                            bookingController.DeclineBooking(outbox.UserId.ToString(), outbox.SiteType, outbox.HostId.ToString(), outbox.ResevationId, outbox.Message, outbox.ThreadId,"", outbox.Id);
                            break;
                        case OutboxType.CancelBooking:
                            bookingController.DeclineBooking(outbox.UserId.ToString(), outbox.SiteType, outbox.HostId.ToString(), outbox.ResevationId, outbox.Message, outbox.ThreadId,"", outbox.Id);
                            break;
                        case OutboxType.SpecialOffer:
                            //bookingController.
                            break;
                        case OutboxType.Alter:
                            //bookingController.
                            break;
                        case OutboxType.CancelAlter:
                            //bookingController.
                            break;
                    }
                }

            });
        }
    }
}