﻿using BlackListScrapper.Sites;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BlackListScrapper.Job
{
    [DisallowConcurrentExecutionAttribute]
    public class GuestReviewJob:IJob
    {
        private  Airbnb _AirbnbScrapeManager = null;

        private Vrbo _VrboScrapeManager = null;

        //private Booking.com.ScrapeManager _BookingComScrapeManager = null;

        Homeaway _HomeawayScrapeManager = null;

         Airbnb AirbnbScrapper
        {
            get
            {
                if (_AirbnbScrapeManager == null)
                    _AirbnbScrapeManager = new Airbnb();

                return _AirbnbScrapeManager;
            }
            set
            {
                _AirbnbScrapeManager = value;
            }
        }

         Vrbo VrboScrapper
        {
            get
            {
                if (_VrboScrapeManager == null)
                    _VrboScrapeManager = new Vrbo();

                return _VrboScrapeManager;
            }
            set
            {
                _VrboScrapeManager = value;
            }
        }
        Homeaway HomeawayScrapper
        {
            get
            {
                if (_HomeawayScrapeManager == null)
                    _HomeawayScrapeManager = new Homeaway();

                return _HomeawayScrapeManager;
            }
            set
            {
                _HomeawayScrapeManager = value;
            }
        }
        IJobExecutionContext jobContext;
        public async Task Execute(IJobExecutionContext context)
        {
            int hostId = context.JobDetail.JobDataMap["hostId"].ToInt();
            int siteType = context.JobDetail.JobDataMap["siteType"].ToInt();
            string userId = context.JobDetail.JobDataMap["userId"].ToString();
            await Task.Factory.StartNew(() =>
            {
                var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId);
                switch (siteType)
                {
                    case 1:
                        AirbnbScrapper.GuestReviewsToHost(token,hostId,userId);
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }

            });
        }
    }
}