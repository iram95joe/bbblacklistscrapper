﻿using BlackListScrapper.Database;
using BlackListScrapper.Database.Entity;
using BlackListScrapper.Helpers;
using BlackListScrapper.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace BlackListScrapper.SignalR
{
    //[HubName("reviewsHub")]
    public class ShowNewReservationReview:Hub
    {
        public static async Task<bool> NotifyUserLogin(string userId, Enumerations.NotificationType type, string message)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", userId);
                reqParams.Add("type", type.ToInt().ToString());
                reqParams.Add("message", message);
                var reqContent = new FormUrlEncodedContent(reqParams);
                var result = await client.PostAsync("Import/NotifyUserLogin", reqContent);
                return true;
            }
        }

        public static async Task<bool> UpdateReservationStatus(string userId, Enumerations.NotificationType type, string message,Outbox outbox)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", userId);
                reqParams.Add("type",type.ToInt().ToString());
                reqParams.Add("message", message);
                reqParams.Add("outbox", JsonConvert.SerializeObject(outbox));
                var reqContent = new FormUrlEncodedContent(reqParams);
                var result = await client.PostAsync("Booking/UpdateReservationStatus", reqContent);
                return true;
            }
        }
        public static async Task<bool> ShowNewData(string userId,Database.Entity.Reservation reservation)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId",userId);
                //reqParams.Add("hostId", hostId.ToString());
                //reqParams.Add("siteType", ((int)siteType).ToString());
                reqParams.Add("reservation", JsonConvert.SerializeObject(reservation));

                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Home/ShowNewData", reqContent);

                if (result != null && result.Content != null)
                {
                    var message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    return message["result"].ToBoolean();
                }
                return true;
            }
        }

        public static async Task<bool> ShowNewInquiry(string userId,int Id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", userId);
                reqParams.Add("Id", Id.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);
                var result = await client.PostAsync("Home/ShowNewInquiry", reqContent);

                if (result != null && result.Content != null)
                {
                    var message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    return message["result"].ToBoolean();
                }
                return true;
            }
        }
        //Show Downloading toaster in ui
        public static async Task<bool> ShowDownloading(string userId,int hostId)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", userId);
                reqParams.Add("hostId", hostId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Home/ShowDownloading", reqContent);

                if (result != null && result.Content != null)
                {
                    var message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    return message["result"].ToBoolean();
                }
                return true;
            }
        }

        public static async Task<bool> DoneDownloading(string userId,int hostId)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", userId);
                reqParams.Add("hostId", hostId.ToString());
                var reqContent = new FormUrlEncodedContent(reqParams);

                var result = await client.PostAsync("Home/DoneDownloading", reqContent);

                if (result != null && result.Content != null)
                {
                    var message = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                    return message["result"].ToBoolean();
                }
                return true;
            }
        }
    }
}