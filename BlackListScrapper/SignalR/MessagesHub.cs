﻿using BlackListScrapper.Database.Entity;
using BlackListScrapper.Helpers;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace BlackListScrapper.SignalR
{
    public class MessagesHub: Hub
    {
        public static async Task<bool> UpdateThreadMessages(int hostId, Message message)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("message", JsonConvert.SerializeObject(message));
                var reqContent = new FormUrlEncodedContent(reqParams);
                var result = await client.PostAsync("Messages/UpdateThreadMessages", reqContent);
                return true;
            }
        }
    }
}