﻿using BlackListScrapper.Enumerations;
using BlackListScrapper.Helpers;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace BlackListScrapper.SignalR
{
    [HubName("autoLoginHub")]
    public class AutoLoginHub : Hub
    {
        #region TriggerLogin
        public async static Task<bool> TriggerLogin(int hostId, string loginModel)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", GlobalVariables.UserId.ToString());
                //reqParams.Add("hostId", hostId.ToString());
                //reqParams.Add("siteType", ((int)siteType).ToString());
                reqParams.Add("loginModel", loginModel);

                var reqContent = new FormUrlEncodedContent(reqParams);

                var results = await client.PostAsync("Import/ShowLogin", reqContent);
                return true;
            }

        }
        #endregion
        #region TriggerAirlock
        public async static Task<bool> TriggerAirlock(int hostId, SiteType siteType,string userId)
        {
            if (!GlobalVariables.HostWithPendingLoginRequest.Contains(hostId))
            {
                GlobalVariables.HostWithPendingLoginRequest.Add(hostId);
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                    var reqParams = new Dictionary<string, string>();
                    reqParams.Add("userId", userId);
                    reqParams.Add("hostId", hostId.ToString());
                    reqParams.Add("siteType", ((int)siteType).ToString());
                    
                    var reqContent = new FormUrlEncodedContent(reqParams);

                    var results = await client.PostAsync("Import/AutoAirlock", reqContent);

                    return true;
                }
            }

            return false;
        }
        #endregion
        public async static Task<bool> SuccessLoginRemoveToastr(string userId,int hostId)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", userId);
                reqParams.Add("hostId", hostId.ToString());

                var reqContent = new FormUrlEncodedContent(reqParams);

                var results = await client.PostAsync("Import/SuccessLoginRemoveToastr", reqContent);
                return true;
            }

        }

        public async static Task<bool> ShowBlockUser(string BlockDetails)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(GlobalVariables.ApiBaseUrl());
                var reqParams = new Dictionary<string, string>();
                reqParams.Add("userId", GlobalVariables.UserId.ToString());
                //reqParams.Add("hostId", hostId.ToString());
                reqParams.Add("BlockDetails", BlockDetails);

                var reqContent = new FormUrlEncodedContent(reqParams);

                var results = await client.PostAsync("Import/ShowBlockUser", reqContent);
                return true;
            }

        }
    }
}