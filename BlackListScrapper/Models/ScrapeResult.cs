﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Models
{
    public class ScrapeResult
    {   public bool IsComplete { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }

        public int TotalRecords { get; set; }

        public string ReservationId { get; set; }
    }
}