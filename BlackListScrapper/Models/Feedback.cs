﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Models
{
    public class Feedback
    {
        public string Firstname { get; set; }

        public DateTime DateCreated { get; set; }

        public string FeedBack { get; set; }

        public string Hostname { get; set; }
    }
}
