﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Models
{
    public class GuestReviews
    {
        public string ConfirmationCode { get; set; }

        public string GuestId { get; set; }

        public string SiteProfileUrl { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Photo { get; set; }

        public string Review { get; set; }

        public int Rating { get; set; }

        public List<DateTime> BookingDates { get; set; }

        public string Property { get; set; }

        public string DateRange { get; set; }

        public string Location { get; set; }

        public string CheckinDate { get; set; }

        public string Host { get; set; }
    }
}
