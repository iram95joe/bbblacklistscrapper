﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Models
{
    public class LogInResult
    {
        public Airlock Airlock { get; set; }

        public string Cookie { get; set; }

        public bool IsNewLogIn { get; set; }

        public string Message { get; set; }

        public bool Success { get; set; }
    }
}
