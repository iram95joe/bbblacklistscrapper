﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Models
{
    public class SpecialOfferSummary
    {
        public bool IsSuccess { get; set; }
        public decimal Subtotal { get; set; }
        public decimal GuestPays { get; set; }
        public decimal HostEarns { get; set; }
        public bool IsAvailable { get; set; }
    }
}