﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Models
{
    public class Airlock
    {
        public string ChallengeToken { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Code { get; set; }

        public string Email { get; set; }

        public Dictionary<string, string> HiddenFields { get; set; }

        public string FormAction { get; set; }

        public bool IsNew { get; set; }

        public string LockId { get; set; }

        public string Name { get; set; }

        public string PhoneId { get; set; }

        public string PhoneNumber { get; set; }

        public Dictionary<string, string> PhoneNumbers { get; set; }

        public string ProfileImageUrl { get; set; }

        public int SelectedChoice { get; set; }

        public string SelectedPhoneNumber { get; set; }

        public string UserId { get; set; }

        public string AppUserId { get; set; }

        public int CompanyId { get; set; }
    }
}
