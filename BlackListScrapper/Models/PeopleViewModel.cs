﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Models
{
    public class PeopleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string BODate { get; set; }
        public string Age { set; get; }
        public string Link { get; set; }
        public string SocialMedia { get; set; }
        public string Picture { get; set; }
        public List<string> Relatives = new List<string>();
        public List<string> Neighbors = new List<string>();
        public List<string> Address = new List<string>();
        public List<string> Emails = new List<string>();
        public List<string> PhoneNumber = new List<string>();
        public List<string> Properties = new List<string>();
        public List<string> CriminalRecord = new List<string>();
    }
}