﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Models
{
    public class InboxReservationModel
    {
        public string Id { get; set; }
        public string GuestId { get; set; }
        public string GuestFirstName { get; set; }
        public string GuestLastName { get; set; }
        public long ListingId { get; set; }
        public DateTime LastMessageAt { get; set; }
        public bool Unread { get; set; }
        public string MessageSnippet { get; set; }
        public bool InquiryOnly { get; set; }
        public string InquiryDate { get; set; }
        public string Status { get; set; }
        public string ReservationCode { get; set; }
        public DateTime StayStartDate { get; set; }
        public DateTime StayEndDate { get; set; }
        public string Type { get; set; }
        public string ReplyToEmail { get; set; }
    }
}