﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Models
{
    public class CaptchaViewModel
    {
        public string SiteKey { get; set; }
        public string LockId { get; set; }
        public string CaptchaResponse { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}