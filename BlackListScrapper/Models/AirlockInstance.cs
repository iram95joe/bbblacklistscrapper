﻿using BlackListScrapper.Sites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Models
{
    public class AirlockInstance
    {
        public string Username { get; set; }
        public int SiteType { get; set; }
        public Airbnb Instance { get; set; }
    }
}