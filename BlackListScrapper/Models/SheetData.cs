﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Models
{
    public class SheetData
    {
        public string Timestamp { get; set; }

        public string GuestFirstname { get; set; }

        public string GuestLastname { get; set; }

        public string GuestResidence { get; set; }

        public string GuestProfileLink { get; set; }

        public string GuestSocialMediaLink { get; set; }

        public string IncidentDetail { get; set; }

        public string DateOfIncident { get; set; }

        public string Hostname { get; set; }

        public string DatePostedInFB { get; set; }

        public List<string> Photos { get; set; }
    }
}
