﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Models
{
    public class Host
    {
        public string SiteHostId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string ProfilePictureUrl { get; set; }

        public string Username { get; set; }
    }
}
