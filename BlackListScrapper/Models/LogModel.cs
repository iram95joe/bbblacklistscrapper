﻿using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Models
{
    public class LogModel
    {
        public User User { get; set; }
        public List<Log> Activity= new List<Log>(); 
    }
}