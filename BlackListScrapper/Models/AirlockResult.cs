﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Models
{
    public class AirlockResult
    {
        public string Cookie { get; set; }

        public bool Success { get; set; }

        public string Message { get; set; }
    }
}
