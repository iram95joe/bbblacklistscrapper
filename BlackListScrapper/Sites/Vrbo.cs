﻿using BaseScrapper;
using BlackListScrapper.Enumerations;
using BlackListScrapper.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using BlackListScrapper.Database.Entity;
using BlackListScrapper.SignalR;
using BlackListScrapper.Database;
using BlackListScrapper.Models;
using System.Data.Entity;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using BaseScrapper.Extensions;
using BlackListScrapper.Job;
using System.Text.RegularExpressions;

namespace BlackListScrapper.Sites
{
    public class Vrbo : CommunicationEngine, IDisposable
    {
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        bool disposed = false;
        private CommunicationPage commPage = null;
        public Vrbo()
        {
            commPage = new CommunicationPage("https://admin.vrbo.com");
        }
        public async Task<bool> StartImporting(string token, int hostId, string userId)
        {
            await Task.Factory.StartNew(() =>
            {
                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Start Scrapping for host {0}", hostId));
                ShowNewReservationReview.ShowDownloading(userId, hostId);
                Scrapping.Stop(hostId + "import");
                //JobScheduler.PropertyJobScheduler.StartImport("property" + hostId + "import", hostId, 2, userId);
                //JobScheduler.InboxJobScheduler.StartImport("inbox" + hostId + "import", hostId, 2, userId);
                //JobScheduler.ReservationJobScheduler.StartImport("reservation" + hostId + "import", hostId, 2, userId);
                //JobScheduler.GuestReviewJobScheduler.Start("guestreview" + hostId + "import", hostId, 2, userId);
                ScrapeMyListings(token, hostId, userId);
                ScrapeInbox(token, hostId, false, InboxThreadType.All, userId);
                ScrapeReservation(userId, token, hostId, SyncType.All, true, true, true, true);
                GuestReviewsToHost(token);
                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Finished Scrapping for host {0}", hostId));
            });
            return true;
        }
        private void VrboSecuritySettingCheck(CommunicationPage commPage)
        {
            if (commPage.IsValid && commPage.Html.Contains("Please wait while we check your security settings"))
            {
                var xd = commPage.ToXml();
                var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
                var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

                commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.PersistCookies = true;
                commPage.Referer = "";
                commPage.Parameters = hdnFields;
                ProcessRequest(commPage);
            }
        }
        public bool CancelAlteration(string reservationCode)
        {
            try
            {
                commPage.RequestURL = commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();


                ProcessRequest(commPage);
                string alterationId = "";
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {


                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-alter_cancel_data']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);
                        var alts = (JArray)ob["reservation"]["reservation_alterations"];
                        if (alts != null)
                        {
                            alterationId = alts[0]["reservation_alteration"]["id"].ToSafeString();
                        }
                    }
                }

                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                var url = "https://www.airbnb.com/reservation_alterations/cancel/" + alterationId;
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;

                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("utf8", "✓");
                commPage.Parameters.Add("authenticity_token", token);
                commPage.RequestHeaders = new Dictionary<string, string>();

                commPage.Referer = string.Format("https://www.airbnb.com/reservation_alterations/{0}", alterationId);


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsoluteUri.Contains("reservation/itinerary"))
                    {
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Cancel alteration {0}",reservationCode), "Success");

                        return true;
                    }
                }

            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in process alteration reservationCode {0}",reservationCode), "Error");


                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error in cancel alteration"), "Error");

            //ElmahLogger.LogInfo(string.Format("Error in cancel alteration"));

            return false;
        }

        #region RESERVATION ACTIONS

        public ScrapeResult AcceptOffer(string token, string reservationId, string message, string threadId, string userId)
        {
            var sresult = new ScrapeResult();
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;


                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        //commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/acceptv2?_restfully=true");
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/bookingRequests/accept?_restfully=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        //commPage.JsonStringToPost = string.Format("{{\"acceptRequestPaymentType\":\"ONLINE\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\"}}", reservationId, message);
                        //commPage.JsonStringToPost = string.Format("{{\"acceptRequestPaymentType\":\"ONLINE\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\"}}", reservationId, message);
                        commPage.JsonStringToPost = string.Format("{{\"message\":{{\"text\":\"{0}\",\"attachmentUrls\":[]}},\"acceptPaymentType\":\"ONLINE\",\"reservationUuid\":\"{1}\"}}", message, reservationId);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";


                        ProcessRequest(commPage);
                        sresult.Message = "Compage not valid";
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            sresult.Message = "Operation successful.";
                            sresult.Success = true;
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Accept offer {0}", reservationId), "Success", hostId.ToString(), "Vrbo");

                            using (var db = new ApplicationDbContext())
                            {
                                var reservation = db.Reservations.Where(x => x.ThreadId == threadId).FirstOrDefault();
                                reservation.BookingStatusCode = "A";
                                db.Entry(reservation).State = EntityState.Modified;
                                db.SaveChanges();
                                var inbox = db.Inboxes.Where(x => x.ThreadId == threadId).FirstOrDefault();
                                inbox.Status = "A";
                                db.Entry(inbox).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            var msg = ScrapeLastMessage(threadId);
                            Database.Actions.Messages.Add(msg);
                            return sresult;
                            //var status = ob["reservationUuid"].ToSafeString();
                            //if (!string.IsNullOrEmpty(status))
                            //{
                            //    sresult.Message = "Operation successful.";
                            //    sresult.Success = true;
                            //    Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Accept offer {0}", reservationId), "Success", hostId.ToString(), "Vrbo");
                            //    return sresult;
                            //}
                        }
                    }
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in accepting offer with reservationId {0}", reservationId), "Error", hostId.ToString(), "Vrbo");

                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                    sresult.Message = e.ToString();
                }
            }
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error in accepting offer,Could not login, please check your credentials"), "Warning", hostId.ToString(), "Vrbo");

            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        #region Send Message To Inbox
        public ScrapeResult SendMessageToInbox(string token, string threadId, string messageText, string userId)
        {
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&declineReasonsVersion=2&include=conversation&conversationId={0}&_restfully=true", threadId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.Referer = "";


                    ProcessRequest(commPage);



                    //commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/conversations/message?conversationId={0}&blockUntilResponseScore=true&_restfully=true", threadId);
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/v1/conversations/message?conversationId={0}&_restfully=true", threadId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    //CommPage.JsonStringToPost = string.Format("{{\"attachments\":[],\"message\":\"{0}\",\"typeKey\":\"REPLIED\"}}", messageText);
                    commPage.JsonStringToPost = string.Format("{{\"attachments\":[],\"message\":\"{0}\",\"typeKey\":\"REPLIED\"}}", messageText);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    //CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    commPage.Referer = "";


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var status = ob["typeKey"].ToSafeString();
                        if (status == "REPLIED")
                        {
                            var msg = ScrapeLastMessage(threadId);
                            Database.Actions.Messages.Add(msg);
                            //var addResult = Core.API.Vrbo.InboxMessages.Add(threadId, msg.MessageId, msg.Message, msg.IsMyMessage, msg.Timestamp, userId, true);
                            sresult.Success = true;
                            //ScrapeInbox(token,true, InboxThreadType.All);
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Message sent to Inbox {0}", threadId), "Success", hostId.ToString(), "Vrbo");
                        }
                    }
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in sending message to threadId {0} {1} {2}", threadId, e, e.InnerException), "Error", hostId.ToString(), "Vrbo");
                    sresult.Message = e.Message;
                }
            }
            else
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to send message, Please check your login credentials"), "Warning", hostId.ToString(), "Vrbo");
                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }
        #endregion

        public Message ScrapeLastMessage(string threadId)
        {
            Message message = new Message();
            string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&conversationId={0}&_restfully=true", threadId);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(commPage.Html);
                var arr = (JArray)ob["messages"];
                if (arr.Count > 0)
                {

                    var items = arr.Where(f => f["from"]["role"].ToSafeString() != "SYSTEM").ToList();

                    var userId = items[0]["from"]["participantUuid"].ToSafeString();
                    var myMessage = false;

                    var sender = items[0]["from"]["name"].ToSafeString();
                    if (sender == "You")
                    {
                        myMessage = true;
                    }
                    var profilelink = "https://www.vrbo.com/traveler/profiles/" + userId;
                    var profileImagelink = items[0]["from"]["avatarThumbnailUrl"].ToSafeString();

                    var messageContent = items[0]["body"].ToSafeString();
                    var createdAt = items[0]["sentTimestamp"].ToDateTime();
                    var messageId = items[0]["uuid"].ToSafeString();
                    var type = items[0]["title"].ToSafeString();
                    if (!string.IsNullOrEmpty(messageContent))
                    {
                        message = new Message { MessageContent= messageContent,CreatedDate= createdAt, MessageId = messageId, IsMyMessage = myMessage,ThreadId = threadId };
                        //MessagesHub.UpdateThreadMessages(Database.Actions.Inboxes.GetHostIdByThreadId(threadId), message);
                    }

                }
            }
            return message;
        }
        public ScrapeResult Pre_Approve_Inquiry(string token, string conversationId, string message)
        {
            var isLogedIn = ValidateTokenAndLoadCookies(token);
            var sresult = new ScrapeResult();
            try
            {
                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&declineReasonsVersion=2&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                // commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.1157366.1705606/g-{0}", conversationId);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "*");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                //var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                //var auth_token = cc["crumb"].Value.DecodeURL();
                //commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);



                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var quote = (JObject)ob["quote"];

                    quote["commissionable"] = true;
                    quote["commissioned"] = true;

                    try
                    {

                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?locale=en_US_VRBO&useRates=true&conversationId={0}&_restfully=true", conversationId);//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");
                                                                                                                                                                                                                // commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?locale=en_US_VRBO&_restfully=true", conversationId);//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");

                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);




                        //commPage.Referer = string.Format("https://admin.vrbo.com/rm/message/l-321.827199.1375137/g-{0}", conversationId);


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(commPage.Html);
                        }

                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/book?_restfully=true");
                        ob.Remove("bodyText");
                        ob.Add("bodyText", message);


                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(ob);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                        cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(commPage.Html);
                            var quoteGuid = ob["quoteGuid"].ToSafeString();
                            if (string.IsNullOrEmpty(quoteGuid))
                            {
                                //ElmahLogger.LogInfo(string.Format("Error in pre-approve"));
                                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error in pre-approve"), "Error", hostId.ToString(), "Vrbo");
                                sresult.Success = false;
                                sresult.IsComplete = false;
                                return sresult;
                            }
                            sresult.Success = true;
                            sresult.IsComplete = true;
                            return sresult;
                        }

                    }
                    catch (Exception e)
                    {
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in preapprove inquiry", e, e.InnerException), "Error", hostId.ToString(), "Vrbo");
                        //ElmahLogger.LogInfo(string.Format("Error {0}", e));

                    }
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in preapprove inquiry {0} {1}", e, e.InnerException), "Error", hostId.ToString(), "Vrbo");
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }
            //ElmahLogger.LogInfo(string.Format("Error in pre-approve"));
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error in pre-approve"), "Error", hostId.ToString(), "Vrbo");
            return sresult;
        }
        public ScrapeResult RejectOffer(string token, string reservationId, string reasonType, string reason, string message, string threadId)
        {
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;


                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/declinev2?_restfully=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = string.Format("{{\"declineReason\":\"{3}\",\"attachments\":[],\"reservationUuid\":\"{0}\",\"messageBody\":\"{1}\",\"customDeclineReason\":\"{2}\"}}", reservationId, message, reason, reasonType);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var status = ob["reservationUuid"].ToSafeString();
                            if (!string.IsNullOrEmpty(status))
                            {
                                sresult.Message = "Operation successful.";
                                sresult.Success = true;
                                sresult.IsComplete = true;
                                return sresult;
                            }
                            else
                            {
                                sresult.Success = true;
                                sresult.IsComplete = false;
                                return sresult;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in rejecting offer reservationId {0} {1} {2}", reservationId, e, e.InnerException), "Error", hostId.ToString(), "Vrbo");

                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                    sresult.Message = e.ToString();
                }
            }
            //ElmahLogger.LogInfo(string.Format("Error in reject offer"));
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error in rejecting offer reservationId {0}, please check your login credentials", reservationId), "Error", hostId.ToString(), "Vrbo");

            sresult.Message = ",Could not login, Please check you details and try again.";
            return sresult;
        }

        public Tuple<bool, string, string> GetAlterReservationDetail(string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string token = "")
        {
            var isLoaded = false;

            if (token != "")
            {
                isLoaded = ValidateTokenAndLoadCookies(token);
            }

            var reservationCost = "";
            var total = "";
            var isSuccess = false;
            try
            {
                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                commPage.RequestURL = detailUrl;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var quote = ob["quote"];
                    quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                    quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");

                    var fees = ((JArray)ob["quote"]["fees"]).ToList();

                    var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                    if (rentalNode != null)
                        rentalNode["amount"]["amount"] = price;

                    quote["numChildren"] = children;
                    quote["numAdults"] = adults;

                    try
                    {
                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true&checkinDate={0}&checkoutDate={1}&checkInDate={0}&checkOutDate={1}&useRates=true&conversationId={2}", checkInDate.ToString("yyyy-MM-dd"), checkoutDate.ToString("yyyy-MM-dd"), conversationId);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(commPage.Html);
                            total = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                            fees = ((JArray)ob["fees"]).ToList();

                            rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                            if (rentalNode != null)
                                reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                            isSuccess = true;
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get alter reservation"), "Success");

                        }
                    }
                    catch (Exception e)
                    {
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in getting alter reservation details conversationId {0}",conversationId), "Error");


                        //ElmahLogger.LogInfo(string.Format("Error {0}", e));

                    }
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in getting alter reservation details conversationId {0}", conversationId), "Error");
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }

            return new Tuple<bool, string, string>(isSuccess, reservationCost, total);
        }

        public bool ProcessAlterReservation(string token, string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message = "")
        {
            var alterResult = ProcessAlterReservation(token, conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Add);
            if (alterResult == false)
                alterResult = ProcessAlterReservation(token, conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Replace);
            if (alterResult == false)
                alterResult = ProcessAlterReservation(token, conversationId, checkInDate, checkoutDate, price, adults, children, message, AlterType.Update);

            return alterResult;
        }

        private bool ProcessAlterReservation(string token, string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string message, AlterType alterType)
        {
            bool processAlteration = true;
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            var isLoaded = ValidateTokenAndLoadCookies(token);

            if (isLoaded)
            {
                try
                {


                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&conversationId={0}&_restfully=true", conversationId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var quote = ob["quote"];
                        quote["checkinDate"] = checkInDate.ToString("yyyy-MM-dd");
                        quote["checkoutDate"] = checkoutDate.ToString("yyyy-MM-dd");

                        // quote["paymentRequests"][0]["status"] = "SCHEDULED";
                        var fees = ((JArray)ob["quote"]["fees"]).ToList();



                        var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                        if (rentalNode != null)
                            rentalNode["amount"]["amount"] = price;

                        quote["numChildren"] = children;
                        quote["numAdults"] = adults;
                        quote["bodyText"] = message;

                        if (processAlteration)
                        {
                            try
                            {

                                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true&checkinDate={0}&checkoutDate={1}&checkInDate={0}&checkOutDate={1}&useRates=true&conversationId={2}", checkInDate.ToString("yyyy-MM-dd"), checkoutDate.ToString("yyyy-MM-dd"), conversationId);//string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true"); //string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");
                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                                commPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                                var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                                var auth_token = cc["crumb"].Value.DecodeURL();
                                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                                commPage.Referer = "";


                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(commPage.Html);
                                    if (ob["violations"] != null)
                                    {
                                        //ElmahLogger.LogInfo(string.Format("Error in process alter reservation"));
                                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Alter reservation violation"), "Warning", hostId.ToString(), "Vrbo");
                                        return false;

                                    }
                                }



                                switch (alterType)
                                {
                                    case AlterType.Update:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                        break;
                                    case AlterType.Add:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/add?locale=en_US_VRBO&_restfully=true");

                                        break;
                                    case AlterType.Replace:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomReservationRequest/replace?_restfully=true");

                                        break;
                                    default:
                                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/update?_restfully=true&site=VRBO&locale=en_US");

                                        break;
                                }

                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                                commPage.JsonStringToPost = commPage.Html;//JsonConvert.SerializeObject(quote);
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                                cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                                auth_token = cc["crumb"].Value.DecodeURL();
                                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                                commPage.Referer = "";


                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(commPage.Html);
                                    var quoteGuid = ob["quoteGuid"].ToSafeString();
                                    if (string.IsNullOrEmpty(quoteGuid))
                                    {
                                        //Core.Helper.EmailService.SendEmail("Error in alter reservation", string.Format("Alter reservation failed conversationId:{0} CompanyId:{1} User:{2}", conversationId, GlobalVariables.CompanyId, GlobalVariables.UserId), GlobalVariables.ErrorEmail());
                                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Alter reservation failed {0}", conversationId), "Error", hostId.ToString(), "Vrbo");

                                        return false;
                                    }
                                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Alter reservation {0}", conversationId), "Success", hostId.ToString(), "Vrbo");
                                    return true;
                                }

                            }
                            catch (Exception e)
                            {
                                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in process alter reservation conversationId {0} {1} {2}", conversationId, e, e.InnerException), "Error", hostId.ToString(), "Vrbo");

                            }
                        }
                        else
                        {
                            try
                            {
                                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/calculateTotal?_restfully=true&locale=en_US_VRBO&numPayments=1&isPetIncluded=true");
                                commPage.PersistCookies = true;
                                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                                commPage.JsonStringToPost = JsonConvert.SerializeObject(quote);
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                                commPage.Referer = "";


                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(commPage.Html);
                                    var quoteTotal = ob["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                                    fees = ((JArray)ob["fees"]).ToList();

                                    var reservationCost = "";
                                    var serviceFee = "";
                                    var cleaningFee = "";
                                    var refundableDamageDeposit = "";

                                    rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                                    if (rentalNode != null)
                                        reservationCost = rentalNode["amount"]["amount"].ToSafeString();
                                }

                            }
                            catch (Exception e)
                            {
                                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in process alter reservation conversationId {0}", conversationId), "Error", hostId.ToString(), "Vrbo");

                                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

                            }

                        }

                    }
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in process alter reservation conversationId {0}", conversationId), "Error", hostId.ToString(), "Vrbo");


                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));

                }
            }

            return false;
        }

        public enum AlterType
        {
            Update,
            Add,
            Replace

        }

        public Tuple<Dictionary<string, string>, string> GetCancelReservationReasons(string token)
        {
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/cancellationReasonOptions?_restfully=true&site=vrbo");
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                    commPage.Referer = "";


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var reasons = (JArray)ob["cancellationReasonOptions"];
                        Dictionary<string, string> declineReasons = new Dictionary<string, string>();
                        reasons.ToList().Where(f => !string.IsNullOrEmpty(f["value"].ToSafeString())).ToList().ForEach(s => declineReasons.Add(s["value"].ToSafeString(), s["label"].ToSafeString()));

                        var reasonVersion = ob["version"].ToSafeString();
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get cancel alteration reason"), "success", hostId.ToString(), "Vrbo");

                        return new Tuple<Dictionary<string, string>, string>(declineReasons, reasonVersion);

                    }

                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in getting cancel reservation reasons"), "Error", hostId.ToString(), "Vrbo");


                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));

                }
            }
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error in get alter details"), "Error", hostId.ToString(), "Vrbo");

            //ElmahLogger.LogInfo(string.Format("Error in get alter reservation details"));

            return null;

        }

        public JObject CancelReservationTotals(string quoteGuid, string unitLink)
        {

            try
            {



                commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/cancellationRefundTotals?_restfully=true&locale=en_US&quoteGuid={0}&unitLink={1}&override=false&overrideType=NO_OVERRIDE", quoteGuid, unitLink);
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                commPage.Referer = "";


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    return ob;

                }

            }
            catch (Exception e)
            {
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }


            //ElmahLogger.LogInfo(string.Format("Error in cancel reservation totals"));

            return null;

        }

        public ScrapeResult CancelReservation(string token, string conversationId, string cancelReason)
        {
            var sresult = new ScrapeResult();
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=messageTemplateTypes&include=paymentDetails&include=property&conversationId={0}&_restfully=true", conversationId);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var cancellationPolicies = (JArray)ob["quote"]["cancellationPolicies"];
                        var quoteUuid = ob["quote"]["quoteGuid"].ToSafeString();
                        var unitLink = ob["quote"]["unitLink"].ToSafeString();
                        var reservationId = ob["reservation"]["reservationReferenceNumber"].ToSafeString();


                        var cancelTotals = CancelReservationTotals(quoteUuid, unitLink);

                        string postJsonRaw = string.Format("{{\"overridable\":false,\"unitLink\":\"{0}\",\"quoteUuid\":\"{1}\",\"currencyCode\":\"USD\",\"resId\":\"{2}\",\"hasLodgingTax\":false,\"isPPB\":false,\"damageDepositType\":null,\"damageDepositAutoRefundStatus\":null,\"amountEntered\":0,\"maxRefund\":0,\"maxPartialRefund\":0,\"cancelableAmount\":0,\"paymentScheduleUuid\":null,\"description\":null,\"shouldRefundProducts\":true,\"type\":\"REFUND\",\"cancelReservation\":true,\"cancellationRefundPolicyOverrideType\":\"NO_OVERRIDE\",\"cancellationReason\":\"{3}\",\"cancellationReasonsVersion\":\"ppb1t\"}}", unitLink, quoteUuid, reservationId, cancelReason);

                        var postJson = JObject.Parse(postJsonRaw);
                        postJson.Add("cancellationPolicies", cancellationPolicies);
                        postJson.Add("refundTotals", cancelTotals);


                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/refundAndCancel?_restfully=true");
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(postJson);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";


                        ProcessRequest(commPage);
                        if (commPage.Html == "" && (commPage.StatusCode == HttpStatusCode.OK || commPage.StatusCode == HttpStatusCode.NoContent))
                        {
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Cancel reservation {0}", conversationId), "Success", hostId.ToString(), "Vrbo");
                            sresult.Success = true;
                            sresult.IsComplete = true;
                            return sresult;
                        }
                    }
                }

                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in cancel reservation conversationId {0} {1} {2}", conversationId, e, e.InnerException), "Error", hostId.ToString(), "Vrbo");


                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                }
            }
            return sresult;
        }

        public bool EditReservation(string token, string threadId, DateTime? checkinDate = null, DateTime? checkoutDate = null, int? adults = null, int? children = null)
        {
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            string quoteGuid = "";
            string apiReservationUrl = "";
            string amount = "";
            float maxRefundable = 0;
            string unitLink = "";
            var isLogedIn = ValidateTokenAndLoadCookies(token);
            if (isLogedIn)
            {
                try
                {

                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");




                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        unitLink = ob.SelectTokens("unitLink").FirstOrDefault().ToSafeString();
                        quoteGuid = ob.SelectTokens("quoteGuid").FirstOrDefault().ToSafeString();
                        var reservation = (JObject)ob["reservation"];
                        var reservationId = reservation["id"].ToString();
                        if (checkinDate.HasValue)
                            reservation["checkinDate"] = checkinDate.Value.ToString("yyyy-MM-dd");
                        if (checkoutDate.HasValue)
                            reservation["checkoutDate"] = checkoutDate.Value.ToString("yyyy-MM-dd");
                        if (adults.HasValue)
                            reservation["numAdults"] = adults.Value;
                        if (children.HasValue)
                            reservation["numChildren"] = children.Value;
                        try
                        {

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/reservations/update?_restfully=true&locale=en_US&site=vrbo&reservationId={0}", reservationId);

                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                            commPage.JsonStringToPost = JsonConvert.SerializeObject(reservation);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                            var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                            var auth_token = cc["crumb"].Value.DecodeURL();
                            commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);



                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                apiReservationUrl = ob["apiReservationUrl"].ToSafeString();


                            }

                            commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomQuote/getAdjustedRent?externalRefLink={0}&_restfully=true", apiReservationUrl);

                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                            commPage.Referer = "";


                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                amount = ob["amount"].ToSafeString();
                                var curencyCode = ob["currencyCode"].ToSafeString();

                                var conversationURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                                commPage.RequestURL = conversationURL;
                                commPage.ReqType = CommunicationPage.RequestType.GET;
                                commPage.PersistCookies = true;
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Accept", "*");
                                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");




                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    ob = JObject.Parse(commPage.Html);
                                    var tokens = ob.SelectTokens("refundableAmount");
                                    maxRefundable = tokens.Where(x => x is JObject)
                                          .Where(x => x["amount"].ToShort() > 0)
                                          .Select(x => x["amount"].ToShort()).FirstOrDefault();

                                    // EditReservation_Step2_RefundPayment(1, "testing..", conversationId);
                                    // EditReservation_Step2_AddPaymentRequest(quoteGuid, unitLink, "testing code", DateTime.Now.AddDays(3), "my message", 2);
                                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Edit reservation {0}", threadId), "Success", hostId.ToString(), "Vrbo");

                                    return true;
                                }
                                return true;
                            }

                        }
                        catch (Exception e)
                        {
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in edit reservation threadId {0}", threadId), "Error", hostId.ToString(), "Vrbo");


                            //ElmahLogger.LogInfo(string.Format("Error {0}", e));

                            return false;
                        }
                    }
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in edit reservation threadId {0} {1} {2}", threadId, e, e.InnerException), "Error", hostId.ToString(), "Vrbo");

                    return false;
                }
            }
            return false;
        }

        public bool AddPaymentRequest(string token, string threadId, string description, DateTime dueDate, string message, float amount)
        {
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            try
            {

                bool isAvailable = ValidateTokenAndLoadCookies(token);
                if (isAvailable)
                {
                    string unitLink = "";
                    string quoteGuid = "";

                    var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                    commPage.RequestURL = detailUrl;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        unitLink = ob["quote"]["unitLink"].ToSafeString();
                        quoteGuid = ob["quote"]["quoteGuid"].ToSafeString();
                    }

                    string postJson = string.Format("{{\"quoteGuid\":\"{0}\",\"unitLink\":\"{1}\",\"payment\":{{\"amount\":{2},\"currencyCode\":\"USD\"}},\"description\":\"{3}\",\"ownerMessage\":\"{4}\",\"dueDate\":\"{5}\"}}", quoteGuid, unitLink, amount, description, message, dueDate.ToString("yyyy-MM-dd"));

                    commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/add?_restfully=true");

                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.JsonStringToPost = postJson;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                    var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                    var auth_token = cc["crumb"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                    commPage.Referer = "";


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        quoteGuid = ob["quotePaymentScheduleGuid"].ToSafeString();
                        if (string.IsNullOrEmpty(quoteGuid))
                        {
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error in add payment request"), "Error", hostId.ToString(), "Vrbo");

                            return false;

                        }
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Add payment request"), "Success", hostId.ToString(), "Vrbo");

                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in add payment request threadId {0} {1} {1}", threadId, e, e.InnerException), "Error", hostId.ToString(), "Vrbo");


                return false;
            }
            //ElmahLogger.LogInfo(string.Format("Error in add payment requests"));

            return false;
        }

        public bool SendRefund(string token, string threadId, short amount, string description)
        {
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            try
            {
                bool isLoggedIn = ValidateTokenAndLoadCookies(token);

                if (isLoggedIn)
                {
                    var conversationURL = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?_restfully=true&locale=en_US_VRBO&conversationId={0}&include=conversation&include=reservation&include=inquiry&include=quote&include=overview&include=messages&include=paymentDetails", threadId);
                    commPage.RequestURL = conversationURL;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");




                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var partialRefund = ob.FindTokens("partialRefundableAmount").FirstOrDefault();
                        var maxRefund = ob.FindTokens("refundableAmount").FirstOrDefault();
                        var cancelable = ob.FindTokens("cancelableAmount").FirstOrDefault();

                        var quoteUuid = ob.FindTokens("quoteGuid").FirstOrDefault().ToSafeString();
                        var resId = ob.FindTokens("reservationReferenceNumber").FirstOrDefault().ToSafeString();
                        var paymentScheduleUuid = ob.FindTokens("paymentRequestUUID").FirstOrDefault().ToSafeString();

                        string postJsonRaw = string.Format("{{\"paymentScheduleUuid\":\"{0}\",\"paymentMethodLabel\":\"\",\"quoteUuid\":\"{1}\",\"currencyCode\":\"USD\",\"resId\":\"{2}\",\"isPPB\":false,\"hasLodgingTax\":false,\"isOfflinePayment\":false,\"amountEntered\":\"{3}\",\"description\":\"{4}\",\"shouldRefundProducts\":false,\"type\":\"REFUND\",\"cancelReservation\":false,\"amount\":{{\"amount\":{3},\"currencyCode\":\"USD\",\"localized\":\"${3}\"}},\"totalRefundAmountLocalized\":\"${3}\"}}", paymentScheduleUuid, quoteUuid, resId, amount.ToString("0.0"), description);
                        var postJson = JObject.Parse(postJsonRaw);
                        postJson.Add("maxRefund", maxRefund);
                        postJson.Add("cancelableAmount", cancelable);
                        postJson.Add("maxPartialRefund", partialRefund);

                        commPage.RequestURL = string.Format("https://admin.vrbo.com/rm/proxies/ecomPayment/refund?_restfully=true");

                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                        commPage.JsonStringToPost = JsonConvert.SerializeObject(postJson);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");



                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                        var auth_token = cc["crumb"].Value.DecodeURL();
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                        commPage.Referer = "";


                        ProcessRequest(commPage);
                        if (commPage.StatusCode == HttpStatusCode.NoContent)
                        {
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Send refund {0}", threadId), "Success", hostId.ToString(), "Vrbo");

                            return true;

                        }
                    }
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in send refund threadId {0} {1} {2}", threadId, e, e.InnerException), "Error", hostId.ToString(), "Vrbo");
                return false;
            }
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to send refund threadId {0}", threadId), "Error", hostId.ToString(), "Vrbo");

            //ElmahLogger.LogInfo(string.Format("Error in send refund"));

            return false;
        }

        #endregion
        #region MyListing
        public bool ScrapeMyListings(string token, int hostId, string userId/*bool isImporting = false*/)
        {

            //var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                string url = "https://admin.vrbo.com/haod/properties.html";
                try
                {
                    int count = 1;
                    
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {

                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var rows = xd.SelectNodes("//td[@class='listing-summary']");
                            if (rows.Count > 0)
                            {
                                foreach (XmlNode item in rows)
                                {
                                    var result = GetPublicUuidAndUserId(commPage.Html);

                                    var id = item.GetAttributeFromNode("data-listingguid");

                                    var property = MyProperty(id, result.Item1, result.Item2);

                                    if (property != null)
                                    {
                                        var listingTriad = Helper_ListingToTriad(property.Id.ToString());

                                        url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                                        commPage.RequestURL = url;
                                        commPage.ReqType = CommunicationPage.RequestType.GET;

                                        commPage.RequestHeaders = new Dictionary<string, string>();
                                        commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                                        var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                                        var auth_token = cc["crumb"].Value.DecodeURL();
                                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);



                                        ProcessRequest(commPage);
                                        if (commPage.IsValid && commPage.Html.Contains("Please wait while we check your security settings"))
                                        {
                                            xd = commPage.ToXml();
                                            var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
                                            var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

                                            commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
                                            commPage.ReqType = CommunicationPage.RequestType.POST;
                                            commPage.PersistCookies = true;
                                            commPage.Referer = "";
                                            commPage.Parameters = hdnFields;


                                            ProcessRequest(commPage);
                                            VrboSecuritySettingCheckContinue(hdnFields, autoformAction);
                                        }

                                        if (commPage.IsValid)
                                        {
                                            string calendarRemark = "VRBO_NEW";

                                            try
                                            {
                                                string flagText = commPage.ToXml().SelectSingleNode("//div[@class='le-rates-header']//h2").InnerText;

                                                if (flagText == "Manage Rates to Maximize Revenue")
                                                    calendarRemark = "VRBO_OLD";
                                            }
                                            catch (Exception e)
                                            {
                                            }
                                            string timeZoneName = null;
                                            string timeZoneId = null;
                                            if (!Database.Actions.Properties.PropertyTimeZoneExist(property.ListingId))
                                            {
                                                var timezone = GetTimeZone(property.Longitude, property.Latitude);
                                                timeZoneName = timezone.Item1;
                                                timeZoneId = timezone.Item2;
                                            }
                                        
                                            property.HostId = hostId;
                                            property.TimeZoneId = timeZoneId;
                                            Database.Actions.Properties.AddOrUpdate(property);
                                            count++;

                                        }
                                    }
                                }
                                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Scrape {0} Property for host {1}", count, hostId));
                            }
                            else
                            {
                                //JM 10/07/19 it use when account has single property 
                                var id = commPage.Uri.AbsoluteUri.Replace("https://www.vrbo.com/pxe/feed/", "").Split('?')[0];
                                var result = GetPublicUuidAndUserId(commPage.Html);
                                var property = MyProperty(id, result.Item1, result.Item2);

                                if (property != null)
                                {
                                    var listingTriad = Helper_ListingToTriad(property.Id.ToString());

                                    url = string.Format("https://admin.vrbo.com/haolb/{0}/le/rates.html", listingTriad);
                                    commPage.RequestURL = url;
                                    commPage.ReqType = CommunicationPage.RequestType.GET;

                                    commPage.RequestHeaders = new Dictionary<string, string>();
                                    commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                                    var cc = commPage.COOKIES.GetCookies(new Uri("https://www.vrbo.com"));
                                    var auth_token = cc["crumb"].Value.DecodeURL();
                                    commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);



                                    ProcessRequest(commPage);
                                    if (commPage.IsValid && commPage.Html.Contains("Please wait while we check your security settings"))
                                    {
                                        xd = commPage.ToXml();
                                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
                                        var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

                                        commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
                                        commPage.ReqType = CommunicationPage.RequestType.POST;
                                        commPage.PersistCookies = true;
                                        commPage.Referer = "";
                                        commPage.Parameters = hdnFields;


                                        ProcessRequest(commPage);
                                        VrboSecuritySettingCheckContinue(hdnFields, autoformAction);
                                    }

                                    if (commPage.IsValid)
                                    {
                                        string calendarRemark = "VRBO_NEW";

                                        try
                                        {
                                            string flagText = commPage.ToXml().SelectSingleNode("//div[@class='le-rates-header']//h2").InnerText;

                                            if (flagText == "Manage Rates to Maximize Revenue")
                                                calendarRemark = "VRBO_OLD";
                                        }
                                        catch (Exception e)
                                        {
                                          

                                        }
                                        string timeZoneName = null;
                                        string timeZoneId = null;
                                        if (Database.Actions.Properties.PropertyTimeZoneExist(property.ListingId))
                                        {
                                            var timezone = GetTimeZone(property.Longitude, property.Latitude);
                                            timeZoneName = timezone.Item1;
                                            timeZoneId = timezone.Item2;
                                        }
                                        //var prop = Core.API.Vrbo.Properties.Add(hostId, property.Id, property.ListingUrl, property.Title,
                                        //property.Longitude, property.Latitude, property.MinStay.ToInt(), property.Internet,
                                        //property.Pets, property.WheelChair, property.Description, 0, property.Sleeps,
                                        //property.Bedrooms, property.Bathrooms, property.PropertyType, property.AccomodationType,
                                        //property.Smoking, property.AirCondition, property.SwimmingPool, property.Status,
                                        //calendarRemark, timeZoneName, timeZoneId);
                                        property.HostId = hostId;
                                        property.TimeZoneId = timeZoneId;
                                        Database.Actions.Properties.AddOrUpdate(property);
                                        count++;

                                    }
                                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Scrape {0} Property for host {1}", count, hostId));
                                }
                               
                            }

                            if (count == 0)
                            {
                                Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Possible Changes in Vrbo Properties", IsSentSMS = false });
                            }
                            else
                            {
                                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Scrape {0} Property for host {1}", count, hostId));
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message ="Possible Changes in Vrbo Properties", IsSentSMS = false });
                }
            }
            else
            {
                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Session Expired Scrape Property for host {0}", hostId));
                //AutoLoginHub.TriggerAirlock(hostId, Enumerations.SiteType.VRBO, userId);

            }
            return true;
        }
        public Tuple<string, string> GetPublicUuidAndUserId(string html)
        {
            var publicUuid = "";
            var userUuid = "";
            try
            {
                string url = "https://admin.vrbo.com/p/account-settings/ums/profile";

                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.ReqType = CommunicationPage.RequestType.GET;


                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    userUuid = ob["userUuid"].ToString();
                    publicUuid = ob["publicUuid"].ToString();
                }
            }
            catch (Exception e) { }

            return new Tuple<string, string>(publicUuid, userUuid);
        }

        private string Helper_ListingToTriad(string listingId)
        {
            try
            {
                commPage.RequestURL = "https://admin.vrbo.com/haod/properties.html";
                commPage.ReqType = CommunicationPage.RequestType.GET;



                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();

                    var properties = xd
                        .SelectSingleNode("//table[@class='list-table listingTable table table-hover']//tbody")
                        .SelectNodes("//tr[@class='ACTIVE']//td[@class='listing-summary']");

                    return properties
                        .Cast<XmlNode>()
                        .Where(x => x
                            .Attributes["data-listingguid"]
                            .Value
                            .Contains(listingId))
                        .First()
                        .Attributes["data-listingguid"]
                        .Value;
                }
                else return "";
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Erro in helper listingToTriad"), "Success");
                return "";
            }
        }

        private string Helper_GetUnitUrl(string listingTriad)
        {
            try
            {
                commPage.RequestURL = string.Format("https://admin.vrbo.com/gd/proxies/minimumContent/getChecklistReportByListingTriad?id=PUBLISH_YOUR_LISTING&listingTriad={0}&_restfully=true", listingTriad);
                commPage.ReqType = CommunicationPage.RequestType.GET;



                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);

                    return ob["url"].ToString();
                }
                else return "";
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in Helper GetUnitUrl"), "Success");

                return "";
            }
        }
        public Property MyProperty(string id, string ownerPublicuuid, string ownerUserid)
        {
            Property listing = new Property();
            //listing.OwnerPublicuuid = ownerPublicuuid;
            //listing.OwnerGuid = ownerUserid;

            var propertyId = ScrapeHelper.SplitSafely(id, ".", 1).ToSafeString().ToInt();
            listing.ListingId = propertyId;
            listing.ListingUrl = string.Format("https://www.vrbo.com/{0}", propertyId);

            MyProperty_Location(id, listing);
            MyProperty_Description(id, listing);
            MyProperty_Status(id, listing);
            MyProperty_Photos(id, listing);
            MyProperty_Amenities(id, listing);
            MyProperty_Settings(id, listing);
            MyProperty_Rates(id, listing);
            return listing;

        }
        public void MyProperty_Location(string id, Property listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/loc.html", id);
                //listing.ListingUrl = url;
                commPage.RequestURL = url;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;



                ProcessRequest(commPage);
                VrboSecuritySettingCheck(commPage);

                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        listing.Longitude = xd.SelectSingleNode("//input[@id='locationAddressLng']").GetAttributeFromNode("value");
                        listing.Latitude = xd.SelectSingleNode("//input[@id='locationAddressLat']").GetAttributeFromNode("value");
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get property location of {0}",id), "Success");
                    }
                }
            }
            catch (Exception e)
            {
               

            }

        }
        public void MyProperty_Status(string id, Property listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/experience/panel.json", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;



                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    listing.Status = ob["listingExperienceState"].ToSafeString();
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get property status of {0}",id), "Success");
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in getting status of property {0}",id), "Error");
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }
        }
        public void MyProperty_Description(string id, Property listing)
        {
            try
            {
                var url = "https://www.vrbo.com/pe/api/graphql";

                //JM 08/06/19 This is the list of json for properties that may be a source for future.
                //var datastring =
                //"{{\"operationName\":\"getTranslationSettings\",\"variables\":{{ \"listingId\":\"121.4954200.6142565\"}},\"query\":\"query getTranslationSettings($listingId: ID!) {\n  property(id: $listingId) {\n    id\n    translationSettings {\n      locale\n      translationOptOut\n      __typename\n    }\n    __typename}}}}," +
                //"{{\"operationName\":\"propertyMetadata\",\"variables\":{{\"listingId\":\"121.4954200.6142565\"}},\"query\":\"query propertyMetadata($listingId: String!) {\n  propertyMetadata(id: $listingId) {\n    accommodationsSummary\n    propertyName\n    __typename}}}}," +
                //"{{\"operationName\":\"propertyContent\",\"variables\":{{\"listingId\":\"121.4954200.6142565\",\"locale\":\"en_US\"}},\"query\":\"query propertyContent($listingId: ID!, $locale: String) {\n  propertyContent(id: $listingId, locale: $locale) {\n    description\n    headline\n    __typename}}}},"+
                //"{{\"operationName\":\"getRegistrationNumber\",\"variables\":{{\"listingId\":\"121.4954200.6142565\"}},\"query\":\"query getRegistrationNumber($listingId: ID!) {\n  property(id: $listingId) {\n    id\n    districtRegistrationInformation {\n      showPTR\n      registrationNumber\n      __typename\n    }\n    __typename}}}}," +
                //"{{\"operationName\":\"storyPhotoItem\",\"variables\":{{ \"listingId\":\"121.4954200.6142565\"}},\"query\":\"query storyPhotoItem($listingId: ID!) {\n  storyPhotoItem(propertyTriad: $listingId) {\n    uri(treatment: C1)\n    mediaId\n    __typename\n}}}}," +
                //"{{\"operationName\":\"getPropertyMetadata\",\"variables\":{{\"listingId\":\"121.4954200.6142565\"}},\"query\":\"query getPropertyMetadata($listingId: String!) {\n  propertyMetadata(id: $listingId) {\n    whyPurchased\n    uniqueBenefits\n    yearPurchased\n    ownerListingStory\n    __typename}}}}";

                var queryParam = "query propertyContent($listingId: ID!, $locale: String) {\n  propertyContent(id: $listingId, locale: $locale) {\n    description\n    headline\n    __typename}}";
                var variablesParam = string.Format("{{\"listingId\":\"{0}\",\"locale\":\"en_US\"}}", id);

                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("query", queryParam);
                commPage.Parameters.Add("variables", variablesParam);
                commPage.Parameters.Add("operationName", "propertyContent");
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.POST;

                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "*");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.vrbo.com"));
                var auth_token = cc["crumb"].Value.DecodeURL();
                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                ProcessRequest(commPage);
                var ob = JObject.Parse(commPage.Html);

                listing.Name = ob["data"]["propertyContent"]["headline"].ToString();
                listing.Description = ob["data"]["propertyContent"]["description"].ToString();
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in getting description of property {0}",id), "Error");
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }

        }
        public void MyProperty_Photos(string id, Property listing)
        {
            try
            {
                var url = string.Format("https://admin.vrbo.com/lm/{0}/photos.html", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;



                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        List<string> imgsList = new List<string>();
                        var imgNodes = xd.SelectNodes("//ul[@id='ulPhoto']//img");
                        foreach (XmlNode imgNode in imgNodes)
                        {
                            var imgSrc = imgNode.GetAttributeFromNode("src").Replace(".s8", ".s10").Replace(".c8", ".c10");
                            if (!string.IsNullOrEmpty(imgSrc))
                                imgsList.Add(imgSrc);
                        }
                        listing.Images = string.Join("|", imgsList);
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get property photos of {0}",id), "Success");
                    }
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in getting property photos"), "Error");
                //ElmahLogger.LogInfo(string.Format("Error: {0}", e));

            }

        }
        public void MyProperty_Amenities(string id, Property listing)
        {
            try
            {
                //var url = string.Format("https://admin.vrbo.com/lm/{0}/amenities.html", id);
                var url = "https://admin.vrbo.com/pe/api/graphql";

                var queryParam = "query getPropertyDetails($id: ID!, $enablePopularAmenities: Boolean!, " +
                    "$enableAmenitiesBundle: Boolean!, $enableAmenitiesTrips: Boolean!, $enableSuitability: Boolean!, " +
                    "$enableBathrooms: Boolean!, $enableBedrooms: Boolean!) { property(id: $id) { id details " +
                    "{ elevator { details isPresent __typename } floor livingArea { size " +
                    "unit __typename } maxSleepInBeds propertyTypeId situatedInId __typename " +
                    "} promptedAmenities @include(if: $enablePopularAmenities) { id groupIndex groupNum __typename " +
                    "} promptedAmenitiesTrips @include(if: $enableAmenitiesTrips) { id groupIndex " +
                    "groupNum __typename } amenitiesTrips @include(if: $enableAmenitiesTrips) { " +
                    "id name value hasDescription description rel __typename } " +
                    "popularAmenities: amenities(section: POPULAR) @include(if: $enablePopularAmenities) " +
                    "{ id name available { idAttribute name type value __typename " +
                    "} attributeList { idAttribute name type value __typename " +
                    "} rel __typename } kitchenAmenities: amenities(section: KITCHEN_DINING) " +
                    "@include(if: $enableAmenitiesBundle) { id name available { idAttribute name " +
                    "type value __typename } attributeList { idAttribute name type " +
                    "value __typename } rel __typename } entertainmentAmenities: " +
                    "amenities(section: ENTERTAINMENT) @include(if: $enableAmenitiesBundle) { id name " +
                    "available { idAttribute name type value __typename } attributeList " +
                    "{ idAttribute name type value __typename } rel __typename " +
                    "} outdoorFeaturesAmenities: amenities(section: OUTDOOR_FEATURES) @include(if: " +
                    "$enableAmenitiesBundle) { id name available { idAttribute name type " +
                    "value __typename } attributeList { idAttribute name type value " +
                    "__typename } rel __typename } poolSpaAmenities: amenities(section: " +
                    "POOL_SPA) @include(if: $enableAmenitiesBundle) { id name available { idAttribute " +
                    "name type value __typename } attributeList { idAttribute name type " +
                    "value __typename } rel __typename } suitabilityAmenities: amenities(section: SUITABILITY) " +
                    "@include(if: $enableSuitability) { id name available { idAttribute name type value __typename " +
                    "} attributeList { idAttribute name type value __typename } " +
                    "parentRel rel __typename } suitabilityChkboxAmenities: amenities(section: " +
                    "SUITABILITY_CHKBOX) @include(if: $enableSuitability) { id name available { " +
                    "idAttribute name type value __typename } attributeList { idAttribute " +
                    "name type value __typename } rel __typename } bathrooms: " +
                    "rooms(section: BATHROOM) @include(if: $enableBathrooms) { roomList { spaceId " +
                    "typeValue name description capacity typeOptions { " +
                    "text value __typename } amenities { " +
                    "id name available { idAttribute " +
                    "name type value __typename } " +
                    "attributeList { idAttribute name type " +
                    "value __typename } __typename } " +
                    "__typename } summary __typename } bedrooms: rooms(section: " +
                    "BEDROOM) @include(if: $enableBedrooms) { roomList { spaceId " +
                    "typeValue name description capacity typeOptions { " +
                    "text value __typename } amenities { " +
                    "id name available { idAttribute name " +
                    "type value __typename } attributeList { " +
                    "idAttribute name type value __typename " +
                    "} __typename } __typename } summary __typename " +
                    "} __typename } propertyTypeDictionary(unitId: $id) { propertyTypes { " +
                    "id name showFloor showElevator showSituatedIn __typename } " +
                    "situatedInOptions { id name __typename } unitAreaOptions " +
                    "__typename }}";
                var variablesParam = string.Format("{{\"enableAmenitiesBundle\":\"true\",\"enableAmenitiesTrips\":" +
                    "\"true\",\"enableBathrooms\":\"false\",\"enableBedrooms\":\"false\",\"enablePopularAmenities\":\"true\"," +
                    "\"enableSuitability\":\"true\",\"id\":\"{0}\"}}", id);

                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.POST;

                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "*/*");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                var auth_token = cc["crumb"].Value.DecodeURL();
                commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("query", queryParam);
                commPage.Parameters.Add("variables", variablesParam);
                commPage.Parameters.Add("operationName", "getPropertyDetails");



                ProcessRequest(commPage);
                VrboSecuritySettingCheck(commPage);

                if (commPage.IsValid)
                {

                    #region IG 09/16/2018 - There's already an api on amenities so I replaced Waqas' scrapping above.

                    var ob = JObject.Parse(commPage.Html);

                    listing.PropertyType = ob["data"]["property"]["details"]["propertyTypeId"].ToString();
                    listing.Bedrooms = ob["data"]["property"]["bedrooms"]["roomList"].Count();
                    listing.Bathrooms = ob["data"]["property"]["bathrooms"]["roomList"].Count();

                    var accessible = ob["data"]["property"]["suitabilityAmenities"][6]["available"]["value"].ToString() == "1" ? true : false;
                    var inaccessible = ob["data"]["property"]["suitabilityAmenities"][7]["available"]["value"].ToString() == "1" ? true : false;

                    if (accessible)
                    {
                        listing.WheelChair = "wheelchair accessible";
                    }
                    else if (inaccessible)
                    {
                        listing.WheelChair = "wheelchair inaccessible";
                    }
                    else
                    {
                        listing.WheelChair = "ask owner";
                    }

                    listing.Internet = ob["data"]["property"]["popularAmenities"][0]["available"]["value"].ToString() == "1" ? "Yes" : "No";
                    listing.AirCondition = ob["data"]["property"]["popularAmenities"][1]["available"]["value"].ToString() == "1" ? "Yes" : "No";
                    listing.AccomodationType = "Vacation Rental";

                    listing.SwimmingPool = "No";
                    var pools = ob["data"]["property"]["poolSpaAmenities"];

                    foreach (var pool in pools)
                    {
                        var hasPool = pool["available"]["value"].ToString() == "1" ? true : false;
                        if (hasPool)
                        {
                            listing.SwimmingPool = "Yes";
                            break;
                        }
                    }

                    #endregion
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in scrapping Amenities of property {0}",id), "Error");
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }

        }
        public void MyProperty_Settings(string id, Property listing)
        {
            try
            {

                var url = string.Format("https://admin.vrbo.com/gd/proxies/lodgingPolicy/get?listingId={0}&isOnboarding=false&_restfully=true", id);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;



                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    listing.Sleeps = ob["lodgingPolicy"]["maximumOccupancyRule"]["guests"].ToInt();
                    listing.Pets = ob["lodgingPolicy"]["petsAllowedRule"]["allowed"].ToSafeString();
                    listing.Smoking = ob["lodgingPolicy"]["smokingAllowedRule"]["allowed"].ToSafeString();
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get property setting of {0}",id), "Success");
                }

            }
            catch (Exception e)
            {
            }

        }
        public void MyProperty_Rates(string id, Property listing)
        {
            try
            {
                //var url = string.Format("https://admin.vrbo.com/haolb//le/rates.html", id);
                var url = string.Format("https://admin.vrbo.com/pxcalendars/proxies/px-rates/generalSettings?_restfully=true&locale=en_US_VRBO&unitUrl={0}", Helper_GetUnitUrl(id));
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;



                ProcessRequest(commPage);
                //VrboSecuritySettingCheck(commPage);

                if (commPage.IsValid)
                {
                    #region Waqas' code. I commented out this part because there are changes in the Vrbo.

                    //var html = commPage.Html;
                    //var start = html.IndexOf("HOMEAWAY.com.olb.rates.model = ") + "HOMEAWAY.com.olb.rates.model = ".Length;
                    //var to = html.IndexOf(";", start);
                    //String result = html.Substring(start, to - start);
                    //result = result.Trim().TrimEnd(';');
                    //var ob = JObject.Parse(result);

                    //listing.PriceNightlyMin = ob["basicRate"]["amount"].ToSafeString();
                    //listing.MinStay = ob["basicRate"]["minimumStay"].ToSafeString();
                    ////Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get property rates of {0}",id), "Success");

                    #endregion

                    var ob = JObject.Parse(commPage.Html);
                    var pricesPerDay = ob["ratesAndDiscounts"]["nightlyRate"];
                    var pricesPerDaySum =
                        (
                            pricesPerDay["sunday"].ToDouble() +
                            pricesPerDay["monday"].ToDouble() +
                            pricesPerDay["tuesday"].ToDouble() +
                            pricesPerDay["wednesday"].ToDouble() +
                            pricesPerDay["thursday"].ToDouble() +
                            pricesPerDay["friday"].ToDouble() +
                            pricesPerDay["saturday"].ToDouble()
                        );
                    var hasOneNightlyRate = pricesPerDay["sunday"].ToDouble() == (pricesPerDaySum / 7);

                    listing.MinStay = 1;
                    listing.PriceNightlyMin = hasOneNightlyRate ? (pricesPerDaySum / 7).ToString() : "0.00";
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in scrapping rates of property {0}", id), "Error");
                //ElmahLogger.LogInfo(string.Format("Error {0}", e));

            }
        }
        public Tuple<string, string> GetTimeZone(string longitude, string latitude)
        {
            try
            {
                string api = String.Format("https://maps.googleapis.com/maps/api/timezone/json?location={0},{1}&timestamp=1331161200&key={2}", latitude, longitude, GlobalVariables.GoogleApiKey());
                commPage.RequestURL = api;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);

                var ob = JObject.Parse(commPage.Html);

                return new Tuple<string, string>(ob["timeZoneName"].ToString(), ob["timeZoneId"].ToString());
            }
            catch
            {
            }
            return new Tuple<string, string>(null, null);
        }
        #endregion

        #region ScrapeInboxThreadListing
        private List<InboxReservationModel> ScrapeInboxThreadListing(bool newOnly, InboxThreadType threadType, CommunicationPage currentcommPage = null)
        {
            List<InboxReservationModel> threadList = new List<InboxReservationModel>();
            var pageNumber = 1;
            var hasMore = false;
            do
            {
                try
                {
                    //string url = string.Format("https://admin.vrbo.com/rm/proxies/conversations/inbox?_restfully=true&page={0}&pageSize=25&locale=en_US&sort=received&sortOrder=desc", pageNumber);
                    string url = string.Format("https://vrbo.com/rm/proxies/v1/conversations/inbox?_restfully=true&page={0}&pageSize=25&locale=en_US&sort=received&sortOrder=desc", pageNumber);
                    switch (threadType)
                    {
                        case InboxThreadType.All:
                            break;
                        case InboxThreadType.Inquiry:
                            url = url + "&status=INQUIRY";
                            break;
                        case InboxThreadType.Reservation:
                            url = url + "&status=RESERVATION";
                            break;
                        case InboxThreadType.Tentative:
                            url = url + "&status=TENTATIVE_RESERVATION";
                            break;
                        case InboxThreadType.Cancelled:
                            url = url + "&status=CANCELLED";
                            break;
                        case InboxThreadType.CurrentStay:
                            url = url + "&status=STAYING";
                            break;
                        case InboxThreadType.PostStay:
                            url = url + "&status=POST_STAY";
                            break;
                        default:
                            break;
                    }

                    hasMore = false;
                    JObject ob = null;
                    if (currentcommPage == null)
                    {
                        commPage.RequestURL = url;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.PersistCookies = true;
                        commPage.Referer = "";
                        commPage.RequestHeaders = new Dictionary<string, string>();


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            ob = JObject.Parse(commPage.Html);
                        }
                    }
                    else
                    {
                        currentcommPage.RequestURL = url;
                        currentcommPage.ReqType = CommunicationPage.RequestType.GET;
                        currentcommPage.PersistCookies = true;
                        currentcommPage.Referer = "";
                        currentcommPage.RequestHeaders = new Dictionary<string, string>();


                        ProcessRequest(currentcommPage);
                        if (currentcommPage.IsValid)
                        {
                            ob = JObject.Parse(currentcommPage.Html);
                        }
                    }

                    var ar = (JArray)ob["conversations"];
                    if (ar == null || ar.Count == 0)
                    {
                        Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Possible Changes in Vrbo Inbox/Reservation", IsSentSMS = false });
                        
                    }
                    if (ar.Count > 0)
                    {

                        var currentPage = ob["display"]["page"].ToInt();
                        var pageSize = ob["display"]["pageSize"].ToInt();
                        var totalResults = ob["display"]["totalResults"].ToInt();

                        if ((currentPage * pageSize) < totalResults)
                            hasMore = true;

                        pageNumber++;
                        foreach (var item in ar)
                        {
                            var readStatus = item["unreadMessages"].ToBoolean();
                            //JM 08/05/19 Can`t Understand its purpose 
                            //if (newOnly && readStatus == false)
                            //{
                            //    hasMore = false;
                            //    break;
                            //}

                            var id = item["id"].ToSafeString();

                            var guestPhoneNumber = item["correspondent"]["phone"].ToSafeString();
                            var guestEmail = item["correspondent"]["email"].ToSafeString();

                            var guestFirstName = item["correspondent"]["firstName"].ToSafeString();
                            var guestLastName = item["correspondent"]["lastName"].ToSafeString();
                            var guestId = item["correspondent"]["profileUuid"].ToSafeString();
                            var lastMessageAt = item["lastMessageReceivedDate"].ToDateTime();
                            var messageSnippet = item["lastMessage"]["message"].ToSafeString();
                            var inquiryDateRange = item["created"].ToSafeString();
                            var status = item["status"].ToSafeString();
                            var listingName = item["property"]["headline"].ToSafeString();
                            var listingId = item["property"]["id"].ToLong();
                            var reservationId = item["reservationReferenceNumber"].ToSafeString();
                            var stayStartDate = item["stayStartDate"].ToDateTime();
                            var stayEndDate = item["stayEndDate"].ToDateTime();
                            var isInquiryOnly = status == "INQUIRY";
                            var replyEmail = item["correspondent"]["mediatedEmailAddress"].ToSafeString();
                            //Ison Code
                            if (!string.IsNullOrEmpty(guestPhoneNumber) && guestPhoneNumber != "null")
                            {
                                //Core.API.Vrbo.Guests.UpdateGuestContactNumber(guestId, guestPhoneNumber);
                                //Core.API.Vrbo.Guests.SaveContactNumber(guestId, guestPhoneNumber);
                            }

                            if (!string.IsNullOrEmpty(guestEmail) && guestEmail != "null")
                            {
                                //Core.API.Vrbo.Guests.SaveEmail(guestId, guestEmail);
                                //Core.API.Vrbo.Guests.UpdateGuestEmail(guestId, guestEmail);
                            }
                            Database.Actions.Guests.AddOrUpdate(new Database.Entity.Guest { SiteGuestId = guestId, FirstName = guestFirstName, LastName = guestLastName, SiteProfileUrl = string.Format("https://www.vrbo.com/traveler/profiles/{0}", guestId) });
                            threadList.Add(new InboxReservationModel
                            {
                                Id = id,
                                GuestId = guestId,
                                GuestFirstName = guestFirstName,
                                GuestLastName = guestLastName,
                                LastMessageAt = lastMessageAt,
                                Unread = readStatus,
                                MessageSnippet = messageSnippet,
                                InquiryOnly = isInquiryOnly,
                                InquiryDate = inquiryDateRange,
                                Status = status,
                                ListingId = listingId,
                                ReservationCode = reservationId,
                                StayStartDate = stayStartDate,
                                StayEndDate = stayEndDate,
                                Type = threadType.ToSafeString(),
                                ReplyToEmail = replyEmail

                            });
                        }
                    }
                    else
                    {
                        hasMore = false;
                    }
                }
                catch { }
            } while (hasMore);

            return threadList;
        }
        #endregion

        #region INBOX AND RESERVATION

        #region ScrapeInbox
        public async void ScrapeInbox(string token, int hostId, bool newOnly, InboxThreadType type, string userId, bool isImporting = false)
        {
            await Task.Factory.StartNew(() =>
            {
                var isloaded = ValidateTokenAndLoadCookies(token);
                if (isloaded)
                {
                    try
                    {
                        var inboxDetails = ScrapeInboxThreadListing(newOnly, type);
                        var totalItems = MessageDetailWrapper(hostId, inboxDetails, isImporting, userId, token);
                    }
                    catch (Exception e)
                    {
                        Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Exception on Vrbo Inbox", IsSentSMS = false });
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Error in Scrape Inbox for host {0}", hostId));
                    }
                }
                else
                {
                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Session Expired Scrape Inbox for host {0}", hostId));
                    AutoLoginHub.TriggerAirlock(hostId, Enumerations.SiteType.VRBO, userId);
                }
            });
        }
        #endregion

        #region ScrapeReservation
        public async void ScrapeReservation(string userId,string token, int hostId, SyncType syncType, bool reservations = false, bool inquiries = false, bool tentativeOrPreApproved = false, bool all = false, bool isImporting = false)
        {
            await Task.Factory.StartNew(() =>
            {
                int count = 0;
                var isloaded = ValidateTokenAndLoadCookies(token);
                if (isloaded)
                {
                    try
                    {
                        var newOnly = syncType == SyncType.New;
                        var threads = new List<InboxReservationModel>();
                        if (all)
                            threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.All));
                        else
                        {
                            if (reservations)
                                threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.Reservation));
                            if (inquiries)
                                threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.Inquiry));
                            if (tentativeOrPreApproved)
                                threads.AddRange(ScrapeInboxThreadListing(newOnly, InboxThreadType.Tentative));
                        }
                        foreach (var thread in threads)
                        {
                            try
                            {
                                var detailUrl = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&include=reservation&include=quote&include=property&conversationId={0}&_restfully=true", thread.Id);
                                commPage.RequestURL = detailUrl;
                                commPage.ReqType = CommunicationPage.RequestType.GET;
                                commPage.PersistCookies = true;


                                ProcessRequest(commPage);
                                if (commPage.IsValid)
                                {
                                    var ob = JObject.Parse(commPage.Html);
                                    var ar = (JArray)ob["replyActions"];
                                    if (ar.Count > 0)
                                    {

                                        var adults = ob["quote"]["numAdults"].ToInt();
                                        var children = ob["quote"]["numChildren"].ToInt();
                                        var guestCount = adults + children;
                                        var nights = ob["quote"]["numNights"].ToInt();
                                        var pets = ob["quote"]["numPets"].ToInt();
                                        var quoteId = ob["quote"]["quoteGuid"].ToSafeString();
                                        var quoteTotal = ob["quote"]["quoteTotals"]["totalAmount"]["amount"].ToSafeString();
                                        var quoteSubTotal = ob["quote"]["quoteTotals"]["subTotalAmount"]["amount"].ToSafeString();
                                        var payableToHost = ob["quote"]["quoteTotals"]["amountForOwner"]["amount"].ToSafeString();
                                        var taxAmount = ob["quote"]["quoteTotals"]["totalTax"]["amount"]["amount"].ToSafeString();

                                        var fees = ((JArray)ob["quote"]["fees"]).ToList();


                                        var rentalCost = "";
                                        var serviceFee = "";
                                        var cleaningFee = "";
                                        var refundableDamageDeposit = "";
                                        var taxRate = "";

                                        var rentalNode = fees.Where(f => f["type"].ToSafeString() == "RENTAL_AMOUNT").FirstOrDefault();
                                        if (rentalNode != null)
                                        {
                                            rentalCost = rentalNode["amount"]["amount"].ToSafeString();
                                            if (rentalNode["feeBasis"]["tax"] != null)
                                            {
                                                taxRate = rentalNode["feeBasis"]["tax"]["rate"].ToSafeString();

                                            }
                                        }

                                        var cleaningNode = fees.Where(f => f["type"].ToSafeString() == "FEE").FirstOrDefault();
                                        if (cleaningNode != null)
                                            cleaningFee = cleaningNode["amount"]["amount"].ToSafeString();

                                        var serviceNode = fees.Where(f => f["type"].ToSafeString() == "TRAVELER_FEE").FirstOrDefault();
                                        if (serviceNode != null)
                                            serviceFee = serviceNode["amount"]["amount"].ToSafeString();

                                        var ddNode = fees.Where(f => f["type"].ToSafeString() == "REFUNDABLE_DAMAGE_DEPOSIT").FirstOrDefault();
                                        if (ddNode != null)
                                            refundableDamageDeposit = ddNode["amount"]["amount"].ToSafeString();



                                        var reservationNode = ob["reservation"];
                                        var reservationId = "";
                                        var reservationCreatedAt = "";
                                        var reservationExpiry = "";
                                        if (reservationNode != null)
                                        {
                                            reservationId = ob["reservation"]["id"].ToSafeString();
                                            reservationCreatedAt = ob["reservation"]["created"].ToSafeString();
                                            reservationExpiry = ob["reservation"]["expirationDateTime"].ToSafeString();
                                        }
                                        var reservationDeclineDateTime = "";
                                        var reservationAcceptedDatetime = "";

                                        var messages = ((JArray)ob["messages"]).ToList();

                                        var declineNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_DECLINED").FirstOrDefault();
                                        if (declineNode != null)
                                            reservationDeclineDateTime = declineNode["sentTimestamp"].ToSafeString();

                                        var acceptNode = messages.Where(f => f["type"].ToSafeString() == "RESERVATION_REQUEST_ACCEPTED").FirstOrDefault();
                                        if (acceptNode != null)
                                            reservationAcceptedDatetime = acceptNode["sentTimestamp"].ToSafeString();

                                        var listingUniqueId = ob["property"]["druidUuid"].ToSafeString();


                                        var summary = ob["quote"]["quoteTotals"].ToSafeString();
                                        //var jsonSummary =  JsonConvert.SerializeObject(summary);




                                        var reservation = new Reservation
                                        {
                                            HostId = hostId,
                                            CheckInDate = thread.StayStartDate,
                                            CheckOutDate = thread.StayEndDate,
                                            ConfirmationCode = thread.ReservationCode,
                                            ServiceFee = serviceFee.ToDecimal(),
                                            CleaningFee = cleaningFee.ToDecimal(),
                                            TotalPayout = payableToHost.ToDecimal(),//quoteTotal,
                                            ReservationCost = quoteTotal.ToDecimal(), //reservationCost,
                                                                                      //RefundableDamageDeposit = refundableDamageDeposit,
                                                                                      //RentalCost = rentalCost,
                                                                                      //TaxRate = taxRate,
                                                                                      //TotalTax = taxAmount,
                                                                                      //ReservationCostSummary = summary,
                                            Nights = nights,
                                            GuestCount = guestCount,
                                            Adult = adults,
                                            Children = children,
                                            ListingId = thread.ListingId,
                                            SiteGuestId = thread.GuestId,
                                            InquiryDate = thread.InquiryDate.ToDateTime() == DateTime.MinValue ? (DateTime?)null : thread.InquiryDate.ToDateTime(),
                                            ReservationId = reservationId, //check this
                                            BookingStatusCode = Utilities.GetReservationStatusCode(thread.Status),
                                            IsActive = (thread.Status == "STAYING" || thread.Status == "BOOKED"),
                                            //ReplyActions = actions,
                                            //Pets = pets.ToSafeString(),
                                            BookingDate = reservationCreatedAt.ToDateTime() == DateTime.MinValue ? (DateTime?)null : reservationCreatedAt.ToDateTime(),
                                            //ReservationRequestExpiryDatetime = reservationExpiry,
                                            BookingConfirmDate = reservationAcceptedDatetime.ToDateTime() == DateTime.MinValue ? (DateTime?)null : reservationAcceptedDatetime.ToDateTime(),
                                            BookingCancelDate = reservationDeclineDateTime.ToDateTime() == DateTime.MinValue ? (DateTime?)null : reservationDeclineDateTime.ToDateTime(),
                                            ThreadId = thread.Id,
                                            //ListingUniqueId = listingUniqueId,
                                            Type = thread.Type
                                        };
                                        if (reservation.BookingStatusCode != "I")
                                            Database.Actions.Reservations.AddOrUpdate(userId, reservation);
                                        count++;
                                        //JM Temporary remove   
                                        //var timein = Core.API.AllSite.Property.GetTimein(reservation.ListingId);
                                        //var timeout = Core.API.AllSite.Property.GetTimeout(reservation.ListingId);
                                        //reservation.CheckInDate = Core.Helper.DateTimeZone.ConvertToUTC((Convert.ToDateTime(reservation.CheckInDate).ToString("yyyy-MM-dd") + " " + timein).ToDateTime(), reservation.ListingId);
                                        //reservation.CheckoutDate = Core.Helper.DateTimeZone.ConvertToUTC((Convert.ToDateTime(reservation.CheckoutDate).ToString("yyyy-MM-dd") + " " + timeout).ToDateTime(), reservation.ListingId);

                                        //var addResult = Core.API.Vrbo.Inquiries.Add(reservation.HostId, reservation.Code, reservation.ReservationId,
                                        //    reservation.Status, reservation.CheckInDate, reservation.CheckoutDate, reservation.Nights,
                                        //    reservation.ListingId, reservation.GuestId, reservation.ReservationCost, reservation.CleaningFee,
                                        //    reservation.ServiceFee, reservation.TotalPayout, reservation.InquiryDatetime.ToDateTime(),
                                        //    reservation.ReservationRequestDatetime.ToDateTime(), reservation.GuestCount, reservation.Adult, reservation.Children,
                                        //    reservation.ReservationRequestAcceptedDatetime.ToDateTime(), reservation.ReservationRequestCancelledDatetime.ToDateTime(),
                                        //    reservation.ThreadId, reservation.Type);


                                        if (syncType == SyncType.New /*&& addResult.IsDuplicate*/)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                        }
                                    }
                                }
                            }
                            catch { }   
                            }
                    }
                    catch (Exception e)
                    {
                    }
                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Scrape {0} Reservation for host {1}", count, hostId));
                }
                else
                {
                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Session Expired Scrape Reservation for host {0}", hostId));
                    //AutoLoginHub.TriggerAirlock(hostId, Enumerations.SiteType.VRBO, userId);
                }
                ShowNewReservationReview.DoneDownloading(userId, hostId);
               
            });
        }
        #endregion

        #region MessageDetailWrapper
        //JM 05/21/19 it will save each inbox deatails to database
        private int MessageDetailWrapper(int hostId, List<InboxReservationModel> inboxDetails, bool isImporting,string userId,string token)
        {
            int count = 1;

            foreach (var item in inboxDetails)
            {
                try
                {
                    //JM 05/21/19 Temporary remove
                    //var timein = Core.API.AllSite.Property.GetTimein(item.ListingId);
                    //var timeout = Core.API.AllSite.Property.GetTimeout(item.ListingId);
                    //item.StayStartDate = Core.Helper.DateTimeZone.ConvertToUTC((Convert.ToDateTime(item.StayStartDate).ToString("yyyy-MM-dd") + " " + timein).ToDateTime(), item.ListingId);
                    //item.StayEndDate = Core.Helper.DateTimeZone.ConvertToUTC((Convert.ToDateTime(item.StayEndDate).ToString("yyyy-MM-dd") + " " + timeout).ToDateTime(), item.ListingId);

                    //It will save Inbox to Database
                    var IsSaved = Database.Actions.Inboxes.AddOrUpdate(new Inbox {
                        HostId=hostId,
                        LastMessageAt =item.LastMessageAt,
                        HasUnread = item.Unread,
                        LastMessage = item.MessageSnippet,
                        SiteGuestId=item.GuestId,
                        ThreadId=item.Id,
                        IsInquiryOnly= item.InquiryOnly,
                        Status= Utilities.GetReservationStatusCode(item.Status),
                        CheckInDate= item.StayStartDate,
                        CheckOutDate= item.StayEndDate,
                        ListingId= item.ListingId,
                        ReplyToEmail= item.ReplyToEmail },userId);
                    if (IsSaved)
                    {//Get at save messages to database by thread
                        HostReviewsToGuest(token,item.GuestId);
                        ScrapeMessageDetails(item.Id);
                    }
                    count++;
                }
                catch (Exception e)
                {
                }
            }
            return count;
        }
        #endregion

        #region ScrapeMessageDetails
        //JM 05/21/19 Scrape Messages of thread
        public void ScrapeMessageDetails(string threadId)
        {
            string url = string.Format("https://admin.vrbo.com/rm/proxies/supplierHome/conversation?locale=en_US_VRBO&include=messages&conversationId={0}&_restfully=true", threadId);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;


            ProcessRequest(commPage);
            string profilePic = null;
            if (commPage.IsValid)
            {
                var ob = JObject.Parse(commPage.Html);

                profilePic = ob["overview"]["travelerImageUrl"].ToString();
                var ar_temp = (JArray)ob["messages"];
                if (ar_temp.Count > 0)
                {
                    var ar = ar_temp.Where(f => f["from"]["role"].ToSafeString() != "SYSTEM").ToList();

                    Database.Actions.Inboxes.UpdateInquiryDate(threadId, ar[ar.Count - 1]["sentTimestamp"].ToDateTime());
                    foreach (var item in ar)
                    {
                        var userId = item["from"]["participantUuid"].ToSafeString();
                        var myMessage = false;

                        var sender = item["from"]["name"].ToSafeString();
                        if (sender == "You")
                        {
                            myMessage = true;
                        }
                        var profilelink = "https://www.vrbo.com/traveler/profiles/" + userId;
                        var profileImagelink = item["from"]["avatarThumbnailUrl"].ToSafeString();

                        var message = item["body"].ToSafeString();
                        var createdAt = item["sentTimestamp"].ToDateTime();
                        var messageId = item["uuid"].ToSafeString();
                        var type = item["title"].ToSafeString();
                        if (!string.IsNullOrEmpty(message))
                        {
                            Database.Actions.Messages.Add(new Message { MessageContent = message, CreatedDate = createdAt, MessageId = messageId, IsMyMessage = myMessage, ThreadId = threadId });
                        }
                    }
                }
            }
        }
        #endregion

        #endregion

        #region HOST

        public Database.Entity.Host ScrapeHostAccount(string username, string password, int companyId, string cookie)
        {
            bool isLoggedIn = HasValidCookie(cookie);

            if (isLoggedIn)
            {
                string url = "https://vrbo.com/p/account-settings/ums/profile";

                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);

                        var userUuid = ob["userUuid"].ToString();
                        var publicUuid = ob["publicUuid"].ToString();
                        var firstName = ob["firstName"].ToString();
                        var lastName = ob["lastName"].ToString();
                        var email = ob["emailAddresses"][0]["value"].ToString();
                        var phoneStart = "+" + ob["phoneNumbers"][0]["countryCode"].ToString() + " ";
                        var phoneNumber = phoneStart + ob["phoneNumbers"][0]["number"].ToString();

                        if (!string.IsNullOrEmpty(userUuid) && !string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
                        {
                            Database.Entity.Host host = new Database.Entity.Host
                            {
                                Firstname = firstName,
                                Lastname = lastName,
                                ProfilePictureUrl = "/Content/default-avatar/" + firstName[0].ToString().ToUpper() + ".png",
                                SiteHostId = userUuid,
                                Username = email,
                                SiteType = 2,
                                Password = Security.Encrypt(password),
                               
                            };

                            var h = Database.Actions.Hosts.AddOrUpdate(host);
                            Database.Actions.HostCompany.AddOrUpdate(new HostCompany() { HostId = h.Id, CompanyId = companyId });
                            //GuestReviewsToHost(cookie, h.SiteHostId, h.Id, userId);
                            return h;
                        }
                    }
                }
                catch
                {

                }
            }

            return null;
        }

        #endregion

        #region Reviews
        public async void GuestReviewsToHost(string token)
        {
            await Task.Factory.StartNew(() =>
            {
                var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {

                    commPage.RequestURL = "https://admin.vrbo.com/p/px-reviews";
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var pageNumber = 1;
                        bool hasMore = true;
                        do
                        {


                            commPage.RequestURL = "https://admin.vrbo.com/p/px-reviews/graphql";
                            commPage.PersistCookies = true;
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            //commPage.JsonStringToPost = string.Format("[{{\"operationName\":\"bookings\",\"variables\":{{\"size\":50,\"page\":{0},\"searchText\":\"\",\"sortHeader\":\"reservationBeginDate\",\"sortDirection\":\"desc\",\"filters\":{{\"tabFilter\":0}},\"locale\":\"en\"}},\"query\":\"query bookings($size: Int!, $locale: String!, $page: Int, $searchText: String, $sortHeader: String, $sortDirection: String, $filters: Object) {{\n  bookings(size: $size, locale: $locale, page: $page, searchText: $searchText, sortHeader: $sortHeader, sortDirection: $sortDirection, filters: $filters) {{\n    uuid\n    unitReviewUuid\n    reservationUuid\n    travelerAccountUuid\n    ownerAccountUuid\n    bookingChannel\n    propertyName\n    unitNumber\n    listingNumber\n    listingImage\n    fiveStarRating\n    travelerFirstName\n    travelerLastName\n    travelerLastInitial\n    travelerFullName\n    ratings\n    ratingCategories\n    reviewStatus\n    reviewBody\n    reviewHeadline\n    reviewResponse\n    responseCreateDate\n    responseCreateTime\n    responseStatus\n    reservationBeginShortDate\n    reservationBeginDate\n    reservationBeginLongDate\n    reservationEndDate\n    reservationEndLongDate\n    reviewExpireDate\n    ratingExpireDate\n    reviewDate\n    reviewTime\n    approved\n    hasRating\n    hasReview\n    hasResponse\n    reviewExpired\n    ratingExpired\n    canRate\n    canReview\n    canRequestReview\n    statusAvailable\n    total\n    __typename\n  }}\n}}\n\"}}]", pageNumber);
                            // commPage.JsonStringToPost = "[{\"operationName\":\"bookings\",\"variables\":{},\"query\":\"query bookings {counts}\"}]";
                            commPage.JsonStringToPost = string.Format("[{{\"operationName\":\"bookings\",\"variables\":{{\"size\":50,\"page\":{0},\"searchText\":\"\",\"sortHeader\":\"reservationBeginDate\",\"sortDirection\":\"desc\",\"filters\":{{\"tabFilter\":0}},\"locale\":\"en\"}},\"query\":\"query bookings($size: Int!, $locale: String!, $page: Int, $searchText: String, $sortHeader: String, $sortDirection: String, $filters: Object) {{  bookings(size: $size, locale: $locale, page: $page, searchText: $searchText, sortHeader: $sortHeader, sortDirection: $sortDirection, filters: $filters) {{    uuid    unitReviewUuid    reservationUuid    travelerAccountUuid    ownerAccountUuid    bookingChannel    propertyName    unitNumber    listingNumber    listingImage    fiveStarRating    travelerFirstName    travelerLastName    travelerLastInitial    travelerFullName    ratings    ratingCategories    reviewStatus    reviewBody    reviewHeadline    reviewResponse    responseCreateDate    responseCreateTime    responseStatus    reservationBeginShortDate    reservationBeginDate    reservationBeginLongDate    reservationEndDate    reservationEndLongDate    reviewExpireDate    ratingExpireDate    reviewDate    reviewTime    approved    hasRating    hasReview    hasResponse    reviewExpired    ratingExpired    canRate    canReview    canRequestReview    statusAvailable    total    __typename  }}}}\"}}]", pageNumber);
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "*/*");
                            var cc = commPage.COOKIES.GetCookies(new Uri("https://admin.vrbo.com"));
                            var auth_token = cc["crumb"].Value.DecodeURL();
                            commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);

                            commPage.Referer = "https://admin.vrbo.com/p/px-reviews";
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                pageNumber++;
                                var ob = JArray.Parse(commPage.Html);
                                var bookings = ((JArray)ob[0]["data"]["bookings"]);
                                if (bookings.Count < 50)
                                    hasMore = false;
                                foreach (var booking in bookings)
                                {
                                    var headline = booking["reviewHeadline"].ToSafeString();
                                    var reviewBody = booking["reviewBody"].ToSafeString();
                                    var rating = booking["fiveStarRating"].ToSafeString();
                                    var listingId = booking["listingNumber"].ToSafeString();
                                    var reviewDate = booking["reviewDate"].ToSafeString();
                                    var travellerId = booking["travelerAccountUuid"].ToSafeString();
                                        if (reviewBody != "" && rating != "" && reviewDate != "")
                                        {
                                            Database.Entity.GuestReview guestReview = new Database.Entity.GuestReview();

                                            //guestReview.ConfirmationCode = item["reservation"]["confirmation_code"].ToString();
                                            guestReview.SiteGuestId = booking["travelerAccountUuid"].ToSafeString();
                                            guestReview.Review = booking["reviewBody"].ToSafeString();
                                            guestReview.Rating = booking["fiveStarRating"].ToInt();
                                            guestReview.ReservationId = booking["reservationUuid"].ToString();
                                            //guestReview.HostId = hostId;
                                            Database.Actions.GuestReviews.AddOrUpdate(guestReview);
                                        }
                                        //Pull this travellers reviews given to hosts.
                                        HostReviewsToGuest(token, travellerId);
                                }
                            }
                        } while (hasMore);
                    }
                }
                catch (Exception e)
                {
                }

            }
            });
        }
        public void HostReviewsToGuest(string token, string travellerId)
        {
            //we can use this method to pull both type of his reviews.
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = string.Format("https://www.vrbo.com/traveler/profile/api/user-profile-service/extendedProfile/findByAccount?accountUuid={0}&reviewLevel=LIST", travellerId);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*/*");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {


                        var ob = JObject.Parse(commPage.Html);
                        var reviewsReceived = ((JArray)ob["receivedTravelerReviews"]);

                        //Given to this guest
                        foreach (var rcvdReview in reviewsReceived)
                        {
                            if (rcvdReview["text"].ToSafeString() != "")
                            {
                                Database.Actions.HostReviews.AddOrUpdate(new HostReview
                                {
                                    SiteGuestId = travellerId,
                                    //SiteHostId = review["author_id"].ToString(),
                                    //Name = review["author"]["first_name"].ToString(),
                                    ProfilePictureUrl = rcvdReview["propertyImageUrl"].ToSafeString(),
                                    Comments = rcvdReview["text"].ToSafeString(),
                                    CreatedAt = rcvdReview["reviewDate"].ToDateTime()
                                });
                            }
                        }

                        //Given by this guest
                        var reviewsGiven = ((JArray)ob["givenUnitReviews"]);

                        foreach (var gvnReview in reviewsGiven)
                        {
                            if (gvnReview["text"].ToSafeString() != "")
                            {
                                Database.Actions.HostReviews.AddOrUpdate(new HostReview
                                {
                                    //SiteGuestId = travellerId,
                                    SiteHostId = travellerId,
                                    //Name = review["author"]["first_name"].ToString(),
                                    ProfilePictureUrl = gvnReview["propertyImageUrl"].ToSafeString(),
                                    Comments = gvnReview["text"].ToSafeString(),
                                    CreatedAt = gvnReview["reviewDate"].ToDateTime()
                                });
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
        }

        #endregion


        #region LOGIN
        public void ProcessContinueForm(XmlDocument xd)
        {
            var continueFormNode = xd.SelectSingleNode("//form[@id='continue-form']");
            if (continueFormNode != null)
            {
                var hdn = ScrapeHelper.GetAllHiddenFieldsAsParamWithID(xd, "continue-form");
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.Parameters = hdn;
                //the url will be same.. 
                commPage.RequestURL = commPage.Uri.AbsoluteUri;
                ProcessRequest(commPage);

            }
        }
        public Models.LogInResult LogIn(string username, string password,string userId)
        {
            bool isRelogin = false;
            ReLogin:
            Models.LogInResult result = new Models.LogInResult();
            string url = "https://www.vrbo.com/auth/ui/login";
            commPage = new CommunicationPage(url);
            commPage.PersistCookies = true;
            ProcessRequest(commPage);

            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                if (xd != null)
                {
                    var formAction = "";
                    var jsonPost = $"{{\"userName\":\"{username}\",\"secret\":\"{password}\",\"authType\":\"HOMEAWAY\",\"rememberMe\":false,\"owner\":true,\"devices\":[{{\"type\":\"THREAT_METRIX\",\"deviceId\":\"\"}}]}}";

                    // commPage.RequestURL = "https://cas.homeaway.com" + formAction;
                    commPage.RequestURL = "https://www.vrbo.com/auth/aam/v3/authenticate";
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.JsonStringToPost = jsonPost;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.Parameters = new Dictionary<string, string>();
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*/*");
                    commPage.RequestHeaders.Add("Content-Type", "application/json");
                    ProcessRequest(commPage);

                    jsonPost = $"{{\"devices\":[{{\"type\":\"THREAT_METRIX\",\"deviceId\":\"\"}}]}}";

                    // commPage.RequestURL = "https://cas.homeaway.com" + formAction;
                    commPage.RequestURL = "https://www.vrbo.com/auth/mfa/v1/shouldChallenge";
                    commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                    commPage.JsonStringToPost = jsonPost;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.Parameters = new Dictionary<string, string>();
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "*/*");
                    commPage.RequestHeaders.Add("Content-Type", "application/json");
                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        var chlng = ob["challengeType"].ToSafeString();
                        if (chlng != "COMPLETE")
                        {
                            var chlngToken = ob["challengeTicket"].ToString();
                            jsonPost = $"{{\"challengeTicket\":\"{chlngToken}\"}}";

                            commPage.RequestURL = "https://www.vrbo.com/auth/mfa/v1/2fa/get";
                            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                            commPage.JsonStringToPost = jsonPost;
                            commPage.PersistCookies = true;
                            commPage.Referer = "";
                            commPage.Parameters = new Dictionary<string, string>();
                            commPage.RequestHeaders = new Dictionary<string, string>();
                            commPage.RequestHeaders.Add("Accept", "*/*");
                            commPage.RequestHeaders.Add("Content-Type", "application/json");
                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                var phonesObj = (JArray)ob["phones"];
                                var phoneNumbers = new Dictionary<string, string>();
                                foreach (JObject num in phonesObj)
                                {
                                    var label = num["countryCode"].ToSafeString();
                                    var val = num["phone"].ToSafeString();
                                    phoneNumbers.Add(val, label);
                                }


                                xd = commPage.ToXml();
                                var hdnFields = new Dictionary<string, string>();
                                var authformAction = "";



                                result.Success = false;
                                result.Airlock = new Models.Airlock()
                                {
                                    ChallengeToken = chlngToken,
                                    HiddenFields = hdnFields,
                                    FormAction = "https://www.vrbo.com" + authformAction,
                                    PhoneNumbers = phoneNumbers,

                                };
                                SaveAirlockCookies(username);
                                return result;
                            }

                        }
                        else
                        {
                            commPage.RequestURL = "https://www.vrbo.com/auth/vrbo/login?service=https%3A%2F%2Fwww.vrbo.com%2Fhaod%2Fauth%2Fsignin.html%3Ftarget%3Dhttp%253A%252F%252Fwww.vrbo.com%252Fhaod&authui=false";
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            ProcessRequest(commPage);
                        }

                    }
                    //Old Code
                    //var formAction = xd.SelectSingleNode("//form[@id='login-form']").GetAttributeFromNode("action");
                    var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "login-form");
                    hiddenFields["username"] = username;
                    hiddenFields["password"] = password.Trim();
                    commPage.RequestURL = "https://www.vrbo.com" + formAction;
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);
                    if (commPage.IsValid && commPage.Html.Contains("Please wait while we check your security settings"))
                    {
                        xd = commPage.ToXml();
                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
                        var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

                        for (int x = 0; x < 3; x++)
                        {
                            commPage.RequestURL = "https://www.vrbo.com" + formAction;
                            commPage.ReqType = CommunicationPage.RequestType.POST;
                            commPage.PersistCookies = true;
                            commPage.Referer = "";
                            commPage.Parameters = hdnFields;
                            ProcessRequest(commPage);
                            if (commPage.Html.Contains("continue-form"))
                            {
                                xd = commPage.ToXml();
                                var prevUrl = commPage.Uri.AbsoluteUri;
                                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.vrbo.com"));
                                var sessId = cc["has"].Value.DecodeURL();
                                var visId = cc["ha-device-id"].Value.DecodeURL();
                                var uuid = Regex.Match(commPage.Html, @"analyticsdatalayer.publicuuid = ""(.+?)""").Groups[1].Value;
                                var jsontopost = $"{{\"type\":\"edap:pageview\",\"sessionId\":\"{sessId}\",\"visitorId\":\"{visId}\",\"site\":\"NA\",\"payload\":{{\"appname\":\"ums-cas\",\"appversion\":\"99.19\",\"appenvironment\":\"production\",\"pagetype\":\"ums\",\"pagename\":\"Post Login Edap\",\"proctor\":\"\",\"publicuuid\":\"{uuid}\",\"configbrand\":\"vrbo\",\"pageflow\":\"-1\",\"visitortype\":\"owner\",\"sensitive\":\"true\",\"edapeventname\":\"pageview\",\"pagehref\":\"https://www.vrbo.com/auth/vrbo/login?service=https%3A%2F%2Fwww.vrbo.com%2Fhaod%2Fauth%2Fsignin.html\",\"edapintegrationsversion\":\"10.5.0\",\"clienttimestamp\":\"1571843973883\",\"edapeventid\":\"32080400-2aa9-44da-b9b3-88079e317c27\",\"currentpageviewid\":\"66990edd-2032-430d-8e3f-ca5038770fdb\",\"parentpageviewid\":\"21ceb5bf-6c64-47be-8cfc-0096fd131662\",\"screenwidth\":\"1366\",\"screenheight\":\"768\",\"viewportwidth\":\"1366\",\"viewportheight\":\"657\",\"mqsize\":\"lg\",\"clienttype\":\"web\",\"referralchange\":\"false\",\"preferredlanguage\":\"en-US\"}}}}";
                                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                                commPage.JsonStringToPost = jsontopost;
                                commPage.RequestURL = "https://www.vrbo.com/edap/elo/v1/event/beacon";
                                commPage.Referer = commPage.Uri.AbsoluteUri;
                                commPage.RequestHeaders = new Dictionary<string, string>();
                                commPage.RequestHeaders.Add("Sec-Fetch-Mode", "same-origin");
                                commPage.RequestHeaders.Add("Sec-Fetch-Site", "same-origin");
                                commPage.RequestHeaders.Add("Accept", "*/*");
                                commPage.RequestHeaders.Add("Content-Type", "text/plain");
                                ProcessRequest(commPage);

                                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("edat form continue {0}------ URI {1} Status:{2}", commPage.Html, commPage.Uri,commPage.StatusCode.ToInt()));
                                if (commPage.ResponseHeaders != null)
                                {
                                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("response header form continue {0}------ URI {1}", string.Join(", ", commPage.ResponseHeaders.AllKeys), commPage.Uri));
                                }
                                var continueFormNode = xd.SelectSingleNode("//form[@id='continue-form']");
                                if (continueFormNode != null)
                                {
                                    var hdn = ScrapeHelper.GetAllHiddenFieldsAsParamWithID(xd, "continue-form");
                                    hdn.Add("_eventId", "submit");
                                    commPage.ReqType = CommunicationPage.RequestType.POST;
                                    commPage.RequestHeaders = new Dictionary<string, string>();
                                    commPage.RequestHeaders.Add("Sec-Fetch-Mode", "navigate");
                                    commPage.RequestHeaders.Add("Sec-Fetch-Site", "same-origin");
                                    commPage.RequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                                    commPage.RequestHeaders.Add("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
                                    commPage.RequestURL = prevUrl;
                                    commPage.Parameters = hdn;
                                    commPage.Referer = prevUrl;
                                    ProcessRequest(commPage);
                                    if (commPage.ResponseHeaders != null)
                                    {
                                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("response header form continue {0}------ URI {1}", string.Join(", ", commPage.ResponseHeaders.AllKeys), commPage.Uri));
                                    }
                                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Html After form continue {0}------ URI {1} Status: {2} hidden fields {3}", commPage.Html, commPage.Uri,commPage.StatusCode.ToInt(),hdn.Count));
                                }
                            }
                            else
                            {
                                //copied from JOe code, do we really need this?
                                VrboSecuritySettingCheckContinue(hdnFields, formAction);
                            }
                            if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/")
                                || commPage.Uri.AbsoluteUri.Contains("/pxe/feed/")
                                || commPage.Html.Contains("Two-Factor Authentication")
                                || commPage.Uri.AbsoluteUri.Contains("/haod/properties")))
                            {
                                break;
                            }
                            else
                            {
                                commPage.RequestURL = "https://vrbo.com/haod";
                                commPage.ReqType = CommunicationPage.RequestType.GET;
                                commPage.Referer = "https://www.vrbo.com" + formAction;
                                ProcessRequest(commPage);
                                VrboSecuritySettingCheckContinue(hdnFields, formAction);
                                if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/")
                                    || commPage.Uri.AbsoluteUri.Contains("/pxe/feed/")
                                   || commPage.Html.Contains("Two-Factor Authentication")
                                   || commPage.Uri.AbsoluteUri.Contains("/haod/properties")))
                                {
                                    break;
                                }
                            }
                        }

                    }

                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Html {0}------ URI {1}",commPage.Html,commPage.Uri));
                    if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("/pxe/feed/") || commPage.Uri.AbsoluteUri.Contains("/haod/properties")))
                    {
                        result.Cookie = SerializeCookiesToString(commPage.COOKIES);
                        result.IsNewLogIn = true;
                        result.Message = "Successfully logged in";
                        result.Success = true;
                        return result;
                    }
                    else if (commPage.IsValid && commPage.Html.Contains("Two-Factor Authentication"))
                    {
                        xd = commPage.ToXml();
                        var userName = "";
                        var userProfileImageUrl = "";
                        var phoneNumbers = new Dictionary<string, string>();
                        var radioNodes = xd.SelectNodes("//div[@id='sms-numbers']//div[@class='radio']");

                        foreach (XmlNode radio in radioNodes)
                        {
                            var val = radio.SelectSingleNode(".//input").GetAttributeFromNode("value");
                            var label = radio.GetInnerTextFromNode();
                            phoneNumbers.Add(val, label);
                        }

                        var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-form");
                        var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-form']").GetAttributeFromNode("action");

                        result.Airlock = new Models.Airlock()
                        {
                            HiddenFields = hdnFields,
                            FormAction = "https://www.vrbo.com" + authformAction,
                            PhoneNumbers = phoneNumbers,
                        };

                        result.Cookie = "";
                        result.IsNewLogIn = true;
                        result.Message = "Airlock";
                        result.Success = false;
                        
                        SaveAirlockCookies(username);
                        return result;
                    }
                    else if (commPage.IsValid && commPage.Html.Contains("your account has been locked"))
                    {
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("{0} Vrbo host account has been locked", username));
                        result.Cookie = "";
                        result.IsNewLogIn = true;
                        result.Message = "Account blocked";
                        result.Success = false;

                        return result;
                    }
                    else if (commPage.IsValid && commPage.Html.Contains("you entered is incorrect"))
                    {
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("{0} Vrbo host Incorrect Password", username));
                        result.Cookie = "";
                        result.IsNewLogIn = true;
                        result.Message = "Incorrect username or password";
                        result.Success = false;
                        return result;
                    }
                    else
                    {
                        AllSites.SaveHtmlText(username + " Login", commPage.Html);
                        AllSites.Rotate();
                        if (isRelogin == false)
                        {
                            isRelogin = true;
                            goto ReLogin;
                        }
                    }
                }
            }

            result.Cookie = "";
            result.IsNewLogIn = true;
            result.Message = "An error occured";
            result.Success = false;
            Database.Actions.Logs.Add(0, 0, string.Format("Add host {0}, Url", username, commPage.Uri.AbsoluteUri));
            AllSites.SaveHtmlText(username + " Login", commPage.Html);
            AllSites.Rotate();
            if (isRelogin == false)
            {
                isRelogin = true;
                goto ReLogin;
            }
            return result;
        }

        #endregion

        #region PROCESS AIRLOCK

        public Models.Airlock ProcessAirlockSelection(Models.Airlock airlockModel)
        {
            commPage = InitializeCommunication(airlockModel.Username);
            var sendType = "";
            switch ((AirlockChoice)airlockModel.SelectedChoice)
            {
                case AirlockChoice.SMS:
                    sendType = "SMS";
                    break;
                case AirlockChoice.Call:
                    sendType = "CALL";

                    break;

            }
            var arr = airlockModel.SelectedPhoneNumber.Split('-');
            var selPhone = arr[1].ToSafeString();
            var selCode = arr[0].ToSafeString();
            var jsonPost = $"{{\"userUuid\":null,\"jwtId\":null,\"siteName\":null,\"locale\":null,\"challengeTicket\":\"{airlockModel.ChallengeToken}\",\"sendType\":\"{sendType}\",\"phone\":{{\"phone\":\"{selPhone}\",\"countryCode\":\"{selCode}\"}}}}";

            commPage.RequestURL = "https://www.vrbo.com/auth/mfa/v1/2fa/challenge";
            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
            commPage.JsonStringToPost = jsonPost;
            commPage.PersistCookies = true;
            commPage.Referer = "";
            commPage.Parameters = new Dictionary<string, string>();
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("Accept", "*/*");
            commPage.RequestHeaders.Add("Content-Type", "application/json");
            ProcessRequest(commPage);

            if (commPage.IsValid && commPage.StatusCode == HttpStatusCode.OK)
            {
                SaveAirlockCookies(airlockModel.Username);
                return airlockModel;

            }
            SaveAirlockCookies(airlockModel.Username);
            return airlockModel;
            //Old Code
            //commPage = InitializeCommunication(airlockModel.Username);
            //var parameters = airlockModel.HiddenFields;

            //switch ((AirlockChoice)airlockModel.SelectedChoice)
            //{
            //    case AirlockChoice.SMS:
            //        parameters["sendType"] = "SMS";
            //        break;

            //    case AirlockChoice.Call:
            //        parameters["sendType"] = "CALL";
            //        break;

            //}
            //parameters["phoneId"] = airlockModel.SelectedPhoneNumber;
            //commPage.RequestURL = airlockModel.FormAction;
            //commPage.PersistCookies = true;
            //commPage.ReqType = CommunicationPage.RequestType.POST;
            //commPage.Parameters = parameters;

            //ProcessRequest(commPage);

            //if (commPage.IsValid && commPage.Html.Contains("A unique verification code has been sent"))
            //{
            //    var xd = commPage.ToXml();
            //    var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-verify-form");
            //    var authformAction = xd.SelectSingleNode("//form[@id='two-factor-auth-verify-form']").GetAttributeFromNode("action");

            //    airlockModel.HiddenFields = hdnFields;
            //    // airlockModel.FormAction = "https://cas.homeaway.com" + authformAction;
            //    airlockModel.FormAction = "https://www.vrbo.com" + authformAction;
            //    SaveAirlockCookies(airlockModel.Username);
            //    return airlockModel;
            //}
            //else
            //{

            //}
            //SaveAirlockCookies(airlockModel.Username);
            //return airlockModel;
        }

        public Models.AirlockResult ProcessAirlock(Models.Airlock airlockModel)
        {   Models.AirlockResult result = new Models.AirlockResult();
            result.Success = false;
            result.Cookie = "";
            commPage = InitializeCommunication(airlockModel.Username, "https://www.vrbo.com/auth/mfa/v1/2fa/verify");
            var jsonPost = $"{{\"userUuid\":null,\"jwtId\":null,\"tgtId\":null,\"challengeTicket\":\"{airlockModel.ChallengeToken}\",\"code\":\"{airlockModel.Code}\"}}";

            commPage.RequestURL = "https://www.vrbo.com/auth/mfa/v1/2fa/verify";
            commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
            commPage.JsonStringToPost = jsonPost;
            commPage.PersistCookies = true;
            commPage.Referer = "";
            commPage.Parameters = new Dictionary<string, string>();
            commPage.RequestHeaders = new Dictionary<string, string>();
            commPage.RequestHeaders.Add("Accept", "*/*");
            commPage.RequestHeaders.Add("Content-Type", "application/json");
            ProcessRequest(commPage);
            if (commPage.IsValid && commPage.Html.Contains("COMPLETE"))
            {
                commPage.RequestHeaders = new Dictionary<string, string>();

                commPage.RequestURL = "https://www.vrbo.com/auth/vrbo/login?service=https%3A%2F%2Fwww.vrbo.com%2Fhaod%2Fauth%2Fsignin.html%3Ftarget%3Dhttp%253A%252F%252Fwww.vrbo.com%252Fhaod&authui=false";
                commPage.ReqType = CommunicationPage.RequestType.GET;
                ProcessRequest(commPage);




                if (commPage.IsValid && commPage.Html.Contains("Please wait while we check your security settings"))
                {
                    var xd = commPage.ToXml();
                    var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
                    var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

                    commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "";
                    commPage.Parameters = hdnFields;
                    ProcessRequest(commPage);
                    //copied from JOe code, do we really need this?
                    VrboSecuritySettingCheckContinue(hdnFields, airlockModel.FormAction);

                }

                if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties") || commPage.Uri.AbsoluteUri.Contains("/pob/") || commPage.Uri.AbsoluteUri.Contains("/pxe/feed")))
                {
                    result.Success = true;
                    result.Cookie = SerializeCookiesToString(commPage.COOKIES);
                    return result;

                }
                var redirectcount = 1;
                if (!commPage.IsLoggedIn)
                {
                REDIRECT:
                    var xd = commPage.ToXml();
                    var prms = ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "continue-form");
                    prms["_eventId"] = "submit";
                    commPage.RequestURL = airlockModel.FormAction;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.Parameters = prms;
                    commPage.Referer = airlockModel.FormAction;
                    ProcessRequest(commPage);
                    if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties") || commPage.Uri.AbsoluteUri.Contains("/pob/")))
                    {
                        commPage.IsLoggedIn = true;
                    }
                    else if (commPage.Uri.AbsoluteUri.Contains("/haod/auth/signin.html"))
                    {
                        commPage.RequestURL = "http://admin.vrbo.com/haod";
                        commPage.ReqType = CommunicationPage.RequestType.GET;

                        ProcessRequest(commPage);

                        if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties") || commPage.Uri.AbsoluteUri.Contains("/pob/") || commPage.Uri.AbsoluteUri.Contains("/pxe/feed")))
                        {
                            commPage.IsLoggedIn = true;
                        }
                    }
                    else
                    {
                        if (redirectcount < 2)
                        {
                            redirectcount++;
                            goto REDIRECT;
                        }
                    }
                }
                if (commPage.IsLoggedIn)
                {

                    SaveAirlockCookies(airlockModel.Username);
                    result.Success = true;
                    result.Cookie = SerializeCookiesToString(commPage.COOKIES);
                    return result;
                }

                SaveAirlockCookies(airlockModel.Username);
            }
            return result;
            //commPage = InitializeCommunication(airlockModel.Username);
            //Models.AirlockResult result = new Models.AirlockResult();
            //result.Success = false;
            //result.Cookie = "";
            //var redirectcount = 1;
            //var parameters = airlockModel.HiddenFields;
            //parameters["rememberMe"] = "true";
            //parameters["_rememberMe"] = "on";
            //parameters["code"] = airlockModel.Code;
            //commPage.RequestURL = airlockModel.FormAction;
            //commPage.PersistCookies = true;
            //commPage.ReqType = CommunicationPage.RequestType.POST;
            //commPage.Parameters = parameters;
            //commPage.RequestHeaders = new Dictionary<string, string>();
            ////commPage.RequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
            //commPage.Referer = airlockModel.FormAction;

            //ProcessRequest(commPage);
            //REDIRECT:
            //commPage.RequestURL = "https://www.vrbo.com/haod";
            //commPage.ReqType = CommunicationPage.RequestType.GET;
            //commPage.Referer = airlockModel.FormAction;
            //ProcessRequest(commPage);
            //if (commPage.IsValid && commPage.Html.Contains("Please wait while we check your security settings"))
            //{
            //    //var xd = commPage.ToXml();
            //    //var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
            //    //var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

            //    //commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
            //    //commPage.ReqType = CommunicationPage.RequestType.POST;
            //    //commPage.PersistCookies = true;
            //    //commPage.Referer = "";
            //    //commPage.Parameters = hdnFields;

            //    //ProcessRequest(commPage);
            //    //VrboSecuritySettingCheckContinue(hdnFields, autoformAction);
            //    var xd = commPage.ToXml();
            //    var hdnFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "aa-interstitial-form");
            //    var autoformAction = xd.SelectSingleNode("//form[@id='aa-interstitial-form']").GetAttributeFromNode("action");

            //    for (int x = 0; x < 3; x++)
            //    {
            //        commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
            //        commPage.ReqType = CommunicationPage.RequestType.POST;
            //        commPage.PersistCookies = true;
            //        commPage.Referer = "";
            //        commPage.Parameters = hdnFields;
            //        ProcessRequest(commPage);

            //        VrboSecuritySettingCheckContinue(hdnFields, autoformAction);

            //        if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/")
            //            || commPage.Uri.AbsoluteUri.Contains("/pxe/feed/")
            //            || commPage.Uri.AbsoluteUri.Contains("/haod/properties")))
            //        {
            //            break;
            //        }
            //        else
            //        {
            //            commPage.RequestURL = "https://vrbo.com/haod";
            //            commPage.ReqType = CommunicationPage.RequestType.GET;
            //            commPage.Referer = "https://www.vrbo.com" + autoformAction;
            //            ProcessRequest(commPage);
            //            if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/")
            //                || commPage.Uri.AbsoluteUri.Contains("/pxe/feed/")
            //               || commPage.Uri.AbsoluteUri.Contains("/haod/properties")))
            //            {
            //                break;
            //            }
            //        }
            //    }
            //}
            //if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties") || commPage.Uri.AbsoluteUri.Contains("/pob/") || commPage.Uri.AbsoluteUri.Contains("/pxe/feed")))
            //{
            //    result.Success = true;
            //    result.Cookie = SerializeCookiesToString(commPage.COOKIES);
            //    return result;
            //}


            //if (!commPage.IsLoggedIn)
            //{

            //    var xd = commPage.ToXml();
            //    Dictionary<string, string> prms = new Dictionary<string, string>();

            //    try
            //    {
            //        prms = ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "continue-form");
            //    }
            //    catch (Exception e)
            //    {
            //        prms = ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "id", "two-factor-auth-verify-form");
            //    }

            //    prms["_eventId"] = "submit";
            //    commPage.RequestURL = airlockModel.FormAction;
            //    commPage.PersistCookies = true;
            //    commPage.ReqType = CommunicationPage.RequestType.POST;
            //    commPage.Parameters = prms;
            //    commPage.Referer = airlockModel.FormAction;

            //    ProcessRequest(commPage);

            //    if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties") || commPage.Uri.AbsoluteUri.Contains("/pob/")))
            //    {
            //        commPage.IsLoggedIn = true;
            //    }
            //    else if (commPage.Uri.AbsoluteUri.Contains("/haod/auth/signin.html"))
            //    {
            //        commPage.RequestURL = "http://admin.vrbo.com/haod";
            //        commPage.ReqType = CommunicationPage.RequestType.GET;

            //        ProcessRequest(commPage);

            //        if (commPage.IsValid && (commPage.Uri.AbsoluteUri.Contains("/dash/") || commPage.Uri.AbsoluteUri.Contains("properties") || commPage.Uri.AbsoluteUri.Contains("/pob/") || commPage.Uri.AbsoluteUri.Contains("/pxe/feed")))
            //        {
            //            commPage.IsLoggedIn = true;
            //        }
            //    }
            //    else
            //    {
            //        if (redirectcount < 2)
            //        {
            //            redirectcount++;
            //            goto REDIRECT;
            //        }
            //    }
            //}

            //if (commPage.IsLoggedIn)
            //{
            //    result.Success = true;
            //    result.Cookie = SerializeCookiesToString(commPage.COOKIES);
            //}

            //return result;
        }
        #endregion
        public CommunicationPage InitializeCommunication(string username, string url = "")
        {
            var newcommPage = new CommunicationPage("https://www.admin.vrbo.com/");
            if (!string.IsNullOrEmpty(url))
                newcommPage = new CommunicationPage(url);
            try
            {
                var session = Database.Actions.AirlockSessions.GetAirlockSession(username, 2);
                if (session != null)
                {
                    var cookies = DeSerializeCookiesFromString(session.Cookies);
                    newcommPage.COOKIES = cookies;
                }
            }
            catch (Exception e)
            {
            }
            return newcommPage;
        }

        #region REVIEWS

        #endregion

        #region COOKIES
        public bool ValidateTokenAndLoadCookies(string token, CommunicationPage currentcommPage = null)
        {
            var hasValidCookies = LoadCookies(token, currentcommPage);
            if (hasValidCookies)
            {

                string url = "https://www.vrbo.com/traveler/profile/edit";
                try
                {
                    if (currentcommPage == null)
                    {
                        commPage.RequestURL = url;
                        commPage.PersistCookies = true;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.ReqType = CommunicationPage.RequestType.GET;


                        ProcessRequest(commPage);
                        if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                        {

                            return true;

                        }
                        else
                        {
                            using (var db = new ApplicationDbContext())
                            {
                                var entity = db.HostSessionCookies.Where(x => x.Token == token).FirstOrDefault();
                                if (entity != null)
                                {
                                    entity.IsActive = false;
                                    db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }
                            }
                        }

                    }
                    else
                    {
                        currentcommPage.RequestURL = url;
                        currentcommPage.PersistCookies = true;
                        currentcommPage.RequestHeaders = new Dictionary<string, string>();
                        currentcommPage.ReqType = CommunicationPage.RequestType.GET;


                        ProcessRequest(currentcommPage);
                        if (currentcommPage.IsValid && currentcommPage.Uri.AbsoluteUri.Contains("/profile/edit"))
                        {

                            return true;

                        }
                    }
                }
                catch (Exception e)
                {
                }
            }
          
            return false;
        }
        public bool LoadCookies(string token, CommunicationPage currentcommPage = null)
        {
            var result = false;
            //int hostId = Core.API.Vrbo.HostSessionCookies.GetHostId(token);
            var session = Database.Actions.HostSessionCookies.GetSessionByToken(token);
            if (session != null)
            {
                try
                {
                    var cookies = DeSerializeCookiesFromString(session.Cookies); //= JsonConvert.DeserializeObject<List<Cookie>>(session.Cookies);
                    //CookieCollection cc = new CookieCollection();
                    //foreach (var cok in cookies)
                    //{
                    //    cc.Add(cok);
                    //}
                    //CookieContainer cont = new CookieContainer();

                    //cont.Add(cc);
                    commPage.COOKIES = cookies;
                    if (currentcommPage != null)
                        currentcommPage.COOKIES = cookies;
                    result = true;
                }
                catch (Exception e)
                {
                }
            }
            else
            {
                commPage.COOKIES = null;
            }
            return result;
        }
        private bool HasValidCookie(string cookie)
        {
            try
            {
                commPage.RequestURL = "https://vrbo.com/haod";
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                commPage.COOKIES = DeSerializeCookiesFromString(cookie);

                ProcessRequest(commPage);

                if (commPage.Uri.AbsoluteUri.Contains("/dash/") || 
                    commPage.Uri.AbsoluteUri.Contains("/pxe/feed/") || 
                    commPage.Uri.AbsoluteUri.Contains("/haod/properties"))
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        private static IEnumerable<Cookie> GetAllCookies(CookieContainer c)
        {
            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
            foreach (DictionaryEntry element in k)
            {
                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
                foreach (var e in l)
                {
                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
                    foreach (Cookie fc in cl)
                    {
                        yield return fc;
                    }
                }
            }
        }


        private string SerializeCookiesToString(CookieContainer cookieContainer)
        {
            var cookieList = GetAllCookies(cookieContainer).ToList();

            return JsonConvert.SerializeObject(cookieList);
            
        }

        private CookieContainer DeSerializeCookiesFromString(string cookie)
        {
            try
            {
                var cookies = JsonConvert.DeserializeObject<List<Cookie>>(cookie);
                CookieCollection cc = new CookieCollection();
                foreach (var cok in cookies)
                {
                    cc.Add(cok);
                }
                CookieContainer cont = new CookieContainer();

                cont.Add(cc);
                return cont;
            }
            catch (Exception e)
            {
            }
            return new CookieContainer();

        }
        public void SaveAirlockCookies(string username)
        {
            var serializedCookies = SerializeCookiesToString(commPage.COOKIES);
            Database.Actions.AirlockSessions.AddOrUpdate(new AirlockSession { Cookies = serializedCookies, Username = username, SiteType = 2 });
        }
        private void VrboSecuritySettingCheckContinue(Dictionary<string, string> hdnFields, string autoformAction)
        {
            commPage.RequestURL = "https://www.vrbo.com" + autoformAction;
            commPage.ReqType = CommunicationPage.RequestType.POST;
            commPage.PersistCookies = true;
            commPage.Referer = "";
            commPage.Parameters = hdnFields;
            ProcessRequest(commPage);
        }

        #endregion

        #region DISPOSE
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
        #endregion
    }
}
