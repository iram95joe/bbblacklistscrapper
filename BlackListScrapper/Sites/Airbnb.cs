﻿using BaseScrapper;
using BlackListScrapper.Helpers;
using BlackListScrapper.Database.Entity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using BlackListScrapper.SignalR;
using BlackListScrapper.Database;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using BlackListScrapper.Models;
using System.Data.Entity;
using BlackListScrapper.Job;

namespace BlackListScrapper.Sites
{
    public class Airbnb : CommunicationEngine,IDisposable
    {
        private CommunicationPage commPage = null;
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        #region CONSTRUCTOR
        public Airbnb()
        {
            commPage = new CommunicationPage("https://airbnb.com");
        }
        #endregion

        #region HOST
        public Database.Entity.Host ScrapeHostAccount(string username,string password,int companyId,string cookie)
        {
            bool isLoggedIn = HasValidCookie(cookie);

            if (isLoggedIn)
            {
                string url = "https://www.airbnb.com/account-settings/personal-info";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;

                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {

                            var siteId = xd.SelectSingleNode("//input[@name='user_id']").GetAttributeFromNode("value");
                            if (string.IsNullOrEmpty(siteId))
                            {
                                var formAction = xd.SelectSingleNode("//form[@id='update_form']").GetAttributeFromNode("action");
                                if (!string.IsNullOrEmpty(formAction))
                                    siteId = formAction.Replace("/update/", "");
                            }
                            if(siteId != "") { 
                            var firstName = xd.SelectSingleNode("//input[@id='user_first_name']").GetAttributeFromNode("value");
                            var lastName = xd.SelectSingleNode("//input[@id='user_last_name']").GetAttributeFromNode("value");
                            var imgUrl = "";
                            var mediaUrl = xd.SelectSingleNode("//a[contains(.,'Photos')]").GetAttributeFromNode("href");
                            var gender = "";
                            var birthdate = "";
                            var language = "";
                            var currency = "";
                            var about = "";
                            var school = "";
                            var work = "";
                            var timezone = "";
                            try {gender = xd.SelectSingleNode("//option[contains(@selected,'selected')][ancestor::select[@id='user_sex']]").NextSibling.Value.Replace("\n", "");} catch { }
                            try {birthdate = xd.SelectSingleNode("//p[@class='govt-id-birthdate']").InnerText;}catch { }
                            try { language = xd.SelectSingleNode("//option[contains(@selected,'selected')][ancestor::select[@id='user_profile_info_preferred_language']]").NextSibling.Value.Replace("\n", ""); }catch { }
                            try {currency = xd.SelectSingleNode("//option[contains(@selected,'selected')][ancestor::select[@id='user_profile_info_preferred_currency']]").NextSibling.Value.Replace("\n", "");}catch { }
                            try {about = xd.SelectSingleNode("//textarea[@id='user_profile_info_about']").InnerText;} catch { }
                            try {school = xd.SelectSingleNode("//input[@id='user_profile_info_university']").GetAttributeFromNode("value");}catch { }
                            try {work = xd.SelectSingleNode("//input[@id='user_profile_info_employer']").GetAttributeFromNode("value");}catch { }
                            try {timezone = xd.SelectSingleNode("//option[contains(@selected,'selected')][ancestor::select[@id='user_preference_time_zone']]").NextSibling.Value.Replace("\n", ""); }catch { }
                            commPage.RequestURL = mediaUrl;

                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                xd = commPage.ToXml();
                                if (xd != null)
                                {
                                    imgUrl = xd.SelectSingleNode("//div[@class='media-photo media-round']/img").GetAttributeFromNode("src");
                                }
                            }
                                if (siteId != "" && firstName != "" && lastName != "")
                                {
                                    Database.Entity.Host host = new Database.Entity.Host
                                    {
                                        SiteHostId = siteId,
                                        Firstname = firstName,
                                        Lastname = lastName,
                                        ProfilePictureUrl = imgUrl,
                                        Username = username,
                                        SiteType = 1,
                                        Password = Security.Encrypt(password),
                                        Gender = gender,
                                        Birthdate = birthdate,
                                        Language = language,
                                        Currency = currency,
                                        About = about,
                                        School = school,
                                        Work = work,
                                        TimeZone = timezone

                                    };
                                    var h = Database.Actions.Hosts.AddOrUpdate(host);
                                    Database.Actions.HostCompany.AddOrUpdate(new HostCompany() { HostId = h.Id, CompanyId = companyId });
                                    return h;
                                }
                            }
                            else
                            {
                                var json = xd.SelectSingleNode("//script[@id='data-state']").GetInnerTextFromNode().HtmlDecode().Trim();
                                if (!string.IsNullOrEmpty(json))
                                {
                                    json = json.Replace("//\r\n", "");
                                    json = json.Replace("<!--", "");
                                    json = json.Replace("-->", "");
                                    var ob = JObject.Parse(json);
                                    var hostData = ob["bootstrapData"]["reduxData"]["personalInfo"]["user"];
                                    Database.Entity.Host host = new Database.Entity.Host
                                    {
                                        SiteHostId = hostData["id"].ToSafeString(),
                                        Firstname = hostData["first_name"].ToSafeString(),
                                        Lastname = hostData["last_name"].ToSafeString(),
                                        ProfilePictureUrl = null,
                                        Username = username,
                                        SiteType = 1,
                                        Password = Security.Encrypt(password),
                                        Gender = hostData["gender"].ToSafeString(),
                                        Birthdate = hostData["birthdate"].ToSafeString(),
                                        Language = null,
                                        Currency = null,
                                        About = null,
                                        School = null,
                                        Work = null,
                                        TimeZone = null

                                    };
                                    var h = Database.Actions.Hosts.AddOrUpdate(host);
                                    Database.Actions.HostCompany.AddOrUpdate(new HostCompany() { HostId = h.Id, CompanyId = companyId });
                                    return h;
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                }
            }

            return null;
        }

        #endregion

        #region LOGIN

        public Models.LogInResult LogIn(string username, string password,string userId)
        {
            bool isRelogin = false;
            var retryCount = 0;
            ReLogin:
            //ResponseCheck:
            Models.LogInResult result = new Models.LogInResult();
            string url = "https://www.airbnb.com/login";
            
            
            url = "https://www.airbnb.com/login";
            commPage = new CommunicationPage(url);
            commPage.PersistCookies = true;
            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                if (xd != null)
                {
                    var metadata = xd.SelectSingleNode("//input[@name='metadata1']").GetAttributeFromNode("value");
                    var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "class", "login-form ");
                    hiddenFields.Add("email", username.Trim());
                    hiddenFields.Add("password", password.Trim());
                    hiddenFields.Add("page_controller_action_pair", "");
                    hiddenFields.Add("origin_url", "https://www.airbnb.com/");
                    //hiddenFields["utf8"] = "✓";
                    commPage.RequestURL = "https://www.airbnb.com/authenticate";
                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "https://www.airbnb.com/";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);
                    retry:
                    if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("dashboard") || commPage.Uri.AbsoluteUri.Contains("hosting") || commPage.Uri.AbsoluteUri == "https://www.airbnb.com/")
                    {
                        result.Cookie = SerializeCookiesToString(commPage.COOKIES);
                        result.IsNewLogIn = true;
                        result.Message = "Successfully logged in";
                        result.Success = true;

                        return result;
                    }
                    else if ((int)commPage.StatusCode == 420)
                    {
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("{0} host has captcha", username));
                        SignalR.ShowNewReservationReview.NotifyUserLogin(userId, Enumerations.NotificationType.INFORMATION, "Processing with captcha, Please wait!");
                       
                        var ob = JObject.Parse(commPage.Html);
                        var captcha_site_key = ob["client_error_info"]["airlock"]["friction_data"][0]["data"]["site_key"].ToSafeString();
                        var lockId = ob["client_error_info"]["airlock"]["id"].ToSafeString();

                        var captchaResponse = Captcha.SolveCaptcha(GlobalVariables.CaptchaAPIKey, captcha_site_key, commPage.Uri.AbsoluteUri);
                        if (captchaResponse.Item1 == true)
                        {
                            ProcessCaptchaLock(new CaptchaViewModel { CaptchaResponse = captchaResponse.Item2, LockId = lockId, SiteKey = captcha_site_key, Email = username, Password = password });
                            if (retryCount < 2)
                            {
                                SignalR.ShowNewReservationReview.NotifyUserLogin(userId, Enumerations.NotificationType.INFORMATION, "Process captcha completed!");
                                retryCount++;
                                goto retry;
                            }
                        }

                        if (retryCount < 3)
                        {
                            retryCount++;
                            AllSites.Rotate();
                            isRelogin = true;
                            goto ReLogin;
                        }
                        result.Cookie = "";
                        result.IsNewLogIn = true;
                        result.Message = "Captcha lock";
                        result.Success = false;
                        return result;
                    }
                    else if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("airlock"))
                    {
                        try
                        {
                            xd = commPage.ToXml(true);
                            var userName = "";
                            var userProfileImageUrl = "";
                            var imgNode = xd.SelectSingleNode("//img[contains(@src,'profile_pic')]");
                            if (imgNode != null)
                            {
                                userName = imgNode.GetAttributeFromNode("alt");
                                userProfileImageUrl = imgNode.GetAttributeFromNode("src");
                            }

                            var json = xd.SelectSingleNode("//script[@data-hypernova-key='user_challengesbundlejs']").GetInnerTextFromNode().HtmlDecode().Trim();
                            if (!string.IsNullOrEmpty(json))
                            {
                                json = json.Replace("//\r\n", "");
                                json = json.Replace("<!--", "");
                                json = json.Replace("-->", "");
                                var ob = JObject.Parse(json);

                                var lockId = ob["bootstrapData"]["id"].ToSafeString();
                                var currentUserId = ob["bootstrapData"]["user_id"].ToSafeString();
                                var list = ob["bootstrapData"]["friction_data"].ToList();

                                var captcha = list.Where(f => f["name"].ToSafeString() == "captcha").FirstOrDefault();
                                if (captcha != null)
                                {
                                    var siteKey = captcha["data"]["site_key"].ToString();
                                    var captchaResponse = Captcha.SolveCaptcha(GlobalVariables.CaptchaAPIKey, siteKey, commPage.Uri.AbsoluteUri);
                                    if (captchaResponse.Item1 == true)
                                    {
                                        ProcessCaptchaLock(new CaptchaViewModel { CaptchaResponse = captchaResponse.Item2, LockId = lockId, SiteKey = siteKey, Email = username, Password = password });
                                        if (retryCount < 2)
                                        {
                                            SignalR.ShowNewReservationReview.NotifyUserLogin(userId, Enumerations.NotificationType.INFORMATION, "Process captcha completed!");
                                            retryCount++;
                                            goto retry;
                                        }
                                    }
                                }
                                var required = list.Where(f => f["name"].ToSafeString() == "phone_verification_via_text").FirstOrDefault();
                                if (required == null)
                                {
                                    required = list.Where(f => f["name"].ToSafeString() == "phone_verification_via_call").FirstOrDefault();
                                }
                                var Email = "";
                                var phonePath = required["data"]["phone_numbers"];
                                var phoneNum = "";
                                var phoneNumId = "";
                                foreach (var path in phonePath)
                                {
                                    phoneNumId += path["id"].ToSafeString() + ",";
                                    phoneNum += path["obfuscated"].ToSafeString() + ",";
                                }
                                //var phoneNumId = required["data"]["phone_numbers"][0]["id"].ToSafeString();
                                //var phoneNum = required["data"]["phone_numbers"][0]["obfuscated"].ToSafeString();
                                var emailNode = list.Where(f => f["name"].ToSafeString() == "email_code_verification").FirstOrDefault();
                                if (emailNode != null)
                                    Email = emailNode["data"]["obfuscated_email_address"].ToSafeString();
                                result.Success = false;

                                result.Airlock = new Models.Airlock()
                                {
                                    LockId = lockId,
                                    PhoneId = phoneNumId,
                                    UserId = currentUserId,
                                    Email = Email,
                                    PhoneNumber = phoneNum,
                                    Name = userName,
                                    ProfileImageUrl = userProfileImageUrl,
                                    IsNew = true,
                                    Username =username
                                };
                            }
                            else
                            {
                                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-airlock_data']");
                                var rawJson = meta.GetAttributeFromNode("content").ToSafeString();
                                json = meta.GetAttributeFromNode("content").HtmlDecode();
                                var ob = JObject.Parse(json);
                                var lockId = ob["id"].ToSafeString();
                                var currentUserId = ob["user_id"].ToSafeString();
                                var phoneNumId = ob["friction_init_data"]["phone_verification_via_text"]["phone_numbers"][0]["id"].ToSafeString();
                                var phoneNum = ob["friction_init_data"]["phone_verification_via_text"]["phone_numbers"][0]["obfuscated"].ToSafeString();
                                var Email = ob["friction_init_data"]["email_code_verification"]["obfuscated_email_address"].ToSafeString();
                                result.Success = false;

                                result.Airlock = new Models.Airlock()
                                {
                                    LockId = lockId,
                                    PhoneId = phoneNumId,
                                    UserId = currentUserId,
                                    Email = Email,
                                    PhoneNumber = phoneNum,
                                    Name = userName,
                                    ProfileImageUrl = userProfileImageUrl,
                                    IsNew = false
                                };
                            }
                            
                            SaveAirlockCookies(username);
                            return result;
                        }
                        catch (Exception e)
                        {

                        }

                        result.Cookie = "";
                        result.IsNewLogIn = true;
                        result.Message = "Airlock";
                        result.Success = false;

                        return result;
                    }
                    else
                    {
                        try
                        {
                            //var ob = JObject.Parse(commPage.Html);
                            //var status = ob["status"].ToSafeString();
                            //var messages = ob["message"].ToSafeString();
                            if (commPage.Html.Contains("The password you entered is incorrect. Try again, or choose another login option."))
                            {
                                isRelogin = true;
                                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("{0} Airbnb host Incorrect Password", username));
                                result.Cookie = "";
                                result.IsNewLogIn = true;
                                result.Message = "Incorrect username or password";
                                result.Success = false;
                                return result;
                            }
                            else
                            {
                                Database.Actions.Logs.Add(0, 0, string.Format("Add Airbnb host {0}, Url {1}", username, commPage.Uri.AbsoluteUri));
                                AllSites.SaveHtmlText(username + " Login", commPage.Html);
                                AllSites.Rotate();
                                //if (isRelogin == false)
                                //{
                                //    isRelogin = true;
                                //    goto ReLogin;
                                //}
                                result.Cookie = "";
                                result.IsNewLogIn = true;
                                result.Message = "Account blocked";
                                result.Success = false;

                                return result;
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }
                }
            }

            result.Cookie = "";
            result.IsNewLogIn = true;
            result.Message = "An error occured";
            result.Success = false;
            Database.Actions.Logs.Add(0, 0, string.Format("Add Airbnb host {0}, Url {1}", username,commPage.Uri.AbsoluteUri));
            AllSites.SaveHtmlText(username + " Login", commPage.Html);
            if (isRelogin == false)
            {
                isRelogin = true;
                AllSites.Rotate();
                goto ReLogin;
            }
            return result;
        }

        #endregion

        #region ProcessCaptchaLock
        public bool ProcessCaptchaLock(CaptchaViewModel captchaModel)
        {

            try
            {
                string airlockUrl = commPage.Uri.AbsoluteUri;
                var u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&_format=v1", captchaModel.LockId);
                var pJson = "";

                pJson = string.Format("{{\"friction\":\"captcha\",\"friction_data\":{{\"response\":{{\"captcha_response\":\"{0}\"}}}},\"enable_throw_errors\":true}}", captchaModel.CaptchaResponse);
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();
                commPage.Referer = "https://www.airbnb.com/airlock?al_id=" + captchaModel.LockId;
                commPage.RequestURL = u;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                commPage.JsonStringToPost = pJson;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json");
                //CommPage.RequestHeaders.Add("Content-Type", "application/json");
                commPage.RequestHeaders.Add("X-CSRF-Token", token);

                //commPage.Referer = airlockUrl;
                ProcessRequest(commPage);

                if (commPage.StatusCode == HttpStatusCode.OK)
                {
                    //If json status = 2 then the captcha is supposed to be true
                    commPage.RequestURL = "https://www.airbnb.com/airlock/redirect?url=https://www.airbnb.com/login&flash=true";
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    ProcessRequest(commPage);


                    var hiddenFields = new Dictionary<string, string>(); //BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "class", "login-form ");

                    commPage.RequestURL = "https://www.airbnb.com/authenticate";

                    hiddenFields = new Dictionary<string, string>();
                    hiddenFields.Add("email", captchaModel.Email);
                    hiddenFields.Add("password", captchaModel.Password);
                    hiddenFields["from"] = "email_login";
                    hiddenFields["airlock_id"] = "";
                    //hiddenFields["utf8"] = "✓";

                    commPage.RequestHeaders = new Dictionary<string, string>();
                    if (cc["_csrf_token"] != null)
                        token = cc["_csrf_token"].Value.DecodeURL();
                    commPage.RequestHeaders.Add("X-CSRF-Token", token);
                    commPage.RequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*;q=0.8");

                    commPage.ReqType = CommunicationPage.RequestType.POST;
                    commPage.PersistCookies = true;
                    commPage.Referer = "https://www.airbnb.com/";
                    commPage.Parameters = hiddenFields;
                    ProcessRequest(commPage);
                    return true;

                }
            }
            catch (Exception ex)
            {
            }
            return false;

        }
        #endregion

        #region PROCESS AIRLOCK

        public Models.AirlockResult ProcessAirlockSelection(string lockId, string userId, string phoneId, int selectedChoice, bool isNew,string username)
        {
            Models.AirlockResult result = new Models.AirlockResult();
            result.Success = false;

            try
            {
                var u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&_format=v1", lockId);
                commPage = InitializeCommunication(username, u);
                var pJson = "";

                if (isNew)
                {
                    switch (selectedChoice)
                    {

                        case 1:
                            //pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"attempt\":true,\"enable_throw_errors\":true,\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":\"{0}\"}}}}}}", phoneId);
                            pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"attempt\":true,\"enable_throw_errors\":true,\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}}}}}}", phoneId);
                            break;
                        case 2:
                            pJson = string.Format("{{\"friction\":\"phone_verification_via_call\",\"attempt\":true,\"enable_throw_errors\":true,\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}}}}}}", phoneId);

                            break;
                        case 3:
                            pJson = "{\"friction\":\"email_code_verification\",\"attempt\":true,\"enable_throw_errors\":true}";

                            break;
                        default:
                            pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":\"{0}\"}}}},\"attempt\":true,\"enable_throw_errors\":true}}", phoneId);

                            break;
                    }
                }
                else
                {
                    switch (selectedChoice)
                    {
                        case 1:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{2}}}}}}}", lockId, userId, phoneId);
                            break;

                        case 2:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{2}}}}}}}", lockId, userId, phoneId);
                            break;

                        case 3:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"email_code_verification\",\"friction_data\":{{\"optionSelection\":{{}}}}}}", lockId, userId);
                            pJson = pJson.Replace("^^", "");
                            break;

                        default:
                            pJson = string.Format("{{\"user_id\":{1},\"action_name\":\"account_login\",\"id\":{0},\"attempt\":true,\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{2}}}}}}}", lockId, userId, phoneId);
                            break;
                    }
                }

                var token = "";
                int retryCount = 0;

                Retry:
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                if (cc["_csrf_token"] != null)
                    token = cc["_csrf_token"].Value.DecodeURL();

                commPage.RequestURL = u;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                commPage.JsonStringToPost = pJson;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                //CommPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                commPage.RequestHeaders.Add("X-CSRF-Token", token);
                commPage.Referer = "https://www.airbnb.com/";// airlockUrl;

                ProcessRequest(commPage);

                if (string.IsNullOrEmpty(token))
                {
                    if (retryCount <= 5)
                    {
                        retryCount++;
                        goto Retry;
                    }
                    else
                    {
                        return result;
                    }
                }

                if (commPage.IsValid)
                {
                    result.Success = true;
                    SaveAirlockCookies(username);
                    return result;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public bool AutoLoginAfterAirlock(string email, string password,string userId)
        {
            try
            {
                var retryCount = 0;
                ResponseCheck:
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestURL = "https://www.airbnb.com/login";//"https://www.airbnb.com/airlock/redirect?url=https://www.airbnb.com/login&flash=true";
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.Referer = "";
                commPage.PersistCookies = true;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();

                    if (xd != null)
                    {
                        var metadata = xd.SelectSingleNode("//input[@name='metadata1']").GetAttributeFromNode("value");
                        var hiddenFields = BaseScrapper.ScrapeHelper.GetAllHiddenFieldsAsParamFromForm(xd, "class", "login-form ");
                        hiddenFields.Add("email", email.Trim());
                        hiddenFields.Add("password", password.Trim());
                        hiddenFields["utf8"] = "✓";

                        commPage.RequestURL = "https://www.airbnb.com/authenticate";
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.PersistCookies = true;
                        commPage.Referer = "https://www.airbnb.com/login";
                        commPage.Parameters = hiddenFields;
                        
                        ProcessRequest(commPage);

                        if (commPage.IsValid && commPage.Uri.AbsoluteUri.Contains("dashboard") || commPage.Uri.AbsoluteUri.Contains("hosting") || commPage.Uri.AbsoluteUri =="https://www.airbnb.com/")
                        {
                            return true;
                        }
                        else if ((int)commPage.StatusCode == 420)
                        {
                            SignalR.ShowNewReservationReview.NotifyUserLogin(userId, Enumerations.NotificationType.INFORMATION, "Processing with captcha, Please wait!");
                            Database.Actions.Logs.Add(0, 0, string.Format("{0} host has captcha after airlock",email));

                            var ob = JObject.Parse(commPage.Html);
                            var captcha_site_key = ob["client_error_info"]["airlock"]["friction_data"][0]["data"]["site_key"].ToSafeString();
                            var lockId = ob["client_error_info"]["airlock"]["id"].ToSafeString();

                            var captchaResponse = Captcha.SolveCaptcha(GlobalVariables.CaptchaAPIKey, captcha_site_key, commPage.Uri.AbsoluteUri);
                            if (captchaResponse.Item1 == true)
                            {
                                ProcessCaptchaLock(new CaptchaViewModel { CaptchaResponse = captchaResponse.Item2, LockId = lockId, SiteKey = captcha_site_key, Email = email, Password = password });
                                if (retryCount < 3)
                                {
                                    retryCount++;
                                    SignalR.ShowNewReservationReview.NotifyUserLogin(userId, Enumerations.NotificationType.INFORMATION, "Process captcha completed!");
                                    goto ResponseCheck;
                                }
                            }
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {

            }

            return false;
        }

        public Models.AirlockResult ProcessAirlock(string code, string lockId, string userId, string phoneId, int selectedChoice, bool isNew,string username,string password,string appUserId)
        {
            Models.AirlockResult airlockResult = new Models.AirlockResult();
            airlockResult.Success = false;
            airlockResult.Cookie = "";
            airlockResult.Message = "";
            try
            {
                var u = "";

                if (isNew)
                    u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&_format=v1", lockId);
                else
                    u = string.Format("https://www.airbnb.com/api/v2/airlocks/{0}?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", lockId);

                commPage = InitializeCommunication(username, u);
                var pJson = "";
                if (isNew == false)
                {

                    pJson = string.Format("{{\"user_id\":{2},\"action_name\":\"account_login\",\"id\":{0},\"friction\":\"phone_verification\",\"friction_data\":{{\"optionSelection\":{{\"delivery_method\":1,\"phone_number_id\":{3}}},\"response\":{{\"code\":\"{1}\"}}}}}}", lockId, code, userId, phoneId);
                    if (selectedChoice == 3)
                    {
                        pJson = string.Format("{{\"user_id\":{2},\"action_name\":\"account_login\",\"id\":{0},\"friction\":\"email_code_verification\",\"friction_data\":{{ \"optionSelection\":{{}},\"response\":{{ \"code\":\"{1}\"}}}}}}", lockId, code, userId);

                    }
                    else if (selectedChoice == 2)
                    {
                        pJson = string.Format("{{\"user_id\":{2},\"action_name\":\"account_login\",\"id\":{0},\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{3}}},\"response\":{{\"code\":\"{1}\"}}}}}}", lockId, code, userId, phoneId);

                    }
                }
                else
                {
                    pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}},\"response\":{{\"code\":\"{1}\"}}}},\"enable_throw_errors\":true}}", phoneId, code);
                    //pJson = string.Format("{{\"friction\":\"phone_verification_via_text\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}},\"response\":{{\"code\":\"{1}\"}}}},\"enable_throw_errors\":true}}", phoneId, code);
                    if (selectedChoice == 3)
                    {
                        pJson = string.Format("{{\"friction\":\"email_code_verification\",\"friction_data\":{{\"response\":{{\"code\":\"{0}\"}}}},\"enable_throw_errors\":true}}", code);
                        //pJson = string.Format("{{\"friction\":\"email_code_verification\",\"friction_data\":{{\"response\":{{\"code\":\"{0}\"}}}},\"enable_throw_errors\":true}}", code);

                    }
                    else if (selectedChoice == 2)
                    {
                        pJson = string.Format("{{\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}},\"response\":{{\"code\":\"{1}\"}}}},\"enable_throw_errors\":true}}", phoneId, code);
                        //pJson = string.Format("{{\"friction\":\"phone_verification_via_call\",\"friction_data\":{{\"optionSelection\":{{\"phone_number_id\":{0}}},\"response\":{{\"code\":\"{1}\"}}}},\"enable_throw_errors\":true}}", phoneId, code);
                    }
                }
                for (int x = 0; x < 3; x++)
                {
                    var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                    var token = "";
                    if (cc["_csrf_token"] != null)
                        token = cc["_csrf_token"].Value.DecodeURL();

                    commPage.RequestURL = u;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.JSONPUT;
                    commPage.JsonStringToPost = pJson;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    commPage.RequestHeaders.Add("X-CSRF-Token", token);
                    commPage.Referer = "https://www.airbnb.com/?has_logged_out=1";//string.Format("https://www.airbnb.com/airlock?al_id={0}", lockId);//airlockUrl;

                    ProcessRequest(commPage);

                    if (commPage.IsValid)
                    {
                        var ob = JObject.Parse(commPage.Html);
                        if (ob["error_message"] != null)
                        {
                           airlockResult.Message = ob["error_message"].ToSafeString();
                        }
                        var result = false;

                        if (isNew)
                        {
                            result = ob["airlock"]["status"].ToInt() == 2;
                        }
                        else
                        {
                            result = ob["airlock"]["friction_statuses"]["phone_verification"].ToSafeString().ToBoolean() == true
                                      || ob["airlock"]["friction_statuses"]["phone_verification_via_text"].ToSafeString().ToBoolean() == true
                                      || ob["airlock"]["friction_statuses"]["phone_verification_via_call"].ToSafeString().ToBoolean() == true
                                      || ob["airlock"]["friction_statuses"]["email_code_verification"].ToSafeString().ToBoolean() == true;                      
                        }

                        var redirectUrl = ob["airlock"]["completion_redirect_url"].ToSafeString();

                        if (result == true && (redirectUrl.Contains("/login") || redirectUrl== "https://www.airbnb.com/"))
                        {
                            //TODO: by Joe. Save the password of the account and use below to automate the login process.

                            result = AutoLoginAfterAirlock(username, password,appUserId);
                        }

                        if (result == true)
                        {
                            var serializedCookies = SerializeCookiesToString(commPage.COOKIES);

                            airlockResult.Success = true;
                            airlockResult.Cookie = serializedCookies;

                            return airlockResult;
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AllSites.SaveHtmlText(username+" Process Airlock", commPage.Html);
            }

            //GlobalVariables.CommPage = commPage;
            //GlobalVariables.StaticCommPage = CommPage;

            return airlockResult;
        }

        #endregion

        #region AutoLogin

        #endregion
        
        #region REVIEWS

        public async void GuestReviewsToHost(string token,int hostId,string userId)
        {

            await Task.Factory.StartNew(() =>
            {
                string siteHostId = Database.Actions.Hosts.GetSiteHostId(hostId);
            bool isLoggedIn = ValidateTokenAndLoadCookies(token);
            if (isLoggedIn)
                {
                    try
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        commPage.RequestURL = string.Format("https://www.airbnb.com/api/v2/reviews?_format=for_web_host_stats&_order=recent&reviewee_id={0}&role=guest&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", siteHostId);
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var reviewNode = ((JArray)ob["reviews"]);
                            foreach (JObject item in reviewNode)
                            {
                                Database.Entity.GuestReview guestReview = new Database.Entity.GuestReview();
                                guestReview.ConfirmationCode = item["reservation"]["confirmation_code"].ToString();
                                guestReview.SiteGuestId = item["reviewer"]["id"].ToSafeString();
                                guestReview.Review = item["comments"].ToSafeString();
                                guestReview.Rating = item["rating"].ToSafeString().ToInt();
                                Database.Actions.GuestReviews.AddOrUpdate(guestReview);
                            }
                        }
                    }
                    catch { }
               
            }
            });
        }

        #endregion

        #region ScrapeReservation
        private Tuple<string, bool> getReservationId(string token, string messageThreadLink)
        {
            try
            {
               
                string id = null;
                if (ValidateTokenAndLoadCookies(token))
                {
                    commPage.RequestURL = messageThreadLink;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;


                    ProcessRequest(commPage);

                    XmlDocument xd = commPage.ToXml();
                    var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                    var json = meta.GetAttributeFromNode("content").HtmlDecode();
                    var ob = JObject.Parse(json);
                    id = ob["tracking"]["context"]["reservation_id"].ToString();

                    var isActive = commPage.Html.Contains("Change or Cancel");
                    return new Tuple<string, bool>(id, isActive);
                }
                 }
            catch (Exception e)
            {
            }
            return new Tuple<string, bool>(null, false);

        }
        public async void ScrapeReservation(string token, int hostId, string userId,bool newOnly = false)
        {
            await Task.Factory.StartNew(() =>
            {
                int count = 0;
                var totalData = 10 * Database.Actions.Users.GetLastLoginTotalDayDifference(userId.ToInt());
                bool isLoggedIn = ValidateTokenAndLoadCookies(token);
                if (isLoggedIn)
                {
                    var offset = 0;
                    bool hasMore = false;
                    do
                    {
                        hasMore = false;
                        string api = String.Format("https://www.airbnb.com/api/v2/reservations?_format=for_reservations_list&_limit=10&_offset={0}&collection_strategy=for_reservations_list&date_max=&date_min=&listing_id=&sort_field=start_date&sort_order=desc&status=&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", offset);
                        commPage.RequestURL = api;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.PersistCookies = true;
                        ProcessRequest(commPage);
                        try
                        {
                            AllSites.SaveHtmlText(string.Format("Scrape Reservation host {0}", hostId), commPage.Html);
                        var ob = JObject.Parse(commPage.Html);
                        var ar = (JArray)ob["reservations"];
                       

                            var reservations = ar.ToList();
                            if (reservations.Count > 0)
                            {
                                hasMore = true;
                                offset += 10;
                                foreach (var item in reservations)
                                {
                                    count++;
                                    var code = item["confirmation_code"].ToString();
                                    var airbnbStatus = item["user_facing_status_localized"].ToString();
                                    var bookingStatus = "";
                                    var status = item["user_facing_status_key"].ToString();

                                    var name = item["listing_name"].ToString();
                                    var date = Convert.ToDateTime(item["start_date"]).ToString("MMM d - ") + Convert.ToDateTime(item["end_date"]).ToString("d, yyyy");
                                    var listingLink = "https://www.airbnb.com/rooms/" + item["listing_id"].ToString(); var profileLink = "";
                                    try { profileLink = "https://www.airbnb.com/users/show/" + item["guest_user"]["id"].ToString(); }
                                    catch { }

                                    var userName = item["guest_user"]["first_name"].ToString();
                                    var total = item["earnings"].ToString();
                                    var detailLink = "https://www.airbnb.com/reservation/itinerary?code=" + item["confirmation_code"].ToString();

                                    var messageThreadLink = "https://www.airbnb.com/messaging/qt_for_reservation/" + item["confirmation_code"].ToString();
                                    var data = getReservationId(token, messageThreadLink);
                                    var id = data.Item1;

                                    var guestId = item["guest_user"]["id"].ToLong();



                                    var propertyId = item["listing_id"].ToLong();

                                    if (airbnbStatus == "Confirmed") { bookingStatus = "A"; }
                                    else if (airbnbStatus == "Request") { bookingStatus = "B"; }
                                    else if (airbnbStatus == "Canceled") { bookingStatus = "C"; }
                                    else if (airbnbStatus == "Declined") { bookingStatus = "BC"; }
                                    else if (airbnbStatus == "Inquiry") { bookingStatus = "I"; }
                                    else if (airbnbStatus == "Expired") { bookingStatus = "X"; }
                                    if (airbnbStatus == "Confirmed") { bookingStatus = "A"; }
                                    else if (airbnbStatus == "Request") { bookingStatus = "B"; }
                                    else if (airbnbStatus == "Canceled") { bookingStatus = "C"; }
                                    else if (airbnbStatus == "Declined") { bookingStatus = "BC"; }
                                    else if (airbnbStatus == "Inquiry") { bookingStatus = "I"; }
                                    else if (airbnbStatus == "Expired") { bookingStatus = "X"; }
                                    var fullName = item["guest_user"]["full_name"].ToSafeString();
                                    var contactNumber = item["guest_user"]["phone"].ToSafeString();
                                    if (fullName != null)
                                        Database.Actions.Guests.UpdateLastName(guestId, fullName, bookingStatus);
                                    if (bookingStatus == "A" && id != null)
                                    {
                                        var inquiry = new Database.Entity.Reservation();

                                        inquiry.ReservationId = id.ToString();
                                        inquiry.HostId = hostId;
                                        inquiry.ListingId = propertyId.ToInt();
                                        inquiry.SiteGuestId = guestId.ToString();
                                        inquiry.ConfirmationCode = code;
                                        inquiry.ReservationCost = Utilities.GetPrice(total);
                                        inquiry.BookingStatusCode = bookingStatus;
                                        inquiry.IsAltered = false;
                                        inquiry.IsActive = status == "upcoming";
                                        var timein = Database.Actions.Properties.GetTimein(inquiry.ListingId);
                                        var timeout = Database.Actions.Properties.GetTimeout(inquiry.ListingId);
                                        inquiry.CheckInDate = (Convert.ToDateTime(item["start_date"]).ToString("yyyy-MM-dd") + " " + timein).ToDateTime();
                                        inquiry.CheckOutDate = (Convert.ToDateTime(item["end_date"]).ToString("yyyy-MM-dd") + " " + timeout).ToDateTime();
                                        inquiry.GuestCount = item["guest_details"]["number_of_adults"].ToInt() + item["guest_details"]["number_of_children"].ToInt() + item["guest_details"]["number_of_infants"].ToInt();
                                        bool hasReservationDetailLink = false;
                                        if (!string.IsNullOrEmpty(detailLink))
                                        {
                                            hasReservationDetailLink = true;
                                            //detailLink = "https://www.airbnb.com" + detailLink;
                                            ScrapeInquiryDetails(detailLink, inquiry, token);
                                        }

                                        if (!string.IsNullOrEmpty(messageThreadLink))
                                        {
                                            //messageThreadLink = "https://www.airbnb.com" + messageThreadLink;
                                            GetInquiryDetails(messageThreadLink, inquiry, hasReservationDetailLink, date, token);
                                        }

                                        //JM 8/29/18
                                        //Temporary Remove convert to UTC
                                        //if (inquiry.BookingDate != null) { inquiry.BookingDate = TimeZoneInfo.ConvertTimeToUtc((DateTime)inquiry.BookingDate); /*TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inquiry.BookingDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));*/ }
                                        //if (inquiry.BookingConfirmDate != null) { inquiry.BookingConfirmDate = TimeZoneInfo.ConvertTimeToUtc((DateTime)inquiry.BookingConfirmDate); /*TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inquiry.BookingConfirmDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));*/ }
                                        //if (inquiry.InquiryDate != null) { inquiry.InquiryDate = TimeZoneInfo.ConvertTimeToUtc((DateTime)inquiry.InquiryDate); /*TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inquiry.InquiryDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));*/ }
                                        //if (inquiry.CheckInDate != null) { inquiry.CheckInDate = Core.Helper.DateTimeZone.ConvertToUTC((DateTime)inquiry.CheckInDate, inquiry.PropertyId); /*.ToDateTime().Date + objIList.GetCheckInTime(inquiry.PropertyId);*/ }
                                        //if (inquiry.CheckOutDate != null) { inquiry.CheckOutDate = Core.Helper.DateTimeZone.ConvertToUTC((DateTime)inquiry.CheckOutDate, inquiry.PropertyId); /*.ToDateTime().Date + objIList.GetCheckOutTime(inquiry.PropertyId);*/ }
                                        // end 

                                        //inquiryList.Add(inquiry); // add to list
                                        Database.Actions.Reservations.AddOrUpdate(userId, inquiry);
                                    }

                                }
                                if (newOnly && offset == totalData) { hasMore = false; break; }
                            }
                            else { hasMore = false; }
                        }
                        catch { }
                    } while (hasMore);
                }
                else
                {
                    //AutoLoginHub.TriggerAirlock(hostId, Enumerations.SiteType.Airbnb, userId);
                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Session expired Scrape Reservation for host {0}", hostId));
                }
                ShowNewReservationReview.DoneDownloading(userId, hostId);
              
         });
        }
        public void ScrapeInquiryDetails(string url, Reservation inquiries, string token)
        {
            try
            {

                if (ValidateTokenAndLoadCookies(token))
                {
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-shared_itinerary_app']");
                            var json = meta.GetAttributeFromNode("content").HtmlDecode();
                            var ob = JObject.Parse(json);
                            inquiries.SumPerNightAmount = Utilities.GetPrice(ob["base_price"].ToSafeString());
                            inquiries.TotalPayout = Utilities.GetPrice(ob["total_price"].ToSafeString());
                            inquiries.NetRevenueAmount = Utilities.GetPrice(ob["total_price"].ToSafeString());
                            inquiries.CleaningFee = Utilities.GetPrice(ob["extras_price"].ToSafeString());
                            inquiries.ServiceFee = Utilities.GetPrice(ob["host_fee"].ToSafeString());
                            inquiries.Nights = ob["nights"].ToInt();

                            {
                                var timein = ob["checkin_time"].ToString();
                                timein = timein.Replace("After ", "");
                                timein = timein.ToDateTime().ToString("HH:mm");

                                var timeout = ob["checkout_time"].ToDateTime().ToString("HH:mm");


                                var checkin = ob["checkin_date"].ToDateTime().ToString("yyyy-MM-dd");
                                var checkout = ob["checkout_date"].ToDateTime().ToString("yyyy-MM-dd");

                                inquiries.CheckInDate = (checkin + " " + timein).ToDateTime();
                                inquiries.CheckOutDate = (checkout + " " + timeout).ToDateTime();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
            }
        }

        public void GetInquiryDetails(string url, Reservation inquiry, bool hasReservationDetailLink, string dateRange, string token)
        {
            try
            {
                if (ValidateTokenAndLoadCookies(token))
                {
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                            var json = meta.GetAttributeFromNode("content").HtmlDecode();
                            var ob = JObject.Parse(json);

                            var isReservationAccepted = ob["is_reservation_accepted"].ToSafeString().ToBoolean();
                            var appData = ob["appData"];
                            var StatusType = appData["statusType"].ToInt();
                            if (StatusType == 5)
                            {
                                var alterationNode = appData["alterationRequestInfo"];
                                inquiry.IsAltered = alterationNode != null ? true : false;
                            }

                            var ar = (JArray)ob["appData"]["posts"];
                            if (inquiry.BookingStatusCode.ToUpper() == "B") // pending
                            {
                                var non_response = ob["non_response"].ToSafeString();
                                if (!string.IsNullOrEmpty(non_response))
                                {
                                    var dt = non_response.ToDateTime();
                                    var origDt = dt.AddDays(-1);
                                    inquiry.BookingDate = origDt.ToDateTime();
                                }
                            }
                            if (inquiry.CheckInDate == null && inquiry.CheckOutDate == null)
                            {
                                var tuple = ScrapeDates(dateRange);
                                if (tuple != null)
                                {
                                    var timein = Database.Actions.Properties.GetTimein(inquiry.ListingId);
                                    var timeout = Database.Actions.Properties.GetTimeout(inquiry.ListingId);
                                    inquiry.CheckInDate = (Convert.ToDateTime(tuple.Item1).ToString("yyyy-MM-dd") + " " + timein).ToDateTime();
                                    inquiry.CheckOutDate = (Convert.ToDateTime(tuple.Item2).ToString("yyyy-MM-dd") + " " + timeout).ToDateTime();
                                }
                                else
                                {
                                    try
                                    {
                                        inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["start_date"]);
                                        inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["end_date"]);
                                    }
                                    catch (Exception err1)
                                    {
                                        if (inquiry.BookingStatusCode == "A")
                                        {
                                        }
                                        var ctx = JObject.Parse((ob["tracking"]["context"]).ToString());
                                        inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["context"]["checkin_date"]);
                                        inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["context"]["checkout_date"]);
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    //inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["start_date"]);
                                    //inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["rebooking_promotion"]["end_date"]);
                                }
                                catch (Exception err1)
                                {
                                    if (inquiry.BookingStatusCode == "A")
                                    {
                                    }
                                    //var ctx = JObject.Parse((ob["tracking"]["context"]).ToString());
                                    //inquiry.CheckInDate = Convert.ToDateTime(ob["tracking"]["context"]["checkin_date"]);
                                    //inquiry.CheckOutDate = Convert.ToDateTime(ob["tracking"]["context"]["checkout_date"]);
                                }
                            }


                            try
                            {
                                var dtStrt = inquiry.CheckInDate.ToDateTime();
                                var dtEnd = inquiry.CheckOutDate.ToDateTime();
                                inquiry.Nights = (dtEnd - dtStrt).Days.ToInt();
                            }
                            catch (Exception)
                            {

                            }

                            if (ar.Count > 0)
                            {
                                // booking confirm date
                                if (ar.Where(f => f["template_name"].ToSafeString() == "25" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault() != null)
                                {
                                    var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "25" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault();
                                    inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                    inquiry.BookingDate = inquiry.BookingConfirmDate;
                                }
                                else if (ar.Where(f => f["template_name"].ToSafeString() == "25" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault() != null)
                                {
                                    var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "25" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault();
                                    inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                    inquiry.BookingDate = inquiry.BookingConfirmDate;
                                }
                                else if (ar.Where(f => f["template_name"].ToSafeString() == "44" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault() != null)
                                {
                                    var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "44" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault();
                                    inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                    inquiry.BookingDate = inquiry.BookingConfirmDate;
                                }
                                else if (ar.Where(f => f["template_name"].ToSafeString() == "44" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault() != null)
                                {
                                    var reservationConfirmedPost = ar.Where(f => f["template_name"].ToSafeString() == "44" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).FirstOrDefault();
                                    inquiry.BookingConfirmDate = reservationConfirmedPost["created_at"].ToDateTime();
                                    inquiry.BookingDate = inquiry.BookingConfirmDate;
                                }

                                // booking cancel date
                                var reservationCanceledPost = ar.Where(f => f["template_name"].ToSafeString() == "24" && f["link_id"].ToLong() == inquiry.ReservationId.ToLong()).FirstOrDefault();
                                if (reservationCanceledPost != null)
                                {
                                    inquiry.BookingCancelDate = reservationCanceledPost["created_at"].ToDateTime();
                                }
                                // booking inquiry date
                                var reservationPost = ar.Where(f => f["link_type"].ToSafeString() == "Hosting" && f["created_at"].ToDateTime().Date <= inquiry.CheckInDate.ToDateTime().Date).LastOrDefault();
                                if (reservationPost != null)
                                {
                                    inquiry.InquiryDate = reservationPost["created_at"].ToDateTime();
                                }
                                else
                                {
                                    reservationPost = ar.Where(f => f["link_type"].ToSafeString() == "Hosting").LastOrDefault();
                                    if (reservationPost != null)
                                    {
                                        inquiry.InquiryDate = reservationPost["created_at"].ToDateTime() == null ? inquiry.CheckInDate : reservationPost["created_at"].ToDateTime();
                                    }
                                    else
                                    {
                                        reservationPost = ar.LastOrDefault();
                                        if (reservationPost != null)
                                        {
                                            inquiry.InquiryDate = reservationPost["created_at"].ToDateTime();
                                        }
                                    }
                                }
                            }

                            if (!hasReservationDetailLink)
                            {
                                var paymentInfo = ob["appData"]["paymentInfo"];
                                if (paymentInfo != null)
                                {
                                    inquiry.SumPerNightAmount = Utilities.GetPrice(paymentInfo["basePrice"].ToSafeString());
                                    inquiry.TotalPayout = Utilities.GetPrice(paymentInfo["hostEarns"].ToSafeString());
                                    inquiry.CleaningFee = Utilities.GetPrice(paymentInfo["extrasPrice"].ToSafeString());
                                }
                            }

                            var reservationInfo = ob["appData"]["reservationInfo"];
                            if (reservationInfo != null)
                            {
                                inquiry.GuestCount = reservationInfo["number_of_guests"].ToInt();
                            }
                            long threadId = 0;
                            long.TryParse(ob["thread_id"].ToSafeString(), out threadId);

                            inquiry.ThreadId = threadId.ToString();
                            if (inquiry.ThreadId == "568194179")
                            {

                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
            }
        }

        public static Tuple<DateTime, DateTime> ScrapeDates(string dateRange)
        {
            var dates = dateRange.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (dates.Length == 2)
            {
                var year1 = Utilities.SplitSafely(dates[0], ",", 1);
                var year2 = Utilities.SplitSafely(dates[1], ",", 1);

                var month1 = Utilities.SplitSafely(dates[0], " ", 0);
                var date1 = Utilities.SplitSafely(dates[0], " ", 1).Trim(',');

                var month2 = "";
                var date2 = "";

                var rawDate2WithoutYear = Utilities.SplitSafely(dates[1], ",", 0);
                var date2parts = rawDate2WithoutYear.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (date2parts.Length == 2)
                {
                    month2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 0);
                    date2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 1);
                }
                else
                {
                    date2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 0);
                }

                if (string.IsNullOrEmpty(year1))
                    year1 = year2;
                if (string.IsNullOrEmpty(month2))
                    month2 = month1;

                var datefrom = string.Format("{0} {1} {2}", month1, date1, year1);
                var dateto = string.Format("{0} {1} {2}", month2, date2, year2);

                var dtfrom = datefrom.ToDateTime();
                var dtto = dateto.ToDateTime();
                return new Tuple<DateTime, DateTime>(dtfrom, dtto);
            }
            return null;
        }
        #endregion

        public async Task<bool> StartImporting(string token, int hostId, string userId) {
            await Task.Factory.StartNew(() =>
            {
                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Start Scrapping for host {0}", hostId));
                ShowNewReservationReview.ShowDownloading(userId,hostId);
                //Scrapping.Stop(hostId+"import");
                //JobScheduler.PropertyJobScheduler.StartImport("property" + hostId + "import", hostId, 1, userId);
                //JobScheduler.InboxJobScheduler.StartImport("inbox" + hostId + "import", hostId,1, userId);
                //JobScheduler.ReservationJobScheduler.StartImport("reservation" + hostId + "import", hostId,1, userId);
                //JobScheduler.GuestReviewJobScheduler.Start("guestreview" + hostId+"import", hostId, 1, userId);
                ScrapeMyListings(token, hostId, userId);
                ScrapeInbox(token, hostId, userId);
                GuestReviewsToHost(token, hostId, userId);
                ScrapeReservation(token, hostId, userId);
                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Finished Scrapping for host {0}", hostId));
            });
            return true;
        }

        #region Inbox
        public async void ScrapeInbox(string token,int hostId, string userId,bool newOnly = false)
        {
            await Task.Factory.StartNew(() =>
            {
            int count = 0;
            var totalData =10*Database.Actions.Users.GetLastLoginTotalDayDifference(userId.ToInt());
            var isloaded = ValidateTokenAndLoadCookies(token);

                if (isloaded)
                {
                    int offset = 0;

                    bool hasMore = false;
                    do
                    {
                        string url = string.Format("https://www.airbnb.com/api/v2/threads?_format=for_mobile_inbox&_offset={0}&role={1}&selected_inbox_type=host&include_help_threads=true&include_support_messaging_threads=true&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", offset, "all");
                        hasMore = false;
                        commPage.RequestURL = url;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.PersistCookies = true;

                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            try
                            {
                                AllSites.SaveHtmlText(string.Format("Scrape Inbox host {0}", hostId), commPage.Html);
                                var ob = JObject.Parse(commPage.Html);
                                var ar_tempt = (JArray)ob["threads"];
                                var ar = ar_tempt.ToList();
                                if (ar.Count > 0)
                                {
                                    hasMore = true;
                                    offset += 10;
                                    foreach (var item in ar)
                                    {
                                        count++;
                                        var guestName = item["other_user"]["first_name"].ToSafeString();
                                        var guestUserId = item["other_user"]["id"].ToLong();
                                        var guestProfilePictureUrl = item["other_user"]["picture_url"].ToSafeString();
                                        var threadId = item["id"].ToLong();
                                        var inquiryId = item["inquiry_reservation"].ToSafeString() == "" ? 0 : item["inquiry_reservation"]["id"].ToLong();
                                        var isArchived = item["archived"].ToBoolean();
                                        var lastMessageAt = item["last_message_at"].ToDateTime();
                                        var readStatus = item["unread"].ToBoolean();
                                        var messageContent = item["text_preview"].ToSafeString();
                                        var listingName = item["listing"].SelectToken("name") == null ? "" : item["listing"]["name"].ToSafeString();
                                        var listingId = item["listing"].SelectToken("id") == null ? 0 : item["listing"]["id"].ToLong();
                                        var status = item["status_string"].ToSafeString();
                                        var isInquiryOnly = status == "Special Offer" || status == "Inquiry";
                                        var isSpecialOfferSent = status == "Special Offer";
                                        if (status == "Accepted") { status = "A"; }
                                        else if (status == "Pending") { status = "B"; }
                                        else if (status == "Canceled") { status = "BC"; }
                                        else if (status == "Declined") { status = "C"; }
                                        else if (status == "Inquiry") { status = "I"; }
                                        else if (status == "Special Offer") { status = "I"; }
                                        else if (status == "Not Possible") { status = "NP"; }
                                        else if (status == "Pre-Approved") { status = "I"; }
                                        if (guestUserId != 0)
                                        {
                                            Database.Entity.Guest guest = new Database.Entity.Guest();
                                            guest.SiteGuestId = guestUserId.ToString();
                                            guest.FirstName = guestName;
                                            guest.Location = item["other_user"]["location"].ToSafeString();
                                            guest.ProfilePictureUrl = guestProfilePictureUrl;
                                            guest.SiteProfileUrl = string.Format("https://www.airbnb.com/users/show/{0}", guest.SiteGuestId);

                                            Database.Actions.Guests.AddOrUpdate(guest);
                                            HostReviews(guest.SiteGuestId);
                                            Inbox inbox = new Inbox();
                                            inbox.HostId = hostId;
                                            inbox.ThreadId = threadId.ToString();
                                            inbox.IsArchived = isArchived;
                                            inbox.Status = status;
                                            inbox.HasUnread = readStatus;
                                            inbox.IsInquiryOnly = isInquiryOnly;
                                            inbox.IsSpecialOfferSent = isSpecialOfferSent;
                                            inbox.SiteGuestId = guestUserId.ToString();
                                            //inbox.GuestId = Core.API.Airbnb.Guests.GetLocalId(guestUserId.ToString());
                                            inbox.LastMessage = messageContent;
                                            inbox.LastMessageAt = lastMessageAt;
                                            inbox.ListingId = listingId;
                                            inbox.InquiryId = inquiryId.ToString();

                                            if (inbox.LastMessageAt != null) { inbox.LastMessageAt = TimeZoneInfo.ConvertTimeToUtc(inbox.LastMessageAt); /* TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(inbox.LastMessageAt.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));*/ }
                                            ScrapeMessageDetails(inbox.ThreadId, hostId, inbox);
                                            Database.Actions.Inboxes.AddOrUpdate(inbox, userId);



                                        }
                                    }
                                    //if (newOnly && offset==(propertyCount*10)) { hasMore = false; break; }
                                    if (newOnly && offset == totalData) { hasMore = false; break; }
                                }
                                else
                                {
                                    hasMore = false;
                                }

                                if(offset==10 && ar.Count == 0)
                                {
                                    Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Possible Changes in Airbnb Inbox", IsSentSMS = false });
                                }
                            }
                            catch (Exception e)
                            {
                                Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Exception in Airbnb Inbox", IsSentSMS = false });
                                
                            }
                        }
                    } while (hasMore);
                    if(count>0)
                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Scrape {0} inbox for host {1}", count, hostId));
                }
                else
                {
                    Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Session Expired Scrape inbox for host {0}", hostId));
                    AutoLoginHub.TriggerAirlock(hostId, Enumerations.SiteType.Airbnb, userId);
                }
         
            });
        }

        public List<Message> ScrapeMessageDetails(string threadId, long hostId, Inbox inbox = null)
        {
            List<Message> messages = new List<Message>();
            try
            {
                string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();
                    var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                    var json = meta.GetAttributeFromNode("content").HtmlDecode();
                    var ob = JObject.Parse(json);
                    var ar = (JArray)ob["appData"]["posts"];
                    var currentUserId = ob["appData"]["currentUserId"].ToLong();
                    var guestId = ob["appData"]["guestId"].ToSafeString();

                    if (inbox != null)
                    {
                        var statusType = ob["appData"]["statusType"].ToInt();
                        inbox.StatusType = statusType;
                        if (statusType == 1)
                        {
                            inbox.CanPreApproveInquiry = true;
                            inbox.CanDeclineInquiry = true;
                        }
                        else if (statusType == 13)
                        {
                            inbox.CanWithdrawPreApprovalInquiry = true;
                        }

                        #region Inquiry Related Data for inbox object
                        var inquiryNode = ob["inquiry"];

                        if (inquiryNode != null && inquiryNode.HasValues)
                        {
                            try
                            {
                                inbox.CheckInDate = inquiryNode["checkin_date"].ToDateTime();
                                inbox.CheckOutDate = inquiryNode["checkout_date"].ToDateTime();
                            }
                            catch
                            {
                                inbox.CheckInDate = null;
                                inbox.CheckOutDate = null;
                            }
                        }

                        var reservationNode = ob["appData"]["reservationInfo"];

                        if (reservationNode != null && reservationNode.HasValues)
                        {
                            inbox.GuestCount = string.IsNullOrEmpty(reservationNode["number_of_guests"].ToSafeString()) ? 0 : reservationNode["number_of_guests"].ToInt();
                        }

                        var paymentNode = ob["appData"]["paymentInfo"];

                        if (paymentNode != null && paymentNode.HasValues)
                        {
                            try
                            {
                                inbox.RentalCost = paymentNode["basePrice"].ToDecimal();
                                inbox.CleaningCost = paymentNode["extrasPrice"].ToDecimal();
                                inbox.GuestFee = paymentNode["guestFeeFormatted"].ToDecimal();
                                inbox.ServiceFee = paymentNode["hostFeeFormatted"].ToDecimal();
                                inbox.Subtotal = paymentNode["subtotalPriceString"].ToDecimal();
                                inbox.GuestPay = paymentNode["guestPays"].ToDecimal();
                                inbox.HostEarn = paymentNode["hostEarns"].ToDecimal();
                            }
                            catch { }
                        }
                        #endregion
                    }

                    //Joem 04/29/19 Temporary remove because we don`t need it now.
                    //var timein = Core.API.AllSite.Property.GetTimein(inbox.PropertyId);
                    //var timeout = Core.API.AllSite.Property.GetTimeout(inbox.PropertyId);
                    //if (inbox.CheckInDate != null && inbox.CheckOutDate != null)
                    //{
                    //    inbox.CheckInDate = (Convert.ToDateTime(inbox.CheckInDate).ToString("yyyy-MM-dd") + " " + timein).ToDateTime();
                    //    inbox.CheckInDate = Core.Helper.DateTimeZone.ConvertToUTC((DateTime)inbox.CheckInDate, inbox.PropertyId);

                    //    inbox.CheckOutDate = (Convert.ToDateTime(inbox.CheckOutDate).ToString("yyyy-MM-dd") + " " + timeout).ToDateTime();
                    //    inbox.CheckOutDate = Core.Helper.DateTimeZone.ConvertToUTC((DateTime)inbox.CheckOutDate, inbox.PropertyId);
                    //}
                    //else
                    //{
                    //}

                    if (ar.Count > 0)
                    {
                        inbox.InquiryDate = ar[ar.Count - 1]["created_at"].ToDateTime();
                        foreach (var item in ar)
                        {
                            var userId = item["user_id"].ToLong();
                            var myMessage = false;
                            if (userId == currentUserId)
                            {
                                myMessage = true;
                            }

                            var message = item["message"].ToSafeString();
                            var createdAt = item["created_at"].ToDateTime();
                            var messageId = item["id"].ToSafeString();
                            if (!string.IsNullOrEmpty(message))
                            {
                                Message tm = new Message
                                {
                                    ThreadId = threadId,
                                    MessageId = messageId,
                                    MessageContent = message,
                                    IsMyMessage = myMessage,
                                    CreatedDate = createdAt
                                };
                                if (tm.CreatedDate != null) { tm.CreatedDate = TimeZoneInfo.ConvertTimeToUtc(tm.CreatedDate); }
                                Database.Actions.Messages.Add(tm);
                            }
                        }
                    }
                }
                return messages;
            }
            catch (Exception e)
            {
            }
            return messages;
        }
        #endregion

        #region ScrapeMyListing
        public async void ScrapeMyListings(string token, int hostId, string userId)
        {
            int count = 0;
                var isloaded = ValidateTokenAndLoadCookies(token);

            if (isloaded)
            {
                try
                {
                    string api = String.Format("https://www.airbnb.com/api/v2/rooms_listings?key=d306zoyjsyarp7ifhu67rjxn52tv0t20&_format=default&_limit=25&_offset=0&_order_field=name&_order_type=DESC&_include_all_listing_ids=false");
                    commPage.RequestURL = api;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;


                    ProcessRequest(commPage);
                    var ob = JObject.Parse(commPage.Html);
                    var ar = (JArray)ob["rooms_listings"];
                    var Properties = ar.ToList();
                    if (Properties== null || Properties.Count == 0)
                    {
                        Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Possible Changes in Airbnb Properties", IsSentSMS = false });
                       
                    }
                    if (Properties != null)
                    {
                        foreach (var item in Properties)
                        {
                            var link = "https://www.airbnb.com/rooms/" + item["id"].ToString();
                            //var status = item["has_availability"].ToSafeString();
                            var status = item["listing_state"].ToString();

                            var property = ScrapeListingDetails(hostId, link, status);

                            if (property != null)
                            {
                                if (!Database.Actions.Properties.PropertyTimeZoneExist(property.ListingId))
                                {
                                    var timezone = GetTimeZone(property.Longitude, property.Latitude);
                                    property.TimeZoneName = timezone.Item1;
                                    property.TimeZoneId = timezone.Item2;
                                }
                                count++;
                                Database.Actions.Properties.AddOrUpdate(property);

                            }
                            else
                            {
                                var p = new Property()
                                {
                                    HostId = hostId,
                                    Address = item["smart_location"].ToString(),
                                    ListingId = item["id"].ToInt(),
                                    AccomodationType = item["placeholder_name"].ToString(),
                                    Bathrooms = item["bathrooms"].ToInt(),
                                    Bedrooms = item["bedrooms"].ToInt(),
                                    Description = "",//description.HtmlDecode(),
                                    Internet = "",//isWifiAvailable.ToString(),
                                    ListingUrl = "https://www.airbnb.com/rooms/" + item["id"].ToInt(),//url,
                                    MinStay = 0,// minStay,
                                    Pets = item["is_ptt_allowed"].ToString(),
                                    PriceNightlyMin = "",//nightlyMin.ToSafeString(),
                                    PropertyType = "",//propertyType,
                                    Reviews = 0,//reviewCount,
                                    Sleeps = 0,//sleeps,
                                    Smoking = "",//isSmokingAllowed.ToString(),
                                    Name = item["name_or_placeholder_name"].ToString(),
                                    Longitude = "",//longitude,
                                    Latitude = "",//,latitude,
                                    AirCondition = "",//isAirconditioned.ToString(),
                                    Status = status,
                                    CheckInTime = new TimeSpan(16, 0, 0),//checkin == TimeSpan.MinValue ? new TimeSpan(16, 0, 0) : checkin,
                                    CheckOutTime = new TimeSpan(11, 0, 0)//checkout == TimeSpan.MinValue ? new TimeSpan(11, 0, 0) : checkout
                                };
                                count++;
                                Database.Actions.Properties.AddOrUpdate(p);
                            }
                        }
                        if(count>0)
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Scrape {0} property for host {1}", count, hostId));
                    }

                }
                catch (Exception e)
                {
                    //Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Scrape property {0} for host {1}", e.StackTrace + e + "" + e.InnerException, hostId));

                    AllSites.SaveHtmlText(string.Format("Scrape Listing host {0}", hostId), commPage.Html);
                }

            }
            else
            {
                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Session Expired Scrape property for host {0}", hostId));
            }
           
        }
        public Property ScrapeListingDetails(int hostId, string url, string status)
        {
            long listingId = 0;
            try
            {
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var xd = commPage.ToXml();
                    if (xd != null)
                    {
                        var idNode = xd.SelectSingleNode("//div[@data-hypernova-key and not(contains(@data-hypernova-key,'login'))]");

                        if (idNode != null)
                        {
                            var id = idNode.GetAttributeFromNode("data-hypernova-key").ToSafeString();
                            var json = xd.SelectSingleNode("//script[@data-hypernova-key='" + id + "']").GetInnerTextFromNode().HtmlDecode().Trim();


                            if (string.IsNullOrEmpty(json))
                            {
                                //TODO: notify
                                return null;
                            }
                            else
                            {
                                json = json.Replace("//\r\n", "");
                                json = json.Replace("<!--", "");
                                json = json.Replace("-->", "");
                                var ob = JObject.Parse(json);
                                JToken topNode = null;
                                if (ob["bootstrapData"]["reduxData"]["homePDP"] != null)
                                {
                                    topNode = ob["bootstrapData"]["reduxData"]["homePDP"]["listingInfo"]["listing"];

                                    var guestControlsNode = topNode["guest_controls"];

                                    var isPetsAllowed = guestControlsNode["allows_pets"].ToBoolean();
                                    var isSmokingAllowed = guestControlsNode["allows_smoking"].ToBoolean();

                                    var listingAmenitiesNode = topNode["listing_amenities"];

                                    var wifiNode = listingAmenitiesNode.FirstOrDefault(o => o["name"] != null && o["name"].ToSafeString() == "Wifi");
                                    var isWifiAvailable = wifiNode != null ? wifiNode["is_present"].ToBoolean() : false;

                                    var airconNode = listingAmenitiesNode.FirstOrDefault(o => o["name"] != null && o["name"].ToSafeString() == "Air conditioning");
                                    var isAirconditioned = airconNode != null ? airconNode["is_present"].ToBoolean() : false;

                                    var description = topNode["sectioned_description"]["summary"].ToSafeString();
                                    var minStay = topNode["min_nights"].ToInt();
                                    listingId = topNode["id"].ToLong();
                                    var name = topNode["name"].ToSafeString();
                                    var nightlyMin = GetPrice(listingId);
                                    var bathrooms = Utilities.ExtractNumber(topNode["bathroom_label"].ToSafeString());
                                    var bedrooms = Utilities.ExtractNumber(topNode["bedroom_label"].ToSafeString());
                                    var sleeps = Utilities.ExtractNumber(topNode["bed_label"].ToSafeString());
                                    var propertyType = topNode["room_and_property_type"].ToSafeString();
                                    var accomodationType = topNode["localized_rsoom_type"].ToSafeString();
                                    var address = topNode["p3_summary_address"].ToSafeString();

                                    TimeSpan checkin = new TimeSpan();
                                    TimeSpan checkout = new TimeSpan();
                                    //JM 06/19/19 Temporary remove because not supported
                                    //checkin = Utilities.ExtractCheckInTime(topNode["localized_check_in_time_window"].ToSafeString());
                                  //  checkout = Utilities.ExtractCheckOutTime(topNode["localized_check_out_time"].ToSafeString());
                                    var reviewCount = topNode["visible_review_count"].ToInt();
                                    //DateTime lastReviewAt;
                                    //var sortedReviewsNode = (JArray)topNode["sorted_reviews"];
                                    //if (sortedReviewsNode != null) lastReviewAt = sortedReviewsNode[0]["created_at"].ToDateTime();

                                    var longitude = topNode["lng"].ToSafeString();
                                    var latitude = topNode["lat"].ToSafeString();

                                    return new Property()
                                    {
                                        HostId = hostId,
                                        Address = address,
                                        ListingId = listingId,
                                        AccomodationType = accomodationType,
                                        Bathrooms = bathrooms,
                                        Bedrooms = bedrooms,
                                        Description = description.HtmlDecode(),
                                        Internet = isWifiAvailable.ToString(),
                                        ListingUrl = url,
                                        MinStay = minStay,
                                        Pets = isPetsAllowed.ToString(),
                                        PriceNightlyMin = nightlyMin.ToSafeString(),
                                        PropertyType = propertyType,
                                        Reviews = reviewCount,
                                        Sleeps = sleeps,
                                        Smoking = isSmokingAllowed.ToString(),
                                        Name = name,
                                        Longitude = longitude,
                                        Latitude = latitude,
                                        AirCondition = isAirconditioned.ToString(),
                                        Status = status,
                                        CheckInTime = checkin == TimeSpan.MinValue ? new TimeSpan(16, 0, 0) : checkin,
                                        CheckOutTime = checkout == TimeSpan.MinValue ? new TimeSpan(11, 0, 0) : checkout
                                    };
                                }
                            }
                        }
                        else
                        {
                            var json = xd.SelectSingleNode("//script[@data-state='true']").GetInnerTextFromNode().HtmlDecode().Trim();
                            json = json.Replace("//\r\n", "");
                            json = json.Replace("<!--", "");
                            json = json.Replace("-->", "");
                            var ob = JObject.Parse(json);
                            JToken topNode = null;
                            if (ob["bootstrapData"]["reduxData"]["homePDP"] != null)
                            {
                                topNode = ob["bootstrapData"]["reduxData"]["homePDP"]["listingInfo"]["listing"];

                                var guestControlsNode = topNode["guest_controls"];

                                var isPetsAllowed = guestControlsNode["allows_pets"].ToBoolean();
                                var isSmokingAllowed = guestControlsNode["allows_smoking"].ToBoolean();

                                var listingAmenitiesNode = topNode["listing_amenities"];

                                var wifiNode = listingAmenitiesNode.FirstOrDefault(o => o["name"] != null && o["name"].ToSafeString() == "Wifi");
                                var isWifiAvailable = wifiNode != null ? wifiNode["is_present"].ToBoolean() : false;

                                var airconNode = listingAmenitiesNode.FirstOrDefault(o => o["name"] != null && o["name"].ToSafeString() == "Air conditioning");
                                var isAirconditioned = airconNode != null ? airconNode["is_present"].ToBoolean() : false;

                                var description = topNode["sectioned_description"]["summary"].ToSafeString();
                                var minStay = topNode["min_nights"].ToInt();
                                listingId = topNode["id"].ToLong();
                                var name = topNode["name"].ToSafeString();
                                var nightlyMin = GetPrice(listingId);
                                var bathrooms = Utilities.ExtractNumber(topNode["bathroom_label"].ToSafeString());
                                var bedrooms = Utilities.ExtractNumber(topNode["bedroom_label"].ToSafeString());
                                var sleeps = Utilities.ExtractNumber(topNode["bed_label"].ToSafeString());
                                var propertyType = topNode["room_and_property_type"].ToSafeString();
                                var accomodationType = topNode["localized_room_type"].ToSafeString();
                                var address = topNode["p3_summary_address"].ToSafeString();

                                TimeSpan checkin = new TimeSpan();
                                TimeSpan checkout = new TimeSpan();
                                //checkin = Utilities.ExtractCheckInTime(topNode["localized_check_in_time_window"].ToSafeString());
                                //checkout = Utilities.ExtractCheckOutTime(topNode["localized_check_out_time"].ToSafeString());
                                var reviewCount = topNode["visible_review_count"].ToInt();
                                //DateTime lastReviewAt;
                                //var sortedReviewsNode = (JArray)topNode["sorted_reviews"];
                                //if (sortedReviewsNode != null) lastReviewAt = sortedReviewsNode[0]["created_at"].ToDateTime();

                                var longitude = topNode["lng"].ToSafeString();
                                var latitude = topNode["lat"].ToSafeString();

                                return new Property()
                                {
                                    HostId = hostId,
                                    Address = address,
                                    ListingId = listingId,
                                    AccomodationType = accomodationType,
                                    Bathrooms = bathrooms,
                                    Bedrooms = bedrooms,
                                    Description = description.HtmlDecode(),
                                    Internet = isWifiAvailable.ToString(),
                                    ListingUrl = url,
                                    MinStay = minStay,
                                    Pets = isPetsAllowed.ToString(),
                                    PriceNightlyMin = nightlyMin.ToSafeString(),
                                    PropertyType = propertyType,
                                    Reviews = reviewCount,
                                    Sleeps = sleeps,
                                    Smoking = isSmokingAllowed.ToString(),
                                    Name = name,
                                    Longitude = longitude,
                                    Latitude = latitude,
                                    AirCondition = isAirconditioned.ToString(),
                                    Status = status,
                                    CheckInTime = checkin == TimeSpan.MinValue ? new TimeSpan(16, 0, 0) : checkin,
                                    CheckOutTime = checkout == TimeSpan.MinValue ? new TimeSpan(11, 0, 0) : checkout
                                };
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            return null;
        }

        public string GetPrice(long propertyId, string checkin = "", string checkout = "")
        {

            try
            {
                string url = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?guests=1&listing_id={0}&_format=for_dateless_booking_info_on_web_p3&_interaction_type=pageload&_intents=p3_book_it&_parent_request_uuid=5e7baaa5-bf39-4ee6-8319-37f930b2ab5e&_p3_impression_id=p3_1490352774_Ymuf7NI5Bk956LLB&show_smart_promotion=0&number_of_adults=1&number_of_children=0&number_of_infants=0&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", propertyId);
                if (!string.IsNullOrEmpty(checkout) && !string.IsNullOrEmpty(checkin))
                {
                    url = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?guests=1&listing_id={0}&_format=for_detailed_booking_info_on_web_p3_with_message_data&_interaction_type=pageload&_intents=p3_book_it&_parent_request_uuid=07a8f796-adcb-4863-a0ca-5ae04ef06d0e&_p3_impression_id=p3_1490559375_zW9jEkRLeySfE5eH&show_smart_promotion=0&check_in={1}&check_out={2}&number_of_adults=1&number_of_children=0&number_of_infants=0&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", propertyId, checkin, checkout);
                }
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var price = ob["pricing_quotes"][0]["rate"]["amount"].ToSafeString();
                    var currency = ob["pricing_quotes"][0]["rate"]["currency"].ToSafeString();
                    return price + " " + currency;
                }
            }
            catch (Exception e)
            {
                //ElmahLogger.LogInfo(string.Format("Er"));
            }
            return string.Empty;
        }
        public Tuple<string, string> GetTimeZone(string longitude, string latitude)
        {
            try
            {
                string api = String.Format("https://maps.googleapis.com/maps/api/timezone/json?location={0},{1}&timestamp=1331161200&key={2}", latitude, longitude, GlobalVariables.GoogleApiKey());
                commPage.RequestURL = api;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);

                var ob = JObject.Parse(commPage.Html);

                return new Tuple<string, string>(ob["timeZoneName"].ToString(), ob["timeZoneId"].ToString());
            }
            catch
            {
            }
            return new Tuple<string, string>(null, null);
        }
        #endregion

        #region HostReviews
        public void HostReviews(string guestId,bool isHost =false)
        {
            try
            {
                commPage.RequestURL = string.Format("https://api.airbnb.com/v2/reviews?role=all&reviewee_id={0}&key=d306zoyjsyarp7ifhu67rjxn52tv0t20", guestId);
                commPage.ReqType = CommunicationPage.RequestType.GET;

                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    var reviews = JArray.Parse(ob["reviews"].ToString());

                    foreach (var review in reviews)
                    {
                        if (isHost==false)
                        {
                            HostReviews(review["author_id"].ToString(), true);
                        }
                        Database.Actions.HostReviews.AddOrUpdate(new HostReview
                        {
                            SiteGuestId = guestId,//isHost?guestId : review["author_id"].ToString(),
                            SiteHostId = review["author_id"].ToString(),//isHost?review["author_id"].ToString() : guestId,
                            Name = review["author"]["first_name"].ToString(),
                            ProfilePictureUrl = review["author"]["picture_url"].ToString(),
                            Comments = review["comments"].ToString(),
                            CreatedAt = review["created_at"].ToDateTime()
                        });
                    }
                }
            }
            catch (Exception e)
            {

            }
        }
        #endregion

        #region COOKIES

        private bool HasValidCookie(string cookie)
        {
            try
            {
                commPage.RequestURL = "https://www.airbnb.com/hosting";
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                commPage.COOKIES = DeSerializeCookiesFromString(cookie);

                ProcessRequest(commPage);

                if (commPage.Uri.AbsoluteUri.Contains("hosting"))
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        private string SerializeCookiesToString(CookieContainer cookieContainer)
        {
            using (var stream = new MemoryStream())
            {
                try
                {
                    new BinaryFormatter().Serialize(stream, cookieContainer);
                    return Convert.ToBase64String(stream.ToArray());
                }
                catch (Exception e)
                {
                    return string.Empty;
                }
            }
        }

        private CookieContainer DeSerializeCookiesFromString(string cookies)
        {
            try
            {
                using (var stream = new MemoryStream(Convert.FromBase64String(cookies)))
                {
                    return (CookieContainer)new BinaryFormatter().Deserialize(stream);
                }
            }
            catch (Exception e)
            {
                return new CookieContainer();
            }
        }

        #endregion

        #region INQUIRY ACTIONS
        public ScrapeResult PreApproveInquiry(string threadId, string token)
        {
            var sresult = new ScrapeResult();
            //int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(token);
            if (ValidateTokenAndLoadCookies(token))
            {
                string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                commPage.RequestHeaders = new Dictionary<string, string>();


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();
                    var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                    var json = meta.GetAttributeFromNode("content").HtmlDecode();
                    var ob = JObject.Parse(json);
                    var inquiryPostId = ob["appData"]["inquiryPostId"].ToSafeString();

                    if (!string.IsNullOrEmpty(inquiryPostId))
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/qt_reply_v2/{0}", threadId);
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        var parms = new Dictionary<string, string>();
                        parms.Add("message", "");
                        parms.Add("template", "1");
                        parms.Add("inquiry_post_id", inquiryPostId);
                        commPage.Parameters = parms;
                        commPage.PersistCookies = true;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                        ProcessRequest(commPage);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        if (commPage.IsValid && commPage.Html.Contains("Sent"))
                        {
                            UpdateThreadStatuses(threadId);
                            //JM 06/14/19 Temporary remove because not yet supported
                            //AutomatedResponse.PreApproval(this, token, threadId);
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Preapprove for Thread{0}", threadId), "Success", hostId.ToString(), "Airbnb");
                            sresult.Success= true;
                            sresult.IsComplete = true;
                        }
                        else if (commPage.IsValid && commPage.Html.Contains("you cannot pre-approve this guest")) {
                            sresult.Success = true;
                            sresult.IsComplete = false;
                        }
                    }
                }
            }
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to preapprove, please check your login credential"), "Error", hostId.ToString(), "Airbnb");

            return sresult;
        }
        public ScrapeResult WithdrawPreapprovalInquiry(string threadId, string sessiontoken)
        {
            var sresult = new ScrapeResult();
            if (ValidateTokenAndLoadCookies(sessiontoken))
            {
                string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();
                    var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                    var json = meta.GetAttributeFromNode("content").HtmlDecode();
                    var ob = JObject.Parse(json);
                    var removOfferUrl = ob["ajax_urls"]["remove_offer"].ToSafeString();

                    if (!string.IsNullOrEmpty(removOfferUrl))
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com{0}", removOfferUrl);
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        var parms = new Dictionary<string, string>();
                        parms.Add("_method", "post");
                        parms.Add("authenticity_token", token);

                        commPage.Parameters = parms;
                        commPage.PersistCookies = true;

                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-CSRF-Token", token);
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                        ProcessRequest(commPage);
                        commPage.RequestHeaders = new Dictionary<string, string>();

                        if (commPage.IsValid && commPage.Uri.AbsolutePath.Contains("/z/q/"))
                        {
                            UpdateThreadStatuses(threadId);
                            sresult.Success = true;
                            sresult.IsComplete = true;
                        }
                        else
                        {
                            sresult.Success = true;
                            sresult.IsComplete = false;
                        }
                    }
                }
            }
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to withdraw preapprove for Thread {0}, please chek your login credentials", threadId), "Error", hostId.ToString(), "Airbnb");

            return sresult;
        }
        public bool DeclineInquiry(string threadId, string message, string token)
        {
            if (ValidateTokenAndLoadCookies(token))
            {

                string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.PersistCookies = true;
                commPage.RequestHeaders = new Dictionary<string, string>();


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();
                    var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                    var json = meta.GetAttributeFromNode("content").HtmlDecode();
                    var ob = JObject.Parse(json);
                    var inquiryPostId = ob["appData"]["inquiryPostId"].ToSafeString();

                    if (!string.IsNullOrEmpty(inquiryPostId))
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/qt_reply_v2/{0}", threadId);
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        var parms = new Dictionary<string, string>();
                        parms.Add("message", message);
                        parms.Add("template", "9");
                        parms.Add("inquiry_post_id", inquiryPostId);
                        parms.Add("decline_reason", "other");
                        commPage.Parameters = parms;
                        commPage.PersistCookies = true;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");


                        ProcessRequest(commPage);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        if (commPage.IsValid && commPage.Html.Contains("has been sent"))
                        {
                            UpdateThreadStatuses(threadId);
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Decline inquiry for Thread {0}", threadId), "Success", hostId.ToString(), "Airbnb");

                            return true;
                        }
                    }
                }
            }
            //Core.Helper.EmailService.SendEmail("Error in declining inquiry", string.Format("Failed to decline inquiry for Thread {0}, please check your login credentials", threadId), GlobalVariables.ErrorEmail());
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to decline inquiry for Thread {0}, please check your login credentials", threadId), "Error", hostId.ToString(), "Airbnb");

            return false;
        }
        public bool UpdateThreadStatuses(string threadId)
        {
            using (var db = new ApplicationDbContext())
            {
                var inbox = db.Inboxes.Where(x => x.ThreadId == threadId).FirstOrDefault();
                if (inbox != null)
                {
                    //List<InboxMessage> messages = new List<InboxMessage>();
                    string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                    commPage.RequestURL = url;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.PersistCookies = true;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                        var json = meta.GetAttributeFromNode("content").HtmlDecode();
                        var ob = JObject.Parse(json);
                        var ar = (JArray)ob["appData"]["posts"];
                        var statusType = ob["appData"]["statusType"].ToSafeString();
                        bool canPreApproveInquiry = false;
                        bool canDeclineInquiry = false;
                        bool canWithdrawPreApprovalInquiry = false;
                        if (statusType == "1")
                        {
                            canPreApproveInquiry = true;
                            canDeclineInquiry = true;
                        }
                        else if (statusType == "13")
                        {
                            canWithdrawPreApprovalInquiry = true;
                        }
                        inbox.CanPreApproveInquiry = canPreApproveInquiry;
                        inbox.CanDeclineInquiry = canDeclineInquiry;
                        inbox.CanWithdrawPreApprovalInquiry = canWithdrawPreApprovalInquiry;
                        db.Entry(inbox).State = EntityState.Modified;
                        db.SaveChanges();
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Update Thread {0} status", 0), "Success", inbox.HostId.ToString(), "Airbnb");

                    }
                }
                return false;
            }
        }
        #endregion

        #region SEND MESSAGE
        public ScrapeResult SendMessageToInbox(string sessionToken, string threadId, string messageText, string userId = null)
        {
            //int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(sessionToken);
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/z/q/" + threadId;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();

                        var token = xd.SelectSingleNode("//input[@name='authenticity_token']").GetAttributeFromNode("value").ToSafeString();
                        if (token == "")
                        {
                            var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                            if (cc["_csrf_token"] != null)
                                token = cc["_csrf_token"].Value.DecodeURL();
                        }
                        commPage.RequestURL = "https://www.airbnb.com/messaging/qt_reply_v2/" + threadId;
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>() { { "message", messageText } };
                        commPage.Parameters.Add("authenticity_token", token);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, */*; q=0.01");
                        commPage.Referer = "https://www.airbnb.com/z/q/" + threadId;


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {

                            try
                            {
                                var ob = JObject.Parse(commPage.Html);
                                if (ob["status"].ToSafeString().Contains("Sent!"))
                                {
                                    var message = ScrapeLastMessage(threadId, userId);
                                    Database.Actions.Messages.Add(message);
                                    using (var db = new ApplicationDbContext())
                                    {
                                       
                                        var inbox = db.Inboxes.Where(x => x.ThreadId == threadId).FirstOrDefault();
                                        inbox.HasUnread = true;
                                        db.Entry(inbox).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    sresult.Message = "Success";
                                    sresult.Success = true;
                                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Send Message to Thread {0}", threadId), "Success", hostId.ToString(), "Airbnb");
                                }
                            }
                            catch (Exception e) { }

                        }
                    }
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in sending message to thread {0} {1} {2}", threadId, e, e.InnerException), "Error", hostId.ToString(), "Airbnb");

                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                    sresult.Message = e.Message;
                    //Debug.WriteLine("Send Message Airbnb : " + e.Message);
                }
            }
            else
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to send message to Thread {0}, Please check your login credentials", threadId), "Error", hostId.ToString(), "Airbnb");

                sresult.Message = "Could not login, Please check you details and try again.";
            }

            return sresult;
        }
        #endregion
        public Message ScrapeLastMessage(string threadId, string userId)
        {
            Message message = new Message();

            try
            {
                string url = string.Format("https://www.airbnb.com/z/q/{0}", threadId);
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.GET;


                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();
                    var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                    var json = meta.GetAttributeFromNode("content").HtmlDecode();
                    var ob = JObject.Parse(json);
                    var item = (JArray)ob["appData"]["posts"];
                    var currentUserId = ob["appData"]["currentUserId"].ToLong();

                    if (item.Count > 0)
                    {
                        var hostId = item[0]["user_id"].ToLong();
                        var myMessage = false;
                        if (hostId == currentUserId)
                        {
                            myMessage = true;
                        }

                        var messageContent = item[0]["message"].ToSafeString();
                        var createdAt = item[0]["created_at"].ToDateTime();
                        var messageId = item[0]["id"].ToSafeString();
                        if (!string.IsNullOrEmpty(messageContent))
                        {
                            message = new Message
                            {
                                ThreadId = threadId,
                                MessageId = messageId,
                                MessageContent = messageContent,
                                IsMyMessage = myMessage,
                                CreatedDate = createdAt,
                                UserId = userId.ToInt()
                            };
                            if (message.CreatedDate != null) { message.CreatedDate = TimeZoneInfo.ConvertTimeToUtc(message.CreatedDate); } /*TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(tm.CreatedDate.ToString()), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"));*/

                            //MessagesHub.UpdateThreadMessages(Database.Actions.Inboxes.GetHostIdByThreadId(threadId),message);
                        }

                    }
                }
                
                return message;
            }
            catch (Exception e)
            {

            }
            return message;
        }
        #region BOOKING ACTIONS
        public ScrapeResult AcceptOffer(string sessionToken, string reservationCode, string message, string userId)
        {

            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/z/a/" + reservationCode;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    sresult.Message = "Compage not valid";

                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var token = cc["_csrf_token"].Value.DecodeURL();
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("authenticity_token", token);
                        commPage.Parameters.Add("utf8", "✓");
                        commPage.Parameters.Add("message", message);
                        commPage.Parameters.Add("tos_confirm", "1");
                        commPage.Parameters.Add("decision", "accept");
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.Referer = "https://www.airbnb.com/z/a/" + reservationCode;


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var Success = ob["success"].ToSafeString();
                            if (commPage.Html.Contains("The reservation is no longer pending"))
                            {
                                sresult.Success = true;
                                sresult.IsComplete = false;
                            }
                            else
                            {
                                sresult.Success = true;
                                sresult.IsComplete = true;
                            }
                                try
                                {
                                   
                                    //JM 02/21/19 This code update inbox and inquiry status from B(booking) to A(Confirm) after post and scrape lastmessage
                                    using (var db = new ApplicationDbContext())
                                    {
                                        var reservation = db.Reservations.Where(x => x.ConfirmationCode == reservationCode).FirstOrDefault();
                                        reservation.BookingStatusCode = "A";
                                        db.Entry(reservation).State = EntityState.Modified;
                                        var messageContent = ScrapeLastMessage(reservation.ThreadId, userId);
                                        Database.Actions.Messages.Add(messageContent);
                                        var inbox = db.Inboxes.Where(x => x.ThreadId == reservation.ThreadId).FirstOrDefault();
                                        inbox.HasUnread = true;
                                        inbox.Status = "A";
                                        db.Entry(inbox).State = EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                                catch (Exception e) { }
                            
                            //AutomatedResponse.ConfirmReservation(this, sessionToken, reservationCode);
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Accept offer w/ reservation code {0}", reservationCode), "Success", hostId.ToString(), "Airbnb");

                            return sresult;
                        }
                    }
                }
                catch (Exception e)
                {
                    //Core.Helper.EmailService.SendEmail("Error in accepting offer", string.Format("Exception in accepting offer {0} {1} {2}", reservationCode, e, e.InnerException), GlobalVariables.ErrorEmail());
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in accepting offer {0} {1} {2}", reservationCode, e, e.InnerException), "Error", hostId.ToString(), "Airbnb");

                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                    sresult.Message = e.Message;
                }
            }
            else
            {
                //Core.Helper.EmailService.SendEmail("Error in accepting offer", string.Format("Failed to accept offer, pls check login credentials"), GlobalVariables.ErrorEmail());
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to accept offer, pls check login credentials"), "Warning", hostId.ToString(), "Airbnb");
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        public ScrapeResult DeclineOffer(string sessionToken, string reservationCode, string reason)
        {
            //int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(sessionToken);
            var sresult = new ScrapeResult();
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {
                try
                {
                    commPage.RequestURL = "https://www.airbnb.com/z/a/" + reservationCode;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    sresult.Message = "Compage not valid";
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = "https://www.airbnb.com/reservation/approve";
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("code", reservationCode);
                        commPage.Parameters.Add("decline_reason", reason);
                        commPage.Parameters.Add("block_calendar", "true");
                        commPage.Parameters.Add("decision", "decline");
                        commPage.Parameters.Add("authenticity_token", token);
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.Referer = "https://www.airbnb.com/z/a/" + reservationCode;


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            if (commPage.Uri.AbsolutePath.Contains("my_reservations"))
                            {
                                sresult.Message = "Operation Successful.";
                                sresult.Success = true;
                                sresult.IsComplete = true;
                               
                            }
                            else
                            {
                                sresult.Success = true;
                                sresult.IsComplete = false;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    sresult.Message = e.Message;
                }
            }
            sresult.Message = "Could not login, Please check you details and try again.";
            return sresult;
        }

        public Tuple<bool, string, string, string> GetAlterReservationDetail(string sessionToken, string reservationCode, DateTime newCheckInDate, DateTime newCheckoutDate, decimal price, string listingId, int guests)
        {
            //int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(sessionToken);
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {


                try
                {
                    commPage.RequestURL = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var chkInDateStr = newCheckInDate.ToString("MM/dd/yyyy");
                        var chkOutdateStr = newCheckoutDate.ToString("MM/dd/yyyy");
                        var xd = commPage.ToXml();

                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var token = cc["_csrf_token"].Value.DecodeURL();

                        var url = string.Format("https://www.airbnb.com/reservation_alterations/ajax_price_and_availability?"
                          + "authenticity_token={0}&code={1}&pricing%5Bhosting_id%5D={2}&pricing%5Bguests%5D={3}&pricing%5Bstart_date%5D={4}&pricing%5Bend_date%5D={5}&price={6}",
                          token, reservationCode, listingId, guests, chkInDateStr, chkOutdateStr, price);

                        commPage.RequestURL = url;
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var Success = ob["status"].ToSafeString();
                            var message = ob["availability_response"].ToSafeString();
                            var total = ob["total_price"].ToSafeString();
                            var priceDifference = ob["price_difference_string"].ToSafeString();
                            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get alter reservation details"), "Success", hostId.ToString(), "Airbnb");

                            return new Tuple<bool, string, string, string>(Success == "success", message, priceDifference, total);
                        }
                    }
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in Getting alter reservation details reservationCode:{0} {1} {2}", reservationCode, e, e.InnerException), "Error", hostId.ToString(), "Airbnb");

                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                }
            }
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to get alter reservation details, Pls check login credentials"), "Warning", hostId.ToString(), "Airbnb");

            return new Tuple<bool, string, string, string>(false, "Could not login, Please check you details and try again.", "", "");
        }
        public bool ProcessAlterReservation(string reservationCode, DateTime checkInDate, DateTime checkoutDate, decimal price, string listingId, int guests, string token = "")
        {
            //int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(token);
            try
            {
                var chkInDateStr = checkInDate.ToString("MM/dd/yyyy");
                var chkOutdateStr = checkoutDate.ToString("MM/dd/yyyy");
                var xd = commPage.ToXml();
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var csrfToken = cc["_csrf_token"].Value.DecodeURL();
                var url = "https://www.airbnb.com/reservation_alterations";
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("code", reservationCode);
                commPage.Parameters.Add("pricing[hosting_id]", listingId);
                commPage.Parameters.Add("pricing[guests]", guests.ToSafeString());
                commPage.Parameters.Add("pricing[start_date]", chkInDateStr);
                commPage.Parameters.Add("pricing[end_date]", chkOutdateStr);
                commPage.Parameters.Add("price", price.ToSafeString());
                commPage.Parameters.Add("authenticity_token", csrfToken);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);


                ProcessRequest(commPage);

                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsoluteUri.Contains("reservation/alteration/"))
                    {//06/14/19 temporary removed by Joe Mari because its not yet supported
                        //AutomatedResponse.AlterReservation(this, token, reservationCode);
                        //AutomatedResponse.AlterReservation(this, token, reservationCode);
                        //Helper.ElmahLogger.LogInfo(string.Format("Alter reservation details reservation code>{0}", reservationCode));
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Alter reservation w/ reservation code {0}", reservationCode), "Success", hostId.ToString(), "Airbnb");

                        return true;
                    }
                }
                else
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to alter reservation w/ reservation code {0} because compage is invalid", reservationCode), "Error", hostId.ToString(), "Airbnb");

                    //Helper.ElmahLogger.LogInfo(string.Format("An error occured in alter reservation details reservationCode >>{0}", reservationCode));
                    return false;
                }
            }
            catch (Exception e)
            {
                //Core.Helper.EmailService.SendEmail("Error in process alteration", string.Format("Exception in process alter reservation code {0} {1} {2}", reservationCode, e, e.InnerException), GlobalVariables.ErrorEmail());
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in process alter reservation code {0} {1} {2}", reservationCode, e, e.InnerException), "Error", hostId.ToString(), "Airbnb");

                //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                //Helper.ElmahLogger.LogError(e, GetCurrentMethod());
            }
            return false;
        }
        //Ison Code
        public bool ProcessAlterReservation(string reservationCode, DateTime checkInDate, DateTime checkoutDate, decimal price, string listingId, int guests)
        {
            var hostId = "";
            using (var db = new ApplicationDbContext())
            {
                hostId = db.Properties.Where(x => x.ListingId == listingId.ToLong()).FirstOrDefault().HostId.ToSafeString();
            }
            try
            {
                var chkInDateStr = checkInDate.ToString("MM/dd/yyyy");
                var chkOutdateStr = checkoutDate.ToString("MM/dd/yyyy");
                var xd = commPage.ToXml();
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();
                var url = "https://www.airbnb.com/reservation_alterations";
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("code", reservationCode);
                commPage.Parameters.Add("pricing[hosting_id]", listingId);
                commPage.Parameters.Add("pricing[guests]", guests.ToSafeString());
                commPage.Parameters.Add("pricing[start_date]", chkInDateStr);
                commPage.Parameters.Add("pricing[end_date]", chkOutdateStr);
                commPage.Parameters.Add("price", price.ToSafeString());
                commPage.Parameters.Add("authenticity_token", token);
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsoluteUri.Contains("reservation_alterations/"))
                    {
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Alter reservation w/ reservation code", reservationCode), "Success", hostId.ToString(), "Airbnb");

                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in process alter reservation"), "Error", hostId.ToString(), "Airbnb");

                //ElmahLogger.LogInfo(string.Format("Error {0}", e));
            }
            return false;
        }

        public bool CancelAlteration(string sessionToken, string reservationCode)
        {
            var isloaded = ValidateTokenAndLoadCookies(sessionToken);
            if (isloaded)
            {

                try
                {
                    commPage.RequestURL = commPage.Referer = string.Format("https://www.airbnb.com/reservation/change?code={0}&visited=1", reservationCode);
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();


                    ProcessRequest(commPage);
                    string alterationId = "";
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        if (xd != null)
                        {
                            //var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-alter_cancel_data']");
                            //var json = meta.GetAttributeFromNode("content").HtmlDecode();
                            //var ob = JObject.Parse(json);
                            //var alts = (JArray)ob["reservation"]["reservation_alterations"];
                            //if (alts != null)
                            //{
                            //  alterationId = alts[0]["reservation_alteration"]["id"].ToSafeString();
                            //}
                            var meta = xd.SelectSingleNode("//script[@data-hypernova-key='alteration_redesignbundlejs']");
                            var json = meta.GetInnerTextFromNode().HtmlDecode().Trim();
                            json = json.Replace("//\r\n", "");
                            json = json.Replace("<!--", "");
                            json = json.Replace("-->", "");
                            var ob = JObject.Parse(json);
                            var alts = ob["bootstrapData"]["reduxBootstrap"]["entities"]["alteration"];
                            if (alts != null)
                            {
                                alterationId = alts["id"].ToSafeString();
                            }
                        }
                    }

                    var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                    var token = cc["_csrf_token"].Value.DecodeURL();

                    var url = "https://www.airbnb.com/reservation_alterations/cancel/" + alterationId;
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.POST;

                    commPage.Parameters = new Dictionary<string, string>();
                    commPage.Parameters.Add("utf8", "✓");
                    commPage.Parameters.Add("authenticity_token", token);
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.Referer = string.Format("https://www.airbnb.com/reservation_alterations/{0}", alterationId);


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Cancel Alteration w/reservation code {0}", reservationCode), "Success", hostId.ToString(), "Airbnb");

                        return true;
                    }
                }
                catch (Exception e)
                {
                    //Core.Helper.EmailService.SendEmail("Error in cancel alteration", string.Format("Exception in calcel alteration w/ reservation code {0} {1} {2}", reservationCode, e, e.InnerException), GlobalVariables.ErrorEmail());
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Exception in calcel alteration w/ reservation code {0} {1} {2}", reservationCode, e, e.InnerException), "Error", hostId.ToString(), "Airbnb");

                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                }
            }
            //Core.Helper.EmailService.SendEmail("Error in cancel alteration", string.Format("Failed to Cancel alteration, Invalid Token"), GlobalVariables.ErrorEmail());
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to Cancel alteration, Invalid Token"), "Warning", hostId.ToString(), "Airbnb");

            return false;
        }
        #endregion

        #region Special Offer
        public SpecialOfferSummary GetSpecialOfferSummary(string token, string listingId, string guestId, DateTime startDate, DateTime enddate, int guests, DateTime currentStart, DateTime currentEnd)
        {
            var summary = new SpecialOfferSummary();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                //int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(token);
                try
                {
                    if (startDate == currentStart && enddate == currentEnd)
                    {
                        summary.IsAvailable = true;
                    }
                    else
                    {
                        summary.IsAvailable = IsAvailableForDate(listingId, startDate, enddate);
                    }
                    if (summary.IsAvailable)
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/api/v2/pricing_quotes?_format=for_detailed_booking_info_on_web_p3&_intents=p3_book_it&listing_id={0}&check_in={1}&check_out={2}&guests={3}&key=d306zoyjsyarp7ifhu67rjxn52tv0t20&currency=CAD&locale=en", listingId, startDate.Date.ToString("yyyy-MM-dd"), enddate.Date.ToString("yyyy-MM-dd"), guests);
                        commPage.ReqType = CommunicationPage.RequestType.GET;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                        commPage.RequestHeaders.Add("X-CSRF-Token", auth_token);


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            var amount = ((JArray)ob["pricing_quotes"])[0]["p3_display_rate_with_cleaning_fee"]["amount"].ToSafeString().ToDecimal();
                            summary.Subtotal = amount;

                            commPage.RequestURL = string.Format("https://www.airbnb.com/rooms/pricing/?pricing[start_date]={1}&pricing[end_date]={2}&pricing[hosting_id]={0}&pricing[price]={3}&pricing[is_inline]=true&pricing[guest_id]={4}&pricing[number_of_guests]={5}&pricing[currency]=CAD", listingId, startDate.Date.ToString("yyyy-MM-dd"), enddate.Date.ToString("yyyy-MM-dd"), amount, guestId, guests);
                            commPage.ReqType = CommunicationPage.RequestType.GET;
                            commPage.RequestHeaders = new Dictionary<string, string>();

                            commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                            commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                            commPage.RequestHeaders.Add("X-CSRF-Token", token);


                            ProcessRequest(commPage);
                            if (commPage.IsValid)
                            {
                                ob = JObject.Parse(commPage.Html);
                                summary.GuestPays = ob["guest_pays_native"].ToSafeString().ToDecimal();
                                summary.HostEarns = ob["host_earns_native"].ToSafeString().ToDecimal();
                                summary.IsSuccess = true;
                                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Get special offer summary"), "Success", hostId.ToString(), "Airbnb");

                            }
                        }
                    }
                    else
                    {
                        //summary.IsSuccess = false;
                        //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Date are not available"), "Warning", hostId.ToString(), "Airbnb");
                    }

                    return summary;
                }
                catch (Exception e)
                {
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error: {0} {1}", e, e.InnerException), "Error", hostId.ToString(), "Airbnb");
                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                }
            }
            return summary;
        }

        public bool SendSpecialOffer(string token, string listingId, string threadId, int guests, DateTime startDate, DateTime enddate, string price)
        {
            //int hostId = Core.API.Airbnb.HostSessionCookies.GetHostId(token);
            var summary = new SpecialOfferSummary();
            var isloaded = ValidateTokenAndLoadCookies(token);
            if (isloaded)
            {
                try
                {
                    summary.IsAvailable = IsAvailableForDate(listingId, startDate, enddate);
                    if (summary.IsAvailable)
                    {
                        var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                        var auth_token = cc["_csrf_token"].Value.DecodeURL();

                        commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/qt_reply_v2/{0}", threadId);
                        commPage.PersistCookies = true;
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.Parameters = new Dictionary<string, string>();
                        commPage.Parameters.Add("authenticity_token", auth_token);
                        commPage.Parameters.Add("pricing[hosting_id]", listingId);
                        commPage.Parameters.Add("pricing[start_date]", startDate.Date.ToString("MM/dd/yyyy"));
                        commPage.Parameters.Add("pricing[end_date]", enddate.Date.ToString("MM/dd/yyyy"));
                        commPage.Parameters.Add("pricing[guests]", guests.ToSafeString());
                        commPage.Parameters.Add("pricing[unit]", "nightly");
                        commPage.Parameters.Add("template", "2");
                        commPage.Parameters.Add("message", "Hi!");
                        commPage.Parameters.Add("pricing[price]", price.ToSafeString());


                        ProcessRequest(commPage);
                        if (commPage.IsValid)
                        {
                            var ob = JObject.Parse(commPage.Html);
                            if (ob != null && ob["status"].ToSafeString() == "Sent!")
                            {
                                //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Sent special offer Thread {0}", threadId), "Success", hostId.ToString(), "Airbnb");

                                return true;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    //Core.Helper.EmailService.SendEmail("Error in send special offer", string.Format("Error: {0} {1}", e, e.InnerException), GlobalVariables.ErrorEmail());
                    //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Error: {0}", e, e.InnerException), "Error", hostId.ToString(), "Airbnb");
                    //ElmahLogger.LogInfo(string.Format("Error {0}", e));
                }
            }
            //Core.Helper.EmailService.SendEmail("Error in send special offer", string.Format("Failed to send special offer, pls check login credentials"), GlobalVariables.ErrorEmail());
            //Logger.LogDetails(GlobalVariables.UserId.ToString(), this.GetType().Name, MethodBase.GetCurrentMethod().Name, string.Format("Failed to send special offer, pls check login credentials"), "Warning", hostId.ToString(), "Airbnb");
            //ElmahLogger.LogInfo(string.Format("Error in send special offer"));
            return false;
        }

        public string GetRemoveOfferUrl(string id)
        {
            string url = string.Format("https://www.airbnb.com/z/q/{0}", id);
            commPage.RequestURL = url;
            commPage.ReqType = CommunicationPage.RequestType.GET;
            commPage.PersistCookies = true;


            ProcessRequest(commPage);
            if (commPage.IsValid)
            {
                XmlDocument xd = commPage.ToXml();
                var meta = xd.SelectSingleNode("//meta[@id='_bootstrap-qt2']");
                var json = meta.GetAttributeFromNode("content").HtmlDecode();
                var ob = JObject.Parse(json);
                var removeOfferUrl = ob["ajax_urls"]["remove_offer"].ToSafeString();
                if (!string.IsNullOrEmpty(removeOfferUrl))
                {
                    removeOfferUrl = "https://www.airbnb.com" + removeOfferUrl;
                    return removeOfferUrl;
                }
            }
            return string.Empty;
        }

        public bool WithdrawSpecialOffer(string url)
        {
            try
            {
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                commPage.PersistCookies = true;
                commPage.RequestURL = url;
                commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.Parameters = new Dictionary<string, string>();
                commPage.Parameters.Add("_method", "post");
                commPage.Parameters.Add("authenticity_token", token);

                commPage.Referer = "https://www.airbnb.com/z/q/326933359";


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    if (commPage.Uri.AbsolutePath.Contains("/z/q/"))
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                //ElmahLogger.LogInfo(string.Format("Error in withdraw special offer {0}", e));
            }

            return false;
        }
        #endregion

        #region IsAvailableForDate
        public bool IsAvailableForDate(string listingId, DateTime startDate, DateTime endDate)
        {
            try
            {
                var cc = commPage.COOKIES.GetCookies(new Uri("https://www.airbnb.com"));
                var token = cc["_csrf_token"].Value.DecodeURL();

                commPage.RequestURL = string.Format("https://www.airbnb.com/messaging/ajax_special_offer_dates_available?hosting_id={0}&start_date={1}&end_date={2}", listingId, startDate.Date.ToString("yyyy-MM-dd"), endDate.Date.ToString("yyyy-MM-dd"));
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, *; q=0.01");
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                commPage.RequestHeaders.Add("X-CSRF-Token", token);


                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    var ob = JObject.Parse(commPage.Html);
                    return ob["available"].ToSafeString().ToBoolean();
                }
            }
            catch { }
            return false;
        }
        #endregion
        #region Cookies Validating
        public bool LoadCookies(string token)
        {
            try
            {
                var result = false;
                var session = Database.Actions.HostSessionCookies.GetSessionByToken(token);
                if (session != null)
                {
                    try
                    {
                        var cookies = DeSerializeCookiesFromString(session.Cookies);
                        commPage.COOKIES = cookies;
                        result = true;
                    }
                    catch (Exception e)
                    {
                    }
                }
                else
                {
                    commPage.COOKIES = null;
                }
                return result;
            }
            catch { }
            return false;
        }
        public bool ValidateTokenAndLoadCookies(string token)
        {
            var hasValidCookies = LoadCookies(token);
            if (hasValidCookies)
            {
                string url = "https://www.airbnb.com/account-settings/personal-info";
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.ReqType = CommunicationPage.RequestType.GET;


                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        XmlDocument xd = commPage.ToXml();
                        if (xd != null)
                        {
                            var userId = xd.SelectSingleNode("//input[@name='user_id']").GetAttributeFromNode("value");
                            if (string.IsNullOrEmpty(userId))
                            {
                                var formAction = xd.SelectSingleNode("//form[@id='update_form']").GetAttributeFromNode("action");
                                if (!string.IsNullOrEmpty(formAction))
                                    userId = formAction.Replace("/update/", "");
                                if (!string.IsNullOrEmpty(userId)) return true;
                            }

                            if (string.IsNullOrEmpty(userId))
                            {
                                var json = xd.SelectSingleNode("//script[@id='data-state']").GetInnerTextFromNode().HtmlDecode().Trim();
                                if (!string.IsNullOrEmpty(json))
                                {
                                    json = json.Replace("//\r\n", "");
                                    json = json.Replace("<!--", "");
                                    json = json.Replace("-->", "");
                                    var ob = JObject.Parse(json);
                                    var hostData = ob["bootstrapData"]["reduxData"]["personalInfo"]["user"];
                                    userId = hostData["id"].ToSafeString();

                                    if (!string.IsNullOrEmpty(userId)) return true;
                                }
                            }
                            if (string.IsNullOrEmpty(userId))
                            {
                                using (var db = new ApplicationDbContext())
                                {
                                    var entity = db.HostSessionCookies.Where(x => x.Token == token).FirstOrDefault();
                                    if (entity != null)
                                    {
                                        entity.IsActive = false;
                                        db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }
            return false;
        }
        #endregion

        public void SaveAirlockCookies(string username)
        {
            var serializedCookies = SerializeCookiesToString(commPage.COOKIES);
            Database.Actions.AirlockSessions.AddOrUpdate(new AirlockSession { Cookies = serializedCookies, Username = username, SiteType = 1 });
        }
        public CommunicationPage InitializeCommunication(string username, string url = "")
        {
            var newCommPage = new CommunicationPage("https://www.airbnb.com/");
            if (!string.IsNullOrEmpty(url))
                newCommPage = new CommunicationPage(url);
            try
            {
                var session = Database.Actions.AirlockSessions.GetAirlockSession(username, 1);
                if (session != null)
                {
                    var cookies = DeSerializeCookiesFromString(session.Cookies);
                    newCommPage.COOKIES = cookies;
                }
            }
            catch (Exception e)
            {
            }

            return newCommPage;
        }

        #region DISPOSE
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
            }
            disposed = true;
        }
        #endregion
    }
}
