﻿using BlackListScrapper.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace BlackListScrapper.Sites
{
    public class AllSites
    { //Post a request to change public Ip of instance
        public static void Rotate()
        {
            try
            {
                lock (GlobalVariables.RotateLock)
                {
                    using (HttpClient client = new HttpClient())
                    {
                        HttpRequestMessage requestMessage = new HttpRequestMessage(HttpMethod.Post, GlobalVariables.RotateUrl());
                        HttpResponseMessage response = client.SendAsync(requestMessage).GetAwaiter().GetResult();
                    }
                }
            }
            catch
            {

            }
        }
        //Joem 06/05/19 Save html as text for logs purpose
        public static void SaveHtmlText(string title,string html)
        {
            try
            {
                string path = GlobalVariables.HtmlFolder + @"\" + title + ".txt";
                System.IO.Directory.CreateDirectory(GlobalVariables.HtmlFolder);
                System.IO.File.WriteAllText(path, html);
                //if (!File.Exists(path))
                //{

                //    using (StreamWriter sw = File.CreateText(path))
                //    {
                //        sw.Write(html);

                //    }

                //}
            }
            catch (Exception e){ }
        }
    }
}