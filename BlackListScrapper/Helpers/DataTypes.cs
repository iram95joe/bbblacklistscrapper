﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Helpers
{
    public class DataTypes
    {
        public static string DictionaryToString(Dictionary<string, string> d)
        {
            // Build up each line one-by-one and then trim the end
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<string, string> pair in d)
            {
                builder.Append(pair.Key).Append(":").Append(pair.Value).Append(',');
            }
            string result = builder.ToString();
            // Remove the final delimiter
            result = result.TrimEnd(',');

            return result;
        }

        public static Dictionary<string, string> StringToDictionary(string s)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            // Divide all pairs (remove empty strings)
            string[] tokens = s.Split(new char[] { ':', ',' },
                StringSplitOptions.RemoveEmptyEntries);
            // Walk through each item
            for (int i = 0; i < tokens.Length; i += 2)
            {
                string name = tokens[i];
                string freq = "";

                try
                {
                    freq = tokens[i + 1];
                }
                catch (IndexOutOfRangeException e)
                {
                    freq = "";
                }

                // Parse the int (this can throw)
                //int count = int.Parse(freq);
                // Fill the value in the sorted dictionary
                //if (d.ContainsKey(name))
                //{
                //    d[name] += count;
                //}
                //else
                //{
                //    d.Add(name, freq);
                //}

                d.Add(name, freq);
            }

            return d;
        }
    }
}
