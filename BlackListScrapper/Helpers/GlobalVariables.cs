﻿using BlackListScrapper.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BlackListScrapper.Helpers
{
    public class GlobalVariables
    {
        public static object RotateLock = new object (); 
        public static string CaptchaAPIKey
        {
            get
            {
                return ConfigurationSettings.AppSettings["CaptchaAPIKey"].ToSafeString();
            }
        }
        public static string HtmlFolder { get; set; }

        //JM 06/07/19 List of host that have pending Login request, this is created to allow 1 login request per host.
        public static List<int> HostWithPendingLoginRequest = new List<int>();
        //public static List<AirlockInstance> AirbnbAirlockInstanceList = new List<AirlockInstance>();

        public static string GoogleApiKey()
        {
            return ConfigurationSettings.AppSettings["GoogleApiKey"].ToString();
        }
        public static string RotateUrl()
        {
            return ConfigurationSettings.AppSettings["AirbnbRotateUrl"].ToString();
            //switch (siteType)
            //{
            //    case 1:
            //        return ConfigurationSettings.AppSettings["AirbnbRotateUrl"].ToString();
            //        break;
            //    case 2:
            //        return ConfigurationSettings.AppSettings["VrboRotateUrl"].ToString();
            //        break;
            //    case 3:
            //        return ConfigurationSettings.AppSettings["BookingComRotateUrl"].ToString();
            //        break;

            //    case 4:
            //        return ConfigurationSettings.AppSettings["HomeawayRotateUrl"].ToString();
            //        break;
            //    default:
            //        return "";
            //}
        }
        public static string ApiBaseUrl()
        {
            return ConfigurationSettings.AppSettings["ScrapperUrl"].ToString();
      
        }
        public static string Cookie { get; set; }

        public static string Username { get; set; }


        public static Database.Entity.User User
        {
            get
            {
                if (HttpContext.Current.Session["User"] == null)
                {
                    return null;
                }
                else
                {
                    return (Database.Entity.User)HttpContext.Current.Session["User"];
                }
            }
            set
            {
                HttpContext.Current.Session["User"] = value;
            }
        }

        
        //public static BaseScrapper.CommunicationPage StaticCommPage { get; set; }
        //public static BaseScrapper.CommunicationPage CommPage { get; set; }

        public static int UserId
        {
            get
            {
                if (HttpContext.Current.Session["UserId"] == null)
                {
                    return 0;
                }
                else
                {
                    return HttpContext.Current.Session["UserId"].ToInt();
                }
            }
            set
            {
                HttpContext.Current.Session["UserId"] = value;
            }
        }
        public static int CompanyId
        {
            get
            {
                if (HttpContext.Current.Session["CompanyId"] == null)
                {
                    return 0;
                }
                else
                {
                    return HttpContext.Current.Session["CompanyId"].ToInt();
                }
            }
            set
            {
                HttpContext.Current.Session["CompanyId"] = value;
            }
        }
    }
}
