﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Helpers
{
    public class Captcha
    {
        public static Tuple<bool, string> SolveCaptcha(string apiKey, string siteKey, string pageUrl)
        {
            int attemp = 0;
            var result = SendRecaptchav2Request(apiKey, siteKey, pageUrl);
            if (result.Item1 == true)
            {
                Retry:
                System.Threading.Thread.Sleep(1000 * 15);
                var result2 = GetCaptchaResponse(apiKey, result.Item2);
                if (result2.Item1 == false && result2.Item2 == "CAPCHA_NOT_READY" && attemp<5)
                {
                    attemp++;
                    goto Retry;
                }

                return result2;
            }
            else
            {
                return result;
            }

        }

        public static Tuple<bool, string> SendRecaptchav2Request(string apiKey, string siteKey, string pageUrl)
        {
            //POST
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;
                var request = (HttpWebRequest)WebRequest.Create("http://2captcha.com/in.php");

                var postData = string.Format("key={0}&method=userrecaptcha&googlekey={1}&pageurl={2}", apiKey, siteKey, pageUrl);
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.Timeout = 1000 * 120;
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //  GET
                if (responseString.Contains("OK|"))
                {
                    return new Tuple<bool, string>(true, responseString.Substring(3));
                }
                else
                {
                    return new Tuple<bool, string>(false, responseString);
                }
            }
            catch (Exception e)
            {
                string tt = e.Message;
                return new Tuple<bool, string>(false, tt);

            }

        }

        public static Tuple<bool, string> GetCaptchaResponse(string apiKey, string captchaId)
        {
            var retryCount = 0;
            Retry:
            try
            {

                System.Net.ServicePointManager.Expect100Continue = false;
                var url = string.Format("http://2captcha.com/res.php?key={0}&action=get&id={1}", apiKey, captchaId);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 1000 * 60 * 3;
                request.Method = "GET";

                request.ContentType = "application/x-www-form-urlencoded";
                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //  GET
                if (responseString.Contains("OK|"))
                {
                    return new Tuple<bool, string>(true, responseString.Substring(3));
                }
                else
                {
                    return new Tuple<bool, string>(false, responseString);
                }
            }
            catch (Exception e)
            {
                string tt = e.Message;
                if (tt.Contains("timed out") && retryCount < 5)
                {
                    retryCount++;
                    goto Retry;
                }
                return new Tuple<bool, string>(false, tt);
            }
        }
    }
}
