﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BlackListScrapper.Helpers
{
    public class Utilities
    {
        public static Tuple<DateTime, DateTime> GetCheckinCheckout(string dateRange)
        {
            var dates = dateRange.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (dates.Length == 2)
            {
                var year1 = Utilities.SplitSafely(dates[0], ",", 1);
                var year2 = Utilities.SplitSafely(dates[1], ",", 1);

                var month1 = Utilities.SplitSafely(dates[0], " ", 0);
                var date1 = Utilities.SplitSafely(dates[0], " ", 1).Trim(',');

                var month2 = "";
                var date2 = "";

                var rawDate2WithoutYear = Utilities.SplitSafely(dates[1], ",", 0);
                var date2parts = rawDate2WithoutYear.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (date2parts.Length == 2)
                {
                    month2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 0);
                    date2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 1);
                }
                else
                {
                    date2 = Utilities.SplitSafely(rawDate2WithoutYear, " ", 0);
                }

                if (string.IsNullOrEmpty(year1))
                    year1 = year2;
                if (string.IsNullOrEmpty(month2))
                    month2 = month1;

                var datefrom = string.Format("{0} {1} {2}", month1, date1, year1);
                var dateto = string.Format("{0} {1} {2}", month2, date2, year2);

                var dtfrom = datefrom.ToDateTime();
                var dtto = dateto.ToDateTime();
                return new Tuple<DateTime, DateTime>(dtfrom, dtto);
            }
            return null;
        }
        public static string GetReservationStatusCode(string status)
        {
            if (status == "INQUIRY" || status == "REPLIED" || status == "QUOTE_SENT" || status == "RESERVATION_REQUEST_EXPIRED") return "I";
            else if (status == "PAYMENT_REQUEST_SENT") return "PAYMENT_REQUEST_SENT";
            else if (status == "RESERVATION_REQUEST_DECLINED") return "IC";
            else if (status == "POST_STAY" || status == "BOOKING" || status == "STAYING") return "A";
            else if (status == "RESERVATION_REQUEST") return "B";
            else if (status == "CANCELLED") return "BC";
            else return status;
        }
        public static string GetDefaultAvatar(string name)
        {
            var initial = string.IsNullOrEmpty(name) ? "" : name[0].ToString().ToUpper();
            return string.IsNullOrEmpty(initial) ? string.Empty : ("/Content/img/default-avatar/" + initial + ".png");
        }
        public static TimeSpan ExtractCheckInTime(string input)
        {
            int time = 0;
            string meridiem = "";
            int.TryParse(Regex.Match(input, @"\d+").Value, out time);
            meridiem = input.ToUpper().Contains("AM") ? "AM" : input.ToUpper().Contains("PM") ? "PM" : "";
            if (time != 0 && (meridiem == "AM" || meridiem == "PM"))
            {
                return new TimeSpan(meridiem == "AM" ? time : time + 12, 0,0);
            }
            else
            {
                return new TimeSpan(16, 0, 0);
            }
        }
        public static TimeSpan ExtractCheckOutTime(string input)
        {
            int time = 0;
            string meridiem = "";
            int.TryParse(Regex.Match(input, @"\d+").Value, out time);
            meridiem = input.ToUpper().Contains("AM") ? "AM" : input.ToUpper().Contains("PM") ? "PM" : "";
            if (time != 0 && (meridiem == "AM" || meridiem == "PM"))
            {
                return new TimeSpan(meridiem == "AM" ? time : time + 12, 0,0);
            }
            else
            {
                return new TimeSpan(11, 0, 0);
            }
        }

        public static decimal GetPrice(string input)
        {
            decimal result = 0;
            string price = input.Replace("CAD", "");
            price = price.Replace("$", "");
            result = price.ToDecimal();
            return result;

        }
        public static decimal ExtractDecimal(string input)
        {
            decimal result = 0;
            decimal.TryParse(Regex.Match(input, @"\d+(\.\d+)?").Value, out result);
            return result;
        }

        public static int ExtractNumber(string original)
        {
            int num = 0;
            int.TryParse(new string(original.Where(c => Char.IsDigit(c)).ToArray()), out num);
            return num;
        }


        public static int GetPaidStatus(string text)
        {
            switch (text)
            {
                case "new": return 0;
                case "paid": return 1;
                default: return 0;
            }
        }

        public static string SplitSafely(string source, string splitCharacter, int requiredNumberofElementZeroBase = -1, bool getLast = false)
        {
            string output = string.Empty;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    string special = "!";

                    source = source.Replace(splitCharacter, special);
                    string[] arr = source.Split(special.ToCharArray());
                    if (arr != null && arr.Length > requiredNumberofElementZeroBase)
                    {
                        if (requiredNumberofElementZeroBase > -1)
                        {
                            output = arr[requiredNumberofElementZeroBase].ToSafeString().Trim();
                        }
                        else if (getLast)
                        {
                            output = arr[arr.Length - 1].ToSafeString().Trim();
                        }

                    }
                }
                catch (Exception)
                {

                }
            }
            return output;
        }
    }
}