﻿using BlackListScrapper.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace BlackListScrapper.JobScheduler
{
    public class ReservationJobScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
            { "quartz.threadPool.threadCount", "1000"}
        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public static async void Start(string key, int hostId, int siteType,string userId)
        {
            scheduler = await factory.GetScheduler();

            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<ReservationJob>()
                .UsingJobData("hostId", hostId)
                .UsingJobData("siteType", siteType)
                .UsingJobData("userId", userId)
                .WithIdentity(key)
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                .WithDailyTimeIntervalSchedule
                (
                    s => s.WithIntervalInSeconds(60).OnEveryDay()
                )
                .Build();
            await scheduler.ScheduleJob(job, trigger);
        }

        public static void Stop(string key)
        {
            scheduler.DeleteJob(new JobKey(key));
        }
    }
}