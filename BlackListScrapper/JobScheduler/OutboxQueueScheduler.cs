﻿using BlackListScrapper.Job;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace BlackListScrapper.JobScheduler
{
    public class OutboxQueueScheduler
    {
        static NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
            { "quartz.threadPool.threadCount", "1000"}
        };
        static StdSchedulerFactory factory = new StdSchedulerFactory(props);
        static IScheduler scheduler;

        public static async void Start(string key, int hostId)
        {
            scheduler = await factory.GetScheduler();

            await scheduler.Start();

            IJobDetail job = JobBuilder.Create<OutboxQueueJob>()
                .UsingJobData("hostId", hostId)
                //.UsingJobData("siteType", siteType)
                //.UsingJobData("userId", userId)
                .WithIdentity(key)
                .Build();

            ITrigger trigger = TriggerBuilder.Create()
                // .WithIdentity("job1")
                .WithDailyTimeIntervalSchedule
                (
                    s => s.WithIntervalInSeconds(180).OnEveryDay()
                )
                .Build();

            //ITrigger trigger = TriggerBuilder.Create().StartNow()
            //    .WithSimpleSchedule(s =>
            //    s.WithIntervalInSeconds(120).RepeatForever()
            //).Build();



            await scheduler.ScheduleJob(job, trigger);
        }

        public static void Stop(string key)
        {
            scheduler.DeleteJob(new JobKey(key));
        }
    }
}