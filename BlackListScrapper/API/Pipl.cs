﻿using BaseScrapper;
using BlackListScrapper.Models;
using Pipl.APIs.Search;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BlackListScrapper.API
{
    public class Pipl : CommunicationEngine
    {
        public CommunicationPage commPage = null;
        public Pipl()
        {
            commPage = new CommunicationPage("https://www.beenverified.com");
        }
        public List<PeopleViewModel> SearchPipl(string firstName, string lastName, string city = null, string email = null, int? fromAge = null, int? toAge = null)
        {
            var List = new List<PeopleViewModel>();
            SearchAPIResponse responses = null;
            SearchConfiguration searchConfiguration = new SearchConfiguration(apiKey: "7lfl4pq857o8x77a821y8quo");
            //            List<Field> fields = new List<Field>()
            //{
            //    new Name(first: firstName,last: lastName),
            //    new Address(city: city),
            //    new Email(address:email),
            //    new DOB(dateRange: new DateRange( )
            //};


            SearchAPIRequest request = new SearchAPIRequest(
                email: email,
            firstName: firstName,
            lastName: lastName,
            city: city,
            fromAge: fromAge,
            toAge: toAge
            , requestConfiguration: searchConfiguration);


            try
            {
                responses = request.Send();
                foreach (var response in responses.PossiblePersons)
                {
                    PeopleViewModel temp = new PeopleViewModel();
                    temp.Name = string.Join(",", response.Names);
                    temp.BODate = response.DOB.DateRange.ToSafeString();
                    //temp.Details = response.Addresses[0].City;
                    List.Add(temp);
                }
            }
            catch (SearchAPIError e)
            {
                //Console.Out.WriteLine(e.ToString());
            }
            catch (IOException e)
            {
                // Console.Out.WriteLine(e.ToString());
            }
            return List;
        }
    }
}