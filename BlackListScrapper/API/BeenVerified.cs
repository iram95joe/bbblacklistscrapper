﻿using BaseScrapper;
using BlackListScrapper.Models;
using BlackListScrapper.Sites;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Xml;

namespace BlackListScrapper.API
{
    public class BeenVerified : CommunicationEngine
    {
        public BeenVerified()
        {
            commPage = new CommunicationPage("https://www.beenverified.com");
        }

        public CommunicationPage commPage = null;
        public bool LoadCookies(string username) {
            var cookies=  Database.Actions.SearchAccountSessions.GetCookies(username);
            if (cookies != null)
            {
                commPage.COOKIES = DeSerializeCookiesFromString(cookies);
                
            }
            else
            {
                LoginBV("kendrick_khoe@yahoo.com", "kendrickpublic3");
            }
            return true;
        }
        private static IEnumerable<Cookie> GetAllCookies(CookieContainer c)
        {
            Hashtable k = (Hashtable)c.GetType().GetField("m_domainTable", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(c);
            foreach (DictionaryEntry element in k)
            {
                SortedList l = (SortedList)element.Value.GetType().GetField("m_list", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(element.Value);
                foreach (var e in l)
                {
                    var cl = (CookieCollection)((DictionaryEntry)e).Value;
                    foreach (Cookie fc in cl)
                    {
                        yield return fc;
                    }
                }
            }
        }
        private string SerializeCookiesToString(CookieContainer cookieContainer)
        {
            var cookieList = GetAllCookies(cookieContainer).ToList();

            return JsonConvert.SerializeObject(cookieList);

        }
        private CookieContainer DeSerializeCookiesFromString(string cookie)
        {
            try
            {
                var cookies = JsonConvert.DeserializeObject<List<Cookie>>(cookie);
                CookieCollection cc = new CookieCollection();
                foreach (var cok in cookies)
                {
                    cc.Add(cok);
                }
                CookieContainer cont = new CookieContainer();

                cont.Add(cc);
                return cont;
            }
            catch (Exception e)
            {
            }
            return new CookieContainer();

        }
        public bool LoginBV(string email, string password)
        {
            var isretry = false;
            try
            {   Retry:
                string url = "https://www.beenverified.com/app/login";

                commPage = new CommunicationPage(url);
                //  commPage.ReqType = CommunicationPage.RequestType.POST;
                commPage.PersistCookies = true;
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    XmlDocument xd = commPage.ToXml();
                    if (xd != null)
                    {
                        var gResponse = "";
                        var captchaKey = "6LdMn2YUAAAAAHTVkprlqI803EQl0RIZ_o1js6rG";
                        var captchaResponse = Scrapper.SolveCaptcha("1c4213214ee89367845f9eb1b2f3dca1", captchaKey, commPage.Uri.AbsoluteUri);
                        if (captchaResponse.Item1 == true)
                        {
                            gResponse = captchaResponse.Item2;
                        }
                        var prms = new Dictionary<string, string>();
                        prms["user[email]"] = email;
                        prms["user[password]"] = password.Trim();
                        prms["g-recaptcha-response"] = gResponse;
                        prms["invisible-recaptcha"] = "true";
                        commPage.RequestURL = "https://www.beenverified.com/api/v5/session";
                        commPage.ReqType = CommunicationPage.RequestType.POST;
                        commPage.PersistCookies = true;
                        commPage.Referer = "https://www.beenverified.com/app/login";
                        commPage.Parameters = prms;
                        commPage.RequestHeaders = new Dictionary<string, string>();
                        commPage.RequestHeaders.Add("Accept", "application/json, text/javascript, */*; q=0.01");
                        commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");

                        ProcessRequest(commPage);
                        xd = commPage.ToXml();
                        if (commPage.IsValid)
                        {
                            if (commPage.Html.Contains("We do not see an account that matches that email/password combination"))
                            {
                                if (isretry == false)
                                {
                                    isretry = true;
                                    AllSites.Rotate();
                                    goto Retry;
                                }
                            }
                            else if (commPage.Uri.AbsoluteUri.Contains("v5/session"))
                            {
                                commPage.IsLoggedIn = true;
                                Database.Actions.SearchAccountSessions.AddOrUpdate(new Database.Entity.SearchAccountSession() { Type = 0, Username = email, Cookies = SerializeCookiesToString(commPage.COOKIES) });
                                return true;
                            }
                        }
                        else
                        {
                            if (isretry ==false) {
                                isretry = true;
                                AllSites.Rotate();
                                goto Retry;
                                    }
                        }
                    }
                }
            }
            catch (Exception e)
            {


            }
            return false;
        }
        public List<PeopleViewModel> SearchByName(string firstName, string lastName, string city, string age)
        {
            LoadCookies("kendrick_khoe@yahoo.com");
          
            var List = new List<PeopleViewModel>();
        
                string url = string.Format("https://www.beenverified.com/hk/teaser?age={2}&city={3}&exporttype=json&fn={0}&ln={1}&mn=&state=", firstName, lastName, age, city);
                try
                {
                    commPage.RequestURL = url;
                    commPage.PersistCookies = true;
                    commPage.ReqType = CommunicationPage.RequestType.GET;
                    commPage.RequestHeaders = new Dictionary<string, string>();
                    commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                    ProcessRequest(commPage);
                    if (commPage.IsValid)
                    {
                        var xd = commPage.ToXml();
                        var ob = JObject.Parse(commPage.Html);
                        var records = (JArray)ob["response"]["Records"]["Record"];
                        foreach (var record in records)
                        {
                            PeopleViewModel temp = new PeopleViewModel();
                            if (record["DOBs"] != null)
                            {
                                try
                                {
                                    var BODPath = record["DOBs"]["Item"][0]["DOB"];
                                    temp.BODate = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(BODPath["Month"].ToInt()) + " " + BODPath["Day"].ToSafeString() + " " + BODPath["Year"].ToSafeString();
                                }
                                catch { }
                             }
                            temp.Age = record["age"].ToSafeString();
                            temp.Link = record["link"].ToSafeString();

                            var details = ScrapePeopleDetails(record["bvid"].ToSafeString(), "");
                            var courtPath = details["people"][0]["courts"]["criminal"];
                                foreach (var court in courtPath)
                                {
                                    try
                                    {
                                    var country = "";
                                    var state = "";
                                    var courtoffense = "";
                                    var offensedate = "";
                                    var casenumber = "";
                                    var courtdate = "";
                                    var courtdescription = "";
                                    var courdesposition = "";
                                    var courtlevel = "";

                                    var offensesPath = court["offenses"][0];
                                    try { country = court["county"].ToSafeString(); } catch { }
                                    try { state = court["state"].ToSafeString(); } catch { }
                                    try {courtoffense = offensesPath["court"]["offense"].ToSafeString(); } catch { }
                                    try {offensedate = offensesPath["date"]["full"].ToSafeString().ToDateTime().ToString("MMM dd,yyyy"); } catch { }
                                    try {casenumber = court["case_number"].ToSafeString(); } catch { }
                                    try {courtdate = offensesPath["court"]["date"]["full"].ToSafeString(); } catch { }
                                    try {courtdescription = offensesPath["court"]["description"].ToSafeString(); } catch { }
                                    try {courdesposition = offensesPath["court"]["disposition"].ToSafeString(); } catch { }
                                    try {courtlevel = offensesPath["court"]["level"].ToSafeString(); } catch { }
                                    temp.CriminalRecord.Add("<b>" +country + "," + state+ "</b>" + courtoffense + "<br/>"
                                            + "Offense Date:" +  offensedate+ "<br/>Court Case Number:" + casenumber+
                                            "<br/>Court Date:" + courtdate + "<br/>Court Case Description:" + courtdescription +
                                            "<br/>Court Disposition:" + courdesposition + "<br/>Court Level:" +courtlevel + "<br/>Court Offense:" +courtoffense
                                            );
                                    }
                                    catch (Exception e) { }
                                }
                            
                                var socialMediaPath = details["people"][0]["social"]["profiles"];
                                foreach (var social in socialMediaPath)
                                {
                                    var site = social["site"].ToSafeString();
                                    temp.SocialMedia += (site != "" ? char.ToUpper(site[0]) + site.Substring(1) + ":" : "") + "<a href=\"" + social["url"].ToSafeString() + "\" title=\"\" target=\"_blank\">" + social["url"].ToSafeString() + "</a>,";
                                }

                                try
                                {
                                    var relativePath = details["people"][0]["connections"]["relatives"];
                                    foreach (var relative in relativePath)
                                    {
                                        temp.Relatives.Add(relative["identity"]["names"][0]["full"].ToSafeString());
                                    }
                                }
                                catch { }
                                try
                                {
                                    var neighborPath = details["people"][0]["connections"]["neighbors"];
                                    foreach (var neighbor in neighborPath)
                                    {
                                        temp.Neighbors.Add(neighbor["identity"]["names"][0]["full"].ToSafeString());
                                    }
                                }
                                catch { }

                                try
                                {
                                    var namePath = details["people"][0]["identity"]["names"];
                                    foreach (var name in namePath)
                                    {
                                        var tempName = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(name["full"].ToSafeString().ToLower());
                                        temp.Name += name["full"].ToSafeString() + ",";
                                    }
                                }
                                catch { }
                                try
                                {
                                    var addressPath = details["people"][0]["contact"]["addresses"];
                                    foreach (var address in addressPath)
                                    {
                                        temp.Address.Add(address["full"].ToSafeString());
                                    }
                                }
                                catch { }
                                try
                                {
                                    var emailPath = details["people"][0]["contact"]["emails"];
                                    foreach (var email in emailPath)
                                    {
                                        temp.Emails.Add(email["address"].ToSafeString().ToLower());
                                    }
                                }
                                catch { }
                                try
                                {
                                    var phonePath = details["people"][0]["contact"]["phones"];
                                    foreach (var phone in phonePath)
                                    {
                                        if (phone["number"] != null)
                                        {
                                            var tempNum = phone["number"].ToSafeString();
                                            var number = "(" + tempNum.Substring(0, 3) + ") " + tempNum.Substring(3, 3) + "-" + tempNum.Substring(6);
                                            temp.PhoneNumber.Add(number);
                                        }
                                    }
                                }
                                catch { }

                                List.Add(temp);
                            
                    }
                }}
                catch (Exception e)
                {
                }
            return List;
        }
        public JToken ScrapePeopleDetails(string bvId, string name)
        {
           
            bool isRetry = false;
            Retry:
            string url = string.Format("https://www.beenverified.com/app/generate/person?bvid={0}&name={1}", bvId, name);
            try
            {
                commPage.RequestURL = string.Format("https://www.beenverified.com/api/v5/reports/");
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.JSONPOST;
                commPage.JsonStringToPost = string.Format("{{\"report_type\":\"detailed_person_report\",\"meta\":{{\"person_id\":\"{0}\",\"report_flags\":[]}}}}", bvId);
                commPage.RequestHeaders = new Dictionary<string, string>();
                ProcessRequest(commPage);
                var ob = JObject.Parse(commPage.Html);
                if (commPage.Html.Contains("Access Denied") && isRetry ==false)
                {
                    LoginBV("kendrick_khoe@yahoo.com", "kendrickpublic3");
                    isRetry = true;
                    goto Retry;
                }
                    var id = ob["report"]["permalink"];
                url = string.Format("https://www.beenverified.com/api/v5/reports/{0}", id);

                int count = 1;

                count++;
                commPage.RequestURL = url;
                commPage.PersistCookies = true;
                commPage.ReqType = CommunicationPage.RequestType.GET;
                commPage.RequestHeaders = new Dictionary<string, string>();
                commPage.RequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
                ProcessRequest(commPage);
                if (commPage.IsValid)
                {
                    ob = JObject.Parse(commPage.Html);
                    var records = ob["entities"];
                    return records;
                }



            }
            catch (Exception e)
            {
            }


            return null;
        }
    }
}