﻿using System;
using System.Runtime.Serialization;

namespace BlackListScrapper.API
{
    [Serializable]
    internal class SearchAPIError : Exception
    {
        public SearchAPIError()
        {
        }

        public SearchAPIError(string message) : base(message)
        {
        }

        public SearchAPIError(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SearchAPIError(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}