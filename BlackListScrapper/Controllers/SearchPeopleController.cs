﻿using BlackListScrapper.API;
using BlackListScrapper.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlackListScrapper.Controllers
{
    public class SearchPeopleController : Controller
    {
        BeenVerified beenVerified = null;

        public BeenVerified BeenVerified
        {
            get
            {
                if (beenVerified == null)
                    beenVerified = new BeenVerified();

                return beenVerified;
            }
            set
            {
                beenVerified = value;
            }
        }
        API.Pipl pipl = null;

        public API.Pipl Pipl
        {
            get
            {
                if (pipl == null)
                    pipl = new API.Pipl();

                return pipl;
            }
            set
            {
                pipl = value;
            }
        }

        [HttpPost]
        public ActionResult Search(string firstname = null, string lastname = null, string city = null, string age = null, string email = null)
        {
            var data = GetPossiblePerson(firstname, lastname, city, age, email);

            return Json(new { success = true, data = data }, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        private List<PeopleViewModel> GetPossiblePerson(string firstname = null, string lastname = null, string city = null, string age = null, string email = null)
        {
            var List = new List<PeopleViewModel>();
            int? Age = age.ToInt() == 0 ? null : (int?)age.ToInt();
            //List.AddRange(Pipl.SearchPipl(firstname, lastname, city, email, Age, Age));
            List.AddRange(BeenVerified.SearchByName(firstname, lastname, city, age));
            return List;
        }
    }
}