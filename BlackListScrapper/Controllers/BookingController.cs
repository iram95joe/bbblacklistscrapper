﻿using BlackListScrapper.Database;
using BlackListScrapper.Database.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BlackListScrapper.Controllers
{
  public class BookingController : Controller
    {
        private enum AlterType
        {
            Inquiry, BookingRequest, PreApproved, ConfirmedBooking
        }

        #region AcceptBooking
        //Proccess AcceptBooking when there`s post request
        [HttpPost]
        public ActionResult AcceptBooking(int siteType, string hostId, string reservationCode, string message,string userId, string threadId,int OutboxId=0)
        {
            Task.Factory.StartNew(() =>
            {
            string resultMessage = "";
            Enumerations.SiteType site_type = (Enumerations.SiteType)siteType;

            if (site_type == Enumerations.SiteType.Airbnb)
            {
                using (var scrapper = new Sites.Airbnb())
                {
                    var result = scrapper.AcceptOffer(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), reservationCode, message,userId);
                        Outbox outbox = new Outbox() { BookingStatus = "A", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = reservationCode, ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Accept,UserId =userId.ToInt(),IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success && result.IsComplete)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was successfully accepted!", outbox);
                        }
                        else if (result.Success && !result.IsComplete)
                        { SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was already accepted!", outbox); }
                    }
            }
            else if (site_type == Enumerations.SiteType.VRBO)
            {
                using (var scrapper = new Sites.Vrbo())
                {
                    var result = scrapper.AcceptOffer(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), reservationCode, message, threadId,userId);
                        Outbox outbox = new Outbox() { BookingStatus = "A", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = reservationCode, ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Accept, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success && result.IsComplete)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was successfully accepted!", outbox);
                        }
                        else if (result.Success && !result.IsComplete)
                        { SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was already accepted!", outbox); }
                    }
            }
            else if (site_type == Enumerations.SiteType.Homeaway)
            {
                using (var scrapper = new Sites.Homeaway())
                {
                    var result = scrapper.AcceptOffer(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), reservationCode, message, threadId,userId);
                        Outbox outbox = new Outbox() { BookingStatus = "A", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = reservationCode, ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Accept, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success && result.IsComplete)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was successfully accepted!", outbox);
                        }
                        else if (result.Success && !result.IsComplete)
                        { SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was already accepted!", outbox); }
                    }
            }
            //}
            //return Json(new { success = isSuccess, message = resultMessage }, JsonRequestBehavior.AllowGet);
                //}
                //return Json(new { success = false });
            });
            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CheckReservationValidity

        public ActionResult CheckReservationValidity(int hostId, int siteType, string conversationId, DateTime checkInDate, DateTime checkoutDate, decimal price, int adults, int children, string bookingStatus)
        {
            Enumerations.SiteType type = (Enumerations.SiteType)siteType;

            switch (type)
            {
                case Enumerations.SiteType.VRBO:
                    using (var sm = new Sites.Vrbo())
                    {
                        var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());
                        var result = sm.GetAlterReservationDetail(conversationId, checkInDate, checkoutDate, price, adults, children, token);
                        return Json(new { success = result.Item1, message = result.Item2, difference = result.Item3 }, JsonRequestBehavior.AllowGet);
                    }

                case Enumerations.SiteType.Homeaway:
                    using (var sm = new Sites.Homeaway())
                    {
                        var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());
                        string amount = "0";

                        if (bookingStatus == "A")
                        {
                            amount = sm.EditReservation(token, conversationId, checkInDate, checkoutDate, adults, children).Item2.ToString();
                        }
                        else
                        {
                            amount = sm.GetReservationCalculatedPrice(token, conversationId, checkInDate, checkoutDate);
                        }

                        return Json(new { success = true, message = "Success", difference = amount.Replace("$", "").Replace(",", "") }, JsonRequestBehavior.AllowGet);
                    }

                default:
                    return Json(new { success = false, message = "An error occured", difference = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


        #region DeclineBooking
        [HttpPost]
        //Proccess DeclineBooking when there`s post request
        public ActionResult DeclineBooking(string userId,int siteType, string hostId, string reservationCode, string message, string threadId,string listingId, int OutboxId = 0)
        {
            Task.Factory.StartNew(() =>
            {
            bool isSuccess = false;
            string resultMessage = "";
            //if (Request.IsAjaxRequest())  
            //{
                //if (User.Identity.IsAuthenticated)
                //{
                    Enumerations.SiteType type = (Enumerations.SiteType)siteType;

            switch (type)
            {
                case Enumerations.SiteType.Airbnb:
                    using (Sites.Airbnb sm = new Sites.Airbnb())
                    {
                        var result = sm.DeclineOffer(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), reservationCode, message);
                            Outbox outbox = new Outbox() { BookingStatus = "C", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = reservationCode, ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.DeclineBooking, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                            if (OutboxId == 0)
                            {
                                Database.Actions.Outboxes.Add(outbox);
                            }
                            else
                            {
                                Database.Actions.Outboxes.Update(OutboxId, result.Success);
                            }
                            if (result.Success)
                            {
                                SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was successfully cancelled!", outbox);
                            }
                        } break;

                case Enumerations.SiteType.VRBO:
                    using (var scrapper = new Sites.Vrbo())
                    {
                        var result = scrapper.RejectOffer(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), reservationCode, "OTHER_V2", message, message, threadId);
                            Outbox outbox = new Outbox() { BookingStatus = "C", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = reservationCode, ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.DeclineBooking, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                            if (OutboxId == 0)
                            {
                                Database.Actions.Outboxes.Add(outbox);
                            }
                            else
                            {
                                Database.Actions.Outboxes.Update(OutboxId, result.Success);
                            }
                            if (result.Success)
                            {
                                SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was successfully cancelled!", outbox);
                            }
                        } break;

                //case Enumerations.SiteType.BookingCom:
                //    using (var scrapper = new Booking.com.ScrapeManager())
                //    {
                //        var result = scrapper.RequestCancelBooking(API.Booking.com.Hosts.GetToken(hostId.ToInt()), listingId, reservationCode);
                //        isSuccess = result.Item1;
                //        resultMessage = result.Item2;
                //    }
                //    break;

                case Enumerations.SiteType.Homeaway:
                    using (var scrapper = new Sites.Homeaway())
                    {
                        var result = scrapper.RejectOffer(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), reservationCode, "OTHER_V2", message, message, threadId);
                            Outbox outbox = new Outbox() { BookingStatus = "C", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = reservationCode, ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.DeclineBooking, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                            if (OutboxId == 0)
                            {
                                Database.Actions.Outboxes.Add(outbox);
                            }
                            else
                            {
                                Database.Actions.Outboxes.Update(OutboxId, result.Success);
                            }
                            if (result.Success)
                            {
                                SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Booking was successfully cancelled!", outbox);
                            }
                        }
                    break;

            }
            });
            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
        #region GetDetails
        //Process AlterBooking when there`s post request
        [HttpPost]
        public ActionResult GetDetails(int siteType, int hostId, string reservationId, string listingId, string checkinDate, string checkoutDate, string threadId)
        {
            bool isSuccess = false;
            //if (User.Identity.IsAuthenticated)// && HostSessionCookieManager.HasValidSession(hostId))
            //{
            Enumerations.SiteType site_type = (Enumerations.SiteType)siteType;
            
            var check_in_date = DateTime.Parse(checkinDate);
            var check_out_date = DateTime.Parse(checkoutDate);
            
            if (site_type == Enumerations.SiteType.Homeaway)
            {
                using (Sites.Homeaway sm = new Sites.Homeaway())
                {
                    var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());

                    var totalAmount = sm.GetReservationCalculatedPrice(token, reservationId, check_in_date, check_out_date);
                    isSuccess = true;

                    return Json(new { success = isSuccess, message = "Success!", totalAmount = totalAmount }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = isSuccess, message = "Error occur on alter booking" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region AlterBooking
        //Process AlterBooking when there`s post request
        [HttpPost]
        public ActionResult AlterBooking(int siteType, int hostId, string reservationCode, string listingId, string checkinDate, string checkoutDate, string price, string reservationId = "", bool submitAlteration = false, int id = 0, string threadId = "", int guests = 0, int adults = 0, int children = 0, int alterType = 1)
        {
            Task.Factory.StartNew(() =>
            {

                bool isSuccess = false;
            //if (User.Identity.IsAuthenticated)// && HostSessionCookieManager.HasValidSession(hostId))
            //{
                Enumerations.SiteType site_type = (Enumerations.SiteType)siteType;

                var reservation_code = reservationCode.ToSafeString();
                var listing_id = listingId;
                var check_in_date = DateTime.Parse(checkinDate);
                var check_out_date = DateTime.Parse(checkoutDate);
                var reservation_price = decimal.Parse(price);
              var guest_count = guests;

            if (site_type == Enumerations.SiteType.Airbnb)
            {
                var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());

                using (Sites.Airbnb sm = new Sites.Airbnb())
                {
                    var result = sm.GetAlterReservationDetail(token, reservation_code, check_in_date, check_out_date, reservation_price, listing_id, guest_count);
                    double totalPrice = (double)reservation_price;

                    try
                    {
                        totalPrice = (double)reservation_price +
                            Double.Parse(result.Item3.Remove(result.Item3.IndexOf(' ')), System.Globalization.NumberStyles.Currency);
                    }
#pragma warning disable CS0168 // The variable 'err' is declared but never used
                    catch (Exception err) { totalPrice = (double)reservation_price; }
#pragma warning restore CS0168 // The variable 'err' is declared but never used

                    if (result.Item1 == true)
                    {
                        if (submitAlteration)
                        {
                            var processResult = sm.ProcessAlterReservation(reservation_code, check_in_date, check_out_date, reservation_price, listing_id, guest_count, token);
                            //sm.ScrapeReservation(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), hostId, false, false, reservation_code);
                            if (processResult == true)
                                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                            else
                                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new { success = result.Item1, message = result.Item2, difference = result.Item3, totalPrice = totalPrice }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else if (site_type == Enumerations.SiteType.VRBO)
            {
                using (Sites.Vrbo sm = new Sites.Vrbo())
                {
                    var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());

                    if (sm.ValidateTokenAndLoadCookies(token))
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            AlterType type = (AlterType)alterType;
                            bool processResult = false;
                            var inq = (from i in db.Reservations where i.Id == id select i).FirstOrDefault();

                            var result = sm.GetAlterReservationDetail(threadId, check_in_date, check_out_date, reservation_price, adults, children);

                            if (submitAlteration && type == AlterType.ConfirmedBooking)
                            {
                                bool equalCheckInDate = (DateTime)inq.CheckInDate == check_in_date ? true : false;
                                bool equalCheckOutDate = (DateTime)inq.CheckOutDate == check_out_date ? true : false;
                                bool equalAdults = inq.Adult == adults ? true : false;
                                bool equalChildren = inq.Children == children ? true : false;

                                processResult = sm.EditReservation(token, threadId,
                                    equalCheckInDate ? null : (DateTime?)check_in_date,
                                    equalCheckOutDate ? null : (DateTime?)check_out_date,
                                    equalAdults ? null : (int?)adults,
                                    equalChildren ? null : (int?)children
                                    );

                                if (processResult == true)
                                {
                                    inq.IsAltered = true;
                                    db.SaveChanges();

                                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                            }
                            else if (submitAlteration && result.Item1 == true)
                            {
                                if (submitAlteration)
                                {
                                    if (type == AlterType.BookingRequest)
                                    {
                                        processResult = sm.ProcessAlterReservation(token, threadId, check_in_date, check_out_date, reservation_price, adults, children, "");
                                    }

                                    if (processResult == true)
                                    {
                                        inq.IsAltered = true;
                                        db.SaveChanges();

                                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                                }

                                inq.IsAltered = true;
                                db.SaveChanges();

                                return Json(new { success = result.Item1, message = result.Item2, difference = result.Item3 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
            }
            //else if (site_type == Enumerations.SiteType.BookingCom)
            //{
            //    using (var sm = new Booking.com.ScrapeManager())
            //    {
            //        var result = sm.ChangeAvailabilityAndPrice(API.Booking.com.Hosts.GetToken(hostId),
            //            listingId, reservationCode, reservationId, check_in_date, check_out_date, price.ToDouble());

            //        return Json(new { success = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
            //    }
            //}
            else if (site_type == Enumerations.SiteType.Homeaway)
            {
                using (Sites.Homeaway sm = new Sites.Homeaway())
                {
                    var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());

                    if (sm.ValidateTokenAndLoadCookies(token))
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            AlterType type = (AlterType)alterType;
                            bool processResult = false;
                            Tuple<bool, double, string> editResult = null;
                            var inq = (from i in db.Reservations where i.Id == id select i).FirstOrDefault();

                            var result = sm.GetAlterReservationDetail(threadId, check_in_date, check_out_date, reservation_price, adults, children);

                            if (submitAlteration && type == AlterType.ConfirmedBooking)
                            {
                                bool equalCheckInDate = (DateTime)inq.CheckInDate == check_in_date ? true : false;
                                bool equalCheckOutDate = (DateTime)inq.CheckOutDate == check_out_date ? true : false;
                                bool equalAdults = inq.Adult == adults ? true : false;
                                bool equalChildren = inq.Children == children ? true : false;

                                editResult = sm.EditReservation(token, threadId,
                                    equalCheckInDate ? null : (DateTime?)check_in_date,
                                    equalCheckOutDate ? null : (DateTime?)check_out_date,
                                    equalAdults ? null : (int?)adults,
                                    equalChildren ? null : (int?)children
                                    );

                                var _success = editResult.Item1;
                                var _amountSuggested = editResult.Item2;
                                var _amountCurrency = editResult.Item3;

                                if (_success == true)
                                {
                                    inq.IsAltered = true;
                                    db.SaveChanges();

                                    return Json(new { success = true, message = "Success", amount = _amountSuggested, currency = _amountCurrency }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                    return Json(new { success = false, message = "Error occur on alter booking", amount = _amountSuggested, currency = _amountCurrency }, JsonRequestBehavior.AllowGet);
                            }
                            else if (submitAlteration && result.Item1 == true)
                            {
                                if (submitAlteration)
                                {
                                    if (type == AlterType.BookingRequest || type == AlterType.PreApproved)
                                    {
                                        processResult = sm.ProcessAlterReservation(token, threadId, check_in_date, check_out_date, reservation_price, adults, children, "");
                                    }

                                    if (processResult == true)
                                    {
                                        inq.IsAltered = true;
                                        db.SaveChanges();

                                        return Json(new { success = true, message = "Success" }, JsonRequestBehavior.AllowGet);
                                    }
                                    else
                                        return Json(new { success = false, message = "Error occur on alter booking" }, JsonRequestBehavior.AllowGet);
                                }

                                inq.IsAltered = true;
                                db.SaveChanges();

                                return Json(new { success = result.Item1, message = result.Item2, difference = result.Item3 }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }
            }
            //}
            return Json(new { success = isSuccess, message = "Error occur on alter booking" }, JsonRequestBehavior.AllowGet);
            });
            return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region AddPaymentRequest
        // Process AddpaymentRequest when there`s post request
        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult AddPaymentRequest(int hostId, int siteType, string threadId, string description, DateTime dueDate, string message, float amount)
        {
            Enumerations.SiteType type = (Enumerations.SiteType)siteType;

            if (type == Enumerations.SiteType.VRBO)
            {
                using (var sm = new Sites.Vrbo())
                {
                    try
                    {
                        bool success = sm.AddPaymentRequest(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, description, dueDate, message, amount);

                        if (success)
                            return Json(new { success = true, message = "Payment request successfully added" }, JsonRequestBehavior.AllowGet);
                        else return Json(new { success = false, message = "Error occurred when trying to add payment request" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        return Json(new { success = false, message = "Error occurred when trying to add payment request" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else if (type == Enumerations.SiteType.Homeaway)
            {
                using (var sm = new Sites.Homeaway())
                {
                    try
                    {
                        bool success = sm.AddPaymentRequest(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, description, dueDate, message, amount);

                        if (success)
                            return Json(new { success = true, message = "Payment request successfully added" }, JsonRequestBehavior.AllowGet);
                        else return Json(new { success = false, message = "Error occurred when trying to add payment request" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        return Json(new { success = false, message = "Error occurred when trying to add payment request" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { success = false, message = "Error occurred when trying to add payment request" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SendRefund
        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult SendRefund(int hostId, int siteType, string threadId, short amount, string description)
        {
            Enumerations.SiteType type = (Enumerations.SiteType)siteType;

            if (type == Enumerations.SiteType.VRBO)
            {
                using (var sm = new Sites.Vrbo())
                {
                    try
                    {
                        bool success = sm.SendRefund(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, amount, description);

                        if (success)
                            return Json(new { success = true, message = "Refund successfully sent" }, JsonRequestBehavior.AllowGet);
                        else return Json(new { success = false, message = "Error occurred when trying to send refund" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        return Json(new { success = false, message = "Error occurred when trying to send refund" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else if (type == Enumerations.SiteType.Homeaway)
            {
                using (var sm = new Sites.Homeaway())
                {
                    try
                    {
                        bool success = sm.SendRefund(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, amount, description);

                        if (success)
                            return Json(new { success = true, message = "Refund successfully sent" }, JsonRequestBehavior.AllowGet);
                        else return Json(new { success = false, message = "Error occurred when trying to send refund" }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        return Json(new { success = false, message = "Error occurred when trying to send refund" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { success = false, message = "Error occurred when trying to send refund" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PreApproveInquiry
        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult PreApproveInquiry(string userId,int siteType, string threadId, string hostId, int OutboxId = 0)
        {
            Task.Factory.StartNew(() =>
            {
            Enumerations.SiteType type = (Enumerations.SiteType)siteType;

            if (type == Enumerations.SiteType.Airbnb)
            {
                    using (var sm = new Sites.Airbnb())
                    {
                         var result= sm.PreApproveInquiry(threadId, Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()));
                        Outbox outbox = new Outbox() { BookingStatus = "Preapproved", IsSent = result.Success, SiteType = siteType, Message = "", ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Preapprove, UserId = userId.ToInt(),IsCompleted = result.IsComplete};
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }

                        if (result.Success && result.IsComplete)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Inquiry was successfully pre-approved!", outbox);
                        }
                        else if(result.Success && !result.IsComplete)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Inquiry was already pre-approved!", outbox);
                        }
                    }
            }
            else if (type == Enumerations.SiteType.VRBO)
            {
                    using (var sm = new Sites.Vrbo())
                    {
                        var result = sm.Pre_Approve_Inquiry(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, "");
                        Outbox outbox = new Outbox() { BookingStatus = "Preapproved", IsSent = result.Success, SiteType = siteType, Message = "", ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Preapprove, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Inquiry was successfully pre-approved!", outbox);
                        }
                    }
            }
            else if (type == Enumerations.SiteType.Homeaway)
            {
                    using (var sm = new Sites.Homeaway())
                    {
                        var result = sm.Pre_Approve_Inquiry(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, "");
                        Outbox outbox = new Outbox() { BookingStatus = "Preapproved", IsSent = result.Success, SiteType = siteType, Message = "", ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Preapprove, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Inquiry was successfully pre-approved!", outbox);
                        }
                    }
            }
            });
            return Json(new { success =true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region WithdrawPreApproveInquiry
        [HttpPost]
        public ActionResult WithdrawPreApproveInquiry(string userId,int siteType, string threadId, string hostId, string message ,int OutboxId = 0)
        {
            Task.Factory.StartNew(() =>
            {
            Enumerations.SiteType type = (Enumerations.SiteType)siteType;

            if (type == Enumerations.SiteType.Airbnb)
            {
                    using (var sm = new Sites.Airbnb())
                    {
                        var result = sm.WithdrawPreapprovalInquiry(threadId, Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()));
                        Outbox outbox = new Outbox() { BookingStatus = "I", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.WithdrawPreapprove, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Pre-approved inquiry was successfully cancelled!", outbox);
                        }
                    }
            }
            else if (type == Enumerations.SiteType.VRBO)
            {
                    using (var sm = new Sites.Vrbo())
                    {
                        var result = sm.CancelReservation(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, message);
                        Outbox outbox = new Outbox() { BookingStatus = "I", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.WithdrawPreapprove, UserId = userId.ToInt(), IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Pre-approved inquiry was successfully cancelled!", outbox);
                        }
                    }
            }
            else if (type == Enumerations.SiteType.Homeaway)
            {
                    using (var sm = new Sites.Homeaway())
                    {
                        var result = sm.CancelReservation(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, message);
                        Outbox outbox = new Outbox() { BookingStatus = "I", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.WithdrawPreapprove, UserId = userId.ToInt(),IsCompleted = result.IsComplete };
                        if (OutboxId == 0)
                        {
                            Database.Actions.Outboxes.Add(outbox);
                        }
                        else
                        {
                            Database.Actions.Outboxes.Update(OutboxId, result.Success);
                        }
                        if (result.Success)
                        {
                            SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Pre-approved inquiry was successfully cancelled!", outbox);
                        }
                    }
            }
            });
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CancelAlteration
        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult CancelAlteration(int siteType, string hostId, string reservationCode)
        {
            Task.Factory.StartNew(() =>
            {
                try
            {
                Enumerations.SiteType type = (Enumerations.SiteType)siteType;

                if (type == Enumerations.SiteType.Airbnb)
                {
                    var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());
                    using (Sites.Airbnb sm = new Sites.Airbnb())
                    {
                        var result = sm.CancelAlteration(token, reservationCode);
                        //sm.ScrapeInquiries(API.Airbnb.HostSessionCookies.Token, false, false, reservationCode);
                    }
                }
                else if (type == Enumerations.SiteType.VRBO)
                {
                    using (Sites.Vrbo sm = new Sites.Vrbo())
                    {
                        var result = sm.CancelAlteration(reservationCode);
                        //sm.ScrapeInquiries(API.Airbnb.HostSessionCookies.Token, false, false, reservationCode);
                    }
                }
            }
            catch (Exception e) { }
            });
            return Json(new { success = true, message = "" });
        }
        #endregion

        #region Send Special Offer Airbnb
        [HttpPost]
        public ActionResult SendSpecialOffer(int hostId, string listingId, string threadId, int numOfGuest, string startDate, string endDate, string price,string userId)
        {
            try
            {
                    DateTime start;
                    DateTime end;
                    DateTime.TryParse(startDate, out start);
                    DateTime.TryParse(endDate, out end);
                    //var token = GlobalVariables.HostAccounts.Where(x => x.Key == hostId.ToString()).FirstOrDefault().Value.SessionToken;
                    var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());
                using (Sites.Airbnb sm = new Sites.Airbnb())
                    {
                        var res = sm.SendSpecialOffer(token, listingId, threadId, numOfGuest, start, end, price);
                        sm.ScrapeInbox(token, hostId,userId);
                        return Json(new { success = res }, JsonRequestBehavior.AllowGet);
                    }
                
            }
            catch { }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CheckSpecialOfferValidity Airbnb
        public ActionResult CheckSpecialOfferValidity(string hostId, string listingId, string guestId, string startDate, string endDate, int numOfGuest,string currentStartDate,string currentEndDate)
        {
            try
            {
                //if (GlobalVariables.HostAccounts.ContainsKey(hostId))
                //{
                DateTime currentStart;
                DateTime currentEnd;
                DateTime.TryParse(currentStartDate,out currentStart);
                DateTime.TryParse(currentEndDate, out currentEnd);
                DateTime start;
                DateTime end;
                DateTime.TryParse(startDate, out start);
                DateTime.TryParse(endDate, out end);
                var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());
                using (Sites.Airbnb sm = new Sites.Airbnb())
                {
                    var res = sm.GetSpecialOfferSummary(token, listingId, guestId, start, end, numOfGuest,currentStart,currentEnd);
                    return Json(new { success = res.IsSuccess, result = res }, JsonRequestBehavior.AllowGet);
                }
                //}
            }
            catch { }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region WithdrawSpecialOffer Airbnb
        [HttpPost]
        public ActionResult WithdrawSpecialOffer(string hostId, string threadId,string userId)
        {
            try
            {
                
                    //var token = GlobalVariables.HostAccounts.Where(x => x.Key == hostId).FirstOrDefault().Value.SessionToken;
                    var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());
                using (Sites.Airbnb sm = new Sites.Airbnb())
                    {
                        if (sm.ValidateTokenAndLoadCookies(token))
                        {
                            string removeUrl = sm.GetRemoveOfferUrl(threadId);
                            if (!string.IsNullOrEmpty(removeUrl))
                            {
                                var res = sm.WithdrawSpecialOffer(removeUrl);
                                sm.ScrapeInbox(token, hostId.ToInt(),userId);
                                return Json(new { success = res }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                
            }
            catch { }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region DeclineInquiry Airbnb   
        [HttpPost]
        public ActionResult DeclineInquiry(string userId,string threadId, string hostId, string message ,int OutboxId = 0)
        {
            Task.Factory.StartNew(() =>
            {
                bool isSuccess = false;
            var token = Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt());
            using (Sites.Airbnb sm = new Sites.Airbnb())
            {
                isSuccess = sm.DeclineInquiry(threadId, message,token);
                    Outbox outbox = new Outbox() { BookingStatus = "DeclineInquiry", IsSent = isSuccess, SiteType = 1, Message = "", ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.DeclineInquiry, UserId = userId.ToInt(),IsCompleted=true };
                    if (OutboxId == 0)
                    {
                        Database.Actions.Outboxes.Add(outbox);
                    }
                    else
                    {
                        Database.Actions.Outboxes.Update(OutboxId, isSuccess);
                    }
                    if (isSuccess)
                    {
                        SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Decline inquiry was successful!", outbox);
                    }
                }
            });
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //Part of Booking.com
        #region MarkCreditCardAsInvalid Booking.com
        //[HttpPost]
        //public ActionResult MarkCreditCardAsInvalid(int hostId, string listingId, string reservationId, string reason, string reasonId, string card4Digit)
        //{
        //    try
        //    {
        //        using (var sm = new Booking.com.ScrapeManager())
        //        {
        //            var token = API.Booking.com.Hosts.GetToken(hostId);
        //            var result = sm.MarkCreditCardAsInvalid(token, listingId, reservationId, card4Digit, reasonId, reason);

        //            return Json(new { success = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new { success = false, message = "An error occured." }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion

        #region CheckAvailabilityAndPrice
    //    [HttpPost]
    //    public ActionResult CheckAvailabilityAndPrice(int hostId, string reservationId, string confirmationCode,
    //string listingId, DateTime checkInDate, DateTime checkOutDate)
    //    {
    //        try
    //        {
    //            using (var sm = new Booking.com.ScrapeManager())
    //            {
    //                var token = API.Booking.com.Hosts.GetToken(hostId);
    //                var result = sm.CheckAvailabilityAndPrice(token, reservationId, listingId, checkInDate, checkOutDate);
    //                var ob = result[reservationId];

    //                var json = Json(new
    //                {
    //                    ReservationId = confirmationCode,
    //                    CheckInDate = ob["checkin_text"].ToSafeString(),
    //                    CheckOutDate = ob["checkout_text"].ToSafeString(),
    //                    OldCommission = ob["old_commission"].ToSafeString(),
    //                    NewCommission = ob["old_commission_numeric"].ToSafeString(),
    //                    OldPrice = ob["old_price"].ToSafeString(),
    //                    NewPrice = ob["new_price"].ToSafeString(),
    //                    CurrencySymbol = ob["currencysymbol"].ToSafeString()
    //                });

    //               var  strjson = Newtonsoft.Json.JsonConvert.SerializeObject(json);
    //                return Json(new
    //                {
    //                    success = true,
    //                    message = "Success",
    //                    result = strjson
    //                }, JsonRequestBehavior.AllowGet);
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            return Json(new { success = false, message = "An error occured." }, JsonRequestBehavior.AllowGet);
    //        }
    //    }
        #endregion

        #region MarkAsNoShow
        //[HttpPost]
        //public ActionResult MarkAsNoShow(int hostId, string listingId, string bookingReferenceNumber, string reservationId)
        //{
        //    using (var sm = new Booking.com.ScrapeManager())
        //    {
        //        var result = sm.MarkAsNoShow(API.Booking.com.Hosts.GetToken(hostId), listingId,
        //            bookingReferenceNumber, reservationId);

        //        return Json(new { success = result.Item1, message = result.Item2 }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        #endregion
    }
}