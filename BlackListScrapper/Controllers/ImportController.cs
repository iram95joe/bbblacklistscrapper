﻿using BlackListScrapper.Helpers;
using BlackListScrapper.Database.Actions;
using BlackListScrapper.Models;
using BlackListScrapper.Sites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using BlackListScrapper.Database.Entity;
using Newtonsoft.Json.Linq;
using BlackListScrapper.SignalR;
using BlackListScrapper.Enumerations;
using BlackListScrapper.Job;

namespace BlackListScrapper.Controllers
{
    public class ImportController : Controller
    {
        //Airbnb AirbnbScrapper = new Airbnb();
        //Vrbo VrboScrapper = new Vrbo();
        //Homeaway HomeawayScrapper = new Homeaway();
        private Airbnb _AirbnbScrapeManager = null;

        private Vrbo _VrboScrapeManager = null;

        //private Booking.com.ScrapeManager _BookingComScrapeManager = null;

        private Homeaway _HomeawayScrapeManager = null;

        public Airbnb AirbnbScrapper
        {
            get
            {
                if (_AirbnbScrapeManager == null)
                    _AirbnbScrapeManager = new Airbnb();

                return _AirbnbScrapeManager;
            }
            set
            {
                _AirbnbScrapeManager = value;
            }
        }

        public Vrbo VrboScrapper
        {
            get
            {
                if (_VrboScrapeManager == null)
                    _VrboScrapeManager = new Vrbo();

                return _VrboScrapeManager;
            }
            set
            {
                _VrboScrapeManager = value;
            }
        }

        //public Booking.com.ScrapeManager BookingComScrapper
        //{
        //    get
        //    {
        //        if (_BookingComScrapeManager == null)
        //            _BookingComScrapeManager = new Booking.com.ScrapeManager();

        //        return _BookingComScrapeManager;
        //    }
        //    set
        //    {
        //        _BookingComScrapeManager = value;
        //    }
        //}

        public Homeaway HomeawayScrapper
        {
            get
            {
                if (_HomeawayScrapeManager == null)
                    _HomeawayScrapeManager = new Homeaway();

                return _HomeawayScrapeManager;
            }
            set
            {
                _HomeawayScrapeManager = value;
            }
        }

        private string GetSite(int siteType)
        {
            switch (siteType)
            {
                case 0: return "Local";
                case 1: return "Airbnb";
                case 2: return "Vrbo";
                case 3: return "Booking";
                case 4: return "Homeaway";
                default: return "";
            }
        }    
        public ActionResult LogIn(string username, string password, int siteType, string userId, int companyId,bool isImport = true)
        {
            try
            {
                LogInResult result = new LogInResult();
                Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Add {0} host {1}", GetSite(siteType), username));
                var hostId = Database.Actions.Hosts.GetHostId(username, password, siteType);
                var session = Database.Actions.HostSessionCookies.GetSessionByHostId(hostId);
                bool success = false;
                Scrapping.Stop(hostId.ToString());
                #region Airbnb
                if (siteType == 1)
                {
                    var host = AirbnbScrapper.ScrapeHostAccount(username, password, companyId, (session != null ? session.Cookies : null));
                    if (host != null)
                    {
                        if (isImport)
                        {
                            Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("{0} host {1} has valid cookies", GetSite(siteType), username));
                            AirbnbScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, userId);
                        }
                        else
                        {
                            Scrapping.HostStartScrape(host,userId);
                        }
                        return Json(new { success = true, isAirlock = false }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        result = AirbnbScrapper.LogIn(username, password, userId);
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Add {0} host {1} {2}", GetSite(siteType), username,(result.Success==true && result.Airlock==null)));
                        if (result.Success)
                        {
                            host = AirbnbScrapper.ScrapeHostAccount(username, password, companyId, result.Cookie);
                            if (host != null)
                            {
                                GlobalVariables.HostWithPendingLoginRequest.Remove(host.Id);
                                HostSessionCookies.Add(host.Id, result.Cookie);

                                if (isImport)
                                {
                                    AirbnbScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, userId);
                                }
                                else
                                {
                                    Scrapping.HostStartScrape(host,userId);
                                }
                            }
                            return Json(new { success = true, isAirlock = false }, JsonRequestBehavior.AllowGet);
                        }
                        else if (!result.Success && result.Airlock != null)
                        {
                            Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Add {0} host {1} has Airlock", GetSite(siteType), username));
                            result.Airlock.CompanyId = companyId;
                            result.Airlock.AppUserId = userId;
                            result.Airlock.Username = username;
                            result.Airlock.Password = password;
                            return Json(new { success = false, isAirlock = true, airlock = result.Airlock }, JsonRequestBehavior.AllowGet);
                        }
                    }


                }
                #endregion

                #region Vrbo
                else if (siteType == 2)
                {
                    var host = VrboScrapper.ScrapeHostAccount(username, password, companyId, (session != null ? session.Cookies : null));
                    if (host != null)
                    {
                        if (isImport)
                        {
                            VrboScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, userId);
                        }
                        else
                        {
                            Scrapping.HostStartScrape(host,userId);
                        }
                        return Json(new { success = true, isAirlock = false }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        result = VrboScrapper.LogIn(username, password,userId);
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Add {0} host {1} {2}", GetSite(siteType), username, result.Success));
                        if (result.Success)
                        {
                              host = VrboScrapper.ScrapeHostAccount(username, password, companyId, result.Cookie);
                            if (host != null)
                            {
                                GlobalVariables.HostWithPendingLoginRequest.Remove(host.Id);
                                HostSessionCookies.Add(host.Id, result.Cookie);

                                if (isImport)
                                {
                                    VrboScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, userId);
                                }
                                else
                                {
                                    Scrapping.HostStartScrape(host, userId);
                                }
                            }
                            return Json(new { success = true, isAirlock = false }, JsonRequestBehavior.AllowGet);
                        }
                        else if (!result.Success && result.Airlock != null)
                        {
                            Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Add {0} host {1} has Airlock", GetSite(siteType), username));
                            result.Airlock.CompanyId = companyId;
                            result.Airlock.AppUserId = userId;
                            result.Airlock.Username = username;
                            result.Airlock.Password = password;
                            return Json(new
                            {
                                success = success,
                                isAirlock = true,
                                airlock = result.Airlock,
                                phoneNumbers = result.Airlock.PhoneNumbers
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                #endregion
                #region Commneted Booking.com
                //else if (siteType == 3)
                //{

                //    result = AirbnbScrapper.LogIn(username, password, GetCookie("sessionCookie"));

                //    GlobalVariables.Cookie = result.Cookie;
                //    GlobalVariables.Username = username;

                //    if (result.Success)
                //    {
                //        AddCookie("sessionCookie", result.Cookie);
                //        GlobalVariables.HostDetail = AirbnbScrapper.ScrapeHostAccount(GlobalVariables.Cookie, GlobalVariables.Username);

                //        return RedirectToAction("Reviews");
                //    }
                //    else if (!result.Success && result.Airlock != null)
                //    {
                //        return Json(new
                //        {
                //            success = success,
                //            isAirlock = true,
                //            airlock = result.Airlock,
                //            phoneNumber = result.Airlock.PhoneNumbers,
                //            //phoneNumberKeys = result.AirLock.PhoneNumbersSMS.Keys
                //        }, JsonRequestBehavior.AllowGet);
                //    }

                //}
                #endregion

                #region Homeaway
                else if (siteType == 4)
                {

                    var host = HomeawayScrapper.ScrapeHostAccount(username, password, companyId, (session != null ? session.Cookies : null));
                    if (host != null)
                    {
                        if (isImport)
                        {
                            HomeawayScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, userId);
                        }
                        else
                        {
                            Scrapping.HostStartScrape(host, userId);
                        }
                        return Json(new { success = true, isAirlock = false }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        result = HomeawayScrapper.LogIn(username, password,userId);
                        Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Add {0} host {1} {2}", GetSite(siteType), username, result.Success));
                        if (result.Success)
                        {
                            host = HomeawayScrapper.ScrapeHostAccount(username, password, companyId, result.Cookie);
                            if (host != null)
                            {
                                GlobalVariables.HostWithPendingLoginRequest.Remove(host.Id);
                                HostSessionCookies.Add(host.Id, result.Cookie);

                                if (isImport)
                                {
                                    HomeawayScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, userId);
                                }
                                else
                                {
                                    Scrapping.HostStartScrape(host, userId);
                                }
                            }
                            return Json(new { success = true, isAirlock = false }, JsonRequestBehavior.AllowGet);
                        }
                        else if (!result.Success && result.Airlock != null)
                        {
                            Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Add {0} host {1} has Airlock", GetSite(siteType), username));
                            result.Airlock.CompanyId = companyId;
                            result.Airlock.AppUserId = userId;
                            result.Airlock.Username = username;
                            result.Airlock.Password = password;
                            return Json(new
                            {
                                success = success,
                                isAirlock = true,
                                airlock = result.Airlock,
                                phoneNumbers = result.Airlock.PhoneNumbers
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                #endregion
                return Json(new { isAirlock = false, success = result.Success, message = result.Message }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { message = e }, JsonRequestBehavior.AllowGet);
            }
        }
        #region ProcessAirlockSelection

        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult ProcessAirbnbAirlockSelection(string lockId, string userId, string phoneId, int choice, bool isNew,string username)
        {
            //var airbnbScrapper = GlobalVariables.AirbnbAirlockInstanceList.Where(x => x.SiteType == 1 && x.Username == username).FirstOrDefault();
            
            var sendCodeAllowed = AirbnbScrapper.ProcessAirlockSelection(lockId, userId, phoneId, choice, isNew,username);
            Database.Actions.Logs.Add(userId.ToInt(), 0, string.Format("Process airlock selection,choice{0}, result:{1}",choice,sendCodeAllowed.Success));

            return Json(new { sendCodeAllowed = sendCodeAllowed }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult ProcessVrboAirlockSelection(Airlock model)
        {
            return Json(new { airlock = VrboScrapper.ProcessAirlockSelection(model) }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult ProcessBookingAirlockSelection(Booking.com.Models.AirlockViewModel model)
        //{
        //    return Json(new { airlock = BookingComScrapper.ProcessAirlockSelection(model) }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult ProcessHomeawayAirlockSelection(Airlock model)
        {
            return Json(new { airlock = HomeawayScrapper.ProcessAirlockSelection(model) }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region ProcessAirlock

        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult ProcessAirbnbAirlock(string code, string lockId, string userId, string phoneId, int choice, bool isNew, string username, string password,string appUserId,int companyId)
        {
            AirlockResult result =AirbnbScrapper.ProcessAirlock(code, lockId, userId, phoneId, choice, isNew, username, password,appUserId);

            if (result.Success)
            {
                Database.Actions.Logs.Add(appUserId.ToInt(), 0, string.Format("Process airlock, result:{0}",result.Success));
                var host = AirbnbScrapper.ScrapeHostAccount(username,password,companyId, result.Cookie);
                if (host != null)
                {
                    Database.Actions.HostSessionCookies.Add(host.Id, result.Cookie);
                    AirbnbScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, appUserId);
                    Scrapping.HostStartScrape(host,appUserId);
                    return Json(new { success = true,message = result.Message }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Possible Changes in Airbnb Properties", IsSentSMS = false });
                }

            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult ProcessVrboAirlock(Airlock model)
        {
            AirlockResult result = VrboScrapper.ProcessAirlock(model);

            if (result.Success)
            {
                Database.Actions.Logs.Add(model.AppUserId.ToInt(), 0, string.Format("Process airlock, result:{0}", result.Success));
                var host = VrboScrapper.ScrapeHostAccount(model.Username, model.Password, model.CompanyId, result.Cookie);
                if (host != null)
                {
                    GlobalVariables.HostWithPendingLoginRequest.Remove(host.Id);
                    AutoLoginHub.SuccessLoginRemoveToastr(model.AppUserId, host.Id);
                    Database.Actions.HostSessionCookies.Add(host.Id, result.Cookie);
                    VrboScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id,model.AppUserId);
                    Scrapping.HostStartScrape(host, model.AppUserId);
                }
                else
                {
                    Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Possible Changes in Vrbo Properties", IsSentSMS = false });
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        ////[CheckSessionTimeout]
        //public ActionResult ProcessBookingAirlock(Booking.com.Models.AirlockViewModel model)
        //{
        //    var result = BookingComScrapper.ProcessAirlock(model);
        //    return Json(new { success = result.Item1, token = result.Item2 }, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        //[CheckSessionTimeout]
        public ActionResult ProcessHomeawayAirlock(Airlock model)
        {
            AirlockResult result = HomeawayScrapper.ProcessAirlock(model);

            if (result.Success)
            {
                Database.Actions.Logs.Add(model.AppUserId.ToInt(), 0, string.Format("Process airlock, result:{0}", result.Success));
                var host = HomeawayScrapper.ScrapeHostAccount(model.Username, model.Password, model.CompanyId, result.Cookie);
                if (host != null)
                {
                    GlobalVariables.HostWithPendingLoginRequest.Remove(host.Id);
                    AutoLoginHub.SuccessLoginRemoveToastr(model.AppUserId, host.Id);
                    Database.Actions.HostSessionCookies.Add(host.Id, result.Cookie);
                    HomeawayScrapper.StartImporting(HostSessionCookies.GetTokenByHostId(host.Id), host.Id, model.AppUserId);
                    Scrapping.HostStartScrape(host, model.AppUserId);
                }
                else
                {
                    Database.Actions.ScrapperChangesLogs.AddOrUpdate(new ScrapperChangesLog() { Count = 1, Date = DateTime.Now, Message = "Possible Changes in Homeaway Properties", IsSentSMS = false });
                }
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion


    



     
    }
}