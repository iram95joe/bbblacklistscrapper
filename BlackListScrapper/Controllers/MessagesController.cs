﻿using BlackListScrapper.Database;
using BlackListScrapper.Database.Entity;
using BlackListScrapper.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BlackListScrapper.Controllers
{
    public class MessagesController : Controller
    {
        public ActionResult SendMessageToInbox(int siteType, string hostId, string threadId, string message, string userId,int OutboxId=0)
        {
            Task.Factory.StartNew(() =>
            {
                bool sent = false;
            Enumerations.SiteType type = (Enumerations.SiteType)siteType;
                switch (type)
                {
                    case Enumerations.SiteType.Airbnb:

                        using (var sm = new Sites.Airbnb())
                        {
                            var result = sm.SendMessageToInbox(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, message, userId);
                            Outbox outbox = new Outbox() { BookingStatus = "", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Message, UserId = userId.ToInt() };
                            if (OutboxId == 0)
                            {
                                Database.Actions.Outboxes.Add(outbox);
                            }
                            else
                            {
                                Database.Actions.Outboxes.Update(OutboxId, result.Success);
                            }
                            if (result.Success)
                            {
                                SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Message sent!", outbox);
                            }
                        }
                        break;

                    case Enumerations.SiteType.VRBO:
                        using (var sm = new Sites.Vrbo())
                        {
                            var result = sm.SendMessageToInbox(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, message, userId);
                            Outbox outbox = new Outbox() { BookingStatus = "", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Message, UserId = userId.ToInt() };
                            if (OutboxId == 0)
                            {
                                Database.Actions.Outboxes.Add(outbox);
                            }
                            else
                            {
                                Database.Actions.Outboxes.Update(OutboxId, result.Success);
                            }
                            if (result.Success)
                            {
                                SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Message sent!", outbox);
                            }
                        }
                        break;
                    case Enumerations.SiteType.Homeaway:
                        using (var sm = new Sites.Homeaway())
                        {
                            var result = sm.SendMessageToInbox(Database.Actions.HostSessionCookies.GetTokenByHostId(hostId.ToInt()), threadId, message, userId);
                            Outbox outbox = new Outbox() { BookingStatus = "", IsSent = result.Success, SiteType = siteType, Message = message, ResevationId = "", ThreadId = threadId, HostId = hostId.ToInt(), Type = (int)Enumerations.OutboxType.Message, UserId = userId.ToInt() };
                            if (OutboxId == 0)
                            {
                                Database.Actions.Outboxes.Add(outbox);
                            }
                            else
                            {
                                Database.Actions.Outboxes.Update(OutboxId, result.Success);
                            }
                            if (result.Success)
                            {
                                SignalR.ShowNewReservationReview.UpdateReservationStatus(userId, Enumerations.NotificationType.SUCCESS, "Message sent!", outbox);
                            }

                        }
                        break;

                }
            });
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}