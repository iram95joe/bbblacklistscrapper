﻿using BlackListScrapper.Database;
using BlackListScrapper.Database.Actions;
using BlackListScrapper.Helpers;
using BlackListScrapper.Job;
using BlackListScrapper.Models;
using BlackListScrapper.Sites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BlackListScrapper.Controllers
{
    public class HomeController : Controller
    {
       
        [HttpGet]
        public ActionResult Index()
        {
            List<LogModel> Activities = new List<LogModel>();
            GlobalVariables.HtmlFolder = HttpContext.ApplicationInstance.Server.MapPath("~/App_Data/HtmlText");
            using (var db = new ApplicationDbContext()) {
                DateTime currentDateTime = DateTime.Today.ToUniversalTime();
                var prevDate = currentDateTime.AddDays(-1);
                var Users = db.Users.Where(x => x.LastAccessDate >= currentDateTime).ToList();
                foreach(var user in Users)
                {
                    Activities.Add(new LogModel() { User = user, Activity = db.Logs.Where(x => x.UserId == user.Id && x.Datetime >=currentDateTime).Take(200).OrderByDescending(x=>x.Datetime).ToList()});
                }
                int newUserCount = 0;
                foreach (var user in Users)
                {
                    var temp =db.UserLogins.Where(x => x.UserId == user.Id ).OrderBy(x=>x.DateTime).FirstOrDefault();
                    if (temp != null)
                    {
                        if (prevDate >= temp.DateTime || currentDateTime >= temp.DateTime)
                        {
                            newUserCount++;
                        }
                    }
                }

                var s =db.Hosts.Where(x => x.DateCreated >= prevDate || x.DateCreated >= currentDateTime).ToList();
                ViewBag.NewUser = newUserCount;
                ViewBag.NewHost = db.Hosts.Where(x => x.DateCreated >= prevDate || x.DateCreated >= currentDateTime).ToList().Count;
                ViewBag.TotalHost = db.Hosts.Count(); 
                ViewBag.TotalUsers = db.Users.Count();
                ViewBag.ActiveUser = Users.Count;
            }
            
                return View(Activities);
        }
        [HttpPost]
        public async Task<ActionResult> Index(string userId, int companyId)
        {
            GlobalVariables.HtmlFolder = HttpContext.ApplicationInstance.Server.MapPath("~/App_Data/HtmlText");
            Scrapping.StartIncremental(companyId, userId);      


            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }
    }
}