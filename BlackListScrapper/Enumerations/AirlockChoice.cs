﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackListScrapper.Enumerations
{
    public enum AirlockChoice
    {
        SMS = 1,
        Call = 2
    }
}
