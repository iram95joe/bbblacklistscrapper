﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Enumerations
{

    public enum OutboxType
    {
        Message = 0,
        Preapprove = 1,
        WithdrawPreapprove = 2,
        DeclineInquiry= 3,
        Accept=4,
        DeclineBooking=5,
        CancelBooking=6,
        SpecialOffer=7,
        Alter=8,
        CancelAlter=9
    }
}