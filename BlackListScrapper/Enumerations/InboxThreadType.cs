﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackListScrapper.Enumerations
{
    //JM 05/21/19 This is for Vrbo and homeaway to distinguish the what to get in api
    public enum InboxThreadType
    {
        All,
        Inquiry,
        //ReservationRequests,
        Reservation,
        Tentative,
        Cancelled,
        CurrentStay,
        PostStay
    }
}