﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Collections;
using System.Xml;
using System.Text;
using System.Web.Caching;



/// <summary>
/// Summary description for Extensions
/// </summary>
public static class StringExtensions
{
    /// <summary>
    /// Convert String Initial Case
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string ToInitCase(this string input)
    {
        String pattern = @"(\b)(\w)(\w*\b)";
        String results = Regex.Replace(input, pattern, delegate(Match m)
        { return m.Groups[1].Value + m.Groups[2].Value.ToUpper() + m.Groups[3].Value; });

        return results;
    }

    /// <summary>
    /// Returns the given string truncated to the specified length, suffixed with an elipses (...)
    /// </summary>
    /// <param name="input"></param>
    /// <param name="length">Maximum length of return string</param>
    /// <returns></returns>
    public static string Truncate(this string input, int length)
    {
        return Truncate(input, length, "...");
    }

    /// <summary>
    /// Returns the given string truncated to the specified length, suffixed with the given value
    /// </summary>
    /// <param name="input"></param>
    /// <param name="length">Maximum length of return string</param>
    /// <param name="suffix">The value to suffix the return value with (if truncation is performed)</param>
    /// <returns></returns>
    public static string Truncate(this string input, int length, string suffix)
    {
        if (input == null) return "";
        if (input.Length <= length) return input;

        if (suffix == null) suffix = "...";

        return input.Substring(0, length - suffix.Length) + suffix;
    }

    /// <summary>
    /// Splits a given string into an array based on character line breaks
    /// </summary>
    /// <param name="input"></param>
    /// <returns>String array, each containing one line</returns>
    public static string[] ToLineArray(this string input)
    {
        if (input == null) return new string[] { };
        return System.Text.RegularExpressions.Regex.Split(input, "\r\n");
    }

    /// <summary>
    /// Splits a given string into a strongly-typed list based on character line breaks
    /// </summary>
    /// <param name="input"></param>
    /// <returns>Strongly-typed string list, each containing one line</returns>
    public static List<string> ToLineList(this string input)
    {
        List<string> output = new List<string>();
        output.AddRange(input.ToLineArray());
        return output;
    }

    /// <summary>
    /// Replaces line breaks with self-closing HTML 'br' tags
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ReplaceBreaksWithBR(this string input)
    {
        return string.Join("<br/>", input.ToLineArray());
    }
    /// <summary>
    /// Replaces \n with self-closing HTML 'br' tags
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ReplaceLinesWithBR(this string input)
    {
        return string.Join("<br/>", input.Split("\n".ToCharArray()));
    }
    /// <summary>
    /// Replaces any form of br tag with &nbps;
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public static string ReplaceBRWithSpace(this string input)
    {
        if (!string.IsNullOrEmpty(input))
        {

            return input.Replace("<BR>", " ").Replace("<BR/>", " ").Replace("<BR />", " ").Replace("<br>", " ").Replace("<br/>", " ").Replace("<br />", " ");
        }
        return input;
    }

    /// <summary>
    /// Replaces any single apostrophes with two of the same
    /// </summary>
    /// <param name="input"></param>
    /// <returns>String</returns>
    public static string DoubleApostrophes(this string input)
    {
        return Regex.Replace(input, "'", "''");
    }

    /// <summary>
    /// Encodes the input string as HTML (converts special characters to entities)
    /// </summary>
    /// <param name="input"></param>
    /// <returns>HTML-encoded string</returns>
    public static string EncodeHTML(this string input)
    {
        return HttpUtility.HtmlEncode(input);
    }

    /// <summary>
    /// Encodes the input string as a URL (converts special characters to % codes)
    /// </summary>
    /// <param name="input"></param>
    /// <returns>URL-encoded string</returns>
    public static string EncodeURL(this string input)
    {
        return HttpUtility.UrlEncode(input);
    }


    /// <summary>
    /// Decodes any HTML entities in the input string
    /// </summary>
    /// <param name="input"></param>
    /// <returns>String</returns>
    public static string HtmlDecode(this string input)
    {
        return HttpUtility.HtmlDecode(input);
    }

    /// <summary>
    /// Decodes any URL codes (% codes) in the input string
    /// </summary>
    /// <param name="input"></param>
    /// <returns>String</returns>
    public static string DecodeURL(this string input)
    {
        return HttpUtility.UrlDecode(input);

    }



    /// <summary>
    /// Converts the object to save string to avoid exception.
    /// </summary>
    /// <param name="CreatedBy">VAKAS </param> 
    /// <param name="CreatedDate">09-2010</param>
    /// <param name="ModifiedDate">06-10-2010</param>
    /// <param name="input">string value </param>
    /// <returns>Retruns object value</returns>
    public static object ToDbSafeString(this string input)
    {
        if (!string.IsNullOrEmpty(input))
            return input;
        else
            return DBNull.Value;
    }



    /// <summary>
    /// Convert a String into DateTime (Empty, NULL = DateTime.MinValue)
    /// </summary>
    /// <param name="input"></param>
    /// <returns>String</returns>
    public static string SetDefaultEnum(this string input, string defaultEnumValue)
    {
        if (String.IsNullOrEmpty(input) == true)
        {
            input = defaultEnumValue;
        }

        return input;
    }

    /// <summary>
    /// Manuplate String to prevent SQL Injection
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string PreventSQLInjection(this string input)
    {
        String results = input.Replace("'", "''");
        return results;
    }

    public static string Append(this string input, string str)
    {
        if (input == null || str == null) return "";
        return input + str;
    }




    public static string RemoveLastComma(this string input)
    {
        if (false == string.IsNullOrEmpty(input))
        {
            return input.Trim().TrimEnd(",".ToCharArray());
        }
        else
        {
            return input;
        }
    }

    public static string RemoveLastChar(this string input, char inputChar)
    {
        if (false == string.IsNullOrEmpty(input))
        {
            return input.Trim().TrimEnd(inputChar);
        }
        else
        {
            return input;
        }
    }

    public static string EncodeAscii(this string input)
    {
        if (!string.IsNullOrEmpty(input))
        {
            try
            {
                input = input.Replace("�", "");
                Encoding ascii = Encoding.ASCII;
                Byte[] encodedBytes = ascii.GetBytes(input);
                input = ascii.GetString(encodedBytes);

            }
            catch (Exception)
            {
                input = "";
            }
        }
        return input;

    }



    public static string RemoveAccent(this string txt)
    {
        byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
        return System.Text.Encoding.ASCII.GetString(bytes);
    }

    public static string Mask(this string txt, int numOfCharsToShow)
    {
        string maskedString = string.Empty;
        if (txt.Length > numOfCharsToShow)
        {

            maskedString = string.Concat(
                  "".PadLeft((txt.Length - numOfCharsToShow), '*'),
                  txt.Substring(txt.Length - numOfCharsToShow)
              );

        }
        else
        {
            maskedString = txt;
        }
        return maskedString;
    }
    //public static string GenerateSlug(this string phrase, int maxlength = 200)
    //{
    //    if (!string.IsNullOrEmpty(phrase))
    //    {
    //        phrase = phrase.Replace("&", "and");
    //        string str = phrase.RemoveAccent().ToLower();
    //        invalid chars
    //        str = Regex.Replace(str, @"[^a-z0-9&\s-\.-/]", "");
    //        str = Regex.Replace(str, @"[^ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_~:/?#\[\]@!$&'()*+,;=\s-\.-]", "");
    //        str = Regex.Replace(str, @"[^a-z0-9?#\[\]@$&'()\s-\.-]", "");
    //        convert multiple spaces into one space
    //        str = Regex.Replace(str, @"\s+", " ").Trim();
    //        convert multiple slashes into one space
    //        str = Regex.Replace(str, @"/+", " ").Trim();
    //        remove dots before and after space
    //        str = Regex.Replace(str, @"[.]\s", " ");
    //        str = Regex.Replace(str, @"\s[.]", " ");


    //        str = Regex.Replace(str, @"[.,?!^*+=-_';:@~|""]\s", " ");
    //        str = Regex.Replace(str, @"\s[.,?!^*+=-_';:@~|""]", " ");
    //        str = str.Replace(" -", "");
    //        cut and trim
    //       str = str.Substring(0, str.Length <= maxlength ? str.Length : maxlength).Trim();// it
    //        str = Regex.Replace(str, @"\s", "-"); // hyphens   
    //        convert multiple hyphens to one
    //        str = Regex.Replace(str, @"-+", "-").Trim();
    //        return str;
    //    }
    //    return string.Empty;
    //}

    //public static string ConformURL(this string url)
    //{
    //    if (string.IsNullOrEmpty(url))
    //    {
    //        return url;
    //    }
    //    string adjustedURL = url;
    //    if (!url.Trim().ToLower().StartsWith("http") && !url.Trim().ToLower().StartsWith("www."))
    //    {
    //        adjustedURL = FeedScrapperBase.ApplicationConstants.SourceURL + "/" + url.TrimStart('/');
    //    }


    //    return adjustedURL;
    //}
    //public static string AdjustAllRelativeURLs(this string html, string targetAttributeValue = "")
    //{
    //    string fixedHtml = string.Empty;
    //    try
    //    {
    //    if (!string.IsNullOrEmpty(html))
    //    {
    //        XmlDocument xd = FeedScrapperBase.CommunicationResponse.ConvertToXml(ref html);
    //        if (xd != null)
    //        {
    //            XmlNodeList aList = xd.SelectNodes("//a");
    //            if (aList != null)
    //            {
    //                foreach (XmlNode item in aList)
    //                {

    //                    string t = item.GetAttributeFromNode("href");
    //                    string fixedURL = t.ConformURL();
    //                    //if (removeTargetAttribute)
    //                    //{
    //                    //    item.Attributes.Remove(item.Attributes["target"]);
    //                    //}
    //                    if (!t.Equals(fixedURL)) // if it was not a relative url
    //                    {
    //                        string newNode = string.Format("<a href='{0}' target='{2}'> {1} </a>", fixedURL, item.GetInnerTextFromNode(), targetAttributeValue);
    //                        html = html.Replace(item.GetOuterXML(), newNode);

    //                    }
    //                }
    //                fixedHtml = html;
    //            }
    //        }
    //    }
    //    }
    //    catch (Exception)
    //    {
    //    }
    //    return fixedHtml;
    //}
    public static bool IsValidURL(this string url)
    {
       
        bool result = false;
        if (string.IsNullOrEmpty(url))
        {
            return result;
        }
        try
        {

            new Uri(url);
            result = true;
        }
        catch
        {
            return false;
        }

        return result;
    }

    public static string ParseQueryString(this string queryString, string valueToGet)
    {
        string retValue = string.Empty;
        try
        {
            if (!string.IsNullOrWhiteSpace(queryString) && !string.IsNullOrWhiteSpace(valueToGet))
            {
                  retValue= HttpUtility.ParseQueryString(queryString).Get(valueToGet);
            }
          
        }
        catch (Exception)
        {
            
            
        }
        return retValue;
    }
}
