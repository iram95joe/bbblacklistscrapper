﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace BaseScrapper
{
    public class ScrapeHelper
    {
        public static string USER_AGENT = "User-Agent:Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10;";

        public static char POUND = (char)163;
        public static char SEP = (char)0x1B; //ESCAPE CHARACTER

        public static char[] SPLIT_PIPE = { '|' };
        public static char[] SPLIT_SPACE = { ' ' };
        public static char[] SPLIT_COMMA = { ',' };

        public static string ImageAsBase64(string url)
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            Stream response_stream = null;

            try
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                request.UserAgent = USER_AGENT;
                request.Method = "GET";

                response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    response_stream = response.GetResponseStream();

                    int read = 0;
                    bool loop = true;
                    byte[] buffer = new byte[4096];
                    MemoryStream stream = new MemoryStream();

                    while (loop)
                    {
                        read = response_stream.Read(buffer, 0, buffer.Length);

                        if (read < 1)
                            break;

                        stream.Write(buffer, 0, read);
                    }

                    byte[] data = stream.ToArray();

                    stream.Close();

                    return Convert.ToBase64String(data);
                }
            }
            catch
            {

            }
            finally
            {
                if (response_stream != null)
                    response_stream.Close();

                if (response != null)
                    response.Close();
            }

            return string.Empty;
        }

        public static string ParseAmount(string temp)
        {
            if (string.IsNullOrEmpty(temp))
            {
                return temp;
            }
            temp = temp.Trim();
            temp = temp.Replace("�", string.Empty);
            temp = temp.Replace("&pound;", string.Empty);
            temp = temp.Replace("&euro;", string.Empty);
            temp = temp.Replace("Â£", string.Empty);
            temp = HttpUtility.HtmlDecode(temp);
            temp = temp.Replace(POUND, ' ').Trim();

            return temp;
        }

        public static string Trim(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            input = input.Replace("\r", string.Empty);
            input = input.Replace("\t", string.Empty);
            input = input.Replace("\n", string.Empty);
            input = input.Trim();

            return input;
        }

        public static string TwoDigitYear(object fourdigityear)
        {
            if (fourdigityear == null)
                return string.Empty;

            string s = fourdigityear.ToString();

            return s.Substring(2, 2);
        }

        public static string CleanText(string arg)
        {
            arg = arg.Replace("\r", "");
            arg = arg.Replace("\t", "");
            arg = arg.Replace("&pound;", "Ã‚Â£");
            arg = arg.Replace("\n", "");
            arg = HttpUtility.HtmlDecode(arg);

            return arg;
        }



        public static Dictionary<string, string> GetAllHiddenFieldsAsParam(XmlDocument xd)
        {
            XmlNodeList hiddenInputXNL = xd.SelectNodes("//input[@type='hidden' or @type='Hidden' or @type='HIDDEN']");

            Dictionary<string, string> parms = new Dictionary<string, string>();
            foreach (XmlNode hiddenNode in hiddenInputXNL)
            {
                parms.Add(hiddenNode.GetAttributeFromNode("name"), hiddenNode.GetAttributeFromNode("value"));
            }

            return parms;
        }

        public static Dictionary<string, string> GetAllHiddenFieldsAsParam(XmlDocument xd, string formID)
        {
            XmlNodeList hiddenInputXNL = xd.SelectNodes("//form[@id='" + formID + "']/..//input[@type='hidden' or @type='Hidden' or @type='HIDDEN']");

            Dictionary<string, string> parms = new Dictionary<string, string>();
            foreach (XmlNode hiddenNode in hiddenInputXNL)
            {
                parms.Add(hiddenNode.GetAttributeFromNode("name"), hiddenNode.GetAttributeFromNode("value"));
            }

            return parms;
        }

        public static Dictionary<string, string> GetAllHiddenFieldsAsParamWithID(XmlDocument xd, string formID)
        {
            XmlNodeList hiddenInputXNL = xd.SelectNodes("//form[@id='" + formID + "']/..//input[@type='hidden' or @type='Hidden' or @type='HIDDEN']");

            Dictionary<string, string> parms = new Dictionary<string, string>();
            foreach (XmlNode hiddenNode in hiddenInputXNL)
            {
                string key = hiddenNode.GetAttributeFromNode("name");

                if (string.IsNullOrEmpty(key))
                {
                    key = hiddenNode.GetAttributeFromNode("id");
                }


                if (string.IsNullOrEmpty(key))
                {
                    continue;
                }

                parms.Add(key, hiddenNode.GetAttributeFromNode("value"));
            }

            return parms;
        }
        public static Dictionary<string, string> GetAllHiddenFieldsAsParamFromForm(XmlDocument xd, string attributeName,string attributeValue)
        {
            XmlNodeList hiddenInputXNL = xd.SelectNodes("//form[@" + attributeName + "='" + attributeValue + "']/..//input[@type='hidden' or @type='Hidden' or @type='HIDDEN']");

            Dictionary<string, string> parms = new Dictionary<string, string>();
            foreach (XmlNode hiddenNode in hiddenInputXNL)
            {
                string key = hiddenNode.GetAttributeFromNode("name");

                if (string.IsNullOrEmpty(key))
                {
                    key = hiddenNode.GetAttributeFromNode("id");
                }


                if (string.IsNullOrEmpty(key) || parms.ContainsKey(key))
                {
                    continue;
                }
                parms.Add(key, hiddenNode.GetAttributeFromNode("value"));
            }

            return parms;
        }
        public static Dictionary<string, string> GetAllHiddenFieldsAsParamFromForm(XmlNode node, string attributeName, string attributeValue)
        {
            XmlNodeList hiddenInputXNL = node.SelectNodes(".//form[@" + attributeName + "='" + attributeValue + "']/..//input[@type='hidden' or @type='Hidden']");

            Dictionary<string, string> parms = new Dictionary<string, string>();
            foreach (XmlNode hiddenNode in hiddenInputXNL)
            {
                string key = hiddenNode.GetAttributeFromNode("name");

                if (string.IsNullOrEmpty(key))
                {
                    key = hiddenNode.GetAttributeFromNode("id");
                }


                if (string.IsNullOrEmpty(key) || parms.ContainsKey(key))
                {
                    continue;
                }
                parms.Add(key, hiddenNode.GetAttributeFromNode("value"));
            }

            return parms;
        }
        public static string SplitSafely(string source, string splitCharacter, int requiredNumberofElementZeroBase=-1,bool getLast=false)
        {
            string output = string.Empty;
            if (!string.IsNullOrEmpty(source))
            {
                try
                {
                    string special = "!";

                    source = source.Replace(splitCharacter, special);
                    string[] arr = source.Split(special.ToCharArray());
                    if (arr != null && arr.Length > requiredNumberofElementZeroBase)
                    {
                        if (requiredNumberofElementZeroBase>-1)
                        {
                             output = arr[requiredNumberofElementZeroBase].ToSafeString().Trim();
                        }
                        else if (getLast)
                        {
                           output = arr[arr.Length-1].ToSafeString().Trim();  
                        }
                       
                    }
                }
                catch (Exception)
                {
                }
            }
            return output;
        }

        public static bool EmailAddressIsValid(string input)
        {
            if (string.IsNullOrEmpty(input))
                return false;

            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(input);
                return true;
            }
            catch { }

            return false;
        }
    }
}