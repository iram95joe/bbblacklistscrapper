﻿using System;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using System.Web;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;

using HtmlAgilityPack;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BaseScrapper
{
    public abstract class CommunicationEngine
    {
        #region Actual class
        public CommunicationPage CommPage { get; set; }

        public void ProcessRequest(CommunicationPage CommPage)
        {
            this.CommPage = CommPage;
            if (CommPage.ReqType == CommunicationPage.RequestType.GET)
            {

                GetPage();
            }
            else if (CommPage.ReqType == CommunicationPage.RequestType.JSONGET)
            {

                GetPageJSON();
            }
            else if (CommPage.ReqType == CommunicationPage.RequestType.POST)
            {

                GetPost();
            }
            else if (CommPage.ReqType == CommunicationPage.RequestType.JSONPOST)
            {

                GetPostJSON();
            }
            else if (CommPage.ReqType == CommunicationPage.RequestType.JSONPUT)
            {

                GetPostJSON(isPut: true);
            }
            else if (CommPage.ReqType == CommunicationPage.RequestType.DELETE)
            {

                DELETE();
            }
            else if (CommPage.ReqType == CommunicationPage.RequestType.FILEDOWNLOAD)
            {
                // DownloadImage();
            }
        }

        #endregion

        #region GET PAGE



        public CommunicationPage GetPage()
        {
            CheckAndSleep();
            // CommunicationResponse communication = new CommunicationResponse(url);

            HttpWebRequest request = null;
            CookieContainer cookies = null;
            HttpWebResponse response = null;
            StreamReader response_stream = null;
            if (CommPage.PersistCookies)
            {
                cookies = CommPage.COOKIES;
            }
            else
            {
                cookies = null; //-Responder.COOKIES; CRITICAL. in GET request. always clearing the cooking
            }
            //This is priority
            if (CommPage.UseProxy)
                cookies = null;

            if (cookies == null)
            {
                cookies = new CookieContainer();
            }

            try
            {
                request = (HttpWebRequest)WebRequest.Create(CommPage.RequestURL);
                if (CommPage.UserAgent == CommunicationPage.USER_AGENT.Random)
                    request.UserAgent = CommPage.CurrentUA = CommunicationPage.UserAgentList[CommPage.UARandom.Next(CommunicationPage.UserAgentList.Count)];
                else
                    request.UserAgent = CommPage.CurrentUA = CommunicationPage.userAgents[CommPage.UserAgent.ToSafeString()];

                request.Method = "GET";
                request.AllowAutoRedirect = true;
                request.CookieContainer = cookies;
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
                // request.Accept = "*";
                if (CommPage.UseProxy)
                    request.KeepAlive = false;
                else
                    request.KeepAlive = true;

                //request.Timeout = 1000 * 60;

                if (CommPage.RequestHeaders != null && CommPage.RequestHeaders.Count > 0)
                {
                    foreach (var item in CommPage.RequestHeaders)
                    {
                        if (item.Key.ToLower() == "accept")
                        {
                            request.Accept = item.Value;
                        }
                        else
                        {
                            request.Headers.Add(item.Key, item.Value);
                        }
                    }
                }

                //request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                //request.Headers.Add("Accept-Language", "en-US,en;q=0.8");
                //request.Headers.Add("X-Requested-With", "XMLHttpRequest");



                //request.Proxy = null;
                // request.ContentType = "text/html; charset=UTF-8";
                //request.Accept = "application/json";

                request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                request.Headers.Add("Accept-Language", "en-US,en;q=0.8");
                request.Headers.Add("Upgrade-Insecure-Requests", "1");

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                if (string.IsNullOrEmpty(CommPage.Referer) == false)
                    request.Referer = CommPage.Referer;
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogRequest(request); }
                }
                catch (Exception e)
                {


                }
                //            response = (HttpWebResponse)await Task.Factory
                //.FromAsync<WebResponse>(request.BeginGetResponse,
                //                        request.EndGetResponse,
                //                        null);


                //IWebProxy proxy = request.Proxy;

                //if (request.Proxy != null)
                //{
                //    // Console.WriteLine("Removing proxy: {0}", proxy.GetProxy(request.RequestUri));
                //    request.Proxy = null;
                //}


                ////////Stream ReceiveStream = response.GetResponseStream();


                ////////Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

                ////////// Pipe the stream to a higher level stream reader with the required encoding format. 
                ////////StreamReader readStream = new StreamReader(ReceiveStream, encode);
                ////////Console.WriteLine("\nResponse stream received");
                ////////Char[] read = new Char[256];

                ////////// Read 256 charcters at a time.    
                ////////int count = readStream.Read(read, 0, 256);
                ////////Console.WriteLine("HTML...\r\n");

                ////////while (count > 0)
                ////////{
                ////////    // Dump the 256 characters on a string and display the string onto the console.
                ////////    String str = new String(read, 0, count);
                ////////    Console.Write(str);
                ////////    count = readStream.Read(read, 0, 256);
                ////////}

                ////////Console.WriteLine("");
                ////////// Release the resources of stream object.
                ////////readStream.Close();

                ////////// Release the resources of response object.
                ////////response.Close();



                if (CommPage.UseProxy)
                {
                    request.Proxy = new WebProxy(CommPage.ProxyHost, CommPage.ProxyPort);
                }
                else
                {


                    IWebProxy proxy = request.Proxy;

                    if (request.Proxy != null)
                    {
                        // Console.WriteLine("Removing proxy: {0}", proxy.GetProxy(request.RequestUri));
                        request.Proxy = null;
                    }
                }


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                response = (HttpWebResponse)request.GetResponse();





                if (response.StatusCode == HttpStatusCode.OK)
                {

                    if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                    {
                        //using (StreamReader responseStream = new StreamReader(response.GetResponseStream()))
                        //{
                        //    CommPage.Html = responseStream.ReadToEnd();
                        //}
                        response_stream = new StreamReader(response.GetResponseStream());
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    else
                    {
                        response_stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                        CommPage.Html = response_stream.ReadToEnd();
                        // CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                    }


                }
                #region Do this whatever the status is returned
                if (response != null)
                {
                    CommPage.StatusCode = response.StatusCode;
                    if (response.StatusCode != HttpStatusCode.OK) //if OK, already set above
                    {
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    // CommPage.Cookies = response.Cookies;
                    CommPage.Uri = response.ResponseUri;
                    cookies.Add(response.Cookies);
                    CommPage.COOKIES = cookies;
                }



                #endregion
            }
            catch (WebException e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    // Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        //Console.WriteLine("Sleeping for {0} MS", ApplicationConstants.ExceptionDelay);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        Console.WriteLine("Woke up....");
                    }
                    // Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (retryCount + 1), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPage();
                }
                #endregion
                using (WebResponse Exresponse = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)Exresponse;
                    if (httpResponse != null)
                    {


                        CommPage.StatusCode = httpResponse.StatusCode;
                        //CommPage.Cookies = httpResponse.Cookies;
                        CommPage.Uri = httpResponse.ResponseUri;

                        try
                        {
                            if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream());
                                CommPage.Html = response_stream.ReadToEnd();
                            }
                            else
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                                CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                            }
                        }
                        catch { }

                        cookies.Add(httpResponse.Cookies);
                        CommPage.COOKIES = cookies;

                    }


                }
            }
            catch (Exception e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        Console.WriteLine("Sleeping for {0} MS", CommPage.DelayInCaseOfException);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        ////Console.WriteLine("Woke up....");
                    }
                    Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (CommPage.RequestedGrantedAtTryCount), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPage();
                }
                else
                {
                    CommPage.Clear();
                }
                #endregion

            }
            finally
            {
                if (response_stream != null)
                    response_stream.Close();

                if (response != null)
                    response.Close();

                if (CommPage.CB != null)
                {
                    CommPage = CommPage.CB(CommPage);
                }
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogResponse(communication, request.Method.Trim() == "POST" ? true : false); }
                }
                catch (Exception e)
                {
                    //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url); 
                }
            }

            return CommPage;
        }
        public CommunicationPage GetPageJSON()
        {
            CheckAndSleep();
            // CommunicationResponse communication = new CommunicationResponse(url);

            HttpWebRequest request = null;
            CookieContainer cookies = null;
            HttpWebResponse response = null;
            StreamReader response_stream = null;
            if (CommPage.PersistCookies)
            {
                cookies = CommPage.COOKIES;
            }
            else
            {
                cookies = null; //-Responder.COOKIES; CRITICAL. in GET request. always clearing the cooking
            }

            if (cookies == null)
            {
                cookies = new CookieContainer();
            }

            try
            {
                request = (HttpWebRequest)WebRequest.Create(CommPage.RequestURL);
                request.UserAgent = CommunicationPage.userAgents[CommPage.UserAgent.ToSafeString()];
                request.Method = "GET";
                request.AllowAutoRedirect = true;
                request.CookieContainer = cookies;
                request.Accept = "application/json, text/plain, *";
                //request.KeepAlive = true;
                //request.Timeout = 1000 * 60;

                if (CommPage.RequestHeaders != null && CommPage.RequestHeaders.Count > 0)
                {
                    foreach (var item in CommPage.RequestHeaders)
                    {
                        request.Headers.Add(item.Key, item.Value);
                    }
                }

                //request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                //request.Headers.Add("Accept-Language", "en-US,en;q=0.8");
                //request.Headers.Add("X-Requested-With", "XMLHttpRequest");



                //request.Proxy = null;
                // request.ContentType = "text/html; charset=UTF-8";
                //request.Accept = "application/json";

                request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                request.Headers.Add("Accept-Language", "en-US,en;q=0.8");
                // request.Headers.Add("Upgrade-Insecure-Requests", "1");

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                if (string.IsNullOrEmpty(CommPage.Referer) == false)
                    request.Referer = CommPage.Referer;
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogRequest(request); }
                }
                catch (Exception e)
                {


                }
                //            response = (HttpWebResponse)await Task.Factory
                //.FromAsync<WebResponse>(request.BeginGetResponse,
                //                        request.EndGetResponse,
                //                        null);


                IWebProxy proxy = request.Proxy;

                if (request.Proxy != null)
                {
                    Console.WriteLine("Removing proxy: {0}", proxy.GetProxy(request.RequestUri));
                    request.Proxy = null;
                }


                response = (HttpWebResponse)request.GetResponse();

                ////////Stream ReceiveStream = response.GetResponseStream();


                ////////Encoding encode = System.Text.Encoding.GetEncoding("utf-8");

                ////////// Pipe the stream to a higher level stream reader with the required encoding format. 
                ////////StreamReader readStream = new StreamReader(ReceiveStream, encode);
                ////////Console.WriteLine("\nResponse stream received");
                ////////Char[] read = new Char[256];

                ////////// Read 256 charcters at a time.    
                ////////int count = readStream.Read(read, 0, 256);
                ////////Console.WriteLine("HTML...\r\n");

                ////////while (count > 0)
                ////////{
                ////////    // Dump the 256 characters on a string and display the string onto the console.
                ////////    String str = new String(read, 0, count);
                ////////    Console.Write(str);
                ////////    count = readStream.Read(read, 0, 256);
                ////////}

                ////////Console.WriteLine("");
                ////////// Release the resources of stream object.
                ////////readStream.Close();

                ////////// Release the resources of response object.
                ////////response.Close();

















                if (response.StatusCode == HttpStatusCode.OK)
                {

                    if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                    {
                        //using (StreamReader responseStream = new StreamReader(response.GetResponseStream()))
                        //{
                        //    CommPage.Html = responseStream.ReadToEnd();
                        //}
                        response_stream = new StreamReader(response.GetResponseStream());
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    else
                    {
                        response_stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                        CommPage.Html = response_stream.ReadToEnd();
                        // CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                    }


                }
                #region Do this whatever the status is returned
                if (response != null)
                {
                    CommPage.StatusCode = response.StatusCode;
                    if (response.StatusCode != HttpStatusCode.OK) //if OK, already set above
                    {
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    // CommPage.Cookies = response.Cookies;
                    CommPage.Uri = response.ResponseUri;
                    cookies.Add(response.Cookies);
                    CommPage.COOKIES = cookies;
                }



                #endregion
            }
            catch (WebException e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    // Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        //Console.WriteLine("Sleeping for {0} MS", ApplicationConstants.ExceptionDelay);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        Console.WriteLine("Woke up....");
                    }
                    // Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (retryCount + 1), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPage();
                }
                #endregion
                using (WebResponse Exresponse = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)Exresponse;
                    if (httpResponse != null)
                    {


                        CommPage.StatusCode = httpResponse.StatusCode;
                        //CommPage.Cookies = httpResponse.Cookies;
                        CommPage.Uri = httpResponse.ResponseUri;

                        try
                        {
                            if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream());
                                CommPage.Html = response_stream.ReadToEnd();
                            }
                            else
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                                CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                            }
                        }
                        catch { }

                        cookies.Add(httpResponse.Cookies);
                        CommPage.COOKIES = cookies;

                    }


                }
            }
            catch (Exception e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        Console.WriteLine("Sleeping for {0} MS", CommPage.DelayInCaseOfException);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        ////Console.WriteLine("Woke up....");
                    }
                    Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (CommPage.RequestedGrantedAtTryCount), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPage();
                }
                else
                {
                    CommPage.Clear();
                }
                #endregion

            }
            finally
            {
                if (response_stream != null)
                    response_stream.Close();

                if (response != null)
                    response.Close();

                if (CommPage.CB != null)
                {
                    CommPage = CommPage.CB(CommPage);
                }
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogResponse(communication, request.Method.Trim() == "POST" ? true : false); }
                }
                catch (Exception e)
                {
                    //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url); 
                }
            }

            return CommPage;
        }

        //public static CommunicationResponse GetPageCustom(HttpWebRequest custom, byte[] postDatToLog = null, List<string> paramsToMask = null)
        //{
        //    CheckAndSleep();

        //    CommunicationResponse communication = new CommunicationResponse(custom.RequestUri.ToString());

        //    HttpWebRequest request = custom;

        //    HttpWebResponse response = null;
        //    StreamReader response_stream = null;
        //    CookieContainer cookies = custom.CookieContainer;
        //    if (cookies == null)
        //    {
        //        cookies = new CookieContainer();
        //    }
        //    //try
        //    //{
        //    //    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
        //    //    //{ LogRequest(request, postDatToLog, paramsToMask); }
        //    //}
        //    //catch (Exception e) { 
        //    //    Responder.LogException(e.Message, e.ToString()); 
        //    //}
        //    try
        //    {
        //        response = (HttpWebResponse)request.GetResponse();

        //        if (response.StatusCode == HttpStatusCode.OK)
        //        {

        //            if (string.IsNullOrEmpty(Responder.SourceCharsetEncoding))
        //            {
        //                response_stream = new StreamReader(response.GetResponseStream());
        //                communication.Html = response_stream.ReadToEnd();
        //            }
        //            else
        //            {
        //                response_stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(Responder.SourceCharsetEncoding));
        //                communication.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
        //            }
        //        }

        //        if (response != null)
        //        {
        //            communication.StatusCode = response.StatusCode;
        //            if (response.StatusCode != HttpStatusCode.OK) //if OK, already set above
        //            {
        //                communication.Html = response_stream.ReadToEnd();
        //            }
        //            communication.Cookies = response.Cookies;
        //            communication.Uri = response.ResponseUri;
        //            cookies.Add(communication.Cookies);
        //            Responder.COOKIES = cookies;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        //Responder.LogException(e.Message, e.ToString());
        //        communication.Clear();
        //    }
        //    finally
        //    {
        //        if (response_stream != null)
        //            response_stream.Close();

        //        if (response != null)
        //            response.Close();

        //        //try
        //        //{
        //        //    if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
        //        //    {
        //        //        LogResponse(communication, request.Method.Trim() == "POST" ? true : false);
        //        //    }
        //        //}
        //        //catch (Exception e) { Responder.LogException(e.Message, e.ToString()); }
        //    }

        //    return communication;
        //}

        //public static bool IsHTTP200AtPage(string url, USER_AGENT userAgent = USER_AGENT.Windows)
        //{
        //    HttpWebRequest request = null;
        //    HttpWebResponse response = null;

        //    try
        //    {
        //        request = (HttpWebRequest)WebRequest.Create(url);
        //        request.UserAgent = userAgents[userAgent.ToSafeString()];
        //        request.Method = "GET";

        //        response = (HttpWebResponse)request.GetResponse();

        //        if (response.StatusCode == HttpStatusCode.OK)
        //            return true;
        //    }
        //    catch
        //    {

        //    }
        //    finally
        //    {
        //        if (response != null)
        //            response.Close();
        //    }

        //    return false;
        //}

        #endregion

        #region POST PAGE


        public CommunicationPage GetPost()
        {
            CheckAndSleep();
            // CommunicationResponse communication = new CommunicationResponse(url);

            HttpWebRequest request = null;
            CookieContainer cookies = null;
            if (CommPage.PersistCookies)
            {
                cookies = CommPage.COOKIES;
            }
            else
            {
                cookies = null;
            }

            HttpWebResponse response = null;
            StreamReader response_stream = null;
            if (cookies == null)
            {
                cookies = new CookieContainer();
            }
            try
            {
                byte[] data = PostData(this.CommPage.Parameters);

                request = (HttpWebRequest)WebRequest.Create(CommPage.RequestURL);
                request.UserAgent = CommunicationPage.userAgents[CommPage.UserAgent.ToSafeString()];
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";// "application/x-www-form-urlencoded";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*;q=0.8";

                // request.Accept = "application/json, text/javascript, *; q=0.01";//"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*;q=0.8";
                request.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                request.Headers.Add("Accept-Language", "en-US,en;q=0.8");

                request.Headers.Add("Upgrade-Insecure-Requests", "1");
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                if (CommPage.RequestHeaders != null && CommPage.RequestHeaders.Count > 0)
                {
                    foreach (var item in CommPage.RequestHeaders)
                    {
                        if (item.Key.ToLower() == "accept")
                        {
                            request.Accept = item.Value;
                        }
                        else
                        {
                            request.Headers.Add(item.Key, item.Value);
                        }
                    }
                }



                request.CookieContainer = cookies;
                request.AllowAutoRedirect = true;
                request.ContentLength = data.Length;

                if (string.IsNullOrEmpty(CommPage.Referer) == false)
                    request.Referer = CommPage.Referer;

                Stream request_stream = request.GetRequestStream();
                request_stream.Write(data, 0, data.Length);
                request_stream.Close();

                //try
                //{
                //    if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                //    {
                //        LogRequest(request, data, toMaskParams);

                //    }
                //}
                //catch (Exception e) { Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url); }

                //IWebProxy proxy = request.Proxy;

                //if (request.Proxy != null)
                //{
                //    Console.WriteLine("Removing proxy: {0}", proxy.GetProxy(request.RequestUri));
                //    request.Proxy = null;
                //}
                response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {


                    if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                    {
                        response_stream = new StreamReader(response.GetResponseStream());
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    else
                    {
                        response_stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                        CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                    }


                }
                #region Do this whatever the status is returned
                if (response != null)
                {
                    CommPage.StatusCode = response.StatusCode;
                    if (response.StatusCode != HttpStatusCode.OK) // if ok then aleady set above
                    {
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    // CommPage.Cookies = response.Cookies;
                    CommPage.Uri = response.ResponseUri;
                    cookies.Add(response.Cookies);
                    CommPage.COOKIES = cookies;
                }



                #endregion
            }
            catch (WebException e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    //Console.WriteLine("Exception occurred: {0}",e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        // Console.WriteLine("Sleeping for {0} MS", ApplicationConstants.ExceptionDelay);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        // Console.WriteLine("Woke up....");
                    }

                    //Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (retryCount + 1), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPost();
                }
                #endregion
                using (WebResponse Exresponse = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)Exresponse;
                    if (httpResponse != null)
                    {


                        CommPage.StatusCode = httpResponse.StatusCode;
                        //CommPage.Cookies = httpResponse.Cookies;
                        CommPage.Uri = httpResponse.ResponseUri;
                        try
                        {
                            if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream());
                                CommPage.Html = response_stream.ReadToEnd();
                            }
                            else
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                                CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                            }
                        }
                        catch { }
                        cookies.Add(httpResponse.Cookies);
                        CommPage.COOKIES = cookies;

                    }
                }

            }
            catch (Exception e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                CommPage.Clear();
            }
            finally
            {
                if (response_stream != null)
                    response_stream.Close();

                if (response != null)
                    response.Close();

                if (CommPage.CB != null)
                {
                    CommPage = CommPage.CB(CommPage);
                }
                //try
                //{
                //    if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                //    {
                //        LogResponse(communication, request.Method.Trim() == "POST" ? true : false);
                //    }
                //}
                //catch (Exception e)
                //{
                //    Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                //}
            }

            return CommPage;
        }

        public CommunicationPage GetPostJSON(bool isPut = false)
        {
            CheckAndSleep();
            // CommunicationResponse communication = new CommunicationResponse(url);

            HttpWebRequest request = null;
            CookieContainer cookies = null;
            if (CommPage.PersistCookies)
            {
                cookies = CommPage.COOKIES;
            }
            else
            {
                cookies = null;
            }

            HttpWebResponse response = null;
            StreamReader response_stream = null;
            if (cookies == null)
            {
                cookies = new CookieContainer();
            }
            try
            {
                byte[] data = PostData(this.CommPage.JsonStringToPost);

                request = (HttpWebRequest)WebRequest.Create(CommPage.RequestURL);
                request.UserAgent = CommunicationPage.userAgents[CommPage.UserAgent.ToSafeString()];
                if (isPut)
                    request.Method = "PUT";
                else
                    request.Method = "POST";
                request.ContentType = "application/json;charset=UTF-8";
                request.Accept = "application/json, text/plain, *";
                request.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                request.Headers.Add("Accept-Language", "en-US,en;q=0.8");

                // request.Headers.Add("Upgrade-Insecure-Requests", "1");
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                request.CookieContainer = cookies;
                request.AllowAutoRedirect = true;
                // request.ContentLength = data.Length;

                if (string.IsNullOrEmpty(CommPage.Referer) == false)
                    request.Referer = CommPage.Referer;



                if (CommPage.RequestHeaders != null && CommPage.RequestHeaders.Count > 0)
                {
                    foreach (var item in CommPage.RequestHeaders)
                    {
                        if (item.Key.ToLower() == "accept")
                        {
                            request.Accept = item.Value;
                        }
                        else if (item.Key.ToLower() == "content-type")
                        {
                            request.ContentType = item.Value;
                        }
                        else
                        {
                            request.Headers.Add(item.Key, item.Value);
                        }
                    }
                }


                //IWebProxy proxy = request.Proxy;

                //if (request.Proxy != null)
                //{
                //    Console.WriteLine("Removing proxy: {0}", proxy.GetProxy(request.RequestUri));
                //    request.Proxy = null;
                //}
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = this.CommPage.JsonStringToPost;
                    //"{\"user\":\"test\"," +
                    //"\"password\":\"bla\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                //StreamWriter request_stream = request.GetRequestStream();
                //request_stream.Write(data, 0, data.Length);
                //request_stream.Close();

                //try
                //{
                //    if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                //    {
                //        LogRequest(request, data, toMaskParams);

                //    }
                //}
                //catch (Exception e) { Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url); }

                ////IWebProxy proxy = request.Proxy;

                ////if (request.Proxy != null)
                ////{
                ////    Console.WriteLine("Removing proxy: {0}", proxy.GetProxy(request.RequestUri));
                ////    request.Proxy = null;
                ////}
                response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode == HttpStatusCode.OK)
                {


                    if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                    {
                        response_stream = new StreamReader(response.GetResponseStream());
                        CommPage.Html = response_stream.ReadToEnd();
                        CommPage.ResponseHeaders = response.Headers;
                    }
                    else
                    {
                        response_stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                        CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                    }


                }
                #region Do this whatever the status is returned
                if (response != null)
                {
                    CommPage.StatusCode = response.StatusCode;
                    if (response.StatusCode != HttpStatusCode.OK) // if ok then aleady set above
                    {
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    // CommPage.Cookies = response.Cookies;
                    CommPage.Uri = response.ResponseUri;
                    cookies.Add(response.Cookies);
                    CommPage.COOKIES = cookies;
                }



                #endregion
            }
            catch (WebException e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    //Console.WriteLine("Exception occurred: {0}",e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        // Console.WriteLine("Sleeping for {0} MS", ApplicationConstants.ExceptionDelay);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        // Console.WriteLine("Woke up....");
                    }

                    //Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (retryCount + 1), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPost();
                }
                #endregion
                using (WebResponse Exresponse = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)Exresponse;
                    if (httpResponse != null)
                    {


                        CommPage.StatusCode = httpResponse.StatusCode;
                        //CommPage.Cookies = httpResponse.Cookies;
                        CommPage.Uri = httpResponse.ResponseUri;
                        try
                        {
                            if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream());
                                CommPage.Html = response_stream.ReadToEnd();
                            }
                            else
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                                CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                            }
                        }
                        catch { }
                        cookies.Add(httpResponse.Cookies);
                        CommPage.COOKIES = cookies;

                    }
                }

            }
            catch (Exception e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                CommPage.Clear();
            }
            finally
            {
                if (response_stream != null)
                    response_stream.Close();

                if (response != null)
                    response.Close();

                if (CommPage.CB != null)
                {
                    CommPage = CommPage.CB(CommPage);
                }
                //try
                //{
                //    if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                //    {
                //        LogResponse(communication, request.Method.Trim() == "POST" ? true : false);
                //    }
                //}
                //catch (Exception e)
                //{
                //    Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                //}
            }

            return CommPage;
        }

        private static byte[] PostData(Dictionary<string, string> parameters)
        {
            StringBuilder buffer = new StringBuilder();

            bool first = true;

            foreach (KeyValuePair<string, string> p in parameters)
            {
                if (first)
                {
                    buffer.Append(HttpUtility.UrlEncode(p.Key) + "=" + (p.Value == null ? "" : HttpUtility.UrlEncode(p.Value)));
                    first = false;
                    continue;
                }

                buffer.Append("&");
                buffer.Append(HttpUtility.UrlEncode(p.Key) + "=" + (p.Value == null ? "" : HttpUtility.UrlEncode(p.Value)));
            }

            return Encoding.UTF8.GetBytes(buffer.ToString());
        }
        private static byte[] PostData(string json)
        {

            return Encoding.UTF8.GetBytes(json);
        }
        #endregion

        #region DELETE
        public CommunicationPage DELETE()
        {
            CheckAndSleep();
            // CommunicationResponse communication = new CommunicationResponse(url);

            HttpWebRequest request = null;
            CookieContainer cookies = null;
            HttpWebResponse response = null;
            StreamReader response_stream = null;
            if (CommPage.PersistCookies)
            {
                cookies = CommPage.COOKIES;
            }
            else
            {
                cookies = null; //-Responder.COOKIES; CRITICAL. in GET request. always clearing the cooking
            }

            if (cookies == null)
            {
                cookies = new CookieContainer();
            }

            try
            {
                request = (HttpWebRequest)WebRequest.Create(CommPage.RequestURL);
                request.UserAgent = CommunicationPage.userAgents[CommPage.UserAgent.ToSafeString()];
                request.Method = "DELETE";
                request.AllowAutoRedirect = true;
                request.CookieContainer = cookies;
                request.Accept = "*";
                // request.Accept = "*";
                request.KeepAlive = true;
                //request.Timeout = 1000 * 60;

                if (CommPage.RequestHeaders != null && CommPage.RequestHeaders.Count > 0)
                {
                    foreach (var item in CommPage.RequestHeaders)
                    {
                        if (item.Key.ToLower() == "accept")
                        {
                            request.Accept = item.Value;
                        }
                        else
                        {
                            request.Headers.Add(item.Key, item.Value);
                        }
                    }
                }

                //request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                //request.Headers.Add("Accept-Language", "en-US,en;q=0.8");
                //request.Headers.Add("X-Requested-With", "XMLHttpRequest");



                //request.Proxy = null;
                // request.ContentType = "text/html; charset=UTF-8";
                //request.Accept = "application/json";

                request.Headers.Add("Accept-Encoding", "gzip, deflate, sdch, br");
                request.Headers.Add("Accept-Language", "en-US,en;q=0.8");
                request.Headers.Add("Upgrade-Insecure-Requests", "1");

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                if (string.IsNullOrEmpty(CommPage.Referer) == false)
                    request.Referer = CommPage.Referer;
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogRequest(request); }
                }
                catch (Exception e)
                {


                }
                //            response = (HttpWebResponse)await Task.Factory
                //.FromAsync<WebResponse>(request.BeginGetResponse,
                //                        request.EndGetResponse,
                //                        null);


                IWebProxy proxy = request.Proxy;

                if (request.Proxy != null)
                {
                    // Console.WriteLine("Removing proxy: {0}", proxy.GetProxy(request.RequestUri));
                    request.Proxy = null;
                }


                response = (HttpWebResponse)request.GetResponse();


                if (response.StatusCode == HttpStatusCode.OK)
                {

                    if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                    {
                        //using (StreamReader responseStream = new StreamReader(response.GetResponseStream()))
                        //{
                        //    CommPage.Html = responseStream.ReadToEnd();
                        //}
                        response_stream = new StreamReader(response.GetResponseStream());
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    else
                    {
                        response_stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                        CommPage.Html = response_stream.ReadToEnd();
                        // CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                    }


                }
                #region Do this whatever the status is returned
                if (response != null)
                {
                    CommPage.StatusCode = response.StatusCode;
                    if (response.StatusCode != HttpStatusCode.OK) //if OK, already set above
                    {
                        CommPage.Html = response_stream.ReadToEnd();
                    }
                    // CommPage.Cookies = response.Cookies;
                    CommPage.Uri = response.ResponseUri;
                    cookies.Add(response.Cookies);
                    CommPage.COOKIES = cookies;
                }



                #endregion
            }
            catch (WebException e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    // Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        //Console.WriteLine("Sleeping for {0} MS", ApplicationConstants.ExceptionDelay);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        Console.WriteLine("Woke up....");
                    }
                    // Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (retryCount + 1), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPage();
                }
                #endregion
                using (WebResponse Exresponse = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)Exresponse;
                    if (httpResponse != null)
                    {


                        CommPage.StatusCode = httpResponse.StatusCode;
                        //CommPage.Cookies = httpResponse.Cookies;
                        CommPage.Uri = httpResponse.ResponseUri;

                        try
                        {
                            if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream());
                                CommPage.Html = response_stream.ReadToEnd();
                            }
                            else
                            {
                                response_stream = new StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                                CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                            }
                        }
                        catch { }

                        cookies.Add(httpResponse.Cookies);
                        CommPage.COOKIES = cookies;

                    }


                }
            }
            catch (Exception e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        Console.WriteLine("Sleeping for {0} MS", CommPage.DelayInCaseOfException);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        ////Console.WriteLine("Woke up....");
                    }
                    Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (CommPage.RequestedGrantedAtTryCount), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    GetPage();
                }
                else
                {
                    CommPage.Clear();
                }
                #endregion

            }
            finally
            {
                if (response_stream != null)
                    response_stream.Close();

                if (response != null)
                    response.Close();

                if (CommPage.CB != null)
                {
                    CommPage = CommPage.CB(CommPage);
                }
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogResponse(communication, request.Method.Trim() == "POST" ? true : false); }
                }
                catch (Exception e)
                {
                    //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url); 
                }
            }

            return CommPage;
        }

        #endregion
        public string SournceEncodingToUTF8(string textToConvert)
        {
            Encoding iso8859 = Encoding.GetEncoding(this.CommPage.SourceCharsetEncoding);
            Encoding UTF8 = Encoding.UTF8;
            byte[] srcTextBytes = iso8859.GetBytes(textToConvert);
            byte[] destTextBytes = Encoding.Convert(iso8859, UTF8, srcTextBytes);
            char[] destChars = new char[UTF8.GetCharCount(destTextBytes, 0, destTextBytes.Length)];
            UTF8.GetChars(destTextBytes, 0, destTextBytes.Length, destChars, 0);

            StringBuilder result = new StringBuilder(textToConvert.Length + (int)(textToConvert.Length * 0.1));

            foreach (char c in destChars)
            {
                int value = Convert.ToInt32(c);
                if (value > 127)
                    result.AppendFormat("&#{0};", value);
                else
                    result.Append(c);
            }

            return result.ToString();
        }

        //private static void LogRequest(HttpWebRequest req, byte[] data = null, List<string> toMask = null)
        //{
        //    bool isPost = false;
        //    string postData = string.Empty;
        //    if (req.Method.Trim() == "POST")
        //    {
        //        isPost = true;
        //    }
        //    if (isPost)
        //    {
        //        postData = Encoding.UTF8.GetString(data);
        //        if (toMask != null && toMask.Count > 0)
        //        {
        //            postData = MaskSenstiveData(postData, toMask);
        //        }
        //    }
        //    string url = req.RequestUri != null ? req.RequestUri.AbsoluteUri : "";
        //    string parameters = isPost ? postData : (req.RequestUri != null ? req.RequestUri.Query : "");
        //    string referer = req.Referer;
        //    System.Net.CookieCollection cookieCol = new CookieCollection();
        //    cookieCol = req.CookieContainer.GetCookies(new Uri(ApplicationConstants.SourceURL));
        //    cookieCol.Add(req.CookieContainer.GetCookies(new Uri(ApplicationConstants.SourceURLSecure)));
        //    StringBuilder sbCookies = new StringBuilder();
        //    if (cookieCol != null)
        //    {
        //        foreach (Cookie objCookie in cookieCol)
        //        {
        //            sbCookies.AppendFormat("{0} : {1}", objCookie.Name, objCookie.Value);
        //        }
        //    }

        //    if (isPost)
        //    {
        //        Responder.LogDetail(LogType.Post_Reqeust_Server_To_Server, string.Format("Referer:{0} /r/n Cookies:[{1}]", referer, sbCookies.ToSafeString()), "", url, (data != null) ? Encoding.UTF8.GetString(data) : "");

        //    }
        //    else
        //    {
        //        Responder.LogDetail(LogType.Get_Request_Server_To_Server, string.Format("Referer:{0} /r/n Cookies:[{1}]", referer, sbCookies.ToSafeString()), "", url, parameters);
        //    }

        //}

        //private static void LogResponse(CommunicationResponse resp, bool isPost = false)
        //{
        //    try
        //    {
        //        string url = resp.Uri != null ? resp.Uri.AbsoluteUri : "";
        //        StringBuilder sbCookies = new StringBuilder();
        //        if (resp.Cookies != null)
        //        {
        //            foreach (Cookie objCookie in resp.Cookies)
        //            {
        //                sbCookies.AppendFormat("{0} : {1}", objCookie.Name, objCookie.Value);
        //            }
        //        }


        //        if (isPost)
        //        {
        //            Responder.LogDetail(LogType.Post_Request_Response_Server_To_Server, string.Format("Status:{0} /r/n Cookies:[{1}]", resp.StatusCode.ToSafeString(), sbCookies.ToSafeString()), resp.Html.EncodeHTML(), url);

        //        }
        //        else
        //        {
        //            Responder.LogDetail(LogType.Get_Request_Response_Server_To_Server, string.Format("Status:{0} /r/n Cookies:[{1}]", resp.StatusCode.ToSafeString(), sbCookies.ToSafeString()), resp.Html.EncodeHTML(), url);
        //        }
        //    }
        //    catch { }

        //}

        //private static string MaskSenstiveData(string postData, List<string> toMask)
        //{
        //    string maskedData = postData;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(postData) && toMask != null && toMask.Count > 0)
        //        {
        //            string[] keyVals = postData.Split('&');
        //            if (keyVals != null && keyVals.Length > 0)
        //            {
        //                foreach (string item in keyVals)
        //                {
        //                    string[] parms = item.Split('=');
        //                    if (parms != null && parms.Length == 2)
        //                    {
        //                        if (toMask.Contains(parms[0].DecodeURL()))
        //                        {
        //                            maskedData = maskedData.Replace(parms[1], parms[1].Mask(0));
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {


        //    }



        //    return maskedData;

        //}

        private void CheckAndSleep()
        {
            if (this.CommPage.DelayInEachRequest > 0)
            {
                System.Threading.Thread.Sleep(this.CommPage.DelayInEachRequest);
                ////Responder.swDelay.Stop();
                ////double elapsedMS = Responder.swDelay.Elapsed.TotalMilliseconds;
                ////if (elapsedMS < this.CommPage.DelayInEachRequest)
                ////{
                ////    int remainingMS = (this.CommPage.DelayInEachRequest - elapsedMS).ToInt();
                ////    //Console.WriteLine("SLEEPING.... {0} MS", remainingMS);
                ////    System.Threading.Thread.Sleep(remainingMS);
                ////    //Console.WriteLine("WOKE UP..", remainingMS);
                ////}
                ////Responder.swDelay.Restart();
            }
        }
        private static void SleepWhenBlocked()
        {
            System.Threading.Thread.Sleep(1000 * 60 * 5);

        }



        public void DownloadImage()
        {
            CheckAndSleep();
            // CommunicationResponse communication = new CommunicationResponse(url);

            HttpWebRequest request = null;
            CookieContainer cookies = null;
            HttpWebResponse response = null;
            StreamReader response_stream = null;
            if (CommPage.PersistCookies)
            {
                cookies = CommPage.COOKIES;
            }
            else
            {
                cookies = null;
            }

            if (cookies == null)
            {
                cookies = new CookieContainer();
            }

            try
            {
                request = (HttpWebRequest)WebRequest.Create(CommPage.RequestURL);
                request.UserAgent = CommunicationPage.userAgents[CommPage.UserAgent.ToSafeString()];
                request.Method = "GET";
                request.AllowAutoRedirect = true;
                request.CookieContainer = cookies;
                request.Timeout = 1000 * 60 * 2;

                if (string.IsNullOrEmpty(CommPage.Referer) == false)
                    request.Referer = CommPage.Referer;
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogRequest(request); }
                }
                catch (Exception e)
                {


                }
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {



                    using (BinaryReader reader = new BinaryReader(response.GetResponseStream()))
                    {
                        Byte[] lnByte = reader.ReadBytes(1 * 1024 * 1024 * 10);
                        string physicalPath = CommPage.TargetPath;// HttpContext.Current.Server.MapPath("~" + CommPage.TargetPath);
                        using (FileStream lxFS = new FileStream(physicalPath, FileMode.Create))
                        {
                            lxFS.Write(lnByte, 0, lnByte.Length);
                        }
                        try
                        {
                            using (var ms = new System.IO.MemoryStream(lnByte))
                            {
                                using (var img = System.Drawing.Image.FromStream(ms))
                                {
                                    CommPage.ImageWidth = img.Width;
                                    CommPage.ImageHeight = img.Height;
                                    CommPage.FileSize = lnByte.Length;
                                }
                            }
                        }
                        catch { }
                    }

                    CommPage.IsDownloaded = true;






                    //if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                    //{
                    //    response_stream = new StreamReader(response.GetResponseStream());
                    //    CommPage.Html = response_stream.ReadToEnd();
                    //}
                    //else
                    //{
                    //response_stream = new StreamReader(response.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                    //CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                    //}


                }
                #region Do this whatever the status is returned
                //if (response != null)
                //{
                //    CommPage.StatusCode = response.StatusCode;
                //    if (response.StatusCode != HttpStatusCode.OK) //if OK, already set above
                //    {
                //        CommPage.Html = response_stream.ReadToEnd();
                //    }
                //    // CommPage.Cookies = response.Cookies;
                //    CommPage.Uri = response.ResponseUri;
                //    cookies.Add(response.Cookies);
                //    CommPage.COOKIES = cookies;
                //}



                #endregion
            }
            catch (WebException e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    // Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        //Console.WriteLine("Sleeping for {0} MS", ApplicationConstants.ExceptionDelay);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        //Console.WriteLine("Woke up....");
                    }
                    // Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (retryCount + 1), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    DownloadImage();
                }
                #endregion
                //using (WebResponse Exresponse = e.Response)
                //{
                //    HttpWebResponse httpResponse = (HttpWebResponse)Exresponse;
                //    if (httpResponse != null)
                //    {


                //        CommPage.StatusCode = httpResponse.StatusCode;
                //        //CommPage.Cookies = httpResponse.Cookies;
                //        CommPage.Uri = httpResponse.ResponseUri;

                //        try
                //        {
                //            if (string.IsNullOrEmpty(CommPage.SourceCharsetEncoding))
                //            {
                //                response_stream = new StreamReader(httpResponse.GetResponseStream());
                //                CommPage.Html = response_stream.ReadToEnd();
                //            }
                //            else
                //            {
                //                response_stream = new StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(CommPage.SourceCharsetEncoding));
                //                CommPage.Html = SournceEncodingToUTF8(response_stream.ReadToEnd());
                //            }
                //        }
                //        catch { }

                //        cookies.Add(httpResponse.Cookies);
                //        CommPage.COOKIES = cookies;

                //    }


                //}
            }
            catch (Exception e)
            {
                //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url);
                #region Sleep if blocked
                if (CommPage.RequestedGrantedAtTryCount < CommPage.MaxRetryCount)
                {


                    // Console.WriteLine("Exception occurred: {0}", e.Message);
                    if (CommPage.DelayInCaseOfException > 0)
                    {
                        //Console.WriteLine("Sleeping for {0} MS", ApplicationConstants.ExceptionDelay);
                        System.Threading.Thread.Sleep(CommPage.DelayInCaseOfException);
                        //Console.WriteLine("Woke up....");
                    }
                    //Console.WriteLine("Retrying {0}/{1}... clearing cookies...", (retryCount + 1), 3);
                    CommPage.COOKIES = null;
                    CommPage.RequestedGrantedAtTryCount++;
                    DownloadImage();
                }
                else
                {
                    CommPage.Clear();
                }
                #endregion

            }
            finally
            {
                if (response_stream != null)
                    response_stream.Close();

                if (response != null)
                    response.Close();

                if (CommPage.CB != null)
                {
                    CommPage = CommPage.CB(CommPage);
                }
                try
                {
                    //if (ApplicationConstants.LogVerbosity == LoggingLevel.Verbose)
                    //{ LogResponse(communication, request.Method.Trim() == "POST" ? true : false); }
                }
                catch (Exception e)
                {
                    //Responder.LogException(e.Message, e.ToString(),Flow.UNKNOWN,url); 
                }
            }

        }

    }
}
