﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace BaseScrapper
{
    public class CommunicationPage
    {
        public enum RequestType
        {
            GET = 1,
            POST = 2,
            JSONPOST=3,
            JSONGET=4,
            JSONPUT=5,
            DELETE=6,

            FILEDOWNLOAD,
        }
        public string Html = string.Empty;
        // public CookieCollection Cookies;
        public CookieContainer COOKIES { get; set; }
        public List<Cookie> CookiesList { get; set; }
        public Uri Uri = null;
        public object CallbackMethodReturnedValue { get; set; }
        public HttpStatusCode StatusCode;
        public string RequestURL = string.Empty;
        public delegate CommunicationPage CallBack(CommunicationPage commResp);
        public CallBack CB { get; set; }
        public int DelayInEachRequest { get; set; }
        public int DelayInCaseOfException { get; set; }
        public int MaxRetryCount { get; set; }
        public int RequestedGrantedAtTryCount { get; set; }
        public RequestType ReqType { get; set; }
        public string Referer { get; set; }
        public string SourceCharsetEncoding { get; set; }
        public Dictionary<string, string> Parameters { get; set; }
        public string JsonStringToPost { get; set; }
        public bool PersistCookies { get; set; }
        public string TargetPath { get; set; }
        public int ImageWidth { get; internal set; }
        public int ImageHeight { get; internal set; }
        public int FileSize { get; internal set; }
        public bool IsDownloaded { get; internal set; }
        public Dictionary<string, string> RequestHeaders { get; set; }
        public WebHeaderCollection ResponseHeaders { get; set; }
        public bool IsLoggedIn { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
        public string ProxyHost { get; set; }
        public int ProxyPort { get; set; }
        public bool UseProxy {
            get {
                if (!string.IsNullOrEmpty(ProxyHost) && ProxyPort > 0)
                    return true;
                return false;
            }
        }
        public string CurrentUA { get; set; }
        public enum USER_AGENT
        {
            Windows,
            Iphone,
            Random
        }
        public static Dictionary<string, string> userAgents = new Dictionary<string, string>()
        {
        //{USER_AGENT.Windows.ToSafeString(),"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36" },//"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36" },//"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1"},
        {USER_AGENT.Windows.ToSafeString(),"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36" },//"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1"},

            { USER_AGENT.Iphone.ToSafeString(),"Mozilla/5.0 (iPhone; U; CPU OS 4_2_1 like Mac OS X) AppleWebKit/532.9 (KHTML, like Gecko) Version/5.0.3 Mobile/8B5097d Safari/6531.22.7"}
        };

        public  USER_AGENT UserAgent { get; set; }
        public  Random UARandom { get; set; }

        public static List<string> UserAgentList = new List<string>() { 
      //Chrome
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
      "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
      "Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
      "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
      "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36",
      "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
      "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
      "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36",
      //#Firefox
      "Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)",
      "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
      "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
      "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko",
      "Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko",
      "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
      "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)",
      "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko",
      "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)",
      "Mozilla/5.0 (Windows NT 6.1; Win64; x64; Trident/7.0; rv:11.0) like Gecko",
      "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
      "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)",
      "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)"
        };

        public CommunicationPage(string requestURL)
        {
            this.UserAgent = USER_AGENT.Windows;
            this.RequestURL = requestURL;
            this.RequestedGrantedAtTryCount = 1;
            this.ReqType = RequestType.GET;
            RequestHeaders = new Dictionary<string, string>();
            UARandom = new Random();
        }
        public CommunicationPage(string requestURL, RequestType reqType)
        {
            this.UserAgent = USER_AGENT.Windows;
            this.ReqType = reqType;
            this.RequestURL = requestURL;
            this.RequestedGrantedAtTryCount = 1;
            RequestHeaders = new Dictionary<string, string>();
        }
        public void UpdateCookiesList(CookieCollection cc)
        {
            if (CookiesList == null)
                CookiesList = new List<Cookie>();
            foreach (Cookie c in cc)
            {
                var existing = CookiesList.FirstOrDefault(f => f.Name == c.Name);
                if (existing == null)
                {
                    CookiesList.Add(c);
                }
                else {
                    CookiesList.Remove(existing);
                    CookiesList.Add(c);
                }
            }
        }
        public CommunicationPage()
        { }

        public bool IsValid
        {
            get
            {
                if (IsDownloaded)
                {
                    return true;
                }
                if (Uri == null)
                {
                    // Responder.LogDetail(LogType.Invalid_Response, "Null Response URI");
                    return false;
                }


                if (string.IsNullOrEmpty(Html) && !IsDownloaded)
                {
                    // Responder.LogDetail(LogType.Invalid_Response, "Empty Response HTML");
                    return false;
                }

                return true;
            }
        }

        public void Clear()
        {
            Uri = null;
            Html = string.Empty;
           // COOKIES = null;
        }

        public XmlDocument ToXml(bool dontClean= false)
        {

            return ConvertToXml(ref Html,dontClean);
        }

        public static XmlDocument ConvertToXml(ref string html, bool dontClean = false)
        {
            try
            {

                //   var stringReader = new StringReader(html);
                //return   FromHtml(stringReader);
                if (!string.IsNullOrEmpty(html))
                {

                    //html = html.Replace("<-", "");
                    //string regEx1 = "<!--(?s:.*?)-->";
                    //html = Regex.Replace(html, regEx1, "");
                }
                HtmlDocument hd = new HtmlDocument();

                hd.LoadHtml(html);

                hd.OptionOutputAsXml = true;

                StringBuilder sb = new StringBuilder();
                StringWriter sw = new StringWriter(sb);
                hd.Save(sw);

                string xmlStr = sb.ToString();

                XmlDocument xd = new XmlDocument();
                //string regEx = "<!--(?s:.*?)-->";
                //xmlStr = Regex.Replace(xmlStr, regEx, "");
                xmlStr = xmlStr.Replace(" xmlns=\"https://www.w3.org/1999/xhtml\"", "");
                xmlStr = xmlStr.Replace(" xmlns=\"https://www.w3.org/1999/html\"", "");

                xmlStr = xmlStr.Replace(" xmlns=\"http://www.w3.org/1999/xhtml\"", "");
                xmlStr = xmlStr.Replace(" xmlns=\"http://www.w3.org/1999/html\"", "");

                xmlStr = xmlStr.Replace("-=\"\"", "");
                // xmlStr = xmlStr.Replace("(?i)<title[^>]*>", "");
               // xmlStr = Regex.Replace(xmlStr, "(?i)<meta[^>]*>", " ").Replace("\\s+", " ").Trim();

               //   xmlStr= xmlStr.Replace(@"</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>", "");
               //xmlStr=  Regex.Replace(xmlStr, "</?(?i:script|embed|object|frameset|frame|iframe|meta|link|style)(.|\n)*?>", "");
                xmlStr = xmlStr.Replace("&AMP;", "&amp;");
                if (dontClean==false)
                {
                xmlStr = RemoveTroublesomeCharacters(xmlStr);
                xmlStr = CleanInvalidXmlChars(xmlStr);

                }
                xd.LoadXml(xmlStr);

                return xd;
            }
            catch (Exception e)
            {
                string t = e.Message;
                //- Responder.LogDetail(LogType.Null_XML, e.Message, html.ToHTMLEncoded());
            }

            return null;
        }
        public static string CleanInvalidXmlChars(string text)
        {
            string re = @"[^\x09\x0A\x0D\x20-\xD7FF\xE000-\xFFFD\x10000-x10FFFF]";
            return Regex.Replace(text, re, "");
        }
        public XmlDocument ToXmlWithSGML(string html)
        {
            try
            {
                // setup SgmlReader
                Sgml.SgmlReader sgmlReader = new Sgml.SgmlReader();
                sgmlReader.DocType = "HTML";
                sgmlReader.WhitespaceHandling = WhitespaceHandling.All;
                sgmlReader.CaseFolding = Sgml.CaseFolding.ToLower;
                sgmlReader.InputStream = new StringReader(html);

                // create document
                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = true;
                doc.XmlResolver = null;
                doc.Load(sgmlReader);
                return doc;
            }
            catch (Exception e)
            {
                return new XmlDocument();
            }
        }
        /// <summary>
        /// Removes control characters and other non-UTF-8 characters
        /// </summary>
        /// <param name="inString">The string to process</param>
        /// <returns>A string with no control characters or entities above 0x00FD</returns>
        public static string RemoveTroublesomeCharacters(string inString)
        {
            if (inString == null) return null;

            StringBuilder newString = new StringBuilder();
            char ch;

            for (int i = 0; i < inString.Length; i++)
            {

                ch = inString[i];
                // remove any characters outside the valid UTF-8 range as well as all control characters
                // except tabs and new lines
                //if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
                //if using .NET version prior to 4, use above logic
                if (XmlConvert.IsXmlChar(ch)) //this method is new in .NET 4
                {
                    newString.Append(ch);
                }
            }
            return newString.ToString();

        }
        public static XmlDocument FromHtml(TextReader reader)
        {

            // setup SgmlReader
            Sgml.SgmlReader sgmlReader = new Sgml.SgmlReader();
            sgmlReader.DocType = "HTML";
            sgmlReader.WhitespaceHandling = WhitespaceHandling.All;
            sgmlReader.CaseFolding = Sgml.CaseFolding.ToLower;
            sgmlReader.InputStream = reader;

            // create document
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.XmlResolver = null;
            doc.Load(sgmlReader);
            return doc;
        }
        public static XmlDocument ConvertXmlToXmlDocument(ref string xml)
        {
            try
            {


                XmlDocument xd = new XmlDocument();

                xd.LoadXml(xml);

                return xd;

            }
            catch (Exception e)
            {
                string t = e.Message;
                //- Responder.LogDetail(LogType.Null_XML, e.Message, html.ToHTMLEncoded());
            }

            return null;
        }
    }
}
