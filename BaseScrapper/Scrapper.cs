﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Amib.Threading;

namespace BaseScrapper
{
   public class Scrapper
    {
        private static SmartThreadPool pool = new SmartThreadPool();
        public static Stopwatch sw = new Stopwatch();

        public Scrapper()
        {
            Initialize();
        }
        private Scrapper _API = null;

        public Scrapper GetAPI(Type t)
        {
            if (_API == null)
                _API = (Scrapper)Activator.CreateInstance(t);
            //_API.Start(Request, Response);

            return _API;
        }

        public static Tuple<bool, string> SolveCaptcha(string apiKey, string siteKey, string pageUrl)
        {
            var result = SendRecaptchav2Request(apiKey, siteKey, pageUrl);
            if (result.Item1 == true)
            {
                Retry:
                System.Threading.Thread.Sleep(1000 * 15);
                var result2 = GetCaptchaResponse(apiKey, result.Item2);
                if (result2.Item1 == false && result2.Item2 == "CAPCHA_NOT_READY")
                    goto Retry;

                return result2;
            }
            else
            {
                return result;
            }

        }
        public static Tuple<bool, string> SendRecaptchav2Request(string apiKey, string siteKey, string pageUrl)
        {
            //POST
            try
            {
                System.Net.ServicePointManager.Expect100Continue = false;
                var request = (HttpWebRequest)WebRequest.Create("http://2captcha.com/in.php");

                var postData = string.Format("key={0}&method=userrecaptcha&googlekey={1}&pageurl={2}", apiKey, siteKey, pageUrl); ;
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.Timeout = 1000 * 120;
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //  GET
                if (responseString.Contains("OK|"))
                {
                    return new Tuple<bool, string>(true, responseString.Substring(3));
                }
                else
                {
                    return new Tuple<bool, string>(false, responseString);
                }
            }
            catch (Exception e)
            {
                string tt = e.Message;
                return new Tuple<bool, string>(false, tt);

            }

        }
        public static Tuple<bool, string> GetCaptchaResponse(string apiKey, string captchaId)
        {
            var retryCount = 0;
            Retry:
            try
            {

                System.Net.ServicePointManager.Expect100Continue = false;
                var url = string.Format("http://2captcha.com/res.php?key={0}&action=get&id={1}", apiKey, captchaId);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 1000 * 60 * 3;
                request.Method = "GET";

                request.ContentType = "application/x-www-form-urlencoded";
                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                //  GET
                if (responseString.Contains("OK|"))
                {
                    return new Tuple<bool, string>(true, responseString.Substring(3));
                }
                else
                {
                    return new Tuple<bool, string>(false, responseString);
                }
            }
            catch (Exception e)
            {
                string tt = e.Message;
                if (tt.Contains("timed out") && retryCount < 5)
                {
                    retryCount++;
                    goto Retry;
                }
                return new Tuple<bool, string>(false, tt);
            }

        }
        public void Initialize()
        {


            Init();

            ProcessStart(); //CALL LAST, CALLER WILL HAVE VALID CONNECTION TO USE
        }

        /// <summary>
        /// allows the url of type http://x/y./z
        /// </summary>
        private void AllowTrailingDotsURL()
        {
            try
            {
                //originally felt need for 
                //http://www.woodentoysuk.com/dice-shop-uk-dice-games/dice-games/book-dice-games-properly-explained-by-reiner-knizia.?zenid=5epla5u9tgbb2piqah3vcla5i4

                //http://stackoverflow.com/questions/856885/httpwebrequest-to-url-with-dot-at-the-end
                //this is actually fixed in .NET framework 4.5
                var surl = "http://x/y./z";

                var url2 = new Uri(surl);
                // Console.WriteLine("Broken: " + url2.ToString());

                MethodInfo getSyntax = typeof(UriParser).GetMethod("GetSyntax", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);
                FieldInfo flagsField = typeof(UriParser).GetField("m_Flags", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
                if (getSyntax != null && flagsField != null)
                {
                    foreach (string scheme in new[] { "http", "https" })
                    {
                        UriParser parser = (UriParser)getSyntax.Invoke(null, new object[] { scheme });
                        if (parser != null)
                        {
                            int flagsValue = (int)flagsField.GetValue(parser);
                            // Clear the CanonicalizeAsFilePath attribute
                            if ((flagsValue & 0x1000000) != 0)
                                flagsField.SetValue(parser, flagsValue & ~0x1000000);
                        }
                    }
                }

                url2 = new Uri(surl);
                // Console.WriteLine("Fixed: " + url2.ToString());
            }
            catch (Exception e)
            {

                //LogException("while trying to allow the trailing dots url" + e.Message, e.ToSafeString(), Flow.UNKNOWN);
            }
        }

        protected virtual void ProcessStart()
        {

        }
        /// <summary>
        /// this func is called after all of the processing, before generting xml,summary, exception, closing conn
        /// </summary>
        protected virtual void ProcessFinish()
        {

        }

        private void Init()
        {
            sw.Start();
            //grouping = System.Guid.NewGuid().ToString("N");
            //loggingLevel = ApplicationConstants.LogVerbosity;
            AllowTrailingDotsURL();
            int threads = 100;

            pool.MinThreads = 50;
            pool.MaxThreads = threads;
            pool.Concurrency = 100;
            // Connection(); not required yet
            //ProcessStart();
        }
        private void WaitTillPoolIsIdle()
        {
            while (true)
            {
                //if (Abort)
                //    return;

                if (pool.IsIdle)
                    return;
                Console.WriteLine("WaitTillPoolIsIdle... Sleeping............");
                System.Threading.Thread.Sleep(1000);
            }
        }
        private void Finish()
        {
            // Wait for the completion of all work items
            //pool.WaitForIdle();
            WaitTillPoolIsIdle();
            pool.Shutdown();

            ProcessFinish();
           // GenerateXML(objFeed.Catalogue);
            sw.Stop();// it should be before generating the summary(that will use it)
            //WriteSummaryToFileAndPrint();
            //WriteExceptionsToFile();

            //SQLHelper.ConnectionClose(connection);
        }





        public void ScrapeListing(string url)
        {

        }




    }
}
